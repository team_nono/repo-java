/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin;

import com.ehaqui.ehlogin.security.crypts.description.HashedPassword;
import java.util.UUID;

public class User {
    private String name;
    private UUID offlineUUID;
    private UUID premiumUUID;
    private HashedPassword password;
    private String passwordType;
    private String firstIP = "awd";
    private String lastIP = "ad";
    private boolean premium = false;
    private long lastLogin = 0;
    private Status status = Status.OFFLINE;
    private boolean autoLogin = false;
    private boolean canExpire = true;
    private boolean isRegistred;
    private boolean apiError = false;
    private int passwordErrors = 0;

    public User(String string) {
        this.setName(string);
    }

    public boolean isLogged() {
        return this.getStatus() == Status.LOGGED;
    }

    public String getName() {
        return this.name;
    }

    public UUID getOfflineUUID() {
        return this.offlineUUID;
    }

    public UUID getPremiumUUID() {
        return this.premiumUUID;
    }

    public HashedPassword getPassword() {
        return this.password;
    }

    public String getPasswordType() {
        return this.passwordType;
    }

    public String getFirstIP() {
        return this.firstIP;
    }

    public String getLastIP() {
        return this.lastIP;
    }

    public boolean isPremium() {
        return this.premium;
    }

    public long getLastLogin() {
        return this.lastLogin;
    }

    public Status getStatus() {
        return this.status;
    }

    public boolean isAutoLogin() {
        return this.autoLogin;
    }

    public boolean isCanExpire() {
        return this.canExpire;
    }

    public boolean isRegistred() {
        return this.isRegistred;
    }

    public boolean isApiError() {
        return this.apiError;
    }

    public int getPasswordErrors() {
        return this.passwordErrors;
    }

    public void setName(String string) {
        this.name = string;
    }

    public void setOfflineUUID(UUID uUID) {
        this.offlineUUID = uUID;
    }

    public void setPremiumUUID(UUID uUID) {
        this.premiumUUID = uUID;
    }

    public void setPassword(HashedPassword hashedPassword) {
        this.password = hashedPassword;
    }

    public void setPasswordType(String string) {
        this.passwordType = string;
    }

    public void setFirstIP(String string) {
        this.firstIP = string;
    }

    public void setLastIP(String string) {
        this.lastIP = string;
    }

    public void setPremium(boolean bl) {
        this.premium = bl;
    }

    public void setLastLogin(long l) {
        this.lastLogin = l;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setAutoLogin(boolean bl) {
        this.autoLogin = bl;
    }

    public void setCanExpire(boolean bl) {
        this.canExpire = bl;
    }

    public void setRegistred(boolean bl) {
        this.isRegistred = bl;
    }

    public void setApiError(boolean bl) {
        this.apiError = bl;
    }

    public void setPasswordErrors(int n) {
        this.passwordErrors = n;
    }

    public String toString() {
        return "User(name=" + this.getName() + ", offlineUUID=" + this.getOfflineUUID() + ", premiumUUID=" + this.getPremiumUUID() + ", password=" + this.getPassword() + ", passwordType=" + this.getPasswordType() + ", firstIP=" + this.getFirstIP() + ", lastIP=" + this.getLastIP() + ", premium=" + this.isPremium() + ", lastLogin=" + this.getLastLogin() + ", status=" + (Object)((Object)this.getStatus()) + ", autoLogin=" + this.isAutoLogin() + ", canExpire=" + this.isCanExpire() + ", isRegistred=" + this.isRegistred() + ", apiError=" + this.isApiError() + ", passwordErrors=" + this.getPasswordErrors() + ")";
    }

    public static enum Status {
        ONLINE,
        OFFLINE,
        LOGGED;
        

        private Status() {
        }
    }

}

