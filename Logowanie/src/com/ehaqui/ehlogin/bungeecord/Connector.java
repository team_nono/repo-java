/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.bungeecord;

import com.ehaqui.ehlogin.bungeecord.EhLoginBungeeCord;
import com.ehaqui.ehlogin.bungeecord.Messages;
import com.ehaqui.ehlogin.bungeecord.Setting;
import com.ehaqui.ehlogin.bungeecord.util.Logger;
import com.ehaqui.ehlogin.lib.config.Configuration;
import com.ehaqui.ehlogin.lib.database.Database;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Scanner;

public class Connector {
    private static Database database;
    private static Configuration configuration;

    public static boolean setup() {
        Logger.info("Loading configuration", new Object[0]);
        Configuration.loadLocalConfig(EhLoginBungeeCord.getInstance(), Setting.class);
        Logger.info("Loading language file for: '" + Setting.LANGUAGE + "'", new Object[0]);
        Messages.loadLanguage(Setting.LANGUAGE);
        if (!Connector.setupDatabase()) {
            return false;
        }
        return true;
    }

    private static boolean setupDatabase() {
        EhLoginBungeeCord.getInstance().getLogger().info("Connecting to Database");
        database = new Database(EhLoginBungeeCord.getInstance().getLogger(), Setting.DATABASE_IP, null, Setting.DATABASE_NAME, Setting.DATABASE_USERNAME, Setting.DATABASE_PASSWORD);
        try {
            database.setup();
            if (database.getConnection() != null) {
                database.checkTable("mc_users", Connector.loadFile("table_mc_users.sql"));
                if (!database.checkTable("mc_users_ips")) {
                    Logger.info("[Database] Criando tablela 'mc_users_ips'", new Object[0]);
                    database.update(Connector.loadFile("table_mc_users_ips.sql"), new Object[0]);
                    database.update(Connector.loadFile("table_mc_users_ips_data.sql"), new Object[0]);
                }
                database.checkTable("mc_users_sessions", Connector.loadFile("table_mc_users_sessions.sql"));
                database.checkTable("mc_user_infos", Connector.loadFile("table_mc_user_infos.sql"));
                return true;
            }
        }
        catch (Exception var0) {
            var0.printStackTrace();
            return false;
        }
        return false;
    }

    private static String loadFile(String string) {
        InputStream inputStream = EhLoginBungeeCord.getInstance().getResourceAsStream(string);
        Scanner scanner = new Scanner(inputStream).useDelimiter("\\A");
        return scanner.hasNext() ? scanner.next() : "";
    }

    public static Database getDatabase() {
        return database;
    }

    public static Configuration getConfiguration() {
        return configuration;
    }
}

