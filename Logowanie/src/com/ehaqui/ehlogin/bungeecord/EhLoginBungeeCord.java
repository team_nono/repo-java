/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  net.md_5.bungee.BungeeCord
 *  net.md_5.bungee.api.ProxyServer
 *  net.md_5.bungee.api.plugin.Command
 *  net.md_5.bungee.api.plugin.Listener
 *  net.md_5.bungee.api.plugin.Plugin
 *  net.md_5.bungee.api.plugin.PluginManager
 *  net.md_5.bungee.config.Configuration
 *  net.md_5.bungee.config.ConfigurationProvider
 *  net.md_5.bungee.config.YamlConfiguration
 */
package com.ehaqui.ehlogin.bungeecord;

import com.ehaqui.ehlogin.bungeecord.Connector;
import com.ehaqui.ehlogin.bungeecord.Setting;
import com.ehaqui.ehlogin.bungeecord.command.AdminCommand;
import com.ehaqui.ehlogin.bungeecord.command.AntiBotCommand;
import com.ehaqui.ehlogin.bungeecord.command.AutoLoginCommand;
import com.ehaqui.ehlogin.bungeecord.command.ChangePasswordCommand;
import com.ehaqui.ehlogin.bungeecord.command.LoginCommand;
import com.ehaqui.ehlogin.bungeecord.command.RegisterCommand;
import com.ehaqui.ehlogin.bungeecord.command.UnregisterCommand;
import com.ehaqui.ehlogin.bungeecord.listener.ChatListener;
import com.ehaqui.ehlogin.bungeecord.listener.PlayerListener;
import com.ehaqui.ehlogin.bungeecord.util.AntiBot;
import com.ehaqui.ehlogin.bungeecord.util.Logger;
import com.ehaqui.ehlogin.bungeecord.util.mojang.MojangValidate;
import com.ehaqui.ehlogin.bungeecord.util.mojang.api.MCAPICA;
import com.ehaqui.ehlogin.bungeecord.util.mojang.api.MCAPIDE;
import com.ehaqui.ehlogin.bungeecord.util.mojang.api.MinePay;
import com.ehaqui.ehlogin.bungeecord.util.mojang.api.Mojang;
import com.ehaqui.ehlogin.security.PasswordSecurity;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

public final class EhLoginBungeeCord
extends Plugin {
    private static String USER = "250245";
    public static EhLoginBungeeCord instance;
    private static PasswordSecurity passwordSecurity;

    public void onEnable() {
        EhLoginBungeeCord.loadConfig();
        instance = this;
        try {
            if (!Connector.setup()) {
                Logger.severe("Can't connect to Database. Stoping the server!", new Object[0]);
                BungeeCord.getInstance().stop();
            }
            this.setupAPIs();
            passwordSecurity = new PasswordSecurity(Setting.REGISTER_PASSWORD_TYPE);
            Connector.getDatabase().updateFast("TRUNCATE `mc_users_sessions`;", new Object[0]);
            try {
                this.loadUpdates();
            }
            catch (IOException var1_1) {
                var1_1.printStackTrace();
            }
            AntiBot.setupAntiBotService();
            this.getProxy().getPluginManager().registerCommand((Plugin)this, (Command)new AntiBotCommand());
            this.getProxy().getPluginManager().registerCommand((Plugin)this, (Command)new ChangePasswordCommand());
            this.getProxy().getPluginManager().registerCommand((Plugin)this, (Command)new LoginCommand());
            this.getProxy().getPluginManager().registerCommand((Plugin)this, (Command)new RegisterCommand());
            this.getProxy().getPluginManager().registerCommand((Plugin)this, (Command)new UnregisterCommand());
            this.getProxy().getPluginManager().registerCommand((Plugin)this, (Command)new AutoLoginCommand());
            this.getProxy().getPluginManager().registerCommand((Plugin)this, (Command)new AdminCommand());
            this.getProxy().getPluginManager().registerListener((Plugin)this, (Listener)new PlayerListener());
            this.getProxy().getPluginManager().registerListener((Plugin)this, (Listener)new ChatListener());
            Logger.info("\tPurchase by https://www.spigotmc.org/members/." + USER + "/", new Object[0]);
            Logger.info("\tThank you for buying!", new Object[0]);
        }
        catch (Exception var1_2) {
            Logger.severe("Someting wrong happen. Stoping the server!", new Object[0]);
            BungeeCord.getInstance().stop();
        }
    }

    public void onDisable() {
    }

    private void setupAPIs() {
        MojangValidate.addApi(new Mojang());
        MojangValidate.addApi(new MinePay());
        MojangValidate.addApi(new MCAPICA());
        MojangValidate.addApi(new MCAPIDE());
    }

    private void loadUpdates() {
        int n;
        Configuration configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File(this.getDataFolder(), "config.yml"));
        int n2 = n = configuration.getInt("version");
        if (n < 1) {
            n2 = 1;
        }
        configuration.set("version", (Object)n2);
        ConfigurationProvider.getProvider(YamlConfiguration.class).save(configuration, new File(this.getDataFolder(), "config.yml"));
    }

    public static String colorize(String string) {
        if (string == null) {
            return null;
        }
        return string.replace("&", "\u00a7");
    }

    public static EhLoginBungeeCord getInstance() {
        return instance;
    }

    public static PasswordSecurity getPasswordSecurity() {
        return passwordSecurity;
    }
}

