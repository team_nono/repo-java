/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  net.md_5.bungee.api.CommandSender
 *  net.md_5.bungee.api.chat.BaseComponent
 *  net.md_5.bungee.api.chat.TextComponent
 *  net.md_5.bungee.api.connection.ProxiedPlayer
 */
package com.ehaqui.ehlogin.bungeecord;

import com.ehaqui.ehlogin.bungeecord.EhLoginBungeeCord;
import com.ehaqui.ehlogin.lib.config.ConfigValue;
import com.ehaqui.ehlogin.lib.config.Configuration;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class Messages {
    @ConfigValue(value="version")
    public static int VERSION = 1;
    @ConfigValue(value="prefix")
    public static String PREFIX = "&9[Auth]";
    @ConfigValue(value="dont-have-permission")
    public static String DONT_HAVE_PERMISSION = "&cYou don't have permission";
    @ConfigValue(value="api-error.kick-reason")
    public static String API0ERROR_KICK0REASON = "&7UUID API error! Contact the server admin!";
    @ConfigValue(value="player.logged-with-premium")
    public static String PLAYER_LOGGED_WITH_PREMIUM = "&7Signed via premium account";
    @ConfigValue(value="player.restored-session")
    public static String PLAYER_RESTORED_SESSION = "&7Your session has been restored";
    @ConfigValue(value="player.pre-signin")
    public static String PLAYER_PRE_SIGNIN = "&cSign in using: &e/login <password>";
    @ConfigValue(value="player.pre-signup")
    public static String PLAYER_PRE0SIGNUP = "&cSign up using: &e/register <password> <password>";
    @ConfigValue(value="player.already-online")
    public static String PLAYER_ALREADY0ONLINE = "&cThe player with this nickname is already online.";
    @ConfigValue(value="player.invalid-name")
    public static String PLAYER_INVALID0NAME = "Invalid Name!";
    @ConfigValue(value="player.invalid-ip")
    public static String PLAYER_INVALID0IP = "Invalid IP!";
    @ConfigValue(value="player.player-too-much-ip")
    public static String PLAYER_TOO0MUCH0IP = "Too many connections from this IP address";
    @ConfigValue(value="changepassword.player-isnt-logged")
    public static String CHANGEPASSWORD_PLAYER_ISNT_LOGGED = "&cLog in first to change your password";
    @ConfigValue(value="changepassword.correct-usage")
    public static String CHANGEPASSWORD_CORRECT_USAGE = "&cCorrect use: &e/changepass <password> <repeat password>";
    @ConfigValue(value="changepassword.incorrect-password")
    public static String CHANGEPASSWORD_INCORRECT_PASSWORD = "&cPasswords do not match!";
    @ConfigValue(value="changepassword.changed-password")
    public static String CHANGEPASSWORD_CHANGED_PASSWORD = "&7Your password has been changed successfully";
    @ConfigValue(value="login.player-already-logged")
    public static String LOGIN_PLAYER0ALREADY0LOGGED = "&cYou are already logged in";
    @ConfigValue(value="login.player-arent-registered")
    public static String LOGIN_PLAYER_ARENT_REGISTERED = "&cYou aren't registered.";
    @ConfigValue(value="login.correct-usage")
    public static String LOGIN_CORRECT0USAGE = "&7Correct use: &e/login <password>";
    @ConfigValue(value="login.incorrect-password")
    public static String LOGIN_INCORRECT0PASSWORD = "&cIncorrect password.";
    @ConfigValue(value="login.incorrect-password-premium")
    public static String LOGIN_INCORRECT0PASSWORD0PREMIUM = "&cIf you forget you password. Type /protect false to disable the login";
    @ConfigValue(value="login.successfully-login")
    public static String LOGIN_SUCCESSFULLY0LOGIN = "&7You have been logged in successfully.";
    @ConfigValue(value="login.premium-account-1")
    public static String LOGIN_PREMIUM0ACCOUNT01 = "&aIt seems that your account is premium. Enable automatic login &e/autologin true";
    @ConfigValue(value="login.premium-account-2")
    public static String LOGIN_PREMIUM0ACCOUNT02 = "&cBut beware, if you do not use an original launcher, you will be unable to enter the server";
    @ConfigValue(value="autologin.player-is-not-premium")
    public static String AUTOLOGIN_PLAYER0IS0NOT0PREMIUM = "&cYou are not premium!";
    @ConfigValue(value="autologin.correct-usage")
    public static String AUTOLOGIN_CORRECT0USAGE = "&cEnable login using: &e/autologin <true/false>";
    @ConfigValue(value="autologin.login-enabled")
    public static String AUTOLOGIN_LOGIN0ENABLED = "&cLogin enabled! Now you need to autenticate every time";
    @ConfigValue(value="autologin.login-disabled")
    public static String AUTOLOGIN_LOGIN0DISABLED = "&cLogin disabled! You need to have a premium account and a original minecraft launcher";
    @ConfigValue(value="autologin.player-must-to-be-logged")
    public static String AUTOLOGIN_PLAYER0MUST0TO0BE0LOGGED = "&cYou must be logged";
    @ConfigValue(value="register.register-disabled")
    public static String REGISTER_REGISTER0DISABLED = "&cIn-game registration is disabled!";
    @ConfigValue(value="register.instructions")
    public static String REGISTER_INSTRUCTIONS = "Tell us how to register";
    @ConfigValue(value="register.player-is-already-logged")
    public static String REGISTER_PLAYER0IS0ALREADY0LOGGED = "&cYou are already logged in";
    @ConfigValue(value="register.player-is-already-registered")
    public static String REGISTER_PLAYER0IS0ALREADY0REGISTERED = "&cYou are already registered";
    @ConfigValue(value="register.correct-usage")
    public static String REGISTER_CORRECT0USAGE = "&cCorrect use: &e/register <password> <repeat password>";
    @ConfigValue(value="register.successfully-registered")
    public static String REGISTER_SUCCESSFULLY_REGISTERED = "&7You have been registered successfully";
    @ConfigValue(value="register.password-doesnt-match")
    public static String REGISTER_PASSWORD_DOESNT_MATCH = "&cPasswords doesn't match";
    @ConfigValue(value="register.multi-accounts")
    public static String REGISTER_MULTI_ACCOUNTS = "&cYou have other accounts";
    @ConfigValue(value="register.premium-account-1")
    public static String REGISTER_PREMIUM_ACCOUNT_1 = "&aIt seems that your account is premium. Enable automatic login &e/autologin true";
    @ConfigValue(value="register.premium-account-2")
    public static String REGISTER_PREMIUM_ACCOUNT_2 = "&cBut beware, if you do not use an original launcher, you will be unable to enter the server";
    @ConfigValue(value="unregister.correct-usage")
    public static String UNREGISTER_CORRECT0USAGE = "&7Correct use: &e/unregister <password>";
    @ConfigValue(value="unregister.player-must-to-be-logged")
    public static String UNREGISTER_PLAYER0MUST0TO0BE0LOGGED = "&cYou must be logged";
    @ConfigValue(value="unregister.incorrect-password")
    public static String UNREGISTER_INCORRECT0PASSWORD = "&cIncorrect password";
    @ConfigValue(value="unregister.successfully-unregistered")
    public static String UNREGISTER_SUCCESSFULLY0UNREGISTERED = "&7You have been unregistered in successfully";
    @ConfigValue(value="antibot.enable")
    public static String ANTIBOT_ENABLE = "&4[AntiBotService] AntiBot enabled due to the huge number of connections!";
    @ConfigValue(value="antibot.disable")
    public static String ANTIBOT_DISABLE = "&2[AntiBotService] AntiBot disabled disabled after %m minutes!";
    @ConfigValue(value="antibot.kick-enabled")
    public static String ANTIBOT_KICK0ENABLED = "AntiBot protection mode is enabled! You have to wait some minutes before joining the server";
    @ConfigValue(value="admin.unregister.correct-usage")
    public static String ADMIN_UNREGISTER_CORRECT0USAGE = "&cCorrect usage: &e/ehlogin unregister <player>";
    @ConfigValue(value="admin.unregister.dont-find-the-player")
    public static String ADMIN_UNREGISTER_DONT0FIND0THE0PLAYER = "&cDon't find a player!";
    @ConfigValue(value="admin.unregister.player-isnt-registered")
    public static String ADMIN_UNREGISTER_PLAYER0ISNT0REGISTERED = "&cPlayer isn't registered!";
    @ConfigValue(value="admin.unregister.successfully-unregistered")
    public static String ADMIN_UNREGISTER_SUCCESSFULLY0UNREGISTERED = "&cPlayer unregister successfully!";
    @ConfigValue(value="admin.unregister.kick")
    public static String ADMIN_UNREGISTER_KICK = "&cYour account has been unregistered!";
    @ConfigValue(value="admin.register.correct-usage")
    public static String ADMIN_REGISTER_CORRECT0USAGE = "&cCorrect usage: &e/ehlogin register <player> <password> <autologin>";
    @ConfigValue(value="admin.register.find-player")
    public static String ADMIN_REGISTER_FIND0PLAYER = "&cPlayer already in register!";
    @ConfigValue(value="admin.register.successfully-registered-other")
    public static String ADMIN_REGISTER_SUCCESSFULLY0REGISTERED0OTHER = "&7Player have been registered successfully";
    @ConfigValue(value="admin.register.successfully-registered")
    public static String ADMIN_REGISTER_SUCCESSFULLY0REGISTERED = "&7You have been registered successfully";
    @ConfigValue(value="admin.changepassword.successfully-changed")
    public static String ADMIN_CHANGEPASSWORD_SUCCESSFULLY0CHANGED = "&7Password changed successfully";
    @ConfigValue(value="admin.changepassword.correct-usage")
    public static String ADMIN_CHANGEPASSWORD_CORRECT0USAGE = "&cCorrect usage: &e/ehlogin cpw <player> <password>";

    public static void loadLanguage(String string) {
        Configuration.loadLocalConfig(EhLoginBungeeCord.getInstance(), Messages.class, "messages_" + string + ".yml");
    }

    public static void sendMessageP(ProxiedPlayer proxiedPlayer, String string) {
        proxiedPlayer.sendMessage(TextComponent.fromLegacyText((String)EhLoginBungeeCord.colorize(PREFIX + " " + string)));
    }

    public static void sendMessageP(CommandSender commandSender, String string) {
        commandSender.sendMessage(TextComponent.fromLegacyText((String)EhLoginBungeeCord.colorize(PREFIX + " " + string)));
    }

    public static void sendMessage(ProxiedPlayer proxiedPlayer, String string) {
        proxiedPlayer.sendMessage(TextComponent.fromLegacyText((String)EhLoginBungeeCord.colorize(string)));
    }
}

