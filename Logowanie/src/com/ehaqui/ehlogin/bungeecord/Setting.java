/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.bungeecord;

import com.ehaqui.ehlogin.lib.config.ConfigValue;
import java.util.Arrays;
import java.util.List;

public class Setting {
    @ConfigValue(value="version")
    public static int VERSION = 1;
    @ConfigValue(value="debug")
    public static boolean DEBUG = false;
    @ConfigValue(value="database.ip")
    public static String DATABASE_IP = "localhost";
    @ConfigValue(value="database.password")
    public static String DATABASE_PASSWORD = "passworld";
    @ConfigValue(value="database.username")
    public static String DATABASE_USERNAME = "username";
    @ConfigValue(value="database.name")
    public static String DATABASE_NAME = "minecraft";
    @ConfigValue(value="config.language")
    public static String LANGUAGE = "en";
    @ConfigValue(value="config.uuidFix")
    public static boolean UUID_FIX = true;
    @ConfigValue(value="config.guestCanUseChat")
    public static boolean ENABLE_GUEST_CHAT = false;
    @ConfigValue(value="config.playersCanUsePremiumNickname")
    public static boolean ENABLE_PREMIUM_NICKNAME = true;
    @ConfigValue(value="config.uuidCache.useCache")
    public static boolean CACHE_USE = true;
    @ConfigValue(value="config.uuidCache.expireTimeInDays")
    public static int CACHE_EXPIRE_TIME = 7;
    @ConfigValue(value="restrictions.allowedCommands")
    public static List<String> ALLOWED_COMMANDS = Arrays.asList("/login", "/register");
    @ConfigValue(value="session.useSession")
    public static boolean SESSION_ENABLED = true;
    @ConfigValue(value="session.sessionTime")
    public static String SESSION_TIME = "1h";
    @ConfigValue(value="register.enabled")
    public static boolean REGISTER_ENABLED = true;
    @ConfigValue(value="register.showRegisterInstructions")
    public static boolean REGISTER_SHOW_INSTRUCTIONS = false;
    @ConfigValue(value="register.maxAccountsByIP")
    public static int REGISTER_MAX_ACCOUNTS = 2;
    @ConfigValue(value="register.passwordHash")
    public static String REGISTER_PASSWORD_TYPE = "XAUTH";
    @ConfigValue(value="login.maxConnectionsPerIP")
    public static int LOGIN_MAX_CONNECTIONS_BY_IP = 3;
    @ConfigValue(value="login.hideLoggedWithPremiumMessage")
    public static boolean HIDE_LOGGED_WITH_PREMIUM = false;
    @ConfigValue(value="login.auto-disable-autologin-for-non-premium")
    public static boolean AUTO_DISABLE_AUTOLOGIN_NON_PREMIUM = true;
    @ConfigValue(value="protection.country.useCountryProtection")
    public static boolean PROTECTION_COUNTRY = false;
    @ConfigValue(value="protection.country.whitelist")
    public static List<String> PROTECTION_COUNTRY_WHITELIST = Arrays.asList("BR", "PT");
    @ConfigValue(value="protection.country.blacklist")
    public static List<String> PROTECTION_COUNTRY_BLACKLIST = Arrays.asList("A1");
    @ConfigValue(value="protection.antibot.useAntiBot")
    public static boolean PROTECTION_ANTIBOT_ENABLED = true;
    @ConfigValue(value="protection.antibot.duration")
    public static int PROTECTION_ANTIBOT_DURATION = 15;
    @ConfigValue(value="protection.antibot.sensibility")
    public static int PROTECTION_ANTIBOT_SENSIBILITY = 10;
    @ConfigValue(value="protection.proxy.checkPlayerIP")
    public static boolean PROTECTION_PROXY_IP_CHECK = false;
    @ConfigValue(value="protection.proxy.urlCheck")
    public static String PROTECTION_PROXY_IP_URL = "http://check.getipintel.net/check.php?contact=contatc@email.com&flags=m&ip={IP}";
    @ConfigValue(value="protection.proxy.regexPositive")
    public static String PROTECTION_PROXY_IP_POSITIVE = "1";
    @ConfigValue(value="purge.useAutoPurge")
    public static boolean PURGE_ENABLED = false;
    @ConfigValue(value="purge.daysBeforeRemovePlayer")
    public static int PURGE_DAYS_BEFORE_REMOVE = 60;
}

