/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  net.md_5.bungee.BungeeCord
 *  net.md_5.bungee.api.connection.PendingConnection
 *  net.md_5.bungee.api.connection.ProxiedPlayer
 *  net.md_5.bungee.connection.InitialHandler
 */
package com.ehaqui.ehlogin.bungeecord;

import com.ehaqui.ehlogin.User;
import com.ehaqui.ehlogin.bungeecord.Connector;
import com.ehaqui.ehlogin.bungeecord.EhLoginBungeeCord;
import com.ehaqui.ehlogin.bungeecord.Setting;
import com.ehaqui.ehlogin.lib.util.DateUtil;
import com.ehaqui.ehlogin.lib.util.Reflection;
import com.ehaqui.ehlogin.security.crypts.description.HashedPassword;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.connection.PendingConnection;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.connection.InitialHandler;

public class UserManager {
    private static LinkedList<User> users = new LinkedList();

    public static User getUser(ProxiedPlayer proxiedPlayer) {
        return UserManager.getUser(proxiedPlayer.getName());
    }

    public static User getUser(String string) {
        for (User user : users) {
            if (!user.getName().equalsIgnoreCase(string)) continue;
            return user;
        }
        return UserManager.loadUser(string);
    }

    public static boolean hasAccount(String string) {
        try {
            PreparedStatement preparedStatement = Connector.getDatabase().getConnection().prepareStatement("SELECT * FROM `mc_users` WHERE `name` = ?");
            preparedStatement.setString(1, string);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return true;
            }
        }
        catch (Exception var1_2) {
            var1_2.printStackTrace();
        }
        return false;
    }

    public static int getTotalAccounts(String string) {
        int n = 0;
        try {
            PreparedStatement preparedStatement = Connector.getDatabase().getConnection().prepareStatement("SELECT count(*) as total FROM `mc_users` WHERE firstIP = ?");
            preparedStatement.setString(1, string);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                n = resultSet.getInt(1);
            }
        }
        catch (Exception var2_3) {
            var2_3.printStackTrace();
        }
        return n;
    }

    public static void setStatus(User user, User.Status status) {
        user.setStatus(status);
        try {
            if (status == User.Status.OFFLINE) {
                String string = "DELETE FROM `mc_users_sessions` WHERE `name` = ?";
                PreparedStatement preparedStatement = Connector.getDatabase().getConnection().prepareStatement(string);
                preparedStatement.setString(1, user.getName());
                preparedStatement.execute();
                preparedStatement.close();
            } else {
                String string = "INSERT INTO `mc_users_sessions` (`name`, `premium`, `status`) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE `status` = ?";
                PreparedStatement preparedStatement = Connector.getDatabase().getConnection().prepareStatement(string);
                preparedStatement.setString(1, user.getName());
                preparedStatement.setBoolean(2, user.isPremium());
                preparedStatement.setString(3, status.toString());
                preparedStatement.setString(4, status.toString());
                preparedStatement.execute();
            }
        }
        catch (SQLException var2_4) {
            var2_4.printStackTrace();
        }
    }

    public static void setAutoLogin(User user, boolean bl) {
        user.setAutoLogin(bl);
        try {
            if (user.isPremium()) {
                String string = "UPDATE `mc_users` SET `login_active` = ? WHERE `name` = ? OR `puuid` = ?";
                PreparedStatement preparedStatement = Connector.getDatabase().getConnection().prepareStatement(string);
                preparedStatement.setBoolean(1, !bl);
                preparedStatement.setString(2, user.getName());
                preparedStatement.setString(3, user.getPremiumUUID().toString());
                preparedStatement.execute();
            } else {
                String string = "UPDATE `mc_users` SET `login_active` = ? WHERE `name` = ?";
                PreparedStatement preparedStatement = Connector.getDatabase().getConnection().prepareStatement(string);
                preparedStatement.setBoolean(1, !bl);
                preparedStatement.setString(2, user.getName());
                preparedStatement.execute();
            }
        }
        catch (SQLException var2_4) {
            var2_4.printStackTrace();
        }
    }

    public static List<String> getAccounts(String string) {
        ArrayList<String> arrayList = new ArrayList<String>();
        try {
            PreparedStatement preparedStatement = Connector.getDatabase().getConnection().prepareStatement("SELECT name FROM `mc_users` WHERE firstIP = ? OR firstIP = ?");
            preparedStatement.setString(1, string);
            preparedStatement.setString(2, string);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                arrayList.add(resultSet.getString("name"));
            }
        }
        catch (Exception var2_3) {
            var2_3.printStackTrace();
        }
        return arrayList;
    }

    public static User loadUser(String string) {
        User user = new User(string);
        try {
            Connection connection = Connector.getDatabase().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM `mc_users` WHERE `name` = ?");
            preparedStatement.setString(1, string);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user.setRegistred(true);
                user.setPassword(new HashedPassword(resultSet.getString("password"), resultSet.getString("passwordSalt")));
                user.setPasswordType(resultSet.getString("passwordType"));
                user.setFirstIP(resultSet.getString("firstIP"));
                user.setLastIP(resultSet.getString("lastIP"));
                user.setLastLogin(resultSet.getLong("lastLogin"));
                user.setAutoLogin(!resultSet.getBoolean("login_active"));
                user.setCanExpire(resultSet.getBoolean("can_expire"));
            } else {
                user.setRegistred(false);
            }
            resultSet.close();
            preparedStatement.close();
        }
        catch (SQLException var2_3) {
            var2_3.printStackTrace();
            user.setRegistred(false);
        }
        users.add(user);
        return user;
    }

    public static void register(User user) {
        try {
            Connection connection = Connector.getDatabase().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO `mc_users` (name, password, passwordSalt, passwordType, firstIP, lastIP, lastLogin, login_active, can_expire, puuid)VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getPassword().getHash());
            preparedStatement.setString(3, user.getPassword().getSalt());
            preparedStatement.setString(4, user.getPasswordType() != null ? user.getPasswordType().toString() : null);
            preparedStatement.setString(5, user.getFirstIP());
            preparedStatement.setString(6, user.getLastIP());
            preparedStatement.setLong(7, user.getLastLogin());
            preparedStatement.setBoolean(8, user.isAutoLogin());
            preparedStatement.setBoolean(9, user.isCanExpire());
            preparedStatement.setString(10, user.getPremiumUUID() != null ? user.getPremiumUUID().toString() : null);
            preparedStatement.execute();
            preparedStatement.close();
        }
        catch (SQLException var1_2) {
            var1_2.printStackTrace();
        }
    }

    public static void update(User user) {
        try {
            Connection connection = Connector.getDatabase().getConnection();
            PreparedStatement preparedStatement = user.isPremium() ? connection.prepareStatement("UPDATE `mc_users` SET name = ?, password = ?, passwordSalt = ?, passwordType = ?, lastIP = ?,lastLogin = ?,login_active = ?, can_expire = ? WHERE `puuid` = ?;") : connection.prepareStatement("UPDATE `mc_users` SET name = ?, password = ?, passwordSalt = ?, passwordType = ?, lastIP = ?,lastLogin = ?,login_active = ?, can_expire = ?, puuid = ? WHERE `name` = ?;");
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getPassword().getHash());
            preparedStatement.setString(3, user.getPassword().getSalt());
            preparedStatement.setString(4, user.getPasswordType() != null ? user.getPasswordType() : null);
            preparedStatement.setString(5, user.getLastIP());
            preparedStatement.setLong(6, user.getLastLogin());
            preparedStatement.setBoolean(7, !user.isAutoLogin());
            preparedStatement.setBoolean(8, user.isCanExpire());
            if (user.isPremium()) {
                preparedStatement.setString(9, user.getPremiumUUID().toString());
            } else {
                preparedStatement.setString(9, null);
                preparedStatement.setString(10, user.getName());
            }
            preparedStatement.executeUpdate();
            preparedStatement.close();
        }
        catch (SQLException var1_2) {
            var1_2.printStackTrace();
        }
    }

    public static void unregistera(User user) {
        try {
            Connection connection = Connector.getDatabase().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM `mc_users` WHERE `name` = ?");
            preparedStatement.setString(1, user.getName());
            preparedStatement.executeUpdate();
            preparedStatement = connection.prepareStatement("DELETE FROM `mc_user_infos` WHERE `name` = ?");
            preparedStatement.setString(1, user.getName());
            preparedStatement.executeUpdate();
            preparedStatement = connection.prepareStatement("DELETE FROM `mc_users_sessions` WHERE `name` = ?");
            preparedStatement.setString(1, user.getName());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        }
        catch (SQLException var1_2) {
            var1_2.printStackTrace();
        }
        user.setRegistred(false);
    }

    public static boolean isActiveSession(User user) {
        long l = System.currentTimeMillis() - user.getLastLogin();
        return Setting.SESSION_ENABLED && l <= DateUtil.parseDateDiff(Setting.SESSION_TIME);
    }

    public static boolean comparePassword(User user, String string) {
        return EhLoginBungeeCord.getPasswordSecurity().comparePassword(string, user);
    }

    public static void setUserUUID(boolean bl, PendingConnection pendingConnection, User user) {
        try {
            InitialHandler initialHandler = (InitialHandler)pendingConnection;
            if (bl) {
                Reflection.setInstanceValue((Object)initialHandler, "uniqueId", user.getOfflineUUID());
            } else {
                Reflection.setInstanceValue((Object)initialHandler, "uniqueId", user.getPremiumUUID());
            }
        }
        catch (ReflectiveOperationException var3_4) {
            BungeeCord.getInstance().getLogger().log(Level.SEVERE, "Error setting premium uuid", var3_4);
        }
    }

    public static LinkedList<User> getUsers() {
        return users;
    }
}

