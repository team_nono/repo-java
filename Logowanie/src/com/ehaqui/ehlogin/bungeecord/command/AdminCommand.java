/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  net.md_5.bungee.api.CommandSender
 *  net.md_5.bungee.api.ProxyServer
 *  net.md_5.bungee.api.chat.BaseComponent
 *  net.md_5.bungee.api.chat.TextComponent
 *  net.md_5.bungee.api.connection.ProxiedPlayer
 *  net.md_5.bungee.api.plugin.Command
 */
package com.ehaqui.ehlogin.bungeecord.command;

import com.ehaqui.ehlogin.User;
import com.ehaqui.ehlogin.bungeecord.EhLoginBungeeCord;
import com.ehaqui.ehlogin.bungeecord.Messages;
import com.ehaqui.ehlogin.bungeecord.Setting;
import com.ehaqui.ehlogin.bungeecord.UserManager;
import com.ehaqui.ehlogin.lib.config.Configuration;
import com.ehaqui.ehlogin.security.crypts.description.HashedPassword;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class AdminCommand
extends Command {
    public AdminCommand() {
        super("ehlogin", null, new String[]{"auth"});
    }

    public void execute(CommandSender commandSender, String[] arrstring) {
        if (!commandSender.hasPermission("ehlogin.admin")) {
            Messages.sendMessageP(commandSender, Messages.DONT_HAVE_PERMISSION);
            return;
        }
        if (arrstring.length == 0) {
            commandSender.sendMessage(TextComponent.fromLegacyText((String)"\u00a7cArguments: \u00a7eregister, unregister, cpw, autologin, reload"));
            return;
        }
        if (arrstring[0].equalsIgnoreCase("register")) {
            this.register(commandSender, arrstring);
        } else if (arrstring[0].equalsIgnoreCase("unregister")) {
            this.unregister(commandSender, arrstring);
        } else if (arrstring[0].equalsIgnoreCase("cpw")) {
            this.changePassword(commandSender, arrstring);
        } else if (arrstring[0].equalsIgnoreCase("autologin")) {
            this.changeAutoLogin(commandSender, arrstring);
        } else if (arrstring[0].equalsIgnoreCase("reload")) {
            Configuration.loadLocalConfig(EhLoginBungeeCord.getInstance(), Setting.class);
            Messages.loadLanguage(Setting.LANGUAGE);
            EhLoginBungeeCord.getPasswordSecurity().reload(Setting.REGISTER_PASSWORD_TYPE);
            Messages.sendMessageP(commandSender, "&cConfig reloaded!");
        } else if (arrstring[0].equalsIgnoreCase("user")) {
            User user = UserManager.getUser(arrstring[1]);
            Messages.sendMessageP(commandSender, user.toString());
        }
    }

    private void register(CommandSender commandSender, String[] arrstring) {
        if (arrstring.length < 3) {
            Messages.sendMessageP(commandSender, Messages.ADMIN_REGISTER_CORRECT0USAGE);
            return;
        }
        User user = UserManager.getUser(arrstring[1]);
        if (user.isRegistred()) {
            Messages.sendMessageP(commandSender, Messages.ADMIN_REGISTER_FIND0PLAYER);
            return;
        }
        user.setPasswordType(Setting.REGISTER_PASSWORD_TYPE);
        user.setPassword(EhLoginBungeeCord.getPasswordSecurity().computeHash(arrstring[2], user.getName()));
        if (arrstring.length == 4) {
            user.setAutoLogin(Boolean.valueOf(arrstring[3]));
        } else {
            user.setAutoLogin(true);
        }
        UserManager.register(user);
        Messages.sendMessageP(commandSender, Messages.ADMIN_REGISTER_SUCCESSFULLY0REGISTERED0OTHER);
        ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(arrstring[1]);
        if (proxiedPlayer != null) {
            Messages.sendMessageP(proxiedPlayer, Messages.ADMIN_REGISTER_SUCCESSFULLY0REGISTERED);
            user.setStatus(User.Status.LOGGED);
        }
    }

    private void unregister(CommandSender commandSender, String[] arrstring) {
        if (arrstring.length < 2) {
            Messages.sendMessageP(commandSender, Messages.ADMIN_UNREGISTER_CORRECT0USAGE);
            return;
        }
        User user = UserManager.getUser(arrstring[1]);
        if (!user.isRegistred()) {
            Messages.sendMessageP(commandSender, Messages.ADMIN_UNREGISTER_DONT0FIND0THE0PLAYER);
            return;
        }
        UserManager.unregister(user);
        ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(user.getName());
        if (proxiedPlayer != null) {
            proxiedPlayer.disconnect(TextComponent.fromLegacyText((String)EhLoginBungeeCord.colorize(Messages.ADMIN_UNREGISTER_KICK)));
        } else {
            UserManager.getUsers().remove(user);
        }
        Messages.sendMessageP(commandSender, Messages.ADMIN_UNREGISTER_SUCCESSFULLY0UNREGISTERED);
    }

    private void changePassword(CommandSender commandSender, String[] arrstring) {
        if (arrstring.length < 2) {
            Messages.sendMessageP(commandSender, Messages.ADMIN_CHANGEPASSWORD_CORRECT0USAGE);
            return;
        }
        User user = UserManager.getUser(arrstring[1]);
        if (!user.isRegistred()) {
            Messages.sendMessageP(commandSender, Messages.ADMIN_UNREGISTER_DONT0FIND0THE0PLAYER);
            return;
        }
        user.setPasswordType(Setting.REGISTER_PASSWORD_TYPE);
        user.setPassword(EhLoginBungeeCord.getPasswordSecurity().computeHash(arrstring[2], user.getName()));
        UserManager.update(user);
        Messages.sendMessageP(commandSender, Messages.ADMIN_CHANGEPASSWORD_SUCCESSFULLY0CHANGED);
    }

    private void changeAutoLogin(CommandSender commandSender, String[] arrstring) {
        if (arrstring.length < 2) {
            Messages.sendMessageP(commandSender, Messages.AUTOLOGIN_CORRECT0USAGE);
            return;
        }
        User user = UserManager.getUser(arrstring[1]);
        if (!user.isRegistred()) {
            Messages.sendMessageP(commandSender, Messages.ADMIN_UNREGISTER_DONT0FIND0THE0PLAYER);
            return;
        }
        user.setAutoLogin(Boolean.valueOf(arrstring[2]));
        UserManager.update(user);
        if (user.isAutoLogin()) {
            Messages.sendMessageP(commandSender, Messages.AUTOLOGIN_LOGIN0ENABLED);
        } else {
            Messages.sendMessageP(commandSender, Messages.AUTOLOGIN_LOGIN0DISABLED);
        }
    }
}

