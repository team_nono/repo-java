/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  net.md_5.bungee.api.CommandSender
 *  net.md_5.bungee.api.plugin.Command
 */
package com.ehaqui.ehlogin.bungeecord.command;

import com.ehaqui.ehlogin.bungeecord.Messages;
import com.ehaqui.ehlogin.bungeecord.util.AntiBot;
import com.ehaqui.ehlogin.bungeecord.util.ProxyChecker;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class AntiBotCommand
extends Command {
    public AntiBotCommand() {
        super("antibot", null, new String[0]);
    }

    public void execute(CommandSender commandSender, String[] arrstring) {
        if (!commandSender.hasPermission("ehlogin.admin")) {
            Messages.sendMessageP(commandSender, "&cYou do not have permission to use that command!");
            return;
        }
        block4 : switch (arrstring[0]) {
            case "blacklist": {
                switch (arrstring[1]) {
                    case "add": {
                        ProxyChecker.save(arrstring[2], true);
                        Messages.sendMessageP(commandSender, "&fIP Added!");
                        break block4;
                    }
                    case "remove": {
                        ProxyChecker.remove(arrstring[2]);
                        Messages.sendMessageP(commandSender, "&fIP Removed!");
                        break block4;
                    }
                }
                Messages.sendMessageP(commandSender, "&c/antibot blacklist &6<add/remove> <ip>");
                break;
            }
            case "protection": {
                switch (arrstring[1]) {
                    case "normalize": {
                        AntiBot.overrideAntiBotStatus(false);
                        Messages.sendMessageP(commandSender, "&aAntibot normalized!");
                    }
                }
                break;
            }
            default: {
                Messages.sendMessageP(commandSender, "&c/antibot blacklist &6<add/remove> <ip>");
                Messages.sendMessageP(commandSender, "&c/antibot protection &6normalize");
            }
        }
    }
}

