/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  net.md_5.bungee.api.CommandSender
 *  net.md_5.bungee.api.connection.ProxiedPlayer
 *  net.md_5.bungee.api.plugin.Command
 */
package com.ehaqui.ehlogin.bungeecord.command;

import com.ehaqui.ehlogin.User;
import com.ehaqui.ehlogin.bungeecord.Messages;
import com.ehaqui.ehlogin.bungeecord.UserManager;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class AutoLoginCommand
extends Command {
    public AutoLoginCommand() {
        super("autologin", null, new String[0]);
    }

    public void execute(CommandSender commandSender, String[] arrstring) {
        if (!(commandSender instanceof ProxiedPlayer)) {
            return;
        }
        ProxiedPlayer proxiedPlayer = (ProxiedPlayer)commandSender;
        if (arrstring.length == 0) {
            Messages.sendMessageP(proxiedPlayer, Messages.AUTOLOGIN_CORRECT0USAGE);
            return;
        }
        User user = UserManager.getUser(proxiedPlayer);
        if (!user.isRegistred()) {
            Messages.sendMessageP(proxiedPlayer, Messages.LOGIN_PLAYER_ARENT_REGISTERED);
            return;
        }
        if (!user.isPremium()) {
            Messages.sendMessageP(proxiedPlayer, Messages.AUTOLOGIN_PLAYER0IS0NOT0PREMIUM);
            return;
        }
        if (!user.isLogged()) {
            Messages.sendMessageP(proxiedPlayer, Messages.AUTOLOGIN_PLAYER0MUST0TO0BE0LOGGED);
            return;
        }
        if (!arrstring[0].equalsIgnoreCase("true") && !arrstring[0].equalsIgnoreCase("false")) {
            Messages.sendMessageP(proxiedPlayer, Messages.AUTOLOGIN_CORRECT0USAGE);
            return;
        }
        UserManager.setAutoLogin(user, Boolean.parseBoolean(arrstring[0]));
        if (user.isAutoLogin()) {
            Messages.sendMessageP(proxiedPlayer, Messages.AUTOLOGIN_LOGIN0DISABLED);
        } else {
            Messages.sendMessageP(proxiedPlayer, Messages.AUTOLOGIN_LOGIN0ENABLED);
        }
    }
}

