/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  net.md_5.bungee.api.CommandSender
 *  net.md_5.bungee.api.connection.ProxiedPlayer
 *  net.md_5.bungee.api.plugin.Command
 */
package com.ehaqui.ehlogin.bungeecord.command;

import com.ehaqui.ehlogin.User;
import com.ehaqui.ehlogin.bungeecord.EhLoginBungeeCord;
import com.ehaqui.ehlogin.bungeecord.Messages;
import com.ehaqui.ehlogin.bungeecord.Setting;
import com.ehaqui.ehlogin.bungeecord.UserManager;
import com.ehaqui.ehlogin.security.crypts.description.HashedPassword;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class ChangePasswordCommand
extends Command {
    public ChangePasswordCommand() {
        super("changepassword", null, new String[]{"changepw", "cpw"});
    }

    public void execute(CommandSender commandSender, String[] arrstring) {
        if (!(commandSender instanceof ProxiedPlayer)) {
            return;
        }
        ProxiedPlayer proxiedPlayer = (ProxiedPlayer)commandSender;
        if (arrstring.length != 2) {
            Messages.sendMessageP(proxiedPlayer, Messages.CHANGEPASSWORD_CORRECT_USAGE);
            return;
        }
        User user = UserManager.getUser(proxiedPlayer);
        if (!user.isRegistred()) {
            Messages.sendMessageP(proxiedPlayer, Messages.LOGIN_PLAYER_ARENT_REGISTERED);
            return;
        }
        if (!user.isLogged()) {
            Messages.sendMessageP(proxiedPlayer, Messages.CHANGEPASSWORD_PLAYER_ISNT_LOGGED);
            return;
        }
        if (!arrstring[0].equals(arrstring[1])) {
            Messages.sendMessageP(proxiedPlayer, Messages.CHANGEPASSWORD_INCORRECT_PASSWORD);
            return;
        }
        user.setPasswordType(Setting.REGISTER_PASSWORD_TYPE);
        user.setPassword(EhLoginBungeeCord.getPasswordSecurity().computeHash(arrstring[0], user.getName()));
        UserManager.update(user);
        Messages.sendMessageP(proxiedPlayer, Messages.CHANGEPASSWORD_CHANGED_PASSWORD);
    }
}

