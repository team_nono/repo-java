/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  net.md_5.bungee.api.CommandSender
 *  net.md_5.bungee.api.chat.BaseComponent
 *  net.md_5.bungee.api.chat.TextComponent
 *  net.md_5.bungee.api.connection.PendingConnection
 *  net.md_5.bungee.api.connection.ProxiedPlayer
 *  net.md_5.bungee.api.plugin.Command
 */
package com.ehaqui.ehlogin.bungeecord.command;

import com.ehaqui.ehlogin.User;
import com.ehaqui.ehlogin.bungeecord.EhLoginBungeeCord;
import com.ehaqui.ehlogin.bungeecord.Messages;
import com.ehaqui.ehlogin.bungeecord.UserManager;
import com.ehaqui.ehlogin.bungeecord.util.Logger;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.PendingConnection;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class LoginCommand
extends Command {
    public LoginCommand() {
        super("login", null, new String[]{"l"});
    }

    public void execute(CommandSender commandSender, String[] arrstring) {
        if (!(commandSender instanceof ProxiedPlayer)) {
            return;
        }
        ProxiedPlayer proxiedPlayer = (ProxiedPlayer)commandSender;
        if (arrstring.length == 0) {
            Messages.sendMessageP(proxiedPlayer, Messages.LOGIN_CORRECT0USAGE);
            return;
        }
        User user = UserManager.getUser(proxiedPlayer);
        if (!user.isRegistred()) {
            Messages.sendMessageP(proxiedPlayer, Messages.LOGIN_PLAYER_ARENT_REGISTERED);
            return;
        }
        if (user.isLogged()) {
            Messages.sendMessageP(proxiedPlayer, Messages.LOGIN_PLAYER0ALREADY0LOGGED);
            return;
        }
        if (!UserManager.comparePassword(user, arrstring[0])) {
            Messages.sendMessageP(proxiedPlayer, Messages.LOGIN_INCORRECT0PASSWORD);
            if (proxiedPlayer.getPendingConnection().isOnlineMode()) {
                Messages.sendMessageP(proxiedPlayer, Messages.LOGIN_INCORRECT0PASSWORD0PREMIUM);
            }
            user.setPasswordErrors(user.getPasswordErrors() + 1);
            if (user.getPasswordErrors() >= 3) {
                ((ProxiedPlayer)commandSender).disconnect(TextComponent.fromLegacyText((String)EhLoginBungeeCord.colorize(Messages.LOGIN_INCORRECT0PASSWORD)));
            }
            return;
        }
        UserManager.setStatus(user, User.Status.LOGGED);
        user.setLastIP(proxiedPlayer.getAddress().getAddress().getHostAddress());
        user.setLastLogin(System.currentTimeMillis());
        UserManager.update(user);
        user.setRegistred(true);
        if (user.isPremium() && !user.isAutoLogin()) {
            Messages.sendMessageP(proxiedPlayer, Messages.LOGIN_PREMIUM0ACCOUNT01);
            Messages.sendMessageP(proxiedPlayer, Messages.LOGIN_PREMIUM0ACCOUNT02);
        }
        Messages.sendMessageP(proxiedPlayer, Messages.LOGIN_SUCCESSFULLY0LOGIN);
        Logger.info("%s is now logged!", proxiedPlayer.getName());
    }
}

