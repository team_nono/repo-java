/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  net.md_5.bungee.api.ChatColor
 *  net.md_5.bungee.api.CommandSender
 *  net.md_5.bungee.api.chat.BaseComponent
 *  net.md_5.bungee.api.chat.TextComponent
 *  net.md_5.bungee.api.connection.ProxiedPlayer
 *  net.md_5.bungee.api.plugin.Command
 *  net.md_5.bungee.chat.ComponentSerializer
 */
package com.ehaqui.ehlogin.bungeecord.command;

import com.ehaqui.ehlogin.User;
import com.ehaqui.ehlogin.bungeecord.EhLoginBungeeCord;
import com.ehaqui.ehlogin.bungeecord.Messages;
import com.ehaqui.ehlogin.bungeecord.Setting;
import com.ehaqui.ehlogin.bungeecord.UserManager;
import com.ehaqui.ehlogin.security.crypts.description.HashedPassword;
import java.net.InetSocketAddress;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.chat.ComponentSerializer;

public class RegisterCommand
extends Command {
    public RegisterCommand() {
        super("register", null, new String[0]);
    }

    public void execute(CommandSender commandSender, String[] arrstring) {
        if (!Setting.REGISTER_ENABLED) {
            if (Setting.REGISTER_SHOW_INSTRUCTIONS) {
                BaseComponent[] arrbaseComponent;
                String string = Messages.REGISTER_INSTRUCTIONS.replace("%player%", commandSender.getName());
                if (string.startsWith("{") || string.startsWith("[")) {
                    try {
                        arrbaseComponent = ComponentSerializer.parse((String)string);
                    }
                    catch (Exception var5_8) {
                        arrbaseComponent = TextComponent.fromLegacyText((String)ChatColor.translateAlternateColorCodes((char)'&', (String)string));
                    }
                } else {
                    arrbaseComponent = TextComponent.fromLegacyText((String)ChatColor.translateAlternateColorCodes((char)'&', (String)string));
                }
                commandSender.sendMessage(arrbaseComponent);
            } else {
                commandSender.sendMessage(TextComponent.fromLegacyText((String)EhLoginBungeeCord.colorize(Messages.REGISTER_REGISTER0DISABLED)));
            }
            return;
        }
        if (!(commandSender instanceof ProxiedPlayer)) {
            return;
        }
        ProxiedPlayer proxiedPlayer = (ProxiedPlayer)commandSender;
        if (arrstring.length != 2) {
            Messages.sendMessageP(proxiedPlayer, Messages.REGISTER_CORRECT0USAGE);
            return;
        }
        if (UserManager.getTotalAccounts(proxiedPlayer.getAddress().getHostString()) > Setting.REGISTER_MAX_ACCOUNTS) {
            Messages.sendMessageP(proxiedPlayer, Messages.REGISTER_MULTI_ACCOUNTS);
            for (String string : UserManager.getAccounts(proxiedPlayer.getAddress().getHostString())) {
                Messages.sendMessageP(proxiedPlayer, "&7" + string);
            }
            return;
        }
        User user = UserManager.getUser(proxiedPlayer);
        if (user.isRegistred()) {
            Messages.sendMessageP(proxiedPlayer, Messages.REGISTER_PLAYER0IS0ALREADY0REGISTERED);
            return;
        }
        if (!arrstring[0].equals(arrstring[1])) {
            Messages.sendMessageP(proxiedPlayer, Messages.REGISTER_PASSWORD_DOESNT_MATCH);
            return;
        }
        user.setPasswordType(Setting.REGISTER_PASSWORD_TYPE);
        user.setPassword(EhLoginBungeeCord.getPasswordSecurity().computeHash(arrstring[0], user.getName()));
        user.setLastIP(proxiedPlayer.getAddress().getHostString());
        user.setFirstIP(proxiedPlayer.getAddress().getHostString());
        user.setLastLogin(System.currentTimeMillis());
        user.setAutoLogin(false);
        user.setRegistred(true);
        UserManager.register(user);
        UserManager.setStatus(user, User.Status.LOGGED);
        Messages.sendMessageP(proxiedPlayer, Messages.REGISTER_SUCCESSFULLY_REGISTERED);
        if (user.isPremium()) {
            Messages.sendMessageP(proxiedPlayer, Messages.REGISTER_PREMIUM_ACCOUNT_1);
            Messages.sendMessageP(proxiedPlayer, Messages.REGISTER_PREMIUM_ACCOUNT_2);
        }
    }
}

