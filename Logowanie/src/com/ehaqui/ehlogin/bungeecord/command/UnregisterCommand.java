/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  net.md_5.bungee.api.CommandSender
 *  net.md_5.bungee.api.chat.BaseComponent
 *  net.md_5.bungee.api.chat.TextComponent
 *  net.md_5.bungee.api.connection.ProxiedPlayer
 *  net.md_5.bungee.api.plugin.Command
 */
package com.ehaqui.ehlogin.bungeecord.command;

import com.ehaqui.ehlogin.User;
import com.ehaqui.ehlogin.bungeecord.EhLoginBungeeCord;
import com.ehaqui.ehlogin.bungeecord.Messages;
import com.ehaqui.ehlogin.bungeecord.UserManager;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class UnregisterCommand
extends Command {
    public UnregisterCommand() {
        super("unregister", null, new String[0]);
    }

    public void execute(CommandSender commandSender, String[] arrstring) {
        if (!(commandSender instanceof ProxiedPlayer)) {
            return;
        }
        ProxiedPlayer proxiedPlayer = (ProxiedPlayer)commandSender;
        if (arrstring.length < 1) {
            Messages.sendMessageP(proxiedPlayer, Messages.UNREGISTER_CORRECT0USAGE);
            return;
        }
        User user = UserManager.getUser(proxiedPlayer);
        if (!user.isRegistred()) {
            Messages.sendMessageP(proxiedPlayer, Messages.LOGIN_PLAYER_ARENT_REGISTERED);
            return;
        }
        if (!user.isLogged()) {
            Messages.sendMessageP(proxiedPlayer, Messages.UNREGISTER_PLAYER0MUST0TO0BE0LOGGED);
            return;
        }
        if (!UserManager.comparePassword(user, arrstring[0])) {
            Messages.sendMessageP(proxiedPlayer, Messages.UNREGISTER_INCORRECT0PASSWORD);
            return;
        }
        UserManager.unregister(user);
        proxiedPlayer.disconnect(TextComponent.fromLegacyText((String)EhLoginBungeeCord.colorize(Messages.UNREGISTER_SUCCESSFULLY0UNREGISTERED)));
    }
}

