/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  net.md_5.bungee.api.connection.Connection
 *  net.md_5.bungee.api.connection.ProxiedPlayer
 *  net.md_5.bungee.api.event.ChatEvent
 *  net.md_5.bungee.api.plugin.Listener
 *  net.md_5.bungee.event.EventHandler
 */
package com.ehaqui.ehlogin.bungeecord.listener;

import com.ehaqui.ehlogin.User;
import com.ehaqui.ehlogin.bungeecord.Messages;
import com.ehaqui.ehlogin.bungeecord.Setting;
import com.ehaqui.ehlogin.bungeecord.UserManager;
import java.util.List;
import net.md_5.bungee.api.connection.Connection;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class ChatListener
implements Listener {
    @EventHandler
    public void onPlayerChat(ChatEvent chatEvent) {
        if (chatEvent.isCancelled()) {
            return;
        }
        if (!(chatEvent.getSender() instanceof ProxiedPlayer)) {
            return;
        }
        ProxiedPlayer proxiedPlayer = (ProxiedPlayer)chatEvent.getSender();
        User user = UserManager.getUser(proxiedPlayer);
        if (user.isLogged() || Setting.ENABLE_GUEST_CHAT) {
            return;
        }
        String string = chatEvent.getMessage().split(" ", 2)[0];
        if (string.startsWith("/")) {
            for (String string2 : Setting.ALLOWED_COMMANDS) {
                if (!string.equalsIgnoreCase(string2.toLowerCase())) continue;
                return;
            }
        }
        if (user.isRegistred() && !user.isAutoLogin()) {
            chatEvent.setCancelled(true);
            Messages.sendMessageP(proxiedPlayer, Messages.PLAYER_PRE_SIGNIN);
        } else {
            chatEvent.setCancelled(true);
            Messages.sendMessageP(proxiedPlayer, Messages.PLAYER_PRE0SIGNUP);
        }
    }
}

