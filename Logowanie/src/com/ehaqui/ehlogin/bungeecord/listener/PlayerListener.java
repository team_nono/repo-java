/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.common.base.Joiner
 *  gnu.trove.TCollections
 *  gnu.trove.map.TObjectIntMap
 *  gnu.trove.map.hash.TObjectIntHashMap
 *  net.md_5.bungee.api.ProxyServer
 *  net.md_5.bungee.api.connection.PendingConnection
 *  net.md_5.bungee.api.connection.ProxiedPlayer
 *  net.md_5.bungee.api.event.LoginEvent
 *  net.md_5.bungee.api.event.PlayerDisconnectEvent
 *  net.md_5.bungee.api.event.PostLoginEvent
 *  net.md_5.bungee.api.event.PreLoginEvent
 *  net.md_5.bungee.api.plugin.Listener
 *  net.md_5.bungee.event.EventHandler
 */
package com.ehaqui.ehlogin.bungeecord.listener;

import com.ehaqui.ehlogin.User;
import com.ehaqui.ehlogin.bungeecord.EhLoginBungeeCord;
import com.ehaqui.ehlogin.bungeecord.Messages;
import com.ehaqui.ehlogin.bungeecord.Setting;
import com.ehaqui.ehlogin.bungeecord.UserManager;
import com.ehaqui.ehlogin.bungeecord.util.AntiBot;
import com.ehaqui.ehlogin.bungeecord.util.Logger;
import com.ehaqui.ehlogin.bungeecord.util.ProxyChecker;
import com.ehaqui.ehlogin.bungeecord.util.mojang.MojangValidate;
import com.google.common.base.Joiner;
import gnu.trove.TCollections;
import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Collection;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.PendingConnection;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.event.PreLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PlayerListener implements Listener {
	private static final String IPV4_REGEX = "(([0-1]?[0-9]{1,2}\\.)|(2[0-4][0-9]\\.)|(25[0-5]\\.)){3}(([0-1]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))";
	private static final String NAME_REGEX = "[a-zA-Z0-9_]{2,16}";
	private final TObjectIntMap<InetAddress> addresses = TCollections
			.synchronizedMap((TObjectIntMap) new TObjectIntHashMap());

	@EventHandler(priority = 32)
	public void onPreLogin(PreLoginEvent preLoginEvent) {
		if (preLoginEvent.isCancelled()) {
			preLoginEvent.getConnection().setOnlineMode(true);
			return;
		}
		PendingConnection pendingConnection = preLoginEvent.getConnection();
		String string = pendingConnection.getName();
		String string2 = pendingConnection.getAddress().getAddress().getHostAddress();
		if (!Pattern.compile("[a-zA-Z0-9_]{2,16}").matcher(string).matches()) {
			preLoginEvent.setCancelled(true);
			preLoginEvent.setCancelReason(EhLoginBungeeCord.colorize(Messages.PLAYER_INVALID0NAME));
			return;
		}
		if (!Pattern
				.compile(
						"(([0-1]?[0-9]{1,2}\\.)|(2[0-4][0-9]\\.)|(25[0-5]\\.)){3}(([0-1]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))")
				.matcher(string2).matches()) {
			preLoginEvent.setCancelled(true);
			preLoginEvent.setCancelReason(EhLoginBungeeCord.colorize(Messages.PLAYER_INVALID0IP));
			return;
		}
		if (ProxyServer.getInstance().getPlayer(string) != null) {
			preLoginEvent.setCancelled(true);
			preLoginEvent.setCancelReason(EhLoginBungeeCord.colorize(Messages.PLAYER_ALREADY0ONLINE));
			return;
		}
		if (this.addresses.get((Object) preLoginEvent.getConnection().getAddress()
				.getAddress()) >= Setting.LOGIN_MAX_CONNECTIONS_BY_IP) {
			preLoginEvent.setCancelReason(EhLoginBungeeCord.colorize(Messages.PLAYER_TOO0MUCH0IP));
			preLoginEvent.setCancelled(true);
			return;
		}
		if (Setting.PROTECTION_ANTIBOT_ENABLED && AntiBot.getAntiBotStatus() == AntiBot.AntiBotStatus.ACTIVE
				&& !UserManager.hasAccount(string)) {
			preLoginEvent.setCancelReason(EhLoginBungeeCord.colorize(Messages.ANTIBOT_KICK0ENABLED));
			preLoginEvent.setCancelled(true);
			return;
		}
		if (ProxyChecker.check(string2)) {
			preLoginEvent.setCancelled(true);
			preLoginEvent.setCancelReason("Connected from proxy!");
			return;
		}
		if (Setting.PROTECTION_ANTIBOT_ENABLED) {
			AntiBot.checkAntiBot(string);
		}
		User user = UserManager.loadUser(string);
		MojangValidate mojangValidate = new MojangValidate(user);
		mojangValidate.setInfos();
		if (user.isApiError()) {
			preLoginEvent.setCancelled(true);
			preLoginEvent.setCancelReason(EhLoginBungeeCord.colorize(Messages.API0ERROR_KICK0REASON));
			UserManager.getUsers().remove(user);
			return;
		}
		if (!user.isPremium() && user.isAutoLogin() && Setting.AUTO_DISABLE_AUTOLOGIN_NON_PREMIUM) {
			UserManager.setAutoLogin(user, false);
		}
		if (user.isRegistred()) {
			if (user.isPremium()) {
				if (user.isAutoLogin()) {
					pendingConnection.setOnlineMode(true);
					UserManager.setStatus(user, User.Status.LOGGED);
					user.setLastIP(preLoginEvent.getConnection().getAddress().getAddress().getHostAddress());
					user.setLastLogin(System.currentTimeMillis());
					UserManager.update(user);
				} else {
					pendingConnection.setOnlineMode(false);
					UserManager.setStatus(user, User.Status.ONLINE);
				}
			} else {
				pendingConnection.setOnlineMode(false);
				UserManager.setStatus(user, User.Status.ONLINE);
			}
		} else if (Setting.ENABLE_PREMIUM_NICKNAME || !user.isPremium()) {
			pendingConnection.setOnlineMode(false);
			UserManager.setStatus(user, User.Status.ONLINE);
		} else {
			pendingConnection.setOnlineMode(true);
			UserManager.setStatus(user, User.Status.LOGGED);
		}
		Logger.debug("Status", new Object[0]);
		Logger.debug("\tUser: %s", string);
		Logger.debug("\tRegistred: %b", user.isRegistred());
		Logger.debug("\tPremium: %b", user.isPremium());
		if (user.isPremium()) {
			Logger.debug("\tP_UUID: %s", user.getPremiumUUID().toString());
			Logger.debug("\tAutoLogin: %b", user.isAutoLogin());
		}
		Logger.debug("\tOnlineMode: %b", pendingConnection.isOnlineMode());
	}

	@EventHandler(priority = 32)
	public void onLogin(LoginEvent loginEvent) {
		String string = loginEvent.getConnection().getName();
		User user = UserManager.getUser(string);
		if (user.isPremium()) {
			if (Setting.UUID_FIX) {
				UserManager.setUserUUID(true, loginEvent.getConnection(), user);
			} else {
				UserManager.setUserUUID(false, loginEvent.getConnection(), user);
			}
		}
	}

	@EventHandler(priority = 32)
	public void onPostLogin(PostLoginEvent postLoginEvent) {
		ProxiedPlayer proxiedPlayer = postLoginEvent.getPlayer();
		User user = UserManager.getUser(proxiedPlayer);
		this.addresses.adjustOrPutValue((InetAddress) postLoginEvent.getPlayer().getAddress().getAddress(), 1, 1);
		if (user.isRegistred() && !user.isAutoLogin()) {
			if (UserManager
					.getTotalAccounts(proxiedPlayer.getAddress().getHostString()) > Setting.REGISTER_MAX_ACCOUNTS) {
				String string = "User %s has %s accounts";
				for (ProxiedPlayer proxiedPlayer2 : ProxyServer.getInstance().getPlayers()) {
					if (!proxiedPlayer2.hasPermission("ehlogin.announce.accounts"))
						continue;
					Messages.sendMessageP(proxiedPlayer2,
							String.format(string, proxiedPlayer.getName(), Joiner.on((String) ", ")
									.join(UserManager.getAccounts(proxiedPlayer.getAddress().getHostString()))));
				}
			}
			if (!user.isLogged()) {
				Messages.sendMessageP(proxiedPlayer, Messages.PLAYER_PRE_SIGNIN);
				return;
			}
			if (UserManager.isActiveSession(user)
					&& user.getLastIP().equals(proxiedPlayer.getAddress().getAddress().getHostAddress())) {
				user.setLastLogin(System.currentTimeMillis());
				UserManager.setStatus(user, User.Status.LOGGED);
				UserManager.update(user);
				Messages.sendMessageP(proxiedPlayer, Messages.PLAYER_RESTORED_SESSION);
				return;
			}
		} else if (user.isPremium() && user.isLogged()) {
			if (!Setting.HIDE_LOGGED_WITH_PREMIUM) {
				Messages.sendMessageP(proxiedPlayer, Messages.PLAYER_LOGGED_WITH_PREMIUM);
			}
		} else if (!user.isRegistred()) {
			Messages.sendMessageP(proxiedPlayer, Messages.LOGIN_PLAYER_ARENT_REGISTERED);
			Messages.sendMessageP(proxiedPlayer, Messages.PLAYER_PRE0SIGNUP);
		}
	}

	@EventHandler
	public void onDisconnect(PlayerDisconnectEvent playerDisconnectEvent) {
		User user;
		InetAddress inetAddress = playerDisconnectEvent.getPlayer().getAddress().getAddress();
		this.addresses.adjustValue((InetAddress) inetAddress, -1);
		if (this.addresses.get((Object) inetAddress) <= 0) {
			this.addresses.remove((Object) inetAddress);
		}
		if ((user = UserManager.getUser(playerDisconnectEvent.getPlayer())).isRegistred()) {
			user.setLastIP(playerDisconnectEvent.getPlayer().getAddress().getHostString());
			user.setLastLogin(System.currentTimeMillis());
			UserManager.update(user);
			UserManager.setStatus(user, User.Status.OFFLINE);
		}
		UserManager.getUsers().remove(user);
	}
}
