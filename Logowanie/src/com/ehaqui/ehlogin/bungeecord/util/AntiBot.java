/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  net.md_5.bungee.api.ProxyServer
 *  net.md_5.bungee.api.chat.BaseComponent
 *  net.md_5.bungee.api.chat.TextComponent
 *  net.md_5.bungee.api.connection.ProxiedPlayer
 *  net.md_5.bungee.api.plugin.Plugin
 *  net.md_5.bungee.api.scheduler.ScheduledTask
 *  net.md_5.bungee.api.scheduler.TaskScheduler
 */
package com.ehaqui.ehlogin.bungeecord.util;

import com.ehaqui.ehlogin.bungeecord.EhLoginBungeeCord;
import com.ehaqui.ehlogin.bungeecord.Messages;
import com.ehaqui.ehlogin.bungeecord.Setting;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.scheduler.ScheduledTask;
import net.md_5.bungee.api.scheduler.TaskScheduler;

public class AntiBot {
    private static final List<String> antibotPlayers = new ArrayList<String>();
    private static AntiBotStatus antiBotStatus = AntiBotStatus.DISABLED;

    public static void setupAntiBotService() {
        ProxyServer.getInstance().getScheduler().schedule((Plugin)EhLoginBungeeCord.getInstance(), new Runnable(){

            @Override
            public void run() {
                AntiBot.antiBotStatus = AntiBotStatus.LISTENING;
            }
        }, 2, TimeUnit.MINUTES);
    }

    public static void overrideAntiBotStatus(boolean bl) {
        if (antiBotStatus != AntiBotStatus.DISABLED) {
            antiBotStatus = bl ? AntiBotStatus.ACTIVE : AntiBotStatus.LISTENING;
        }
    }

    public static AntiBotStatus getAntiBotStatus() {
        return antiBotStatus;
    }

    public static void activateAntiBot() {
        antiBotStatus = AntiBotStatus.ACTIVE;
        for (ProxiedPlayer proxiedPlayer : ProxyServer.getInstance().getPlayers()) {
            if (!proxiedPlayer.hasPermission("ehlogin.antibot.broadcast")) continue;
            proxiedPlayer.sendMessage((BaseComponent)new TextComponent(EhLoginBungeeCord.colorize(Messages.ANTIBOT_ENABLE)));
        }
        final int n = Setting.PROTECTION_ANTIBOT_DURATION;
        ProxyServer.getInstance().getScheduler().schedule((Plugin)EhLoginBungeeCord.getInstance(), new Runnable(){

            @Override
            public void run() {
                if (antiBotStatus == AntiBotStatus.ACTIVE) {
                    AntiBot.antiBotStatus = AntiBotStatus.LISTENING;
                    antibotPlayers.clear();
                    for (ProxiedPlayer proxiedPlayer : ProxyServer.getInstance().getPlayers()) {
                        if (!proxiedPlayer.hasPermission("ehlogin.antibot.broadcast")) continue;
                        proxiedPlayer.sendMessage((BaseComponent)new TextComponent(EhLoginBungeeCord.colorize(Messages.ANTIBOT_DISABLE).replace("%m", Integer.toString(n))));
                    }
                }
            }
        }, (long)n, TimeUnit.MINUTES);
    }

    public static void checkAntiBot(final String string) {
        if (antiBotStatus == AntiBotStatus.ACTIVE || antiBotStatus == AntiBotStatus.DISABLED) {
            return;
        }
        antibotPlayers.add(string.toLowerCase());
        if (antibotPlayers.size() > Setting.PROTECTION_ANTIBOT_SENSIBILITY) {
            AntiBot.activateAntiBot();
            return;
        }
        ProxyServer.getInstance().getScheduler().schedule((Plugin)EhLoginBungeeCord.getInstance(), new Runnable(){

            @Override
            public void run() {
                antibotPlayers.remove(string.toLowerCase());
            }
        }, 15, TimeUnit.SECONDS);
    }

    public static enum AntiBotStatus {
        LISTENING,
        DISABLED,
        ACTIVE;
        

        private AntiBotStatus() {
        }
    }

}

