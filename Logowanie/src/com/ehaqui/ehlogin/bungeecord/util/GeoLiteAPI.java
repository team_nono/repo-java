/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  net.md_5.bungee.BungeeCord
 *  net.md_5.bungee.api.plugin.Plugin
 *  net.md_5.bungee.api.scheduler.ScheduledTask
 *  net.md_5.bungee.scheduler.BungeeScheduler
 */
package com.ehaqui.ehlogin.bungeecord.util;

import com.ehaqui.ehlogin.bungeecord.EhLoginBungeeCord;
import com.maxmind.geoip.Country;
import com.maxmind.geoip.LookupService;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.scheduler.ScheduledTask;
import net.md_5.bungee.scheduler.BungeeScheduler;

public class GeoLiteAPI {
    private static final String LICENSE = "[LICENSE] This product uses data from the GeoLite API created by MaxMind, available at http://www.maxmind.com";
    private static final String GEOIP_URL = "http://geolite.maxmind.com/download/geoip/database/GeoLiteCountry/GeoIP.dat.gz";
    private static final Plugin plugin = EhLoginBungeeCord.getInstance();
    private static LookupService lookupService;

    public static boolean isDataAvailable() {
        if (lookupService != null) {
            return true;
        }
        final File file = new File(plugin.getDataFolder(), "GeoIP.dat");
        if (file.exists()) {
            try {
                lookupService = new LookupService(file);
                plugin.getLogger().info("[LICENSE] This product uses data from the GeoLite API created by MaxMind, available at http://www.maxmind.com");
                return true;
            }
            catch (IOException var1_1) {
                return false;
            }
        }
        BungeeCord.getInstance().getScheduler().runAsync(plugin, new Runnable(){

            @Override
            public void run() {
                try {
                    URL uRL = new URL("http://geolite.maxmind.com/download/geoip/database/GeoLiteCountry/GeoIP.dat.gz");
                    URLConnection uRLConnection = uRL.openConnection();
                    uRLConnection.setConnectTimeout(10000);
                    uRLConnection.connect();
                    InputStream inputStream = uRLConnection.getInputStream();
                    if (uRLConnection.getURL().toString().endsWith(".gz")) {
                        inputStream = new GZIPInputStream(inputStream);
                    }
                    FileOutputStream fileOutputStream = new FileOutputStream(file);
                    byte[] arrby = new byte[2048];
                    int n = inputStream.read(arrby);
                    while (n >= 0) {
                        fileOutputStream.write(arrby, 0, n);
                        n = inputStream.read(arrby);
                    }
                    fileOutputStream.close();
                    inputStream.close();
                }
                catch (IOException var1_2) {
                    // empty catch block
                }
            }
        });
        return false;
    }

    public static String getCountryCode(String string) {
        if (GeoLiteAPI.isDataAvailable()) {
            return lookupService.getCountry(string).getCode();
        }
        return "--";
    }

    public static String getCountryName(String string) {
        if (GeoLiteAPI.isDataAvailable()) {
            return lookupService.getCountry(string).getName();
        }
        return "N/A";
    }

}

