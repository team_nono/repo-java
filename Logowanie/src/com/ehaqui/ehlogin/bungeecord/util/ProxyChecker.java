/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.bungeecord.util;

import com.ehaqui.ehlogin.bungeecord.Connector;
import com.ehaqui.ehlogin.bungeecord.Setting;
import com.ehaqui.ehlogin.bungeecord.util.GeoLiteAPI;
import com.ehaqui.ehlogin.bungeecord.util.Logger;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProxyChecker {
    private static final int TIMEOUT = 1000;
    private static final String USER_AGENT = "Proxy-Checker";
    private static HashMap<String, Boolean> ips = new HashMap();

    public static boolean check(String string) {
        Object object;
        if (Setting.PROTECTION_COUNTRY) {
            object = GeoLiteAPI.getCountryCode(string);
            if (Setting.PROTECTION_COUNTRY_BLACKLIST.contains(object)) {
                return true;
            }
            if (!Setting.PROTECTION_COUNTRY_WHITELIST.contains(object)) {
                return true;
            }
        }
        if (ProxyChecker.load(string)) {
            return ips.get(string);
        }
        if (Setting.PROTECTION_PROXY_IP_CHECK) {
            try {
                object = ProxyChecker.getConnection(Setting.PROTECTION_PROXY_IP_URL.replace("{IP}", string));
                if (object.getResponseCode() == 200) {
                    InputStream inputStream = object.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    String string2 = bufferedReader.readLine();
                    if (string2 != null && !string2.equals("null")) {
                        boolean bl = ProxyChecker.save(string, Pattern.compile(Setting.PROTECTION_PROXY_IP_POSITIVE).matcher(string2).matches());
                        Logger.info("[AntiBot] Checking ip %s - %b", string, bl);
                        return bl;
                    }
                } else {
                    Logger.info("[AntiBot] Error status %d", object.getResponseCode());
                }
            }
            catch (IOException var1_2) {
                var1_2.printStackTrace();
            }
        }
        return false;
    }

    private static boolean load(String string) {
        if (ips.containsKey(string)) {
            return true;
        }
        try {
            PreparedStatement preparedStatement = Connector.getDatabase().getConnection().prepareStatement("SELECT `status` FROM `mc_users_ips` WHERE `ip` = ?");
            preparedStatement.setString(1, string);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                ips.put(string, resultSet.getBoolean("status"));
                return true;
            }
        }
        catch (SQLException var1_2) {
            var1_2.printStackTrace();
        }
        return false;
    }

    public static boolean save(String string, boolean bl) {
        ips.put(string, bl);
        try {
            PreparedStatement preparedStatement = Connector.getDatabase().getConnection().prepareStatement("INSERT INTO `mc_users_ips` (`ip`, `country`, `status`) VALUES (?, ?, ?)");
            preparedStatement.setString(1, string);
            preparedStatement.setString(2, GeoLiteAPI.getCountryCode(string));
            preparedStatement.setBoolean(3, bl);
            preparedStatement.executeUpdate();
        }
        catch (SQLException var3_3) {
            var3_3.printStackTrace();
        }
        return bl;
    }

    public static boolean remove(String string) {
        ips.remove(string);
        try {
            PreparedStatement preparedStatement = Connector.getDatabase().getConnection().prepareStatement("DELETE FROM `mc_users_ips` WHERE `ip` = ?");
            preparedStatement.setString(1, string);
            preparedStatement.executeUpdate();
        }
        catch (SQLException var2_2) {
            var2_2.printStackTrace();
            return false;
        }
        return true;
    }

    private static HttpURLConnection getConnection(String string) {
        HttpURLConnection httpURLConnection = (HttpURLConnection)new URL(string).openConnection();
        httpURLConnection.setConnectTimeout(1000);
        httpURLConnection.setReadTimeout(1000);
        httpURLConnection.setRequestProperty("User-Agent", "Proxy-Checker");
        return httpURLConnection;
    }
}

