/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.common.base.Charsets
 */
package com.ehaqui.ehlogin.bungeecord.util.mojang;

import com.ehaqui.ehlogin.User;
import com.ehaqui.ehlogin.bungeecord.Connector;
import com.ehaqui.ehlogin.bungeecord.Setting;
import com.ehaqui.ehlogin.bungeecord.util.Logger;
import com.ehaqui.ehlogin.bungeecord.util.mojang.TrustModifier;
import com.ehaqui.ehlogin.bungeecord.util.mojang.UUIDApi;
import com.google.common.base.Charsets;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MojangValidate implements Runnable {
	private static List<UUIDApi> apis = new ArrayList<UUIDApi>();
	private User usera;
	private boolean premium;
	private UUID premiumUUID;
	private UUID offlineUUID;
	private boolean APIError;
	private String apiUsed;
	private Timestamp date;

	public MojangValidate(User user) {
		this.user = user;
		this.premium = false;
		this.APIError = false;
		this.offlineUUID = UUID.nameUUIDFromBytes(("OfflinePlayer:" + user.getName()).getBytes(Charsets.UTF_8));
		this.premiumUUID = null;
	}

	public void setInfos() {
		this.run();
		if (this.isAPIError()) {
			this.user.setApiError(true);
		}
		this.user.setPremium(this.isPremium());
		this.user.setPremiumUUID(this.getPremiumUUID());
		this.user.setOfflineUUID(this.getOfflineUUID());
	}

	public static void addApi(UUIDApi uUIDApi) {
		apis.add(uUIDApi);
	}

	@Override
	public void run() {
		this.APIError = false;
		try {
			if (this.hasInfoCached(true)) {
				Logger.info("Load uuid from cache", new Object[0]);
				this.loadFromCache();
				return;
			}
			if (!apis.isEmpty()) {
				for (UUIDApi uUIDApi : apis) {
					Logger.info("Trying to use " + uUIDApi.getName() + " API", new Object[0]);
					this.apiUsed = uUIDApi.getName();
					String string = null;
					try {
						HttpURLConnection httpURLConnection = this
								.getConnection(uUIDApi.getUrl().replace("{PLAYER}", this.user.getName()));
						int n = httpURLConnection.getResponseCode();
						if (n == uUIDApi.getResponseNotPaid()) {
							this.APIError = false;
							this.premium = false;
							Logger.info("%s - %s is cracked a player", this.apiUsed, this.user.getName());
							break;
						}
						if (n == uUIDApi.getResponseOK()) {
							string = this.getContent(httpURLConnection);
							if (uUIDApi.isOfflineParseUse() && string.contains(uUIDApi.getOfflineParseContains())) {
								this.APIError = false;
								this.premium = false;
								Logger.info("[%s] - %s is cracked a player", this.apiUsed, this.user.getName());
								break;
							}
							if (uUIDApi.isErrorParseUse() && string.contains(uUIDApi.getErrorParseContains())) {
								Logger.debugError("[%s] Error: %s", uUIDApi.getName(), MojangValidate.getStringBetween(
										string, uUIDApi.getErrorParseStart(), uUIDApi.getErrorParseEnd()));
								Logger.debugError("Response content", new Object[0]);
								Logger.debugError(string, new Object[0]);
								this.APIError = true;
								continue;
							}
							this.APIError = false;
							this.premium = true;
							this.premiumUUID = this.parseId(MojangValidate.getStringBetween(string,
									uUIDApi.getUuidParseStart(), uUIDApi.getUuidParseEnd()));
							Logger.info("[%s] - %s is a premium player", this.apiUsed, this.user.getName());
							break;
						}
						if (n != uUIDApi.getResponseTooManyRequests())
							continue;
						this.APIError = true;
						Logger.debugError("[%s] - Too manny requests, trying to use another API", this.apiUsed,
								this.user.getName());
					} catch (Exception var4_6) {
						this.APIError = true;
						Logger.debugError("[%s] Error: Response content", this.apiUsed);
						Logger.debugError(string, new Object[0]);
						var4_6.printStackTrace();
					}
				}
				if (this.APIError) {
					if (this.hasInfoCached(false)) {
						Logger.info("All UUID API's is saturated, using last cached data", new Object[0]);
						this.loadFromCache();
						this.APIError = false;
						return;
					}
				} else {
					Logger.info("Loaded infos using %s API", this.apiUsed);
					this.saveInfo();
				}
			} else {
				this.APIError = true;
				Logger.severe("No API to load user details, user has been set to offline", new Object[0]);
			}
		} catch (SQLException var1_2) {
			this.APIError = true;
			Logger.severe("Failed to check if player has a paid account", new Object[0]);
			var1_2.printStackTrace();
		}
	}

	private void loadFromCache() {
		Connection connection = Connector.getDatabase().getConnection();
		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection.prepareStatement("SELECT * FROM mc_user_infos WHERE `name` = ?");

			preparedStatement.setString(1, this.user.getName());
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				this.premium = resultSet.getBoolean("premium");
				if (this.premium) {
					this.premiumUUID = UUID.fromString(resultSet.getString("puuid"));
				}
				this.date = resultSet.getTimestamp("date");
			}
			resultSet.close();
			preparedStatement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean hasInfoCached(boolean bl) {
		Connection connection = Connector.getDatabase().getConnection();
		String string = Setting.CACHE_USE && bl
				? "SELECT * FROM mc_user_infos WHERE `name` = ? AND `date` > DATE_SUB(NOW(), INTERVAL "
						+ Setting.CACHE_EXPIRE_TIME + " DAY);"
				: "SELECT * FROM mc_user_infos WHERE `name` = ?";
		PreparedStatement preparedStatement = connection.prepareStatement(string);
		preparedStatement.setString(1, this.user.getName());
		ResultSet resultSet = preparedStatement.executeQuery();
		return resultSet.next();
	}

	private void saveInfo() {
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		hashMap.put("name", this.user.getName());
		hashMap.put("premium", this.isPremium());
		hashMap.put("ouuid", this.getOfflineUUID().toString());
		hashMap.put("puuid", this.getPremiumUUID() == null ? null : this.getPremiumUUID());
		hashMap.put("api", this.apiUsed);
		HashMap<String, Object> hashMap2 = new HashMap<String, Object>();
		hashMap2.put("name", this.user.getName());
		hashMap2.put("ouuid", this.getOfflineUUID().toString());
		hashMap2.put("puuid", this.getPremiumUUID() == null ? null : this.getPremiumUUID());
		hashMap2.put("date", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		hashMap2.put("api", this.apiUsed);
		Connector.getDatabase().insert(Connector.getDatabase().sql("mc_user_infos", hashMap, hashMap2), new Object[0]);
	}

	private UUID parseId(String string) {
		return UUID.fromString(string.substring(0, 8) + "-" + string.substring(8, 12) + "-" + string.substring(12, 16)
				+ "-" + string.substring(16, 20) + "-" + string.substring(20, 32));
	}

	private HttpURLConnection getConnection(String string) {
		HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(string).openConnection();
		try {
			TrustModifier.relaxHostChecking(httpURLConnection);
		} catch (KeyManagementException | KeyStoreException | NoSuchAlgorithmException var3_3) {
			// empty catch block
		}
		httpURLConnection.setConnectTimeout(1000);
		httpURLConnection.setReadTimeout(1000);
		httpURLConnection.setRequestProperty("Content-Type", "application/json");
		httpURLConnection.setRequestProperty("User-Agent", "Mozilla/5.0");
		Logger.debug("URL: %s , Header Status %d", string, httpURLConnection.getResponseCode());
		return httpURLConnection;
	}

	private String getContent(URLConnection uRLConnection) {
		try {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(uRLConnection.getInputStream()));
			StringBuilder stringBuilder = new StringBuilder();
			String string = null;
			while ((string = bufferedReader.readLine()) != null) {
				stringBuilder.append(string + "\n");
			}
			return stringBuilder.toString();
		} catch (IOException var2_3) {
			var2_3.printStackTrace();
			return "";
		}
	}

	private static String getStringBetween(String string, String string2, String string3) {
		Pattern pattern = Pattern.compile(Pattern.quote(string2));
		Pattern pattern2 = Pattern.compile(Pattern.quote(string3));
		int n = 0;
		int n2 = string.length() - 1;
		Matcher matcher = pattern.matcher(string);
		while (matcher.find()) {
			n = matcher.end();
		}
		Matcher matcher2 = pattern2.matcher(string);
		while (matcher2.find()) {
			n2 = matcher2.start();
		}
		return string.substring(n, n2);
	}

	public User getUser() {
		return this.user;
	}

	public boolean isPremiumaa() {
		return this.premium;
	}

	public UUID getPremiumUUIDa() {
		return this.premiumUUID;
	}

	public UUID getOfflineUUIDa() {
		return this.offlineUUID;
	}

	public boolean isAPIErrora() {
		return this.APIError;
	}

	public String getApiUseda() {
		return this.apiUsed;
	}

	public Timestamp getDatea() {
		return this.date;
	}
}
