/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.bungeecord.util.mojang;

public abstract class UUIDApi {
    private String name;
    private String url;
    private int responseOK;
    private int responseNotPaid;
    private int responseTooManyRequests;
    private String uuidParseStart;
    private String uuidParseEnd;
    private boolean offlineParseUse = false;
    private String offlineParseContains;
    private boolean errorParseUse = false;
    private String errorParseContains;
    private String errorParseStart;
    private String errorParseEnd;

    public String getName() {
        return this.name;
    }

    public String getUrl() {
        return this.url;
    }

    public int getResponseOK() {
        return this.responseOK;
    }

    public int getResponseNotPaid() {
        return this.responseNotPaid;
    }

    public int getResponseTooManyRequests() {
        return this.responseTooManyRequests;
    }

    public String getUuidParseStart() {
        return this.uuidParseStart;
    }

    public String getUuidParseEnd() {
        return this.uuidParseEnd;
    }

    public boolean isOfflineParseUse() {
        return this.offlineParseUse;
    }

    public String getOfflineParseContains() {
        return this.offlineParseContains;
    }

    public boolean isErrorParseUse() {
        return this.errorParseUse;
    }

    public String getErrorParseContains() {
        return this.errorParseContains;
    }

    public String getErrorParseStart() {
        return this.errorParseStart;
    }

    public String getErrorParseEnd() {
        return this.errorParseEnd;
    }

    public void setName(String string) {
        this.name = string;
    }

    public void setUrl(String string) {
        this.url = string;
    }

    public void setResponseOK(int n) {
        this.responseOK = n;
    }

    public void setResponseNotPaid(int n) {
        this.responseNotPaid = n;
    }

    public void setResponseTooManyRequests(int n) {
        this.responseTooManyRequests = n;
    }

    public void setUuidParseStart(String string) {
        this.uuidParseStart = string;
    }

    public void setUuidParseEnd(String string) {
        this.uuidParseEnd = string;
    }

    public void setOfflineParseUse(boolean bl) {
        this.offlineParseUse = bl;
    }

    public void setOfflineParseContains(String string) {
        this.offlineParseContains = string;
    }

    public void setErrorParseUse(boolean bl) {
        this.errorParseUse = bl;
    }

    public void setErrorParseContains(String string) {
        this.errorParseContains = string;
    }

    public void setErrorParseStart(String string) {
        this.errorParseStart = string;
    }

    public void setErrorParseEnd(String string) {
        this.errorParseEnd = string;
    }

    public String toString() {
        return "UUIDApi(name=" + this.getName() + ", url=" + this.getUrl() + ", responseOK=" + this.getResponseOK() + ", responseNotPaid=" + this.getResponseNotPaid() + ", responseTooManyRequests=" + this.getResponseTooManyRequests() + ", uuidParseStart=" + this.getUuidParseStart() + ", uuidParseEnd=" + this.getUuidParseEnd() + ", offlineParseUse=" + this.isOfflineParseUse() + ", offlineParseContains=" + this.getOfflineParseContains() + ", errorParseUse=" + this.isErrorParseUse() + ", errorParseContains=" + this.getErrorParseContains() + ", errorParseStart=" + this.getErrorParseStart() + ", errorParseEnd=" + this.getErrorParseEnd() + ")";
    }
}

