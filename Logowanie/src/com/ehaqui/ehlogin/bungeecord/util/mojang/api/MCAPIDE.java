/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.bungeecord.util.mojang.api;

import com.ehaqui.ehlogin.bungeecord.util.mojang.UUIDApi;

public class MCAPIDE
extends UUIDApi {
    private String name = "mcapi.de";
    private String url = "https://mcapi.de/api/user/{PLAYER}";
    private int responseOK = 200;
    private int responseNotPaid = 404;
    private int responseTooManyRequests = 429;
    private String uuidParseStart = "uuid\":\"";
    private String uuidParseEnd = "\",\"";
    private boolean offlineParseUse = true;
    private String offlineParseContains = "premium\":false";
    private boolean errorParseUse = true;
    private String errorParseContains = "wrong";
    private String errorParseStart;
    private String errorParseEnd;

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getUrl() {
        return this.url;
    }

    @Override
    public int getResponseOK() {
        return this.responseOK;
    }

    @Override
    public int getResponseNotPaid() {
        return this.responseNotPaid;
    }

    @Override
    public int getResponseTooManyRequests() {
        return this.responseTooManyRequests;
    }

    @Override
    public String getUuidParseStart() {
        return this.uuidParseStart;
    }

    @Override
    public String getUuidParseEnd() {
        return this.uuidParseEnd;
    }

    @Override
    public boolean isOfflineParseUse() {
        return this.offlineParseUse;
    }

    @Override
    public String getOfflineParseContains() {
        return this.offlineParseContains;
    }

    @Override
    public boolean isErrorParseUse() {
        return this.errorParseUse;
    }

    @Override
    public String getErrorParseContains() {
        return this.errorParseContains;
    }

    @Override
    public String getErrorParseStart() {
        return this.errorParseStart;
    }

    @Override
    public String getErrorParseEnd() {
        return this.errorParseEnd;
    }
}

