/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.common.io.Files
 *  com.google.gson.Gson
 *  com.google.gson.reflect.TypeToken
 *  com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException
 *  net.md_5.bungee.BungeeCord
 *  net.md_5.bungee.api.ChatColor
 *  net.md_5.bungee.api.plugin.Plugin
 *  net.md_5.bungee.api.plugin.PluginDescription
 *  net.md_5.bungee.config.Configuration
 *  net.md_5.bungee.config.ConfigurationProvider
 *  net.md_5.bungee.config.YamlConfiguration
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.Location
 *  org.bukkit.OfflinePlayer
 *  org.bukkit.World
 *  org.bukkit.configuration.file.FileConfiguration
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.plugin.PluginDescriptionFile
 *  org.bukkit.plugin.java.JavaPlugin
 */
package com.ehaqui.ehlogin.lib.config;

import com.ehaqui.ehlogin.lib.config.ConfigValue;
import com.ehaqui.ehlogin.lib.config.model.FlagPlayer;
import com.ehaqui.ehlogin.lib.config.model.FlagPlugin;
import com.ehaqui.ehlogin.lib.config.model.Players;
import com.ehaqui.ehlogin.lib.database.Database;
import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginDescription;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class Configuration {
    private String pluginName;
    private Logger logger;
    private HashMap<UUID, HashMap<String, String>> playerFlag = new HashMap();
    private HashMap<String, String> pluginFlag = new HashMap();
    private boolean useCaddche = true;
    private Database d;

    public Configuration(Plugin plugin, Database database) {
        this(plugin, true, database);
    }

    public Configuration(JavaPlugin javaPlugin, Database database) {
        this(javaPlugin, true, database);
    }

    public Configuration(Plugin plugin, boolean bl, Database database) {
        this.pluginName = plugin.getDescription().getName();
        this.logger = plugin.getLogger();
        this.useCache = bl;
        this.db = database;
        this.setupDatabase();
        if (bl) {
            this.load();
        }
    }

    public Configuration(JavaPlugin javaPlugin, boolean bl, Database database) {
        this.pluginName = javaPlugin.getDescription().getName();
        this.logger = javaPlugin.getLogger();
        this.useCache = bl;
        this.db = database;
        this.setupDatabase();
        if (bl) {
            this.load();
        }
    }

    public void setupDatabase() {
        this.db.createTable(Players.class);
        this.db.createTable(FlagPlayer.class);
        this.db.createTable(FlagPlugin.class);
    }

    public boolean load() {
        this.db.getConnection();
        try {
            ResultSet resultSet = this.db.query("SELECT `flag_key`, `flag_value` FROM `eh_core_flags_plugin` WHERE `plugin` = '%s'", this.pluginName);
            while (resultSet.next()) {
                this.pluginFlag.put(resultSet.getString("flag_key"), resultSet.getString("flag_value"));
            }
            resultSet.close();
            ResultSet resultSet2 = this.db.query("SELECT `uuid`, `flag_key`, `flag_value` FROM `eh_core_flags_player` f JOIN `eh_core_players` p on f.`player_id` = p.`player_id` WHERE `plugin` = '%s'", this.pluginName);
            while (resultSet2.next()) {
                UUID uUID = UUID.fromString(resultSet2.getString("uuid"));
                if (this.playerFlag.containsKey(uUID)) {
                    this.playerFlag.get(uUID).put(resultSet2.getString("flag_key"), resultSet2.getString("flag_value"));
                    continue;
                }
                HashMap<String, String> hashMap = new HashMap<String, String>();
                hashMap.put(resultSet2.getString("flag_key"), resultSet2.getString("flag_value"));
                this.playerFlag.put(uUID, hashMap);
            }
            resultSet2.close();
        }
        catch (SQLException var1_2) {
            var1_2.printStackTrace();
            this.logger.severe("Error on load flags");
            return false;
        }
        return true;
    }

    public void updatePlayer(UUID uUID, String string) {
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("uuid", uUID.toString());
        hashMap.put("nick", string);
        HashMap<String, Object> hashMap2 = new HashMap<String, Object>();
        hashMap2.put("lastseen", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        hashMap2.put("nick", string);
        this.db.updateFast(this.db.sql("eh_core_players", hashMap, hashMap2), new Object[0]);
    }

    public <T> T addFlagDefault(String string, T t) {
        this.addFlag(string, t, false);
        return t;
    }

    public <T> T addFlag(String string, T t) {
        this.addFlag(string, t, true);
        return t;
    }

    public List<String> addFlagList(String string, List<String> list) {
        Gson gson = new Gson();
        this.addFlag(string, gson.toJson(list));
        return list;
    }

    public boolean addFlag(String string, Object object, boolean bl) {
        String string2 = "INSERT INTO `eh_core_flags_plugin` (`plugin`, `flag_key`, `flag_value`) VALUES ('%s', '%s', '%s')";
        if (bl) {
            string2 = string2 + " ON DUPLICATE KEY UPDATE `flag_value` = '%s'";
        }
        try {
            this.db.getConnection();
            Object[] arrobject = new Object[4];
            arrobject[0] = this.pluginName;
            arrobject[1] = string;
            arrobject[2] = object == null ? "" : object.toString();
            arrobject[3] = object == null ? "" : object.toString();
            this.db.insert(string2, arrobject);
            if (this.useCache) {
                this.pluginFlag.put(string, object == null ? "" : object.toString());
            }
            return false;
        }
        catch (SQLIntegrityConstraintViolationException var5_5) {
            if (!bl) {
                return true;
            }
            var5_5.printStackTrace();
            this.logger.severe("Error on insert flag '" + string + "'");
        }
        catch (SQLException var5_6) {
            var5_6.printStackTrace();
            this.logger.severe("Error on insert flag '" + string + "'");
        }
        return false;
    }

    public <T> T addFlagDefault(UUID uUID, String string, T t) {
        this.addFlag(uUID, string, t, false);
        return t;
    }

    public <T> T addFlag(UUID uUID, String string, T t) {
        this.addFlag(uUID, string, t, true);
        return t;
    }

    public void addFlagList(UUID uUID, String string, Collection<String> collection) {
        Gson gson = new Gson();
        this.addFlag(uUID, string, gson.toJson(collection));
    }

    public boolean addFlag(OfflinePlayer offlinePlayer, String string, Object object) {
        return this.addFlag(offlinePlayer.getUniqueId(), string, object, true);
    }

    public boolean addFlag(OfflinePlayer offlinePlayer, String string, Object object, boolean bl) {
        return this.addFlag(offlinePlayer.getUniqueId(), string, object, bl);
    }

    public boolean addFlag(UUID uUID, String string, Object object, boolean bl) {
        String string2 = "INSERT INTO `eh_core_flags_player` (`plugin`, `player_id`, `flag_key`, `flag_value`) VALUES ('%s', (SELECT `player_id` FROM `eh_core_players` WHERE `uuid` = '%s'), '%s', '%s')";
        if (bl) {
            string2 = string2 + " ON DUPLICATE KEY UPDATE `flag_value` = '%s'";
        }
        try {
            this.db.getConnection();
            Object[] arrobject = new Object[5];
            arrobject[0] = this.pluginName;
            arrobject[1] = uUID.toString();
            arrobject[2] = string;
            arrobject[3] = object == null ? "" : object.toString();
            arrobject[4] = object == null ? "" : object.toString();
            this.db.insert(string2, arrobject);
            if (this.useCache) {
                if (this.playerFlag.containsKey(uUID)) {
                    this.playerFlag.get(uUID).put(string, object == null ? "" : object.toString());
                } else {
                    HashMap<String, String> hashMap = new HashMap<String, String>();
                    hashMap.put(string, object == null ? "" : object.toString());
                    this.playerFlag.put(uUID, hashMap);
                }
            }
            return true;
        }
        catch (MySQLIntegrityConstraintViolationException var6_7) {
            if (!bl) {
                return true;
            }
            var6_7.printStackTrace();
            this.logger.severe("Error on insert flag '" + string + "' of player '" + uUID + "'");
        }
        catch (SQLException var6_8) {
            var6_8.printStackTrace();
            this.logger.severe("Error on insert flag '" + string + "' of player '" + uUID + "'");
        }
        return false;
    }

    public boolean hasFlag(String string) {
        if (this.useCache) {
            return this.pluginFlag.containsKey(string);
        }
        ResultSet resultSet = this.db.query("SELECT `flag_value` FROM `eh_core_flags_plugin` WHERE `plugin` = '%s' AND `flag_key` = '%s'", this.pluginName, string);
        try {
            if (resultSet.isBeforeFirst()) {
                return true;
            }
        }
        catch (SQLException var3_3) {
            var3_3.printStackTrace();
        }
        return false;
    }

    public boolean hasFlag(OfflinePlayer offlinePlayer, String string) {
        return this.hasFlag(offlinePlayer.getUniqueId(), string);
    }

    public boolean hasFlag(UUID uUID, String string) {
        if (this.useCache) {
            if (this.playerFlag.containsKey(uUID)) {
                return this.playerFlag.get(uUID).containsKey(string);
            }
        } else {
            ResultSet resultSet = this.db.query("SELECT `flag_value` FROM `eh_core_flags_player` f JOIN `eh_core_players` p on f.`player_id` = p.`player_id` WHERE `plugin` = '%s' AND `uuid` = '%s' AND `flag_key` = '%s'", this.pluginName, uUID, string);
            try {
                if (resultSet.isBeforeFirst()) {
                    return true;
                }
            }
            catch (SQLException var4_4) {
                var4_4.printStackTrace();
            }
        }
        return false;
    }

    public List<String> getFlagList(String string) {
        String string2 = this.getFlag(string);
        Gson gson = new Gson();
        Type type = new TypeToken<List<String>>(){}.getType();
        return (List)gson.fromJson(string2, type);
    }

    public Collection<String> getFlagList(OfflinePlayer offlinePlayer, String string) {
        return this.getFlagList(offlinePlayer.getUniqueId(), string);
    }

    public Collection<String> getFlagList(UUID uUID, String string) {
        String string2 = this.getFlag(uUID, string);
        Gson gson = new Gson();
        Type type = new TypeToken<Collection<String>>(){}.getType();
        return (Collection)gson.fromJson(string2, type);
    }

    public String getFlag(String string) {
        if (this.hasFlag(string)) {
            if (this.useCache) {
                return this.pluginFlag.get(string);
            }
            ResultSet resultSet = this.db.query("SELECT `flag_value` FROM `eh_core_flags_plugin` WHERE `plugin` = '%s' AND `flag_key` = '%s'", this.pluginName, string);
            try {
                if (resultSet.next()) {
                    return resultSet.getString("flag_value");
                }
            }
            catch (SQLException var3_3) {
                var3_3.printStackTrace();
            }
        }
        return null;
    }

    public int getFlagInteger(OfflinePlayer offlinePlayer, String string) {
        return Integer.valueOf(this.getFlag(offlinePlayer.getUniqueId(), string));
    }

    public boolean getFlagBoolean(OfflinePlayer offlinePlayer, String string) {
        return Boolean.valueOf(this.getFlag(offlinePlayer.getUniqueId(), string));
    }

    public String getFlag(OfflinePlayer offlinePlayer, String string) {
        return this.getFlag(offlinePlayer.getUniqueId(), string);
    }

    public String getFlag(UUID uUID, String string) {
        if (this.hasFlag(uUID, string)) {
            if (this.useCache) {
                return this.playerFlag.get(uUID).get(string);
            }
            ResultSet resultSet = this.db.query("SELECT `flag_value` FROM `eh_core_flags_player` f JOIN `eh_core_players` p on f.`player_id` = p.`player_id` WHERE `plugin` = '%s' AND `uuid` = '%s' AND `flag_key` = '%s'", this.pluginName, uUID, string);
            try {
                if (resultSet.next()) {
                    return resultSet.getString("flag_value");
                }
            }
            catch (SQLException var4_4) {
                var4_4.printStackTrace();
            }
        }
        return null;
    }

    public boolean deleteFlag(String string) {
        String string2 = "DELETE FROM `eh_core_flags_plugin` WHERE `plugin` = '%s' AND `flag_key` = '%s' AND `player_id` IS NULL;";
        try {
            this.db.getConnection();
            this.db.update(string2, this.pluginName, string);
            if (this.useCache) {
                this.pluginFlag.remove(string);
            }
            return true;
        }
        catch (SQLException var3_3) {
            var3_3.printStackTrace();
            this.logger.severe("Error on delete flag '" + string + "'");
            return false;
        }
    }

    public boolean deleteFlag(OfflinePlayer offlinePlayer, String string) {
        return this.deleteFlag(offlinePlayer.getUniqueId(), string);
    }

    public boolean deleteFlag(UUID uUID, String string) {
        String string2 = "DELETE FROM `eh_core_flags_player` WHERE `plugin` = '%s' AND `player_id` = '%s' AND `flag_key` = '%s';";
        try {
            this.db.getConnection();
            this.db.update(string2, this.pluginName, uUID.toString(), string);
            if (this.useCache) {
                this.pluginFlag.remove(string);
            }
            return true;
        }
        catch (SQLException var4_4) {
            var4_4.printStackTrace();
            this.logger.severe("Error on delete flag '" + string + "' of player '" + uUID + "'");
            return false;
        }
    }

    public <T> T loadClass(Class<T> class_) {
        T t = null;
        try {
            t = class_.newInstance();
            if (t == null) {
                throw new IllegalArgumentException("class cannot be null");
            }
        }
        catch (IllegalAccessException | InstantiationException var3_3) {
            var3_3.printStackTrace();
        }
        for (Field field : class_.getDeclaredFields()) {
            try {
                Object object;
                List<String> list;
                ConfigValue configValue = field.getAnnotation(ConfigValue.class);
                if (configValue == null) continue;
                field.setAccessible(true);
                List<String> list2 = field.get(t);
                if (this.hasFlag(configValue.value())) {
                    Object object2;
                    list = this.getFlag(configValue.value());
                    if (field.getType() == String.class) {
                        list = ((String)((Object)list)).replace("&", "\u00a7");
                    } else if (field.getType() == Integer.class || field.getType() == Integer.TYPE) {
                        list = Integer.parseInt((String)((Object)list));
                    } else if (field.getType() == Long.class || field.getType() == Long.TYPE) {
                        list = Long.parseLong((String)((Object)list));
                    } else if (field.getType() == Double.class || field.getType() == Double.TYPE) {
                        list = Double.parseDouble((String)((Object)list));
                    } else if (field.getType() == Boolean.class || field.getType() == Boolean.TYPE) {
                        list = Boolean.parseBoolean((String)((Object)list));
                    } else if (field.getType() == Location.class) {
                        object = null;
                        if (!list.equals((Object)"")) {
                            object2 = ((String)((Object)list)).split(",");
                            object = new Location(Bukkit.getServer().getWorld(object2[0].trim()), Double.parseDouble(object2[1].trim()), Double.parseDouble(object2[2].trim()), Double.parseDouble(object2[3].trim()));
                            if (object2.length > 4) {
                                object.setPitch(Float.parseFloat(object2[4].trim()));
                                object.setYaw(Float.parseFloat(object2[5].trim()));
                            }
                        }
                        list = object;
                    } else if (field.getType() == List.class) {
                        list = this.getFlagList(configValue.value());
                        object = list.listIterator();
                        while (object.hasNext()) {
                            object2 = object.next();
                            if (!(object2 instanceof String)) continue;
                            object.set(((String)object2).replace("&", "\u00a7"));
                        }
                    }
                    field.set(t, list);
                    continue;
                }
                list = list2;
                if (list2 != null && list2 instanceof List) {
                    object = (Location)list2;
                    list = object.getWorld().getName() + "," + object.getBlockX() + "," + object.getBlockY() + "," + object.getBlockZ() + "," + object.getPitch() + "," + object.getYaw();
                }
                this.addFlag(configValue.value(), list);
                continue;
            }
            catch (Exception var7_9) {
                throw new RuntimeException("Failed to set config value for field '" + field.getName() + "' in " + class_, var7_9);
            }
        }
        return t;
    }

    public <T> T loadClassBungee(Class<T> class_) {
        T t = null;
        try {
            t = class_.newInstance();
            if (t == null) {
                throw new IllegalArgumentException("class cannot be null");
            }
        }
        catch (IllegalAccessException | InstantiationException var3_3) {
            var3_3.printStackTrace();
        }
        for (Field field : class_.getDeclaredFields()) {
            try {
                Object object;
                List<String> list;
                ConfigValue configValue = field.getAnnotation(ConfigValue.class);
                if (configValue == null) continue;
                field.setAccessible(true);
                List<String> list2 = field.get(t);
                if (this.hasFlag(configValue.value())) {
                    list = this.getFlag(configValue.value());
                    if (field.getType() == String.class) {
                        list = ((String)((Object)list)).replace("&", "\u00a7");
                    } else if (field.getType() == Integer.class || field.getType() == Integer.TYPE) {
                        list = Integer.parseInt((String)((Object)list));
                    } else if (field.getType() == Long.class || field.getType() == Long.TYPE) {
                        list = Long.parseLong((String)((Object)list));
                    } else if (field.getType() == Double.class || field.getType() == Double.TYPE) {
                        list = Double.parseDouble((String)((Object)list));
                    } else if (field.getType() == Boolean.class || field.getType() == Boolean.TYPE) {
                        list = Boolean.parseBoolean((String)((Object)list));
                    } else if (field.getType() == List.class) {
                        list = this.getFlagList(configValue.value());
                        object = list.listIterator();
                        while (object.hasNext()) {
                            Object e = object.next();
                            if (!(e instanceof String)) continue;
                            object.set(((String)e).replace("&", "\u00a7"));
                        }
                    }
                    field.set(t, list);
                    continue;
                }
                list = list2;
                if (list2 != null && list2 instanceof List) {
                    object = (Location)list2;
                    list = object.getWorld().getName() + "," + object.getBlockX() + "," + object.getBlockY() + "," + object.getBlockZ() + "," + object.getPitch() + "," + object.getYaw();
                }
                this.addFlag(configValue.value(), list);
                continue;
            }
            catch (Exception var7_9) {
                throw new RuntimeException("Failed to set config value for field '" + field.getName() + "' in " + class_, var7_9);
            }
        }
        return t;
    }

    public static <T> T loadLocalConfig(org.bukkit.plugin.Plugin plugin, Class<T> class_) {
        return Configuration.loadLocalConfig(plugin, class_, "config.yml");
    }

    public static <T> T loadLocalConfig(org.bukkit.plugin.Plugin plugin, Class<T> class_, String string) {
        T t = null;
        try {
            t = class_.newInstance();
            if (t == null) {
                throw new IllegalArgumentException("class cannot be null");
            }
        }
        catch (IllegalAccessException | InstantiationException var4_4) {
            var4_4.printStackTrace();
        }
        FileConfiguration fileConfiguration = plugin.getConfig();
        for (Field field : class_.getDeclaredFields()) {
            try {
                ConfigValue configValue = field.getAnnotation(ConfigValue.class);
                if (configValue == null) continue;
                field.setAccessible(true);
                if (fileConfiguration.contains(configValue.value())) {
                    Object object = fileConfiguration.get(configValue.value());
                    if (object instanceof String) {
                        object = org.bukkit.ChatColor.translateAlternateColorCodes((char)'&', (String)((String)object));
                    }
                    if (object instanceof List) {
                        ListIterator<String> listIterator = ((List)object).listIterator();
                        while (listIterator.hasNext()) {
                            Object e = listIterator.next();
                            if (!(e instanceof String)) continue;
                            listIterator.set(org.bukkit.ChatColor.translateAlternateColorCodes((char)'&', (String)((String)e)));
                        }
                    }
                    field.set(t, object);
                    continue;
                }
                fileConfiguration.set(configValue.value(), field.get(t));
                continue;
            }
            catch (Exception var9_11) {
                throw new RuntimeException("Failed to set config value for field '" + field.getName() + "' in " + class_, var9_11);
            }
        }
        plugin.saveConfig();
        return t;
    }

    public static <T> T loadLocalConfig(Plugin plugin, Class<T> class_) {
        return Configuration.loadLocalConfig(plugin, class_, "config.yml");
    }

    public static <T> T loadLocalConfig(Plugin plugin, Class<T> class_, String string) {
        Object object;
        Object object2;
        T t = null;
        try {
            t = class_.newInstance();
            if (t == null) {
                throw new IllegalArgumentException("class cannot be null");
            }
        }
        catch (IllegalAccessException | InstantiationException var4_4) {
            var4_4.printStackTrace();
        }
        File file = new File(plugin.getDataFolder(), string);
        if (!file.exists()) {
            try {
                BungeeCord.getInstance().getLogger().log(Level.INFO, "Creating file: " + file.getName());
                file.getParentFile().mkdirs();
                object2 = plugin.getResourceAsStream(file.getName());
                if (object2 != null) {
                    int n;
                    object = null;
                    object = new FileOutputStream(file);
                    byte[] arrby = new byte[1024];
                    while ((n = object2.read(arrby)) > 0) {
                        object.write(arrby, 0, n);
                    }
                    object.close();
                } else {
                    file.createNewFile();
                }
            }
            catch (IOException var5_7) {
                BungeeCord.getInstance().getLogger().log(Level.SEVERE, "Could not create file: " + file.getName());
            }
        }
        object2 = null;
        try {
            object2 = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
        }
        catch (IOException var6_9) {
            var6_9.printStackTrace();
        }
        for (Field field : class_.getDeclaredFields()) {
            try {
                ConfigValue configValue = field.getAnnotation(ConfigValue.class);
                if (configValue == null) continue;
                field.setAccessible(true);
                if (object2.get(configValue.value()) != null) {
                    Object object3 = object2.get(configValue.value());
                    if (object3 instanceof String) {
                        object3 = ChatColor.translateAlternateColorCodes((char)'&', (String)((String)object3));
                    }
                    if (object3 instanceof List) {
                        ListIterator<String> listIterator = ((List)object3).listIterator();
                        while (listIterator.hasNext()) {
                            Object e = listIterator.next();
                            if (!(e instanceof String)) continue;
                            listIterator.set(ChatColor.translateAlternateColorCodes((char)'&', (String)((String)e)));
                        }
                    }
                    field.set(t, object3);
                    continue;
                }
                object2.set(configValue.value(), field.get(t));
                continue;
            }
            catch (Exception var10_16) {
                throw new RuntimeException("Failed to set config value for field '" + field.getName() + "' in " + class_, var10_16);
            }
        }
        try {
            Files.createParentDirs((File)file);
            object = File.createTempFile(file.getName(), null, file.getParentFile());
            object.deleteOnExit();
            ConfigurationProvider.getProvider(YamlConfiguration.class).save((net.md_5.bungee.config.Configuration)object2, (File)object);
            file.delete();
            object.renameTo(file);
            object.delete();
        }
        catch (Exception var6_10) {
            var6_10.printStackTrace();
        }
        return t;
    }

    public Logger getLogger() {
        return this.logger;
    }

}

