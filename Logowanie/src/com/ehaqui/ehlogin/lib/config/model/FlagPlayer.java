/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.lib.config.model;

import com.ehaqui.ehlogin.lib.database.DatabaseField;
import com.ehaqui.ehlogin.lib.database.DatabaseScript;
import com.ehaqui.ehlogin.lib.database.DatabaseTable;

@DatabaseTable(value="eh_core_flags_player")
@DatabaseScript(value={"ALTER TABLE `eh_core_flags_player`ADD CONSTRAINT `eh_core_flags_player_ibfk_1` FOREIGN KEY (`player_id`) REFERENCES `eh_core_players` (`player_id`) ON DELETE CASCADE ON UPDATE CASCADE;", "ALTER TABLE `eh_core_flags_player` ADD UNIQUE( `plugin`, `player_id`, `flag_key`);"})
public class FlagPlayer {
    @DatabaseField(value="plugin")
    private String plugin;
    @DatabaseField(value="player_id", type="int(80)", nullField=1)
    private int playerid;
    @DatabaseField(value="flag_key")
    private String flag_key;
    @DatabaseField(value="flag_value", type="text")
    private String flag_value;

    public String getPlugin() {
        return this.plugin;
    }

    public int getPlayerid() {
        return this.playerid;
    }

    public String getFlag_key() {
        return this.flag_key;
    }

    public String getFlag_value() {
        return this.flag_value;
    }

    public void setPlugin(String string) {
        this.plugin = string;
    }

    public void setPlayerid(int n) {
        this.playerid = n;
    }

    public void setFlag_key(String string) {
        this.flag_key = string;
    }

    public void setFlag_value(String string) {
        this.flag_value = string;
    }

    public String toString() {
        return "FlagPlayer(plugin=" + this.getPlugin() + ", playerid=" + this.getPlayerid() + ", flag_key=" + this.getFlag_key() + ", flag_value=" + this.getFlag_value() + ")";
    }
}

