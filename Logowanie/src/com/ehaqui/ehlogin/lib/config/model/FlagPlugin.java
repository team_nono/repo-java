/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.lib.config.model;

import com.ehaqui.ehlogin.lib.database.DatabaseField;
import com.ehaqui.ehlogin.lib.database.DatabaseScript;
import com.ehaqui.ehlogin.lib.database.DatabaseTable;

@DatabaseTable(value="eh_core_flags_plugin")
@DatabaseScript(value={"ALTER TABLE `eh_core_flags_plugin` ADD UNIQUE KEY `plugin` (`plugin`,`flag_key`);"})
public class FlagPlugin {
    @DatabaseField(value="plugin")
    private String plugin;
    @DatabaseField(value="flag_key")
    private String flag_key;
    @DatabaseField(value="flag_value", type="text")
    private String flag_value;

    public String getPlugin() {
        return this.plugin;
    }

    public String getFlag_key() {
        return this.flag_key;
    }

    public String getFlag_value() {
        return this.flag_value;
    }

    public void setPlugin(String string) {
        this.plugin = string;
    }

    public void setFlag_key(String string) {
        this.flag_key = string;
    }

    public void setFlag_value(String string) {
        this.flag_value = string;
    }

    public String toString() {
        return "FlagPlugin(plugin=" + this.getPlugin() + ", flag_key=" + this.getFlag_key() + ", flag_value=" + this.getFlag_value() + ")";
    }
}

