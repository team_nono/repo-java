/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.lib.config.model;

import com.ehaqui.ehlogin.lib.database.DatabaseField;
import com.ehaqui.ehlogin.lib.database.DatabaseTable;
import java.util.Date;
import java.util.UUID;

@DatabaseTable(value="eh_core_players")
public class Players {
    @DatabaseField(value="player_id", id=1)
    private int id;
    @DatabaseField(value="uuid", unique=1)
    private UUID uuid;
    @DatabaseField(value="nick")
    private String name;
    @DatabaseField(value="lastseen", type="TIMESTAMP on update CURRENT_TIMESTAMP", defaultValue="CURRENT_TIMESTAMP")
    private Date lastseen;

    public int getId() {
        return this.id;
    }

    public UUID getUuid() {
        return this.uuid;
    }

    public String getName() {
        return this.name;
    }

    public Date getLastseen() {
        return this.lastseen;
    }

    public void setId(int n) {
        this.id = n;
    }

    public void setUuid(UUID uUID) {
        this.uuid = uUID;
    }

    public void setName(String string) {
        this.name = string;
    }

    public void setLastseen(Date date) {
        this.lastseen = date;
    }

    public String toString() {
        return "Players(id=" + this.getId() + ", uuid=" + this.getUuid() + ", name=" + this.getName() + ", lastseen=" + this.getLastseen() + ")";
    }
}

