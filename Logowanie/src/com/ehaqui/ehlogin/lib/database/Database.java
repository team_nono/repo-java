/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.Location
 *  org.bukkit.Sound
 *  org.bukkit.World
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.ItemStack
 */
package com.ehaqui.ehlogin.lib.database;

import com.ehaqui.ehlogin.lib.database.DatabaseField;
import com.ehaqui.ehlogin.lib.database.DatabaseScript;
import com.ehaqui.ehlogin.lib.database.DatabaseTable;
import com.ehaqui.ehlogin.lib.database.DatabaseType;
import com.ehaqui.ehlogin.lib.util.TextUtils;
import com.ehaqui.ehlogin.lib.util.serialization.InventorySerialization;
import com.ehaqui.ehlogin.lib.util.serialization.SingleItemSerialization;
import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class Database {
    private Connection conn = null;
    private Statement statement = null;
    private HashMap<Integer, HashMap<String, Object>> rows = new HashMap();
    private HashMap<String, Object> column = new HashMap();
    private int numRows = 0;
    private Logger logger;
    private DatabaseType type = DatabaseType.SQLITE;
    private String dbHost;
    private String dbPort;
    private String dbDatabase;
    private String dbUser;
    private String dPass;
    private String fileName;
    public String lastQuery;
    public boolean debug = false;
    public String prefix = "[Database] ";
    private File dataFolder;

    public Database(Logger logger, String string, String string2, String string3, String string4, String string5) {
        this.setConfigs(logger, DatabaseType.MYSQL, null, null, string, string2, string3, string4, string5, this.debug);
    }

    public Database(Logger logger, String string, String string2, String string3, String string4, String string5, boolean bl) {
        this.setConfigs(logger, DatabaseType.MYSQL, null, null, string, string2, string3, string4, string5, bl);
    }

    public Database(Logger logger, File file, String string) {
        this.setConfigs(logger, DatabaseType.SQLITE, file, string, null, null, null, null, null, this.debug);
    }

    public Database(Logger logger, File file, String string, boolean bl) {
        this.setConfigs(logger, DatabaseType.SQLITE, file, string, null, null, null, null, null, bl);
    }

    protected void setConfigs(Logger logger, DatabaseType databaseType, File file, String string, String string2, String string3, String string4, String string5, String string6, boolean bl) {
        this.logger = logger;
        this.type = databaseType;
        logger.info(this.prefix + "Using " + (Object)((Object)databaseType));
        switch (databaseType) {
            case SQLITE: {
                this.dataFolder = file;
                this.fileName = string;
                break;
            }
            case MYSQL: {
                this.dbHost = "".equals(string2) || string2 == null ? "localhost" : string2;
                this.dbPort = "".equals(string3) || string3 == null ? "3306" : string3;
                this.dbDatabase = string4;
                this.dbUser = string5;
                this.dPass = string6;
            }
        }
    }

    public void setDebug(boolean bl) {
        this.debug = bl;
    }

    public void setup() {
        if (this.getConnection() != null) {
            this.logger.info(this.prefix + "Connected!");
        } else {
            this.logger.severe(this.prefix + "Erro na conexao com o Banco de dados");
        }
    }

    public Connection getConnection() {
        if (this.conn == null) {
            return this.open();
        }
        return this.conn;
    }

    public Connection open() {
        try {
            switch (this.type) {
                case MYSQL: {
                    Class.forName("com.mysql.jdbc.Driver").newInstance();
                    String string = "jdbc:mysql://" + this.dbHost + ":" + this.dbPort + "/" + this.dbDatabase + "?autoReconnect=true&failOverReadOnly=false&zeroDateTimeBehavior=convertToNull";
                    this.conn = DriverManager.getConnection(string, this.dbUser, this.dPass);
                    break;
                }
                case SQLITE: {
                    Class.forName("org.sqlite.JDBC");
                    String string = "jdbc:sqlite:" + this.dataFolder.getAbsolutePath() + File.separator + this.fileName + ".db";
                    this.conn = DriverManager.getConnection(string, "", "");
                }
            }
            return this.conn;
        }
        catch (Exception var2_3) {
            var2_3.printStackTrace();
            this.logger.warning(this.prefix + var2_3.getMessage());
            return null;
        }
    }

    public void close() {
        try {
            this.statement.close();
            this.conn.close();
        }
        catch (Exception var1_1) {
            this.logger.warning(this.prefix + var1_1.getMessage());
        }
    }

    private void setLastQuery(String string) {
        if (this.debug) {
            this.logger.info(this.prefix + "Query - " + string);
        }
        this.lastQuery = string;
    }

    public void printLastQuery() {
        this.logger.info(this.prefix + "Query - " + this.lastQuery);
    }

    public boolean checkTable(String string) {
        DatabaseMetaData databaseMetaData = null;
        try {
            databaseMetaData = this.open().getMetaData();
            ResultSet resultSet = databaseMetaData.getTables(null, null, string, null);
            if (resultSet.next()) {
                return true;
            }
            return false;
        }
        catch (Exception var3_4) {
            this.logger.severe(this.prefix + this.lastQuery);
            this.logger.warning(this.prefix + var3_4.getMessage());
            return false;
        }
    }

    public void checkTable(String string, String string2) {
        DatabaseMetaData databaseMetaData = null;
        try {
            databaseMetaData = this.open().getMetaData();
            ResultSet resultSet = databaseMetaData.getTables(null, null, string, null);
            if (!resultSet.next()) {
                this.logger.info(this.prefix + "Criando tablela '" + string + "'");
                this.setLastQuery(string2);
                this.updateFast(string2, new Object[0]);
            }
        }
        catch (Exception var4_5) {
            this.logger.severe(this.prefix + this.lastQuery);
            this.logger.warning(this.prefix + var4_5.getMessage());
        }
    }

    public boolean createTable(String string, String[] arrstring, String[] arrstring2) {
        try {
            this.statement = this.conn.createStatement();
            String string2 = "CREATE TABLE " + string + "(";
            for (int i = 0; i < arrstring.length; ++i) {
                if (i != 0) {
                    string2 = string2 + ",";
                }
                string2 = string2 + arrstring[i] + " " + arrstring2[i];
            }
            string2 = string2 + ")";
            this.setLastQuery(string2);
            this.statement.execute(string2);
        }
        catch (Exception var4_5) {
            this.logger.severe(this.prefix + this.lastQuery);
            this.logger.warning(this.prefix + var4_5.getMessage());
        }
        return true;
    }

    public /* varargs */ ResultSet query(String string, Object ... arrobject) {
        this.getConnection();
        try {
            this.statement = this.conn.createStatement();
            string = String.format(string, arrobject);
            this.setLastQuery(string);
            ResultSet resultSet = this.statement.executeQuery(string);
            return resultSet;
        }
        catch (Exception var3_4) {
            this.logger.severe(this.prefix + this.lastQuery);
            if (!var3_4.getMessage().contains("not return ResultSet") || var3_4.getMessage().contains("not return ResultSet") && string.startsWith("SELECT")) {
                this.logger.warning(this.prefix + var3_4.getMessage());
            }
            return null;
        }
    }

    public /* varargs */ int getTotalResults(String string, Object ... arrobject) {
        int n;
        block3 : {
            this.getConnection();
            n = 0;
            try {
                this.statement = this.conn.createStatement();
                string = String.format(string, arrobject);
                this.setLastQuery(string);
                ResultSet resultSet = this.statement.executeQuery(string);
                if (resultSet.next()) {
                    n = resultSet.getInt(1);
                }
            }
            catch (Exception var4_5) {
                this.logger.severe(this.prefix + this.lastQuery);
                if (var4_5.getMessage().contains("not return ResultSet") && (!var4_5.getMessage().contains("not return ResultSet") || !string.startsWith("SELECT"))) break block3;
                this.logger.warning(this.prefix + var4_5.getMessage());
            }
        }
        return n;
    }

    public /* varargs */ HashMap<String, Object> selectRow(String string, Object ... arrobject) {
        this.getConnection();
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        this.statement = this.conn.createStatement();
        string = String.format(string, arrobject);
        this.setLastQuery(string);
        ResultSet resultSet = this.statement.executeQuery(string);
        if (resultSet != null) {
            int n = resultSet.getMetaData().getColumnCount();
            String string2 = "";
            for (int i = 1; i <= n; ++i) {
                if (!"".equals(string2)) {
                    string2 = string2 + ",";
                }
                string2 = string2 + resultSet.getMetaData().getColumnName(i);
            }
            String[] arrstring = string2.split(",");
            while (resultSet.next()) {
                for (String string3 : arrstring) {
                    hashMap.put(string3, resultSet.getObject(string3));
                }
            }
            resultSet.close();
            return hashMap;
        }
        return null;
    }

    public /* varargs */ HashMap<String, Object> selectRowFast(String string, Object ... arrobject) {
        this.getConnection();
        try {
            this.statement = this.conn.createStatement();
            string = String.format(string, arrobject);
            this.setLastQuery(string);
            ResultSet resultSet = this.statement.executeQuery(string);
            if (resultSet != null) {
                int n = resultSet.getMetaData().getColumnCount();
                String string2 = "";
                for (int i = 1; i <= n; ++i) {
                    if (!"".equals(string2)) {
                        string2 = string2 + ",";
                    }
                    string2 = string2 + resultSet.getMetaData().getColumnName(i);
                }
                String[] arrstring = string2.split(",");
                while (resultSet.next()) {
                    for (String string3 : arrstring) {
                        this.column.put(string3, resultSet.getObject(string3));
                    }
                }
                resultSet.close();
                return this.column;
            }
            return null;
        }
        catch (SQLException var3_4) {
            this.logger.severe(this.prefix + this.lastQuery);
            var3_4.printStackTrace();
            return null;
        }
    }

    public /* varargs */ HashMap<Integer, HashMap<String, Object>> selectCol(String string, Object ... arrobject) {
        this.getConnection();
        this.rows.clear();
        this.numRows = 0;
        this.statement = this.conn.createStatement();
        string = String.format(string, arrobject);
        this.setLastQuery(string);
        ResultSet resultSet = this.statement.executeQuery(string);
        if (resultSet != null) {
            int n = resultSet.getMetaData().getColumnCount();
            String string2 = "";
            for (int i = 1; i <= n; ++i) {
                if (!"".equals(string2)) {
                    string2 = string2 + ",";
                }
                string2 = string2 + resultSet.getMetaData().getColumnName(i);
            }
            String[] arrstring = string2.split(",");
            this.numRows = 0;
            while (resultSet.next()) {
                HashMap<String, Object> hashMap = new HashMap<String, Object>();
                for (String string3 : arrstring) {
                    hashMap.put(string3, resultSet.getObject(string3));
                }
                this.rows.put(this.numRows, hashMap);
                ++this.numRows;
            }
            resultSet.close();
            return this.rows;
        }
        return null;
    }

    public /* varargs */ HashMap<Integer, HashMap<String, Object>> selectColFast(String string, Object ... arrobject) {
        this.getConnection();
        this.rows.clear();
        this.numRows = 0;
        try {
            this.statement = this.conn.createStatement();
            string = String.format(string, arrobject);
            this.setLastQuery(string);
            ResultSet resultSet = this.statement.executeQuery(string);
            if (resultSet != null) {
                int n = resultSet.getMetaData().getColumnCount();
                String string2 = "";
                for (int i = 1; i <= n; ++i) {
                    if (!"".equals(string2)) {
                        string2 = string2 + ",";
                    }
                    string2 = string2 + resultSet.getMetaData().getColumnName(i);
                }
                String[] arrstring = string2.split(",");
                this.numRows = 0;
                while (resultSet.next()) {
                    HashMap<String, Object> hashMap = new HashMap<String, Object>();
                    for (String string3 : arrstring) {
                        hashMap.put(string3, resultSet.getObject(string3));
                    }
                    this.rows.put(this.numRows, hashMap);
                    ++this.numRows;
                }
                resultSet.close();
                return this.rows;
            }
            return null;
        }
        catch (SQLException var3_4) {
            var3_4.printStackTrace();
            return null;
        }
    }

    public /* varargs */ int insert(String string, Object ... arrobject) {
        this.getConnection();
        string = String.format(string, arrobject);
        this.setLastQuery(string);
        int n = 0;
        this.statement = this.conn.createStatement();
        this.statement.executeUpdate(string);
        n = this.getLastID();
        return n;
    }

    public /* varargs */ int insertFast(String string, Object ... arrobject) {
        this.getConnection();
        string = String.format(string, arrobject);
        int n = 0;
        try {
            this.statement = this.conn.createStatement();
            this.statement.executeUpdate(string);
            n = this.getLastID();
        }
        catch (Exception var4_4) {
            this.logger.severe(this.prefix + this.lastQuery);
            var4_4.printStackTrace();
        }
        return n;
    }

    public /* varargs */ void update(String string, Object ... arrobject) {
        this.getConnection();
        string = String.format(string, arrobject);
        this.setLastQuery(string);
        this.statement = this.conn.createStatement();
        this.statement.executeUpdate(string);
    }

    public /* varargs */ void updateFast(String string, Object ... arrobject) {
        string = String.format(string, arrobject);
        this.setLastQuery(string);
        try {
            this.statement = this.conn.createStatement();
            this.statement.executeUpdate(string);
        }
        catch (SQLException var3_3) {
            this.logger.severe(this.prefix + this.lastQuery);
            var3_3.printStackTrace();
        }
    }

    public String sql(String string, HashMap<String, Object> hashMap) {
        return this.sql(string, hashMap, new HashMap<String, Object>());
    }

    public String sql(String string, HashMap<String, Object> hashMap, HashMap<String, Object> hashMap2) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("INSERT INTO `" + string + "` (");
        int n = 1;
        for (String object22 : hashMap.keySet()) {
            stringBuilder.append("`");
            stringBuilder.append(object22);
            stringBuilder.append("`");
            if (n >= hashMap.size()) continue;
            stringBuilder.append(", ");
            ++n;
        }
        stringBuilder.append(") VALUES (");
        n = 1;
        for (Object e : hashMap.values()) {
            stringBuilder.append("'");
            stringBuilder.append(e == null ? "" : e.toString());
            stringBuilder.append("'");
            if (n >= hashMap.size()) continue;
            stringBuilder.append(", ");
            ++n;
        }
        stringBuilder.append(")");
        if (!hashMap2.isEmpty()) {
            stringBuilder.append("ON DUPLICATE KEY UPDATE ");
            n = 1;
            for (Map.Entry entry : hashMap2.entrySet()) {
                stringBuilder.append("`");
                stringBuilder.append(((String)entry.getKey()).toString());
                stringBuilder.append("`");
                stringBuilder.append(" = ");
                stringBuilder.append("'");
                stringBuilder.append(entry.getValue() == null ? "" : entry.getValue().toString());
                stringBuilder.append("'");
                if (n >= hashMap2.size()) continue;
                stringBuilder.append(", ");
                ++n;
            }
        }
        return stringBuilder.toString();
    }

    public String sql(String string, HashMap<String, Object> hashMap, String string2) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("UPDATE `" + string + "` ");
        int n = 1;
        if (hashMap != null) {
            stringBuilder.append("SET ");
            n = 1;
            for (Map.Entry<String, Object> entry : hashMap.entrySet()) {
                stringBuilder.append("`");
                stringBuilder.append(entry.getKey().toString());
                stringBuilder.append("`");
                stringBuilder.append(" = ");
                stringBuilder.append("'");
                stringBuilder.append(entry.getValue() == null ? "" : entry.getValue().toString());
                stringBuilder.append("'");
                if (n >= hashMap.size()) continue;
                stringBuilder.append(", ");
                ++n;
            }
        }
        stringBuilder.append("WHERE " + string2 + ";");
        return stringBuilder.toString();
    }

    private int getLastID() {
        ResultSet resultSet = null;
        switch (this.type) {
            case MYSQL: {
                resultSet = this.statement.executeQuery("SELECT LAST_INSERT_ID()");
                break;
            }
            case SQLITE: {
                resultSet = this.statement.executeQuery("SELECT last_insert_rowid()");
            }
        }
        int n = 0;
        while (resultSet.next()) {
            n = resultSet.getInt(1);
        }
        resultSet.close();
        return n;
    }

    public <T> List<T> save(List<T> list) {
        for (T t : list) {
            this.save(t);
        }
        return list;
    }

    public <T> T save(T t) {
        Object object;
        if (t == null) {
            throw new IllegalArgumentException("class cannot be null");
        }
        DatabaseTable databaseTable = t.getClass().getAnnotation(DatabaseTable.class);
        if (databaseTable == null) {
            throw new IllegalArgumentException("class has not a table annotation");
        }
        String string = databaseTable.value();
        Field field = null;
        DatabaseField databaseField = null;
        HashMap<String, String> hashMap = new HashMap<String, String>();
        for (Field field2 : t.getClass().getDeclaredFields()) {
            try {
                object = field2.getAnnotation(DatabaseField.class);
                if (object == null) continue;
                if (object.id()) {
                    field = field2;
                    databaseField = object;
                }
                hashMap.put(field2.getName(), object.value());
                continue;
            }
            catch (Exception var11_18) {
                throw new RuntimeException("Failed to load config value for field '" + field2.getName() + "' in " + t.getClass(), var11_18);
            }
        }
        HashMap hashMap2 = new HashMap();
        try {
            Object object3;
            for (Map.Entry entry : hashMap.entrySet()) {
                String string2 = (String)entry.getValue();
                object = t.getClass().getDeclaredField((String)entry.getKey());
                object.setAccessible(true);
                hashMap2.put(string2, this.parseField(t, (Field)object));
            }
            this.lastQuery = this.sql(string, hashMap2);
            if (field != null && (databaseField.autoincrement() || databaseField.id())) {
                object3 = this.conn.prepareStatement(this.lastQuery, 1);
                object3.executeUpdate();
                ResultSet resultSet = object3.getGeneratedKeys();
                resultSet.next();
                field.setAccessible(true);
                field.set(t, resultSet.getInt(1));
            } else {
                object3 = this.conn.prepareStatement(this.lastQuery);
                object3.executeUpdate();
            }
        }
        catch (IllegalAccessException | IllegalArgumentException | NoSuchFieldException | SecurityException | SQLException var8_10) {
            this.getLogger().info(this.lastQuery);
            var8_10.printStackTrace();
            return null;
        }
        return t;
    }

    public <T> boolean update(Class<T> class_) {
        Object object;
        if (class_ == null) {
            throw new IllegalArgumentException("class cannot be null");
        }
        DatabaseTable databaseTable = class_.getClass().getAnnotation(DatabaseTable.class);
        if (databaseTable == null) {
            throw new IllegalArgumentException("class has not a table annotation");
        }
        String string = databaseTable.value();
        Field field = null;
        DatabaseField databaseField = null;
        HashMap<String, String> hashMap = new HashMap<String, String>();
        for (Field field2 : class_.getClass().getDeclaredFields()) {
            try {
                object = field2.getAnnotation(DatabaseField.class);
                if (object == null) continue;
                if (object.id()) {
                    field = field2;
                    field.setAccessible(true);
                    databaseField = object;
                }
                hashMap.put(field2.getName(), object.value());
                continue;
            }
            catch (Exception var11_17) {
                throw new RuntimeException("Failed to load config value for field '" + field2.getName() + "' in " + class_.getClass(), var11_17);
            }
        }
        if (databaseField == null || field == null) {
            throw new RuntimeException("Failed to load id value");
        }
        HashMap hashMap2 = new HashMap();
        try {
            for (Map.Entry entry : hashMap.entrySet()) {
                String string2 = (String)entry.getValue();
                object = class_.getClass().getDeclaredField((String)entry.getKey());
                object.setAccessible(true);
                hashMap2.put(string2, this.parseField(class_, (Field)object));
            }
            PreparedStatement preparedStatement = this.conn.prepareStatement(this.sql(string, hashMap2, databaseField.value() + " = '" + field.get(class_) + "'"));
            preparedStatement.executeUpdate();
            return true;
        }
        catch (IllegalAccessException | IllegalArgumentException | NoSuchFieldException | SecurityException | SQLException var8_10) {
            var8_10.printStackTrace();
            return false;
        }
    }

    public <T> boolean createTable(Class<T> class_) {
        DatabaseTable databaseTable;
        String[] arrstring;
        Object object = null;
        try {
            object = class_.newInstance();
            if (object == null) {
                throw new IllegalArgumentException("class cannot be null");
            }
        }
        catch (Exception var3_3) {
            // empty catch block
        }
        if ((databaseTable = object.getClass().getAnnotation(DatabaseTable.class)) == null) {
            throw new IllegalArgumentException("class has not a table annotation");
        }
        if (this.checkTable(databaseTable.value())) {
            return false;
        }
        this.logger.warning(this.prefix + "Creating com.ehaqui.ehlogin.lib.database '" + databaseTable.value() + "'");
        DatabaseField databaseField = null;
        HashMap<Field, DatabaseField> hashMap = new HashMap<Field, DatabaseField>();
        for (Field object222 : object.getClass().getDeclaredFields()) {
            try {
                arrstring = object222.getAnnotation(DatabaseField.class);
                if (arrstring == null) continue;
                if (!arrstring.id()) {
                    hashMap.put(object222, (DatabaseField)arrstring);
                    continue;
                }
                databaseField = arrstring;
                continue;
            }
            catch (Exception var10_18) {
                throw new RuntimeException("Failed to load config value for field '" + object222.getName() + "' in " + object.getClass(), var10_18);
            }
        }
        StringBuilder stringBuilder = new StringBuilder();
        try {
            stringBuilder.append("CREATE TABLE IF NOT EXISTS `" + databaseTable.value() + "` (");
            if (databaseField != null) {
                stringBuilder.append(databaseField.value() + " INT NOT NULL AUTO_INCREMENT, ");
            }
            int n = 1;
            for (Map.Entry entry : hashMap.entrySet()) {
                arrstring = (Field)entry.getKey();
                DatabaseField databaseField2 = (DatabaseField)entry.getValue();
                stringBuilder.append(databaseField2.value() + " ");
                String string = !databaseField2.type().equals("") ? databaseField2.type() : this.getFieldType((Field)arrstring);
                stringBuilder.append(string + " ");
                stringBuilder.append(databaseField2.nullField() ? " NULL" : " NOT NULL");
                if (databaseField2.defaultValue().equals("CURRENT_TIMESTAMP")) {
                    stringBuilder.append(" DEFAULT '" + databaseField2.defaultValue() + "' ");
                }
                if (!databaseField2.defaultValue().equals("")) {
                    stringBuilder.append(" DEFAULT '" + databaseField2.defaultValue() + "' ");
                }
                stringBuilder.append(databaseField2.autoincrement() ? " AUTO_INCREMENT" : "");
                stringBuilder.append(databaseField2.now() ? " DEFAULT CURRENT_TIMESTAMP" : "");
                stringBuilder.append(databaseField2.unique() ? " UNIQUE" : " ");
                if (n >= hashMap.size()) continue;
                stringBuilder.append(", \n");
                ++n;
            }
            if (databaseField != null) {
                stringBuilder.append(", PRIMARY KEY ( " + databaseField.value() + " )");
            }
            stringBuilder.append(") ENGINE=InnoDB DEFAULT CHARSET=latin1;");
            PreparedStatement preparedStatement = this.conn.prepareStatement(stringBuilder.toString());
            preparedStatement.executeUpdate();
            DatabaseScript databaseScript = object.getClass().getAnnotation(DatabaseScript.class);
            if (databaseScript != null) {
                for (String string : databaseScript.value()) {
                    PreparedStatement preparedStatement2 = this.conn.prepareStatement(string);
                    preparedStatement2.executeUpdate();
                }
            }
            return true;
        }
        catch (IllegalArgumentException | SecurityException | SQLException var7_9) {
            this.logger.info(stringBuilder.toString());
            var7_9.printStackTrace();
        }
        catch (IllegalAccessException var7_10) {
            var7_10.printStackTrace();
        }
        return false;
    }

    public <T> T load(Class<T> class_, String string, String string2) {
        Object object;
        block13 : {
            Object object2;
            object = null;
            try {
                object = class_.newInstance();
                if (object == null) {
                    throw new IllegalArgumentException("class cannot be null");
                }
            }
            catch (IllegalAccessException | InstantiationException var5_5) {
                var5_5.printStackTrace();
            }
            DatabaseTable databaseTable = object.getClass().getAnnotation(DatabaseTable.class);
            if (databaseTable == null) {
                throw new IllegalArgumentException("class has not a table annotation");
            }
            String string3 = databaseTable.value();
            HashMap<String, String> hashMap = new HashMap<String, String>();
            for (Field object32 : object.getClass().getDeclaredFields()) {
                try {
                    object2 = object32.getAnnotation(DatabaseField.class);
                    if (object2 == null) continue;
                    hashMap.put(object32.getName(), object2.value());
                    continue;
                }
                catch (Exception var12_19) {
                    throw new RuntimeException("Failed to load config value for field '" + object32.getName() + "' in " + object.getClass(), var12_19);
                }
            }
            Object object4 = "SELECT " + TextUtils.join((CharSequence)", ", hashMap.values()) + " FROM " + string3;
            if (string != null) {
                object4 = (String)object4 + " WHERE " + string;
            }
            if (string2 != null) {
                object4 = (String)object4 + " " + string2;
            }
            ResultSet resultSet = this.query((String)object4, new Object[0]);
            try {
                if (resultSet.next()) {
                    for (Map.Entry entry : hashMap.entrySet()) {
                        object2 = object.getClass().getDeclaredField((String)entry.getKey());
                        object2.setAccessible(true);
                        String string4 = (String)entry.getValue();
                        this.parseField(object, (Field)object2, resultSet, string4);
                    }
                    break block13;
                }
                return null;
            }
            catch (IllegalAccessException | IllegalArgumentException | NoSuchFieldException | SecurityException | SQLException var10_14) {
                var10_14.printStackTrace();
            }
        }
        return (T)object;
    }

    public <T> List<T> loadAll(Class<T> class_, String string, String string2) {
        Object object = null;
        try {
            object = class_.newInstance();
        }
        catch (IllegalAccessException | InstantiationException var5_5) {
            var5_5.printStackTrace();
        }
        if (object == null) {
            throw new IllegalArgumentException("class cannot be null");
        }
        Class class_2 = object.getClass();
        DatabaseTable databaseTable = class_2.getAnnotation(DatabaseTable.class);
        if (databaseTable == null) {
            throw new IllegalArgumentException("class has not a table annotation");
        }
        String string3 = databaseTable.value();
        HashMap<String, String> hashMap = new HashMap<String, String>();
        for (Field object22 : class_2.getDeclaredFields()) {
            try {
                DatabaseField databaseField = object22.getAnnotation(DatabaseField.class);
                if (databaseField == null) continue;
                object22.setAccessible(true);
                hashMap.put(object22.getName(), databaseField.value());
                continue;
            }
            catch (Exception var13_20) {
                throw new RuntimeException("Failed to load config value for field '" + object22.getName() + "' in " + class_2, var13_20);
            }
        }
        Object object3 = "SELECT " + TextUtils.join((CharSequence)", ", hashMap.values()) + " FROM " + string3;
        if (string != null) {
            object3 = (String)object3 + " WHERE " + string;
        }
        if (string2 != null) {
            object3 = (String)object3 + " " + string2;
        }
        ResultSet resultSet = this.query((String)object3, new Object[0]);
        ArrayList<T> arrayList = new ArrayList<T>();
        try {
            while (resultSet.next()) {
                T t = class_.newInstance();
                for (Map.Entry entry : hashMap.entrySet()) {
                    Field field = t.getClass().getDeclaredField((String)entry.getKey());
                    field.setAccessible(true);
                    String string4 = (String)entry.getValue();
                    this.parseField(t, field, resultSet, string4);
                }
                arrayList.add(t);
            }
        }
        catch (IllegalAccessException | IllegalArgumentException | InstantiationException | NoSuchFieldException | SecurityException | SQLException var12_18) {
            throw new RuntimeException("Failed to parse config value for field in " + object.getClass(), var12_18);
        }
        return arrayList;
    }

    public <T> boolean delete(T t) {
        if (t == null) {
            throw new IllegalArgumentException("class cannot be null");
        }
        DatabaseTable databaseTable = t.getClass().getAnnotation(DatabaseTable.class);
        if (databaseTable == null) {
            throw new IllegalArgumentException("class has not a table annotation");
        }
        String string = databaseTable.value();
        Field field = null;
        DatabaseField databaseField = null;
        for (Field field2 : t.getClass().getDeclaredFields()) {
            try {
                DatabaseField databaseField2 = field2.getAnnotation(DatabaseField.class);
                if (databaseField2 == null || !databaseField2.id()) continue;
                field = field2;
                field.setAccessible(true);
                databaseField = databaseField2;
                break;
            }
            catch (Exception var10_12) {
                throw new RuntimeException("Failed to load config value for field '" + field2.getName() + "' in " + t.getClass(), var10_12);
            }
        }
        if (field == null) {
            throw new RuntimeException("Failed to delete the model '" + t.getClass() + "': class does not have a id");
        }
        try {
            String string2 = "DELETE FROM " + string + " WHERE " + databaseField.value() + " = " + field.getInt(t);
            this.updateFast(string2, new Object[0]);
            return true;
        }
        catch (IllegalAccessException | IllegalArgumentException | SecurityException var6_7) {
            var6_7.printStackTrace();
            return false;
        }
    }

    private <T> void parseField(T t, Field field, ResultSet resultSet, String string) {
        if (resultSet.getObject(string) == null) {
            field.set(t, null);
        }
        if (field.getType() == String.class) {
            if (resultSet.getString(string) != null) {
                field.set(t, resultSet.getString(string).replace("&", "\u00a7"));
            }
        } else if (field.getType() == Boolean.class || field.getType() == Boolean.TYPE) {
            field.set(t, resultSet.getBoolean(string));
        } else if (field.getType() == Integer.class || field.getType() == Integer.TYPE) {
            field.set(t, resultSet.getInt(string));
        } else if (field.getType() == Timestamp.class || field.getType() == Date.class) {
            field.set(t, resultSet.getTimestamp(string));
        } else if (field.getType() == Long.class || field.getType() == Long.TYPE) {
            field.set(t, resultSet.getLong(string));
        } else if (field.getType() == Double.class || field.getType() == Double.TYPE) {
            field.set(t, resultSet.getDouble(string));
        } else if (field.getType() == UUID.class) {
            try {
                field.set(t, UUID.fromString(resultSet.getString(string)));
            }
            catch (IllegalArgumentException var5_5) {
                field.set(t, null);
            }
        } else if (field.getType() == List.class) {
            field.set(t, new ArrayList<String>(Arrays.asList(resultSet.getString(string).split("::"))));
        } else if (field.getType() == Sound.class) {
            Sound sound = null;
            if (resultSet.getString(string) != null && !resultSet.getString(string).equals("")) {
                sound = Sound.valueOf((String)resultSet.getString(string).toUpperCase());
            }
            field.set(t, (Object)sound);
        } else if (field.getType() == Inventory.class) {
            String string2 = resultSet.getString(string);
            if (string2 == null || string2.equals("")) {
                field.set(t, "");
            } else {
                field.set(t, (Object)InventorySerialization.getInventory(resultSet.getString(string)));
            }
        } else if (field.getType() == Location.class) {
            String[] arrstring = resultSet.getString(string).split(",");
            Location location = new Location(Bukkit.getServer().getWorld(arrstring[0].trim()), Double.parseDouble(arrstring[1].trim()), Double.parseDouble(arrstring[2].trim()), Double.parseDouble(arrstring[3].trim()));
            if (arrstring.length > 4) {
                location.setPitch(Float.parseFloat(arrstring[4].trim()));
                location.setYaw(Float.parseFloat(arrstring[5].trim()));
            }
            field.set(t, (Object)location);
        }
    }

    private <T> Object parseField(T t, Field field) {
        Object object = field.get(t);
        if (object == null) {
            return null;
        }
        if (field.getType() == String.class) {
            return object.toString();
        }
        if (field.getType() == Boolean.class || field.getType() == Boolean.TYPE) {
            return object.toString();
        }
        if (field.getType() == Integer.class || field.getType() == Integer.TYPE) {
            return object.toString();
        }
        if (field.getType() == Date.class) {
            return new Timestamp(((Date)object).getTime());
        }
        if (field.getType() == Long.class || field.getType() == Long.TYPE) {
            return object.toString();
        }
        if (field.getType() == Double.class || field.getType() == Double.TYPE) {
            return object.toString();
        }
        if (field.getType() == UUID.class) {
            return object.toString();
        }
        if (field.getType() == List.class) {
            return TextUtils.join((CharSequence)"::", (List)object);
        }
        if (field.getType() == Sound.class) {
            return ((Sound)object).toString();
        }
        if (field.getType() == ItemStack.class) {
            return SingleItemSerialization.serializeItemAsString((ItemStack)object);
        }
        if (field.getType() == Inventory.class) {
            return InventorySerialization.serializeInventory((Inventory)object);
        }
        if (field.getType() == Location.class) {
            Location location = (Location)object;
            return location.getWorld().getName() + "," + location.getBlockX() + "," + location.getBlockY() + "," + location.getBlockZ() + "," + location.getPitch() + "," + location.getYaw();
        }
        return object.toString();
    }

    private String getFieldType(Field field) {
        if (field.getType() == String.class) {
            return "VARCHAR (255)";
        }
        if (field.getType() == Boolean.class || field.getType() == Boolean.TYPE) {
            return "INT (5)";
        }
        if (field.getType() == Integer.class || field.getType() == Integer.TYPE) {
            return "INT (11)";
        }
        if (field.getType() == Date.class) {
            return "TIMESTAMP";
        }
        if (field.getType() == Long.class || field.getType() == Long.TYPE) {
            return "INT (11)";
        }
        if (field.getType() == Double.class || field.getType() == Double.TYPE) {
            return "DOUBLE";
        }
        if (field.getType() == UUID.class) {
            return "VARCHAR (255)";
        }
        if (field.getType() == List.class) {
            return "TEXT";
        }
        if (field.getType() == Sound.class) {
            return "VARCHAR (255)";
        }
        if (field.getType() == ItemStack.class) {
            return "TEXT";
        }
        if (field.getType() == Inventory.class) {
            return "TEXT";
        }
        if (field.getType() == Location.class) {
            return "VARCHAR (255)";
        }
        return "VARCHAR (255)";
    }

    public Logger getLogger() {
        return this.logger;
    }

}

