/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.lib.database;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value={ElementType.METHOD, ElementType.FIELD})
@Retention(value=RetentionPolicy.RUNTIME)
public @interface DatabaseField {
    public String value();

    public boolean id() default 0;

    public boolean autoincrement() default 0;

    public String type() default "";

    public String defaultValue() default "";

    public boolean now() default 0;

    public boolean nullField() default 0;

    public boolean unique() default 0;
}

