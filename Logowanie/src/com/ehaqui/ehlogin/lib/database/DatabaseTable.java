/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.lib.database;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value={ElementType.TYPE})
@Retention(value=RetentionPolicy.RUNTIME)
public @interface DatabaseTable {
    public String value();
}

