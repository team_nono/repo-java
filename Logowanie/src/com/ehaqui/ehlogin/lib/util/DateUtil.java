/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.lib.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DateUtil {
    public static long parseDateDiff(String string) {
        Pattern pattern = Pattern.compile("(?:([0-9]+)\\s*y[a-z]*[,\\s]*)?(?:([0-9]+)\\s*mo[a-z]*[,\\s]*)?(?:([0-9]+)\\s*w[a-z]*[,\\s]*)?(?:([0-9]+)\\s*d[a-z]*[,\\s]*)?(?:([0-9]+)\\s*h[a-z]*[,\\s]*)?(?:([0-9]+)\\s*m[a-z]*[,\\s]*)?(?:([0-9]+)\\s*(?:s[a-z]*)?)?", 2);
        Matcher matcher = pattern.matcher(string);
        long l = 0;
        boolean bl = false;
        while (matcher.find()) {
            if (matcher.group() == null || matcher.group().isEmpty()) continue;
            for (int i = 0; i < matcher.groupCount(); ++i) {
                if (matcher.group(i) == null || matcher.group(i).isEmpty()) continue;
                bl = true;
                break;
            }
            if (!bl) continue;
            if (matcher.group(1) != null && !matcher.group(1).isEmpty()) {
                l += (long)(31556926 * Integer.parseInt(matcher.group(1)));
            }
            if (matcher.group(2) != null && !matcher.group(2).isEmpty()) {
                l += (long)(2629743 * Integer.parseInt(matcher.group(2)));
            }
            if (matcher.group(3) != null && !matcher.group(3).isEmpty()) {
                l += (long)(604800 * Integer.parseInt(matcher.group(3)));
            }
            if (matcher.group(4) != null && !matcher.group(4).isEmpty()) {
                l += (long)(86400 * Integer.parseInt(matcher.group(4)));
            }
            if (matcher.group(5) != null && !matcher.group(5).isEmpty()) {
                l += (long)(3600 * Integer.parseInt(matcher.group(5)));
            }
            if (matcher.group(6) != null && !matcher.group(6).isEmpty()) {
                l += (long)(60 * Integer.parseInt(matcher.group(6)));
            }
            if (matcher.group(7) == null || matcher.group(7).isEmpty()) continue;
            l += (long)Integer.parseInt(matcher.group(7));
        }
        if (!bl) {
            return -1;
        }
        return l * 1000;
    }
}

