/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.Material
 *  org.bukkit.inventory.ItemFlag
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 *  org.bukkit.inventory.meta.SkullMeta
 */
package com.ehaqui.ehlogin.lib.util;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

public class ItemNamer {
    private List<String> lore = new ArrayList<String>();
    private String name = null;
    private Material material = Material.AIR;

    public void addLore(String string) {
        this.lore.add(ChatColor.translateAlternateColorCodes((char)'&', (String)("&r" + string)));
    }

    public /* varargs */ void addLore(String string, Object ... arrobject) {
        this.lore.add(ChatColor.translateAlternateColorCodes((char)'&', (String)("&r" + String.format(string, arrobject))));
    }

    public void addLore(List<String> list) {
        for (String string : list) {
            this.lore.add(ChatColor.translateAlternateColorCodes((char)'&', (String)("&r" + string)));
        }
    }

    public void setName(String string) {
        this.name = string;
    }

    public void setNameOriginal(String string) {
        this.name = string;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public List<String> getLore() {
        return this.lore;
    }

    public ItemStack setOwnerHead(String string, ItemStack itemStack) {
        SkullMeta skullMeta = (SkullMeta)itemStack.getItemMeta();
        skullMeta.setOwner(string);
        itemStack.setItemMeta((ItemMeta)skullMeta);
        return itemStack;
    }

    public String getName() {
        return this.name;
    }

    public Material getMaterial() {
        return this.material;
    }

    public ItemStack setLore(ItemStack itemStack) {
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setLore(this.getLore());
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    public ItemStack setDisplayName(ItemStack itemStack) {
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(this.getName());
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    public ItemStack getItem(ItemStack itemStack) {
        if (this.material != Material.AIR) {
            itemStack.setType(this.getMaterial());
        }
        ItemMeta itemMeta = itemStack.getItemMeta();
        if (this.name != null) {
            itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes((char)'&', (String)this.getName()));
        }
        if (this.lore.size() > 0) {
            itemMeta.setLore(this.getLore());
        }
        itemMeta.addItemFlags(new ItemFlag[]{ItemFlag.HIDE_ATTRIBUTES});
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    public ItemStack getItem() {
        ItemStack itemStack = new ItemStack(Material.AIR);
        return this.getItem(itemStack);
    }
}

