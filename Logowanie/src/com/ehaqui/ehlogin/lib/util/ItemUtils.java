/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.mojang.authlib.GameProfile
 *  com.mojang.authlib.properties.Property
 *  com.mojang.authlib.properties.PropertyMap
 *  org.bukkit.ChatColor
 *  org.bukkit.Color
 *  org.bukkit.Material
 *  org.bukkit.enchantments.Enchantment
 *  org.bukkit.entity.Player
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.PlayerInventory
 *  org.bukkit.inventory.meta.ItemMeta
 *  org.bukkit.inventory.meta.LeatherArmorMeta
 *  org.bukkit.potion.PotionEffect
 *  org.bukkit.potion.PotionEffectType
 */
package com.ehaqui.ehlogin.lib.util;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import com.mojang.authlib.properties.PropertyMap;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class ItemUtils {
    public static ItemStack getSkull(String string) {
        ItemStack itemStack = new ItemStack(Material.SKULL_ITEM, 1, 3);
        ItemMeta itemMeta = itemStack.getItemMeta();
        GameProfile gameProfile = new GameProfile(UUID.randomUUID(), null);
        gameProfile.getProperties().put((Object)"textures", (Object)new Property("textures", string));
        Field field = null;
        try {
            field = itemMeta.getClass().getDeclaredField("profile");
        }
        catch (NoSuchFieldException var5_5) {
            var5_5.printStackTrace();
        }
        catch (SecurityException var5_6) {
            var5_6.printStackTrace();
        }
        field.setAccessible(true);
        try {
            field.set((Object)itemMeta, (Object)gameProfile);
        }
        catch (IllegalArgumentException var5_8) {
            var5_8.printStackTrace();
        }
        catch (IllegalAccessException var5_9) {
            var5_9.printStackTrace();
        }
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    public static ItemStack parseItem(String string) {
        List<String> list = Arrays.asList(string.split(" "));
        if (list.size() < 2) {
            String[] arrstring;
            return new ItemStack(Material.getMaterial((String)arrstring[0].toUpperCase()), 1, (short)((arrstring = list.get(0).split(":")).length == 2 ? Integer.parseInt(arrstring[1]) : 0));
        }
        ItemStack itemStack = null;
        try {
            if (list.get(0).contains(":")) {
                Material material = Material.getMaterial((String)list.get(0).split(":")[0].toUpperCase());
                int n = Integer.parseInt(list.get(1));
                if (n < 1) {
                    return null;
                }
                short s = (short)Integer.parseInt(list.get(0).split(":")[1].toUpperCase());
                itemStack = new ItemStack(material, n, s);
            } else {
                itemStack = new ItemStack(Material.getMaterial((String)list.get(0).toUpperCase()), Integer.parseInt(list.get(1)));
            }
            if (list.size() > 2) {
                for (int i = 2; i < list.size(); ++i) {
                    if (list.get(i).split(":")[0].equalsIgnoreCase("name")) {
                        ItemMeta itemMeta = itemStack.getItemMeta();
                        itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes((char)'&', (String)list.get(i).split(":")[1].replaceAll("_", " ")));
                        itemStack.setItemMeta(itemMeta);
                        continue;
                    }
                    if (list.get(i).split(":")[0].equalsIgnoreCase("color")) {
                        LeatherArmorMeta leatherArmorMeta = (LeatherArmorMeta)itemStack.getItemMeta();
                        leatherArmorMeta.setColor(ItemUtils.getColor(list.get(i).split(":")[1].toUpperCase()));
                        itemStack.setItemMeta((ItemMeta)leatherArmorMeta);
                        continue;
                    }
                    itemStack.addUnsafeEnchantment(ItemUtils.getEnchant(list.get(i).split(":")[0]), Integer.parseInt(list.get(i).split(":")[1]));
                }
            }
        }
        catch (Exception var3_6) {
            // empty catch block
        }
        return itemStack;
    }

    public static PotionEffect parseEffect(List<String> list) {
        if (list.size() < 2) {
            return null;
        }
        PotionEffect potionEffect = null;
        try {
            PotionEffectType potionEffectType = ItemUtils.getPotionType(list.get(0));
            int n = Integer.parseInt(list.get(1)) == -1 ? Integer.MAX_VALUE : 20 * Integer.parseInt(list.get(1));
            int n2 = Integer.parseInt(list.get(2));
            potionEffect = new PotionEffect(potionEffectType, n, n2);
        }
        catch (Exception var2_4) {
            // empty catch block
        }
        return potionEffect;
    }

    private static PotionEffectType getPotionType(String string) {
        switch (string.toLowerCase()) {
            case "speed": {
                return PotionEffectType.SPEED;
            }
            case "slowness": {
                return PotionEffectType.SLOW;
            }
            case "haste": {
                return PotionEffectType.FAST_DIGGING;
            }
            case "miningfatique": {
                return PotionEffectType.SLOW_DIGGING;
            }
            case "strength": {
                return PotionEffectType.INCREASE_DAMAGE;
            }
            case "instanthealth": {
                return PotionEffectType.HEAL;
            }
            case "instantdamage": {
                return PotionEffectType.HARM;
            }
            case "jumpboost": {
                return PotionEffectType.JUMP;
            }
            case "nausea": {
                return PotionEffectType.CONFUSION;
            }
            case "regeneration": {
                return PotionEffectType.REGENERATION;
            }
            case "resistance": {
                return PotionEffectType.DAMAGE_RESISTANCE;
            }
            case "fireresistance": {
                return PotionEffectType.FIRE_RESISTANCE;
            }
            case "waterbreathing": {
                return PotionEffectType.WATER_BREATHING;
            }
            case "invisibility": {
                return PotionEffectType.INVISIBILITY;
            }
            case "blindness": {
                return PotionEffectType.BLINDNESS;
            }
            case "nightvision": {
                return PotionEffectType.NIGHT_VISION;
            }
            case "hunger": {
                return PotionEffectType.HUNGER;
            }
            case "weakness": {
                return PotionEffectType.WEAKNESS;
            }
            case "poison": {
                return PotionEffectType.POISON;
            }
            case "wither": {
                return PotionEffectType.WITHER;
            }
            case "healthboost": {
                return PotionEffectType.HEALTH_BOOST;
            }
            case "absorption": {
                return PotionEffectType.ABSORPTION;
            }
            case "saturation": {
                return PotionEffectType.SATURATION;
            }
        }
        return null;
    }

    private static Enchantment getEnchant(String string) {
        switch (string.toLowerCase()) {
            case "protection": {
                return Enchantment.PROTECTION_ENVIRONMENTAL;
            }
            case "projectileprotection": {
                return Enchantment.PROTECTION_PROJECTILE;
            }
            case "fireprotection": {
                return Enchantment.PROTECTION_FIRE;
            }
            case "featherfall": {
                return Enchantment.PROTECTION_FALL;
            }
            case "blastprotection": {
                return Enchantment.PROTECTION_EXPLOSIONS;
            }
            case "respiration": {
                return Enchantment.OXYGEN;
            }
            case "aquaaffinity": {
                return Enchantment.WATER_WORKER;
            }
            case "sharpness": {
                return Enchantment.DAMAGE_ALL;
            }
            case "smite": {
                return Enchantment.DAMAGE_UNDEAD;
            }
            case "baneofarthropods": {
                return Enchantment.DAMAGE_ARTHROPODS;
            }
            case "knockback": {
                return Enchantment.KNOCKBACK;
            }
            case "fireaspect": {
                return Enchantment.FIRE_ASPECT;
            }
            case "depthstrider": {
                return Enchantment.DEPTH_STRIDER;
            }
            case "looting": {
                return Enchantment.LOOT_BONUS_MOBS;
            }
            case "power": {
                return Enchantment.ARROW_DAMAGE;
            }
            case "punch": {
                return Enchantment.ARROW_KNOCKBACK;
            }
            case "flame": {
                return Enchantment.ARROW_FIRE;
            }
            case "infinity": {
                return Enchantment.ARROW_INFINITE;
            }
            case "efficiency": {
                return Enchantment.DIG_SPEED;
            }
            case "silktouch": {
                return Enchantment.SILK_TOUCH;
            }
            case "unbreaking": {
                return Enchantment.DURABILITY;
            }
            case "fortune": {
                return Enchantment.LOOT_BONUS_BLOCKS;
            }
            case "luckofthesea": {
                return Enchantment.LUCK;
            }
            case "luck": {
                return Enchantment.LUCK;
            }
            case "lure": {
                return Enchantment.LURE;
            }
            case "thorns": {
                return Enchantment.THORNS;
            }
        }
        return null;
    }

    public static Color getColor(String string) {
        String string2 = string;
        if (string2.equalsIgnoreCase("AQUA")) {
            return Color.AQUA;
        }
        if (string2.equalsIgnoreCase("BLACK")) {
            return Color.BLACK;
        }
        if (string2.equalsIgnoreCase("BLUE")) {
            return Color.BLUE;
        }
        if (string2.equalsIgnoreCase("FUCHSIA")) {
            return Color.FUCHSIA;
        }
        if (string2.equalsIgnoreCase("GRAY")) {
            return Color.GRAY;
        }
        if (string2.equalsIgnoreCase("GREEN")) {
            return Color.GREEN;
        }
        if (string2.equalsIgnoreCase("LIME")) {
            return Color.LIME;
        }
        if (string2.equalsIgnoreCase("MAROON")) {
            return Color.MAROON;
        }
        if (string2.equalsIgnoreCase("NAVY")) {
            return Color.NAVY;
        }
        if (string2.equalsIgnoreCase("OLIVE")) {
            return Color.OLIVE;
        }
        if (string2.equalsIgnoreCase("ORANGE")) {
            return Color.ORANGE;
        }
        if (string2.equalsIgnoreCase("PURPLE")) {
            return Color.PURPLE;
        }
        if (string2.equalsIgnoreCase("RED")) {
            return Color.RED;
        }
        if (string2.equalsIgnoreCase("SILVER")) {
            return Color.SILVER;
        }
        if (string2.equalsIgnoreCase("TEAL")) {
            return Color.TEAL;
        }
        if (string2.equalsIgnoreCase("WHITE")) {
            return Color.WHITE;
        }
        if (string2.equalsIgnoreCase("YELLOW")) {
            return Color.YELLOW;
        }
        return null;
    }

    public static boolean isEnchanted(ItemStack itemStack) {
        if (itemStack.containsEnchantment(Enchantment.ARROW_DAMAGE)) {
            return true;
        }
        if (itemStack.containsEnchantment(Enchantment.ARROW_DAMAGE)) {
            return true;
        }
        if (itemStack.containsEnchantment(Enchantment.ARROW_FIRE)) {
            return true;
        }
        if (itemStack.containsEnchantment(Enchantment.ARROW_INFINITE)) {
            return true;
        }
        if (itemStack.containsEnchantment(Enchantment.ARROW_KNOCKBACK)) {
            return true;
        }
        if (itemStack.containsEnchantment(Enchantment.DAMAGE_ALL)) {
            return true;
        }
        if (itemStack.containsEnchantment(Enchantment.DAMAGE_ARTHROPODS)) {
            return true;
        }
        if (itemStack.containsEnchantment(Enchantment.DAMAGE_UNDEAD)) {
            return true;
        }
        if (itemStack.containsEnchantment(Enchantment.DIG_SPEED)) {
            return true;
        }
        if (itemStack.containsEnchantment(Enchantment.DURABILITY)) {
            return true;
        }
        if (itemStack.containsEnchantment(Enchantment.FIRE_ASPECT)) {
            return true;
        }
        if (itemStack.containsEnchantment(Enchantment.KNOCKBACK)) {
            return true;
        }
        if (itemStack.containsEnchantment(Enchantment.LOOT_BONUS_BLOCKS)) {
            return true;
        }
        if (itemStack.containsEnchantment(Enchantment.LOOT_BONUS_MOBS)) {
            return true;
        }
        if (itemStack.containsEnchantment(Enchantment.LUCK)) {
            return true;
        }
        if (itemStack.containsEnchantment(Enchantment.LURE)) {
            return true;
        }
        if (itemStack.containsEnchantment(Enchantment.OXYGEN)) {
            return true;
        }
        if (itemStack.containsEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL)) {
            return true;
        }
        if (itemStack.containsEnchantment(Enchantment.PROTECTION_EXPLOSIONS)) {
            return true;
        }
        if (itemStack.containsEnchantment(Enchantment.PROTECTION_FALL)) {
            return true;
        }
        if (itemStack.containsEnchantment(Enchantment.PROTECTION_FIRE)) {
            return true;
        }
        if (itemStack.containsEnchantment(Enchantment.PROTECTION_PROJECTILE)) {
            return true;
        }
        if (itemStack.containsEnchantment(Enchantment.SILK_TOUCH)) {
            return true;
        }
        if (itemStack.containsEnchantment(Enchantment.THORNS)) {
            return true;
        }
        if (itemStack.containsEnchantment(Enchantment.WATER_WORKER)) {
            return true;
        }
        return false;
    }

    public static /* varargs */ ItemStack name(ItemStack itemStack, String string, String ... arrstring) {
        ItemMeta itemMeta = itemStack.getItemMeta();
        if (!string.isEmpty()) {
            itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes((char)'&', (String)string));
        }
        if (arrstring.length > 0) {
            ArrayList<String> arrayList = new ArrayList<String>(arrstring.length);
            for (String string2 : arrstring) {
                arrayList.add(ChatColor.translateAlternateColorCodes((char)'&', (String)string2));
            }
            itemMeta.setLore(arrayList);
        }
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    public static int countTotalItens(Player player) {
        return ItemUtils.countTotalItens((Inventory)player.getInventory());
    }

    public static int countTotalItens(Inventory inventory) {
        int n = 0;
        for (ItemStack itemStack : inventory.getContents()) {
            if (itemStack == null) continue;
            ++n;
        }
        return n;
    }

    public static int getEmptySlots(Player player) {
        return ItemUtils.getEmptySlots((Inventory)player.getInventory());
    }

    public static int getEmptySlots(Inventory inventory) {
        int n = 0;
        for (ItemStack itemStack : inventory.getContents()) {
            if (itemStack != null) continue;
            ++n;
        }
        return n;
    }
}

