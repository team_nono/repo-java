/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.lib.util;

import java.lang.reflect.Field;

public class Reflection {
    public static Object getStaticValue(String string, String string2) {
        Field field = Class.forName(string).getDeclaredField(string2);
        field.setAccessible(true);
        return field.get(Class.forName(string));
    }

    public static void setStaticValue(String string, String string2, Object object) {
        Field field = Class.forName(string).getDeclaredField(string2);
        field.setAccessible(true);
        Object object2 = field.get(Class.forName(string));
        field.set(object2, object);
    }

    public static Object getInstanceValue(Object object, String string) {
        Field field = object.getClass().getDeclaredField(string);
        field.setAccessible(true);
        return field.get(object);
    }

    public static void setInstanceValue(Object object, String string, Object object2) {
        Field field = object.getClass().getDeclaredField(string);
        field.setAccessible(true);
        field.set(object, object2);
    }
}

