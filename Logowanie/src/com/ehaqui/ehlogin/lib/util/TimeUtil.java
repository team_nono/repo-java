/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.lib.util;

import java.util.Calendar;
import java.util.Date;

public class TimeUtil {
    private static String _day = "dia ";
    private static String _hour = "hora ";
    private static String _minute = "minuto ";
    private static String _seccond = "segundo ";
    private static String _days = "dias ";
    private static String _hours = "horas ";
    private static String _minutes = "minutos ";
    private static String _secconds = "segundos ";

    public static String formatTimeReaming(String string, long l) {
        if (l > 0) {
            int n = 0;
            int n2 = 0;
            int n3 = 0;
            int n4 = 0;
            for (long i = l - System.currentTimeMillis(); i >= 1000; i -= 1000) {
                ++n4;
            }
            while (n4 >= 60) {
                ++n3;
                n4 -= 60;
            }
            while (n3 >= 60) {
                ++n2;
                n3 -= 60;
            }
            while (n2 >= 24) {
                ++n;
                n2 -= 24;
            }
            return TimeUtil.timeFormat(string, n, n2, n3, n4);
        }
        return null;
    }

    public static String formatTime(int n) {
        if (n > 0) {
            int n2;
            int n3 = 0;
            int n4 = 0;
            int n5 = 0;
            for (n2 = n; n2 >= 60; n2 -= 60) {
                ++n3;
            }
            while (n3 >= 60) {
                ++n4;
                n3 -= 60;
            }
            while (n4 >= 24) {
                ++n5;
                n4 -= 24;
            }
            StringBuilder stringBuilder = new StringBuilder();
            if (n5 > 0) {
                stringBuilder.append("{DAYS} " + (n5 > 1 ? _days : _day));
            }
            if (n4 > 0) {
                stringBuilder.append("{HOURS} " + (n4 > 1 ? _hours : _hour));
            }
            if (n3 > 0) {
                stringBuilder.append("{MINUTES} " + (n3 > 1 ? _minutes : _minute));
            }
            if (n2 > 0) {
                stringBuilder.append("{SECONDS} " + (n2 > 1 ? _secconds : _seccond));
            }
            return TimeUtil.timeFormat(stringBuilder.toString(), n5, n4, n3, n2);
        }
        return null;
    }

    public static String timeFormat(String string, int n, int n2, int n3, int n4) {
        return string.replace("{DAYS}", "" + n).replace("{HOURS}", "" + n2).replace("{MINUTES}", "" + n3).replace("{SECONDS}", "" + n4);
    }

    public static Date addDate(Date date, String string, int n) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date.getTime());
        switch (string) {
            case "seconds": {
                calendar.add(13, n);
                break;
            }
            case "minutes": {
                calendar.add(12, n);
                break;
            }
            case "hours": {
                calendar.add(10, n);
                break;
            }
            case "days": {
                calendar.add(5, n);
                break;
            }
            case "weeks": {
                calendar.add(4, n);
                break;
            }
            case "months": {
                calendar.add(2, n);
                break;
            }
            case "years": {
                calendar.add(1, n);
            }
        }
        return calendar.getTime();
    }

    public static double round(double d, int n) {
        if (n < 0) {
            throw new IllegalArgumentException();
        }
        long l = (long)Math.pow(10.0, n);
        long l2 = Math.round(d *= (double)l);
        return (double)l2 / (double)l;
    }

    public static long getDiff(Long l) {
        return l - System.currentTimeMillis();
    }

    public static boolean isFuture(long l) {
        return l - System.currentTimeMillis() > 0;
    }

    public static boolean isPass(long l) {
        return l - System.currentTimeMillis() < 0;
    }
}

