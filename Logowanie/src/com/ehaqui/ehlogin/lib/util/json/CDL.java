/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.lib.util.json;

import com.ehaqui.ehlogin.lib.util.json.JSONArray;
import com.ehaqui.ehlogin.lib.util.json.JSONException;
import com.ehaqui.ehlogin.lib.util.json.JSONObject;
import com.ehaqui.ehlogin.lib.util.json.JSONTokener;

public class CDL {
    private static String getValue(JSONTokener jSONTokener) {
        char c;
        while ((c = jSONTokener.next()) == ' ' || c == '\t') {
        }
        switch (c) {
            case '\u0000': {
                return null;
            }
            case '\"': 
            case '\'': {
                char c2 = c;
                StringBuffer stringBuffer = new StringBuffer();
                while ((c = jSONTokener.next()) != c2) {
                    if (c == '\u0000' || c == '\n' || c == '\r') {
                        throw jSONTokener.syntaxError("Missing close quote '" + c2 + "'.");
                    }
                    stringBuffer.append(c);
                }
                return stringBuffer.toString();
            }
            case ',': {
                jSONTokener.back();
                return "";
            }
        }
        jSONTokener.back();
        return jSONTokener.nextTo(',');
    }

    public static JSONArray rowToJSONArray(JSONTokener jSONTokener) {
        JSONArray jSONArray = new JSONArray();
        block0 : do {
            String string = CDL.getValue(jSONTokener);
            char c = jSONTokener.next();
            if (string == null || jSONArray.length() == 0 && string.length() == 0 && c != ',') {
                return null;
            }
            jSONArray.put(string);
            do {
                if (c == ',') continue block0;
                if (c != ' ') {
                    if (c == '\n' || c == '\r' || c == '\u0000') {
                        return jSONArray;
                    }
                    throw jSONTokener.syntaxError("Bad character '" + c + "' (" + c + ").");
                }
                c = jSONTokener.next();
            } while (true);
            break;
        } while (true);
    }

    public static JSONObject rowToJSONObject(JSONArray jSONArray, JSONTokener jSONTokener) {
        JSONArray jSONArray2 = CDL.rowToJSONArray(jSONTokener);
        return jSONArray2 != null ? jSONArray2.toJSONObject(jSONArray) : null;
    }

    public static String rowToString(JSONArray jSONArray) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < jSONArray.length(); ++i) {
            Object object;
            if (i > 0) {
                stringBuffer.append(',');
            }
            if ((object = jSONArray.opt(i)) == null) continue;
            String string = object.toString();
            if (string.length() > 0 && (string.indexOf(44) >= 0 || string.indexOf(10) >= 0 || string.indexOf(13) >= 0 || string.indexOf(0) >= 0 || string.charAt(0) == '\"')) {
                stringBuffer.append('\"');
                int n = string.length();
                for (int j = 0; j < n; ++j) {
                    char c = string.charAt(j);
                    if (c < ' ' || c == '\"') continue;
                    stringBuffer.append(c);
                }
                stringBuffer.append('\"');
                continue;
            }
            stringBuffer.append(string);
        }
        stringBuffer.append('\n');
        return stringBuffer.toString();
    }

    public static JSONArray toJSONArray(String string) {
        return CDL.toJSONArray(new JSONTokener(string));
    }

    public static JSONArray toJSONArray(JSONTokener jSONTokener) {
        return CDL.toJSONArray(CDL.rowToJSONArray(jSONTokener), jSONTokener);
    }

    public static JSONArray toJSONArray(JSONArray jSONArray, String string) {
        return CDL.toJSONArray(jSONArray, new JSONTokener(string));
    }

    public static JSONArray toJSONArray(JSONArray jSONArray, JSONTokener jSONTokener) {
        JSONObject jSONObject;
        if (jSONArray == null || jSONArray.length() == 0) {
            return null;
        }
        JSONArray jSONArray2 = new JSONArray();
        while ((jSONObject = CDL.rowToJSONObject(jSONArray, jSONTokener)) != null) {
            jSONArray2.put(jSONObject);
        }
        if (jSONArray2.length() == 0) {
            return null;
        }
        return jSONArray2;
    }

    public static String toString(JSONArray jSONArray) {
        JSONArray jSONArray2;
        JSONObject jSONObject = jSONArray.optJSONObject(0);
        if (jSONObject != null && (jSONArray2 = jSONObject.names()) != null) {
            return CDL.rowToString(jSONArray2) + CDL.toString(jSONArray2, jSONArray);
        }
        return null;
    }

    public static String toString(JSONArray jSONArray, JSONArray jSONArray2) {
        if (jSONArray == null || jSONArray.length() == 0) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < jSONArray2.length(); ++i) {
            JSONObject jSONObject = jSONArray2.optJSONObject(i);
            if (jSONObject == null) continue;
            stringBuffer.append(CDL.rowToString(jSONObject.toJSONArray(jSONArray)));
        }
        return stringBuffer.toString();
    }
}

