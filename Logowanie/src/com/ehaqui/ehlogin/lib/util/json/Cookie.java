/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.lib.util.json;

import com.ehaqui.ehlogin.lib.util.json.JSONException;
import com.ehaqui.ehlogin.lib.util.json.JSONObject;
import com.ehaqui.ehlogin.lib.util.json.JSONTokener;

public class Cookie {
    public static String escape(String string) {
        String string2 = string.trim();
        StringBuffer stringBuffer = new StringBuffer();
        int n = string2.length();
        for (int i = 0; i < n; ++i) {
            char c = string2.charAt(i);
            if (c < ' ' || c == '+' || c == '%' || c == '=' || c == ';') {
                stringBuffer.append('%');
                stringBuffer.append(Character.forDigit((char)(c >>> 4 & 15), 16));
                stringBuffer.append(Character.forDigit((char)(c & 15), 16));
                continue;
            }
            stringBuffer.append(c);
        }
        return stringBuffer.toString();
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public static JSONObject toJSONObject(String string) {
        JSONObject jSONObject = new JSONObject();
        JSONTokener jSONTokener = new JSONTokener(string);
        jSONObject.put("name", jSONTokener.nextTo('='));
        jSONTokener.next('=');
        jSONObject.put("value", jSONTokener.nextTo(';'));
        jSONTokener.next();
        while (jSONTokener.more()) {
            Object object;
            String string2 = Cookie.unescape(jSONTokener.nextTo("=;"));
            if (jSONTokener.next() != '=') {
                if (!string2.equals("secure")) throw jSONTokener.syntaxError("Missing '=' in cookie parameter.");
                object = Boolean.TRUE;
            } else {
                object = Cookie.unescape(jSONTokener.nextTo(';'));
                jSONTokener.next();
            }
            jSONObject.put(string2, object);
        }
        return jSONObject;
    }

    public static String toString(JSONObject jSONObject) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(Cookie.escape(jSONObject.getString("name")));
        stringBuffer.append("=");
        stringBuffer.append(Cookie.escape(jSONObject.getString("value")));
        if (jSONObject.has("expires")) {
            stringBuffer.append(";expires=");
            stringBuffer.append(jSONObject.getString("expires"));
        }
        if (jSONObject.has("domain")) {
            stringBuffer.append(";domain=");
            stringBuffer.append(Cookie.escape(jSONObject.getString("domain")));
        }
        if (jSONObject.has("path")) {
            stringBuffer.append(";path=");
            stringBuffer.append(Cookie.escape(jSONObject.getString("path")));
        }
        if (jSONObject.optBoolean("secure")) {
            stringBuffer.append(";secure");
        }
        return stringBuffer.toString();
    }

    public static String unescape(String string) {
        int n = string.length();
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < n; ++i) {
            char c = string.charAt(i);
            if (c == '+') {
                c = ' ';
            } else if (c == '%' && i + 2 < n) {
                int n2 = JSONTokener.dehexchar(string.charAt(i + 1));
                int n3 = JSONTokener.dehexchar(string.charAt(i + 2));
                if (n2 >= 0 && n3 >= 0) {
                    c = (char)(n2 * 16 + n3);
                    i += 2;
                }
            }
            stringBuffer.append(c);
        }
        return stringBuffer.toString();
    }
}

