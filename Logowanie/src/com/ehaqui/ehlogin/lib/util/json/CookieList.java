/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.lib.util.json;

import com.ehaqui.ehlogin.lib.util.json.Cookie;
import com.ehaqui.ehlogin.lib.util.json.JSONObject;
import com.ehaqui.ehlogin.lib.util.json.JSONTokener;
import java.util.Iterator;

public class CookieList {
    public static JSONObject toJSONObject(String string) {
        JSONObject jSONObject = new JSONObject();
        JSONTokener jSONTokener = new JSONTokener(string);
        while (jSONTokener.more()) {
            String string2 = Cookie.unescape(jSONTokener.nextTo('='));
            jSONTokener.next('=');
            jSONObject.put(string2, Cookie.unescape(jSONTokener.nextTo(';')));
            jSONTokener.next();
        }
        return jSONObject;
    }

    public static String toString(JSONObject jSONObject) {
        boolean bl = false;
        Iterator iterator = jSONObject.keys();
        StringBuffer stringBuffer = new StringBuffer();
        while (iterator.hasNext()) {
            String string = iterator.next().toString();
            if (jSONObject.isNull(string)) continue;
            if (bl) {
                stringBuffer.append(';');
            }
            stringBuffer.append(Cookie.escape(string));
            stringBuffer.append("=");
            stringBuffer.append(Cookie.escape(jSONObject.getString(string)));
            bl = true;
        }
        return stringBuffer.toString();
    }
}

