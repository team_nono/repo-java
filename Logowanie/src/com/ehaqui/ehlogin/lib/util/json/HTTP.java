/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.lib.util.json;

import com.ehaqui.ehlogin.lib.util.json.HTTPTokener;
import com.ehaqui.ehlogin.lib.util.json.JSONException;
import com.ehaqui.ehlogin.lib.util.json.JSONObject;
import java.util.Iterator;

public class HTTP {
    public static final String CRLF = "\r\n";

    public static JSONObject toJSONObject(String string) {
        JSONObject jSONObject = new JSONObject();
        HTTPTokener hTTPTokener = new HTTPTokener(string);
        String string2 = hTTPTokener.nextToken();
        if (string2.toUpperCase().startsWith("HTTP")) {
            jSONObject.put("HTTP-Version", string2);
            jSONObject.put("Status-Code", hTTPTokener.nextToken());
            jSONObject.put("Reason-Phrase", hTTPTokener.nextTo('\u0000'));
            hTTPTokener.next();
        } else {
            jSONObject.put("Method", string2);
            jSONObject.put("Request-URI", hTTPTokener.nextToken());
            jSONObject.put("HTTP-Version", hTTPTokener.nextToken());
        }
        while (hTTPTokener.more()) {
            String string3 = hTTPTokener.nextTo(':');
            hTTPTokener.next(':');
            jSONObject.put(string3, hTTPTokener.nextTo('\u0000'));
            hTTPTokener.next();
        }
        return jSONObject;
    }

    public static String toString(JSONObject jSONObject) {
        Iterator iterator = jSONObject.keys();
        StringBuffer stringBuffer = new StringBuffer();
        if (jSONObject.has("Status-Code") && jSONObject.has("Reason-Phrase")) {
            stringBuffer.append(jSONObject.getString("HTTP-Version"));
            stringBuffer.append(' ');
            stringBuffer.append(jSONObject.getString("Status-Code"));
            stringBuffer.append(' ');
            stringBuffer.append(jSONObject.getString("Reason-Phrase"));
        } else if (jSONObject.has("Method") && jSONObject.has("Request-URI")) {
            stringBuffer.append(jSONObject.getString("Method"));
            stringBuffer.append(' ');
            stringBuffer.append('\"');
            stringBuffer.append(jSONObject.getString("Request-URI"));
            stringBuffer.append('\"');
            stringBuffer.append(' ');
            stringBuffer.append(jSONObject.getString("HTTP-Version"));
        } else {
            throw new JSONException("Not enough material for an HTTP header.");
        }
        stringBuffer.append("\r\n");
        while (iterator.hasNext()) {
            String string = iterator.next().toString();
            if ("HTTP-Version".equals(string) || "Status-Code".equals(string) || "Reason-Phrase".equals(string) || "Method".equals(string) || "Request-URI".equals(string) || jSONObject.isNull(string)) continue;
            stringBuffer.append(string);
            stringBuffer.append(": ");
            stringBuffer.append(jSONObject.getString(string));
            stringBuffer.append("\r\n");
        }
        stringBuffer.append("\r\n");
        return stringBuffer.toString();
    }
}

