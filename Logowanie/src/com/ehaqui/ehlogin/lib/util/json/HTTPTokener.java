/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.lib.util.json;

import com.ehaqui.ehlogin.lib.util.json.JSONException;
import com.ehaqui.ehlogin.lib.util.json.JSONTokener;

public class HTTPTokener
extends JSONTokener {
    public HTTPTokener(String string) {
        super(string);
    }

    public String nextToken() {
        char c;
        StringBuffer stringBuffer = new StringBuffer();
        while (Character.isWhitespace(c = this.next())) {
        }
        if (c == '\"' || c == '\'') {
            char c2 = c;
            do {
                if ((c = this.next()) < ' ') {
                    throw this.syntaxError("Unterminated string.");
                }
                if (c == c2) {
                    return stringBuffer.toString();
                }
                stringBuffer.append(c);
            } while (true);
        }
        while (c != '\u0000' && !Character.isWhitespace(c)) {
            stringBuffer.append(c);
            c = this.next();
        }
        return stringBuffer.toString();
    }
}

