/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.lib.util.json;

import com.ehaqui.ehlogin.lib.util.json.JSONException;
import com.ehaqui.ehlogin.lib.util.json.JSONObject;
import com.ehaqui.ehlogin.lib.util.json.JSONTokener;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

public class JSONArray {
    private final ArrayList myArrayList = new ArrayList();

    public JSONArray() {
    }

    public JSONArray(JSONTokener jSONTokener) {
        this();
        if (jSONTokener.nextClean() != '[') {
            throw jSONTokener.syntaxError("A JSONArray text must start with '['");
        }
        if (jSONTokener.nextClean() != ']') {
            jSONTokener.back();
            block4 : do {
                if (jSONTokener.nextClean() == ',') {
                    jSONTokener.back();
                    this.myArrayList.add(JSONObject.NULL);
                } else {
                    jSONTokener.back();
                    this.myArrayList.add(jSONTokener.nextValue());
                }
                switch (jSONTokener.nextClean()) {
                    case ',': 
                    case ';': {
                        if (jSONTokener.nextClean() == ']') {
                            return;
                        }
                        jSONTokener.back();
                        continue block4;
                    }
                    case ']': {
                        return;
                    }
                }
                break;
            } while (true);
            throw jSONTokener.syntaxError("Expected a ',' or ']'");
        }
    }

    public JSONArray(String string) {
        this(new JSONTokener(string));
    }

    public JSONArray(Collection collection) {
        if (collection != null) {
            Iterator iterator = collection.iterator();
            while (iterator.hasNext()) {
                this.myArrayList.add(JSONObject.wrap(iterator.next()));
            }
        }
    }

    public JSONArray(Object object) {
        this();
        if (object.getClass().isArray()) {
            int n = Array.getLength(object);
            for (int i = 0; i < n; ++i) {
                this.put(JSONObject.wrap(Array.get(object, i)));
            }
        } else {
            throw new JSONException("JSONArray initial value should be a string or collection or array.");
        }
    }

    public Object get(int n) {
        Object object = this.opt(n);
        if (object == null) {
            throw new JSONException("JSONArray[" + n + "] not found.");
        }
        return object;
    }

    public boolean getBoolean(int n) {
        Object object = this.get(n);
        if (object.equals(Boolean.FALSE) || object instanceof String && ((String)object).equalsIgnoreCase("false")) {
            return false;
        }
        if (object.equals(Boolean.TRUE) || object instanceof String && ((String)object).equalsIgnoreCase("true")) {
            return true;
        }
        throw new JSONException("JSONArray[" + n + "] is not a boolean.");
    }

    public double getDouble(int n) {
        Object object = this.get(n);
        try {
            return object instanceof Number ? ((Number)object).doubleValue() : Double.parseDouble((String)object);
        }
        catch (Exception var3_3) {
            throw new JSONException("JSONArray[" + n + "] is not a number.");
        }
    }

    public int getInt(int n) {
        Object object = this.get(n);
        try {
            return object instanceof Number ? ((Number)object).intValue() : Integer.parseInt((String)object);
        }
        catch (Exception var3_3) {
            throw new JSONException("JSONArray[" + n + "] is not a number.");
        }
    }

    public JSONArray getJSONArray(int n) {
        Object object = this.get(n);
        if (object instanceof JSONArray) {
            return (JSONArray)object;
        }
        throw new JSONException("JSONArray[" + n + "] is not a JSONArray.");
    }

    public JSONObject getJSONObject(int n) {
        Object object = this.get(n);
        if (object instanceof JSONObject) {
            return (JSONObject)object;
        }
        throw new JSONException("JSONArray[" + n + "] is not a JSONObject.");
    }

    public long getLong(int n) {
        Object object = this.get(n);
        try {
            return object instanceof Number ? ((Number)object).longValue() : Long.parseLong((String)object);
        }
        catch (Exception var3_3) {
            throw new JSONException("JSONArray[" + n + "] is not a number.");
        }
    }

    public String getString(int n) {
        Object object = this.get(n);
        if (object instanceof String) {
            return (String)object;
        }
        throw new JSONException("JSONArray[" + n + "] not a string.");
    }

    public boolean isNull(int n) {
        return JSONObject.NULL.equals(this.opt(n));
    }

    public String join(String string) {
        int n = this.length();
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < n; ++i) {
            if (i > 0) {
                stringBuffer.append(string);
            }
            stringBuffer.append(JSONObject.valueToString(this.myArrayList.get(i)));
        }
        return stringBuffer.toString();
    }

    public int length() {
        return this.myArrayList.size();
    }

    public int size() {
        return this.myArrayList.size();
    }

    public Object opt(int n) {
        return n < 0 || n >= this.length() ? null : this.myArrayList.get(n);
    }

    public boolean optBoolean(int n) {
        return this.optBoolean(n, false);
    }

    public boolean optBoolean(int n, boolean bl) {
        try {
            return this.getBoolean(n);
        }
        catch (Exception var3_3) {
            return bl;
        }
    }

    public double optDouble(int n) {
        return this.optDouble(n, Double.NaN);
    }

    public double optDouble(int n, double d) {
        try {
            return this.getDouble(n);
        }
        catch (Exception var4_3) {
            return d;
        }
    }

    public int optInt(int n) {
        return this.optInt(n, 0);
    }

    public int optInt(int n, int n2) {
        try {
            return this.getInt(n);
        }
        catch (Exception var3_3) {
            return n2;
        }
    }

    public JSONArray optJSONArray(int n) {
        Object object = this.opt(n);
        return object instanceof JSONArray ? (JSONArray)object : null;
    }

    public JSONObject optJSONObject(int n) {
        Object object = this.opt(n);
        return object instanceof JSONObject ? (JSONObject)object : null;
    }

    public long optLong(int n) {
        return this.optLong(n, 0);
    }

    public long optLong(int n, long l) {
        try {
            return this.getLong(n);
        }
        catch (Exception var4_3) {
            return l;
        }
    }

    public String optString(int n) {
        return this.optString(n, "");
    }

    public String optString(int n, String string) {
        Object object = this.opt(n);
        return JSONObject.NULL.equals(object) ? string : object.toString();
    }

    public JSONArray put(boolean bl) {
        this.put(bl ? Boolean.TRUE : Boolean.FALSE);
        return this;
    }

    public JSONArray put(Collection collection) {
        this.put(new JSONArray(collection));
        return this;
    }

    public JSONArray put(double d) {
        Double d2 = new Double(d);
        JSONObject.testValidity(d2);
        this.put(d2);
        return this;
    }

    public JSONArray put(int n) {
        this.put(new Integer(n));
        return this;
    }

    public JSONArray put(long l) {
        this.put(new Long(l));
        return this;
    }

    public JSONArray put(Map map) {
        this.put(new JSONObject(map));
        return this;
    }

    public JSONArray put(Object object) {
        this.myArrayList.add(object);
        return this;
    }

    public JSONArray put(int n, boolean bl) {
        this.put(n, bl ? Boolean.TRUE : Boolean.FALSE);
        return this;
    }

    public JSONArray put(int n, Collection collection) {
        this.put(n, new JSONArray(collection));
        return this;
    }

    public JSONArray put(int n, double d) {
        this.put(n, new Double(d));
        return this;
    }

    public JSONArray put(int n, int n2) {
        this.put(n, new Integer(n2));
        return this;
    }

    public JSONArray put(int n, long l) {
        this.put(n, new Long(l));
        return this;
    }

    public JSONArray put(int n, Map map) {
        this.put(n, new JSONObject(map));
        return this;
    }

    public JSONArray put(int n, Object object) {
        JSONObject.testValidity(object);
        if (n < 0) {
            throw new JSONException("JSONArray[" + n + "] not found.");
        }
        if (n < this.length()) {
            this.myArrayList.set(n, object);
        } else {
            while (n != this.length()) {
                this.put(JSONObject.NULL);
            }
            this.put(object);
        }
        return this;
    }

    public Object remove(int n) {
        Object object = this.opt(n);
        this.myArrayList.remove(n);
        return object;
    }

    public JSONObject toJSONObject(JSONArray jSONArray) {
        if (jSONArray == null || jSONArray.length() == 0 || this.length() == 0) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        for (int i = 0; i < jSONArray.length(); ++i) {
            jSONObject.put(jSONArray.getString(i), this.opt(i));
        }
        return jSONObject;
    }

    public String toString() {
        try {
            return this.toString(0);
        }
        catch (Exception var1_1) {
            return null;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public String toString(int n) {
        StringWriter stringWriter = new StringWriter();
        StringBuffer stringBuffer = stringWriter.getBuffer();
        synchronized (stringBuffer) {
            return this.write(stringWriter, n, 0).toString();
        }
    }

    public Writer write(Writer writer) {
        return this.write(writer, 0, 0);
    }

    Writer write(Writer writer, int n, int n2) {
        try {
            boolean bl = false;
            int n3 = this.length();
            writer.write(91);
            if (n3 == 1) {
                JSONObject.writeValue(writer, this.myArrayList.get(0), n, n2);
            } else if (n3 != 0) {
                int n4 = n2 + n;
                for (int i = 0; i < n3; ++i) {
                    if (bl) {
                        writer.write(44);
                    }
                    if (n > 0) {
                        writer.write(10);
                    }
                    JSONObject.indent(writer, n4);
                    JSONObject.writeValue(writer, this.myArrayList.get(i), n, n4);
                    bl = true;
                }
                if (n > 0) {
                    writer.write(10);
                }
                JSONObject.indent(writer, n2);
            }
            writer.write(93);
            return writer;
        }
        catch (IOException var4_5) {
            throw new JSONException(var4_5);
        }
    }
}

