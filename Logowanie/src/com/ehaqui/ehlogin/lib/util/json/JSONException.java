/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.lib.util.json;

public class JSONException
extends Exception {
    private static final long serialVersionUID = 0;
    private Throwable cause;

    public JSONException(String string) {
        super(string);
    }

    public JSONException(Throwable throwable) {
        super(throwable.getMessage());
        this.cause = throwable;
    }

    @Override
    public Throwable getCause() {
        return this.cause;
    }
}

