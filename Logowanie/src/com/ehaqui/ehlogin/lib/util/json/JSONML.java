/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.lib.util.json;

import com.ehaqui.ehlogin.lib.util.json.JSONArray;
import com.ehaqui.ehlogin.lib.util.json.JSONException;
import com.ehaqui.ehlogin.lib.util.json.JSONObject;
import com.ehaqui.ehlogin.lib.util.json.XML;
import com.ehaqui.ehlogin.lib.util.json.XMLTokener;
import java.util.Iterator;

public class JSONML {
    private static Object parse(XMLTokener xMLTokener, boolean bl, JSONArray jSONArray) {
        String string = null;
        JSONArray jSONArray2 = null;
        JSONObject jSONObject = null;
        String string2 = null;
        do {
            if (!xMLTokener.more()) {
                throw xMLTokener.syntaxError("Bad XML");
            }
            Object object = xMLTokener.nextContent();
            if (object == XML.LT) {
                object = xMLTokener.nextToken();
                if (object instanceof Character) {
                    if (object == XML.SLASH) {
                        object = xMLTokener.nextToken();
                        if (!(object instanceof String)) {
                            throw new JSONException("Expected a closing name instead of '" + object + "'.");
                        }
                        if (xMLTokener.nextToken() != XML.GT) {
                            throw xMLTokener.syntaxError("Misshaped close tag");
                        }
                        return object;
                    }
                    if (object == XML.BANG) {
                        char c = xMLTokener.next();
                        if (c == '-') {
                            if (xMLTokener.next() == '-') {
                                xMLTokener.skipPast("-->");
                                continue;
                            }
                            xMLTokener.back();
                            continue;
                        }
                        if (c == '[') {
                            object = xMLTokener.nextToken();
                            if (object.equals("CDATA") && xMLTokener.next() == '[') {
                                if (jSONArray == null) continue;
                                jSONArray.put(xMLTokener.nextCDATA());
                                continue;
                            }
                            throw xMLTokener.syntaxError("Expected 'CDATA['");
                        }
                        int n = 1;
                        do {
                            if ((object = xMLTokener.nextMeta()) == null) {
                                throw xMLTokener.syntaxError("Missing '>' after '<!'.");
                            }
                            if (object == XML.LT) {
                                ++n;
                                continue;
                            }
                            if (object != XML.GT) continue;
                            --n;
                        } while (n > 0);
                        continue;
                    }
                    if (object == XML.QUEST) {
                        xMLTokener.skipPast("?>");
                        continue;
                    }
                    throw xMLTokener.syntaxError("Misshaped tag");
                }
                if (!(object instanceof String)) {
                    throw xMLTokener.syntaxError("Bad tagName '" + object + "'.");
                }
                string2 = (String)object;
                jSONArray2 = new JSONArray();
                jSONObject = new JSONObject();
                if (bl) {
                    jSONArray2.put(string2);
                    if (jSONArray != null) {
                        jSONArray.put(jSONArray2);
                    }
                } else {
                    jSONObject.put("tagName", string2);
                    if (jSONArray != null) {
                        jSONArray.put(jSONObject);
                    }
                }
                object = null;
                do {
                    if (object == null) {
                        object = xMLTokener.nextToken();
                    }
                    if (object == null) {
                        throw xMLTokener.syntaxError("Misshaped tag");
                    }
                    if (!(object instanceof String)) break;
                    String string3 = (String)object;
                    if (!bl && ("tagName".equals(string3) || "childNode".equals(string3))) {
                        throw xMLTokener.syntaxError("Reserved attribute.");
                    }
                    object = xMLTokener.nextToken();
                    if (object == XML.EQ) {
                        object = xMLTokener.nextToken();
                        if (!(object instanceof String)) {
                            throw xMLTokener.syntaxError("Missing value");
                        }
                        jSONObject.accumulate(string3, XML.stringToValue((String)object));
                        object = null;
                        continue;
                    }
                    jSONObject.accumulate(string3, "");
                } while (true);
                if (bl && jSONObject.length() > 0) {
                    jSONArray2.put(jSONObject);
                }
                if (object == XML.SLASH) {
                    if (xMLTokener.nextToken() != XML.GT) {
                        throw xMLTokener.syntaxError("Misshaped tag");
                    }
                    if (jSONArray != null) continue;
                    if (bl) {
                        return jSONArray2;
                    }
                    return jSONObject;
                }
                if (object != XML.GT) {
                    throw xMLTokener.syntaxError("Misshaped tag");
                }
                string = (String)JSONML.parse(xMLTokener, bl, jSONArray2);
                if (string == null) continue;
                if (!string.equals(string2)) {
                    throw xMLTokener.syntaxError("Mismatched '" + string2 + "' and '" + string + "'");
                }
                string2 = null;
                if (!bl && jSONArray2.length() > 0) {
                    jSONObject.put("childNodes", jSONArray2);
                }
                if (jSONArray != null) continue;
                if (bl) {
                    return jSONArray2;
                }
                return jSONObject;
            }
            if (jSONArray == null) continue;
            jSONArray.put(object instanceof String ? XML.stringToValue((String)object) : object);
        } while (true);
    }

    public static JSONArray toJSONArray(String string) {
        return JSONML.toJSONArray(new XMLTokener(string));
    }

    public static JSONArray toJSONArray(XMLTokener xMLTokener) {
        return (JSONArray)JSONML.parse(xMLTokener, true, null);
    }

    public static JSONObject toJSONObject(XMLTokener xMLTokener) {
        return (JSONObject)JSONML.parse(xMLTokener, false, null);
    }

    public static JSONObject toJSONObject(String string) {
        return JSONML.toJSONObject(new XMLTokener(string));
    }

    public static String toString(JSONArray jSONArray) {
        int n;
        int n2;
        StringBuffer stringBuffer = new StringBuffer();
        String string = jSONArray.getString(0);
        XML.noSpace(string);
        string = XML.escape(string);
        stringBuffer.append('<');
        stringBuffer.append(string);
        Object object = jSONArray.opt(1);
        if (object instanceof JSONObject) {
            n2 = 2;
            JSONObject jSONObject = (JSONObject)object;
            Iterator iterator = jSONObject.keys();
            while (iterator.hasNext()) {
                String string2 = iterator.next().toString();
                XML.noSpace(string2);
                String string3 = jSONObject.optString(string2);
                if (string3 == null) continue;
                stringBuffer.append(' ');
                stringBuffer.append(XML.escape(string2));
                stringBuffer.append('=');
                stringBuffer.append('\"');
                stringBuffer.append(XML.escape(string3));
                stringBuffer.append('\"');
            }
        } else {
            n2 = 1;
        }
        if (n2 >= (n = jSONArray.length())) {
            stringBuffer.append('/');
            stringBuffer.append('>');
        } else {
            stringBuffer.append('>');
            do {
                object = jSONArray.get(n2);
                ++n2;
                if (object == null) continue;
                if (object instanceof String) {
                    stringBuffer.append(XML.escape(object.toString()));
                    continue;
                }
                if (object instanceof JSONObject) {
                    stringBuffer.append(JSONML.toString((JSONObject)object));
                    continue;
                }
                if (!(object instanceof JSONArray)) continue;
                stringBuffer.append(JSONML.toString((JSONArray)object));
            } while (n2 < n);
            stringBuffer.append('<');
            stringBuffer.append('/');
            stringBuffer.append(string);
            stringBuffer.append('>');
        }
        return stringBuffer.toString();
    }

    public static String toString(JSONObject jSONObject) {
        StringBuffer stringBuffer = new StringBuffer();
        String string = jSONObject.optString("tagName");
        if (string == null) {
            return XML.escape(jSONObject.toString());
        }
        XML.noSpace(string);
        string = XML.escape(string);
        stringBuffer.append('<');
        stringBuffer.append(string);
        Iterator iterator = jSONObject.keys();
        while (iterator.hasNext()) {
            String string2 = iterator.next().toString();
            if ("tagName".equals(string2) || "childNodes".equals(string2)) continue;
            XML.noSpace(string2);
            String string3 = jSONObject.optString(string2);
            if (string3 == null) continue;
            stringBuffer.append(' ');
            stringBuffer.append(XML.escape(string2));
            stringBuffer.append('=');
            stringBuffer.append('\"');
            stringBuffer.append(XML.escape(string3));
            stringBuffer.append('\"');
        }
        JSONArray jSONArray = jSONObject.optJSONArray("childNodes");
        if (jSONArray == null) {
            stringBuffer.append('/');
            stringBuffer.append('>');
        } else {
            stringBuffer.append('>');
            int n = jSONArray.length();
            for (int i = 0; i < n; ++i) {
                Object object = jSONArray.get(i);
                if (object == null) continue;
                if (object instanceof String) {
                    stringBuffer.append(XML.escape(object.toString()));
                    continue;
                }
                if (object instanceof JSONObject) {
                    stringBuffer.append(JSONML.toString((JSONObject)object));
                    continue;
                }
                if (object instanceof JSONArray) {
                    stringBuffer.append(JSONML.toString((JSONArray)object));
                    continue;
                }
                stringBuffer.append(object.toString());
            }
            stringBuffer.append('<');
            stringBuffer.append('/');
            stringBuffer.append(string);
            stringBuffer.append('>');
        }
        return stringBuffer.toString();
    }
}

