/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.lib.util.json;

import com.ehaqui.ehlogin.lib.util.json.JSONArray;
import com.ehaqui.ehlogin.lib.util.json.JSONException;
import com.ehaqui.ehlogin.lib.util.json.JSONString;
import com.ehaqui.ehlogin.lib.util.json.JSONTokener;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

public class JSONObject {
    private static final int keyPoolSize = 100;
    private static HashMap keyPool = new HashMap(100);
    private final Map map = new HashMap();
    public static final Object NULL = new Null();

    public JSONObject() {
    }

    public JSONObject(JSONObject jSONObject, String[] arrstring) {
        this();
        for (int i = 0; i < arrstring.length; ++i) {
            try {
                this.putOnce(arrstring[i], jSONObject.opt(arrstring[i]));
                continue;
            }
            catch (Exception var4_4) {
                // empty catch block
            }
        }
    }

    public JSONObject(JSONTokener jSONTokener) {
        this();
        if (jSONTokener.nextClean() != '{') {
            throw jSONTokener.syntaxError("A JSONObject text must begin with '{'");
        }
        block8 : do {
            char c = jSONTokener.nextClean();
            switch (c) {
                case '\u0000': {
                    throw jSONTokener.syntaxError("A JSONObject text must end with '}'");
                }
                case '}': {
                    return;
                }
            }
            jSONTokener.back();
            String string = jSONTokener.nextValue().toString();
            c = jSONTokener.nextClean();
            if (c == '=') {
                if (jSONTokener.next() != '>') {
                    jSONTokener.back();
                }
            } else if (c != ':') {
                throw jSONTokener.syntaxError("Expected a ':' after a key");
            }
            this.putOnce(string, jSONTokener.nextValue());
            switch (jSONTokener.nextClean()) {
                case ',': 
                case ';': {
                    if (jSONTokener.nextClean() == '}') {
                        return;
                    }
                    jSONTokener.back();
                    continue block8;
                }
                case '}': {
                    return;
                }
            }
            break;
        } while (true);
        throw jSONTokener.syntaxError("Expected a ',' or '}'");
    }

    public JSONObject(Map map) {
        if (map != null) {
            for (Map.Entry entry : map.entrySet()) {
                Object v = entry.getValue();
                if (v == null) continue;
                this.map.put(entry.getKey(), JSONObject.wrap(v));
            }
        }
    }

    public JSONObject(Object object) {
        this();
        this.populateMap(object);
    }

    public JSONObject(Object object, String[] arrstring) {
        this();
        Class class_ = object.getClass();
        for (int i = 0; i < arrstring.length; ++i) {
            String string = arrstring[i];
            try {
                this.putOpt(string, class_.getField(string).get(object));
                continue;
            }
            catch (Exception var6_6) {
                // empty catch block
            }
        }
    }

    public JSONObject(String string) {
        this(new JSONTokener(string));
    }

    public JSONObject(String string, Locale locale) {
        this();
        ResourceBundle resourceBundle = ResourceBundle.getBundle(string, locale, Thread.currentThread().getContextClassLoader());
        Enumeration<String> enumeration = resourceBundle.getKeys();
        while (enumeration.hasMoreElements()) {
            String string2 = enumeration.nextElement();
            if (!(string2 instanceof String)) continue;
            String[] arrstring = string2.split("\\.");
            int n = arrstring.length - 1;
            JSONObject jSONObject = this;
            for (int i = 0; i < n; ++i) {
                String string3 = arrstring[i];
                JSONObject jSONObject2 = jSONObject.optJSONObject(string3);
                if (jSONObject2 == null) {
                    jSONObject2 = new JSONObject();
                    jSONObject.put(string3, jSONObject2);
                }
                jSONObject = jSONObject2;
            }
            jSONObject.put(arrstring[n], resourceBundle.getString(string2));
        }
    }

    public JSONObject accumulate(String string, Object object) {
        JSONObject.testValidity(object);
        Object object2 = this.opt(string);
        if (object2 == null) {
            this.put(string, object instanceof JSONArray ? new JSONArray().put(object) : object);
        } else if (object2 instanceof JSONArray) {
            ((JSONArray)object2).put(object);
        } else {
            this.put(string, new JSONArray().put(object2).put(object));
        }
        return this;
    }

    public JSONObject append(String string, Object object) {
        JSONObject.testValidity(object);
        Object object2 = this.opt(string);
        if (object2 == null) {
            this.put(string, new JSONArray().put(object));
        } else if (object2 instanceof JSONArray) {
            this.put(string, ((JSONArray)object2).put(object));
        } else {
            throw new JSONException("JSONObject[" + string + "] is not a JSONArray.");
        }
        return this;
    }

    public static String doubleToString(double d) {
        if (Double.isInfinite(d) || Double.isNaN(d)) {
            return "null";
        }
        String string = Double.toString(d);
        if (string.indexOf(46) > 0 && string.indexOf(101) < 0 && string.indexOf(69) < 0) {
            while (string.endsWith("0")) {
                string = string.substring(0, string.length() - 1);
            }
            if (string.endsWith(".")) {
                string = string.substring(0, string.length() - 1);
            }
        }
        return string;
    }

    public Object get(String string) {
        if (string == null) {
            throw new JSONException("Null key.");
        }
        Object object = this.opt(string);
        if (object == null) {
            throw new JSONException("JSONObject[" + JSONObject.quote(string) + "] not found.");
        }
        return object;
    }

    public boolean getBoolean(String string) {
        Object object = this.get(string);
        if (object.equals(Boolean.FALSE) || object instanceof String && ((String)object).equalsIgnoreCase("false")) {
            return false;
        }
        if (object.equals(Boolean.TRUE) || object instanceof String && ((String)object).equalsIgnoreCase("true")) {
            return true;
        }
        throw new JSONException("JSONObject[" + JSONObject.quote(string) + "] is not a Boolean.");
    }

    public double getDouble(String string) {
        Object object = this.get(string);
        try {
            return object instanceof Number ? ((Number)object).doubleValue() : Double.parseDouble((String)object);
        }
        catch (Exception var3_3) {
            throw new JSONException("JSONObject[" + JSONObject.quote(string) + "] is not a number.");
        }
    }

    public int getInt(String string) {
        Object object = this.get(string);
        try {
            return object instanceof Number ? ((Number)object).intValue() : Integer.parseInt((String)object);
        }
        catch (Exception var3_3) {
            throw new JSONException("JSONObject[" + JSONObject.quote(string) + "] is not an int.");
        }
    }

    public JSONArray getJSONArray(String string) {
        Object object = this.get(string);
        if (object instanceof JSONArray) {
            return (JSONArray)object;
        }
        throw new JSONException("JSONObject[" + JSONObject.quote(string) + "] is not a JSONArray.");
    }

    public JSONObject getJSONObject(String string) {
        Object object = this.get(string);
        if (object instanceof JSONObject) {
            return (JSONObject)object;
        }
        throw new JSONException("JSONObject[" + JSONObject.quote(string) + "] is not a JSONObject.");
    }

    public long getLong(String string) {
        Object object = this.get(string);
        try {
            return object instanceof Number ? ((Number)object).longValue() : Long.parseLong((String)object);
        }
        catch (Exception var3_3) {
            throw new JSONException("JSONObject[" + JSONObject.quote(string) + "] is not a long.");
        }
    }

    public static String[] getNames(JSONObject jSONObject) {
        int n = jSONObject.length();
        if (n == 0) {
            return null;
        }
        Iterator iterator = jSONObject.keys();
        String[] arrstring = new String[n];
        int n2 = 0;
        while (iterator.hasNext()) {
            arrstring[n2] = (String)iterator.next();
            ++n2;
        }
        return arrstring;
    }

    public static String[] getNames(Object object) {
        if (object == null) {
            return null;
        }
        Class class_ = object.getClass();
        Field[] arrfield = class_.getFields();
        int n = arrfield.length;
        if (n == 0) {
            return null;
        }
        String[] arrstring = new String[n];
        for (int i = 0; i < n; ++i) {
            arrstring[i] = arrfield[i].getName();
        }
        return arrstring;
    }

    public String getString(String string) {
        Object object = this.get(string);
        if (object instanceof String) {
            return (String)object;
        }
        throw new JSONException("JSONObject[" + JSONObject.quote(string) + "] not a string.");
    }

    public boolean has(String string) {
        return this.map.containsKey(string);
    }

    public JSONObject increment(String string) {
        Object object = this.opt(string);
        if (object == null) {
            this.put(string, 1);
        } else if (object instanceof Integer) {
            this.put(string, (Integer)object + 1);
        } else if (object instanceof Long) {
            this.put(string, (Long)object + 1);
        } else if (object instanceof Double) {
            this.put(string, (Double)object + 1.0);
        } else if (object instanceof Float) {
            this.put(string, ((Float)object).floatValue() + 1.0f);
        } else {
            throw new JSONException("Unable to increment [" + JSONObject.quote(string) + "].");
        }
        return this;
    }

    public boolean isNull(String string) {
        return NULL.equals(this.opt(string));
    }

    public Iterator keys() {
        return this.keySet().iterator();
    }

    public Set keySet() {
        return this.map.keySet();
    }

    public int length() {
        return this.map.size();
    }

    public JSONArray names() {
        JSONArray jSONArray = new JSONArray();
        Iterator iterator = this.keys();
        while (iterator.hasNext()) {
            jSONArray.put(iterator.next());
        }
        return jSONArray.length() == 0 ? null : jSONArray;
    }

    public static String numberToString(Number number) {
        if (number == null) {
            throw new JSONException("Null pointer");
        }
        JSONObject.testValidity(number);
        String string = number.toString();
        if (string.indexOf(46) > 0 && string.indexOf(101) < 0 && string.indexOf(69) < 0) {
            while (string.endsWith("0")) {
                string = string.substring(0, string.length() - 1);
            }
            if (string.endsWith(".")) {
                string = string.substring(0, string.length() - 1);
            }
        }
        return string;
    }

    public Object opt(String string) {
        return string == null ? null : this.map.get(string);
    }

    public boolean optBoolean(String string) {
        return this.optBoolean(string, false);
    }

    public boolean optBoolean(String string, boolean bl) {
        try {
            return this.getBoolean(string);
        }
        catch (Exception var3_3) {
            return bl;
        }
    }

    public double optDouble(String string) {
        return this.optDouble(string, Double.NaN);
    }

    public double optDouble(String string, double d) {
        try {
            return this.getDouble(string);
        }
        catch (Exception var4_3) {
            return d;
        }
    }

    public int optInt(String string) {
        return this.optInt(string, 0);
    }

    public int optInt(String string, int n) {
        try {
            return this.getInt(string);
        }
        catch (Exception var3_3) {
            return n;
        }
    }

    public JSONArray optJSONArray(String string) {
        Object object = this.opt(string);
        return object instanceof JSONArray ? (JSONArray)object : null;
    }

    public JSONObject optJSONObject(String string) {
        Object object = this.opt(string);
        return object instanceof JSONObject ? (JSONObject)object : null;
    }

    public long optLong(String string) {
        return this.optLong(string, 0);
    }

    public long optLong(String string, long l) {
        try {
            return this.getLong(string);
        }
        catch (Exception var4_3) {
            return l;
        }
    }

    public String optString(String string) {
        return this.optString(string, "");
    }

    public String optString(String string, String string2) {
        Object object = this.opt(string);
        return NULL.equals(object) ? string2 : object.toString();
    }

    private void populateMap(Object object) {
        Class class_ = object.getClass();
        boolean bl = class_.getClassLoader() != null;
        Method[] arrmethod = bl ? class_.getMethods() : class_.getDeclaredMethods();
        for (int i = 0; i < arrmethod.length; ++i) {
            try {
                Method method = arrmethod[i];
                if (!Modifier.isPublic(method.getModifiers())) continue;
                String string = method.getName();
                String string2 = "";
                if (string.startsWith("get")) {
                    string2 = "getClass".equals(string) || "getDeclaringClass".equals(string) ? "" : string.substring(3);
                } else if (string.startsWith("is")) {
                    string2 = string.substring(2);
                }
                if (string2.length() <= 0 || !Character.isUpperCase(string2.charAt(0)) || method.getParameterTypes().length != 0) continue;
                if (string2.length() == 1) {
                    string2 = string2.toLowerCase();
                } else if (!Character.isUpperCase(string2.charAt(1))) {
                    string2 = string2.substring(0, 1).toLowerCase() + string2.substring(1);
                }
                Object object2 = method.invoke(object, null);
                if (object2 == null) continue;
                this.map.put(string2, JSONObject.wrap(object2));
                continue;
            }
            catch (Exception var6_7) {
                // empty catch block
            }
        }
    }

    public JSONObject put(String string, boolean bl) {
        this.put(string, bl ? Boolean.TRUE : Boolean.FALSE);
        return this;
    }

    public JSONObject put(String string, Collection collection) {
        this.put(string, new JSONArray(collection));
        return this;
    }

    public JSONObject put(String string, double d) {
        this.put(string, new Double(d));
        return this;
    }

    public JSONObject put(String string, int n) {
        this.put(string, new Integer(n));
        return this;
    }

    public JSONObject put(String string, long l) {
        this.put(string, new Long(l));
        return this;
    }

    public JSONObject put(String string, Map map) {
        this.put(string, new JSONObject(map));
        return this;
    }

    public JSONObject put(String string, Object object) {
        if (string == null) {
            throw new JSONException("Null key.");
        }
        if (object != null) {
            JSONObject.testValidity(object);
            String string2 = (String)keyPool.get(string);
            if (string2 == null) {
                if (keyPool.size() >= 100) {
                    keyPool = new HashMap(100);
                }
                keyPool.put(string, string);
            } else {
                string = string2;
            }
            this.map.put(string, object);
        } else {
            this.remove(string);
        }
        return this;
    }

    public JSONObject putOnce(String string, Object object) {
        if (string != null && object != null) {
            if (this.opt(string) != null) {
                throw new JSONException("Duplicate key \"" + string + "\"");
            }
            this.put(string, object);
        }
        return this;
    }

    public JSONObject putOpt(String string, Object object) {
        if (string != null && object != null) {
            this.put(string, object);
        }
        return this;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static String quote(String string) {
        StringWriter stringWriter = new StringWriter();
        StringBuffer stringBuffer = stringWriter.getBuffer();
        synchronized (stringBuffer) {
            try {
                return JSONObject.quote(string, stringWriter).toString();
            }
            catch (IOException var3_3) {
                return "";
            }
        }
    }

    public static Writer quote(String string, Writer writer) {
        if (string == null || string.length() == 0) {
            writer.write("\"\"");
            return writer;
        }
        char c = '\u0000';
        int n = string.length();
        writer.write(34);
        block9 : for (int i = 0; i < n; ++i) {
            char c2 = c;
            c = string.charAt(i);
            switch (c) {
                case '\"': 
                case '\\': {
                    writer.write(92);
                    writer.write(c);
                    continue block9;
                }
                case '/': {
                    if (c2 == '<') {
                        writer.write(92);
                    }
                    writer.write(c);
                    continue block9;
                }
                case '\b': {
                    writer.write("\\b");
                    continue block9;
                }
                case '\t': {
                    writer.write("\\t");
                    continue block9;
                }
                case '\n': {
                    writer.write("\\n");
                    continue block9;
                }
                case '\f': {
                    writer.write("\\f");
                    continue block9;
                }
                case '\r': {
                    writer.write("\\r");
                    continue block9;
                }
                default: {
                    if (c < ' ' || c >= '' && c < '\u00a0' || c >= '\u2000' && c < '\u2100') {
                        writer.write("\\u");
                        String string2 = Integer.toHexString(c);
                        writer.write("0000", 0, 4 - string2.length());
                        writer.write(string2);
                        continue block9;
                    }
                    writer.write(c);
                }
            }
        }
        writer.write(34);
        return writer;
    }

    public Object remove(String string) {
        return this.map.remove(string);
    }

    public static Object stringToValue(String string) {
        block10 : {
            if (string.equals("")) {
                return string;
            }
            if (string.equalsIgnoreCase("true")) {
                return Boolean.TRUE;
            }
            if (string.equalsIgnoreCase("false")) {
                return Boolean.FALSE;
            }
            if (string.equalsIgnoreCase("null")) {
                return NULL;
            }
            char c = string.charAt(0);
            if (c >= '0' && c <= '9' || c == '.' || c == '-' || c == '+') {
                try {
                    if (string.indexOf(46) > -1 || string.indexOf(101) > -1 || string.indexOf(69) > -1) {
                        Double d = Double.valueOf(string);
                        if (!d.isInfinite() && !d.isNaN()) {
                            return d;
                        }
                        break block10;
                    }
                    Long l = new Long(string);
                    if (l == (long)l.intValue()) {
                        return new Integer(l.intValue());
                    }
                    return l;
                }
                catch (Exception var3_4) {
                    // empty catch block
                }
            }
        }
        return string;
    }

    public static void testValidity(Object object) {
        if (object != null && (object instanceof Double ? ((Double)object).isInfinite() || ((Double)object).isNaN() : object instanceof Float && (((Float)object).isInfinite() || ((Float)object).isNaN()))) {
            throw new JSONException("JSON does not allow non-finite numbers.");
        }
    }

    public JSONArray toJSONArray(JSONArray jSONArray) {
        if (jSONArray == null || jSONArray.length() == 0) {
            return null;
        }
        JSONArray jSONArray2 = new JSONArray();
        for (int i = 0; i < jSONArray.length(); ++i) {
            jSONArray2.put(this.opt(jSONArray.getString(i)));
        }
        return jSONArray2;
    }

    public String toString() {
        try {
            return this.toString(0);
        }
        catch (Exception var1_1) {
            return null;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public String toString(int n) {
        StringWriter stringWriter = new StringWriter();
        StringBuffer stringBuffer = stringWriter.getBuffer();
        synchronized (stringBuffer) {
            return this.write(stringWriter, n, 0).toString();
        }
    }

    public static String valueToString(Object object) {
        if (object == null || object.equals(null)) {
            return "null";
        }
        if (object instanceof JSONString) {
            String string;
            try {
                string = ((JSONString)object).toJSONString();
            }
            catch (Exception var2_2) {
                throw new JSONException(var2_2);
            }
            if (string instanceof String) {
                return string;
            }
            throw new JSONException("Bad value from toJSONString: " + string);
        }
        if (object instanceof Number) {
            return JSONObject.numberToString((Number)object);
        }
        if (object instanceof Boolean || object instanceof JSONObject || object instanceof JSONArray) {
            return object.toString();
        }
        if (object instanceof Map) {
            return new JSONObject((Map)object).toString();
        }
        if (object instanceof Collection) {
            return new JSONArray((Collection)object).toString();
        }
        if (object.getClass().isArray()) {
            return new JSONArray(object).toString();
        }
        return JSONObject.quote(object.toString());
    }

    public static Object wrap(Object object) {
        try {
            String string;
            if (object == null) {
                return NULL;
            }
            if (object instanceof JSONObject || object instanceof JSONArray || NULL.equals(object) || object instanceof JSONString || object instanceof Byte || object instanceof Character || object instanceof Short || object instanceof Integer || object instanceof Long || object instanceof Boolean || object instanceof Float || object instanceof Double || object instanceof String) {
                return object;
            }
            if (object instanceof Collection) {
                return new JSONArray((Collection)object);
            }
            if (object.getClass().isArray()) {
                return new JSONArray(object);
            }
            if (object instanceof Map) {
                return new JSONObject((Map)object);
            }
            Package package_ = object.getClass().getPackage();
            String string2 = string = package_ != null ? package_.getName() : "";
            if (string.startsWith("java.") || string.startsWith("javax.") || object.getClass().getClassLoader() == null) {
                return object.toString();
            }
            return new JSONObject(object);
        }
        catch (Exception var1_2) {
            return null;
        }
    }

    public Writer write(Writer writer) {
        return this.write(writer, 0, 0);
    }

    static final Writer writeValue(Writer writer, Object object, int n, int n2) {
        if (object == null || object.equals(null)) {
            writer.write("null");
        } else if (object instanceof JSONObject) {
            ((JSONObject)object).write(writer, n, n2);
        } else if (object instanceof JSONArray) {
            ((JSONArray)object).write(writer, n, n2);
        } else if (object instanceof Map) {
            new JSONObject((Map)object).write(writer, n, n2);
        } else if (object instanceof Collection) {
            new JSONArray((Collection)object).write(writer, n, n2);
        } else if (object.getClass().isArray()) {
            new JSONArray(object).write(writer, n, n2);
        } else if (object instanceof Number) {
            writer.write(JSONObject.numberToString((Number)object));
        } else if (object instanceof Boolean) {
            writer.write(object.toString());
        } else if (object instanceof JSONString) {
            String string;
            try {
                string = ((JSONString)object).toJSONString();
            }
            catch (Exception var5_5) {
                throw new JSONException(var5_5);
            }
            writer.write(string != null ? string.toString() : JSONObject.quote(object.toString()));
        } else {
            JSONObject.quote(object.toString(), writer);
        }
        return writer;
    }

    static final void indent(Writer writer, int n) {
        for (int i = 0; i < n; ++i) {
            writer.write(32);
        }
    }

    Writer write(Writer writer, int n, int n2) {
        try {
            boolean bl = false;
            int n3 = this.length();
            Iterator iterator = this.keys();
            writer.write(123);
            if (n3 == 1) {
                Object e = iterator.next();
                writer.write(JSONObject.quote(e.toString()));
                writer.write(58);
                if (n > 0) {
                    writer.write(32);
                }
                JSONObject.writeValue(writer, this.map.get(e), n, n2);
            } else if (n3 != 0) {
                int n4 = n2 + n;
                while (iterator.hasNext()) {
                    Object e = iterator.next();
                    if (bl) {
                        writer.write(44);
                    }
                    if (n > 0) {
                        writer.write(10);
                    }
                    JSONObject.indent(writer, n4);
                    writer.write(JSONObject.quote(e.toString()));
                    writer.write(58);
                    if (n > 0) {
                        writer.write(32);
                    }
                    JSONObject.writeValue(writer, this.map.get(e), n, n4);
                    bl = true;
                }
                if (n > 0) {
                    writer.write(10);
                }
                JSONObject.indent(writer, n2);
            }
            writer.write(125);
            return writer;
        }
        catch (IOException var4_5) {
            throw new JSONException(var4_5);
        }
    }

    private static final class Null {
        private Null() {
        }

        protected final Object clone() {
            return this;
        }

        public boolean equals(Object object) {
            return object == null || object == this;
        }

        public String toString() {
            return "null";
        }
    }

}

