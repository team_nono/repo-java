/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.lib.util.json;

import com.ehaqui.ehlogin.lib.util.json.JSONWriter;
import java.io.StringWriter;
import java.io.Writer;

public class JSONStringer
extends JSONWriter {
    public JSONStringer() {
        super(new StringWriter());
    }

    public String toString() {
        return this.mode == 'd' ? this.writer.toString() : null;
    }
}

