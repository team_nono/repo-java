/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.lib.util.json;

import com.ehaqui.ehlogin.lib.util.json.JSONArray;
import com.ehaqui.ehlogin.lib.util.json.JSONException;
import com.ehaqui.ehlogin.lib.util.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;

public class JSONTokener {
    private long character;
    private boolean eof;
    private long index;
    private long line;
    private char previous;
    private Reader reader;
    private boolean usePrevious;

    public JSONTokener(Reader reader) {
        this.reader = reader.markSupported() ? reader : new BufferedReader(reader);
        this.eof = false;
        this.usePrevious = false;
        this.previous = '\u0000';
        this.index = 0;
        this.character = 1;
        this.line = 1;
    }

    public JSONTokener(InputStream inputStream) {
        this(new InputStreamReader(inputStream));
    }

    public JSONTokener(String string) {
        this(new StringReader(string));
    }

    public void back() {
        if (this.usePrevious || this.index <= 0) {
            throw new JSONException("Stepping back two steps is not supported");
        }
        --this.index;
        --this.character;
        this.usePrevious = true;
        this.eof = false;
    }

    public static int dehexchar(char c) {
        if (c >= '0' && c <= '9') {
            return c - 48;
        }
        if (c >= 'A' && c <= 'F') {
            return c - 55;
        }
        if (c >= 'a' && c <= 'f') {
            return c - 87;
        }
        return -1;
    }

    public boolean end() {
        return this.eof && !this.usePrevious;
    }

    public boolean more() {
        this.next();
        if (this.end()) {
            return false;
        }
        this.back();
        return true;
    }

    public char next() {
        int n;
        if (this.usePrevious) {
            this.usePrevious = false;
            n = this.previous;
        } else {
            try {
                n = this.reader.read();
            }
            catch (IOException var2_2) {
                throw new JSONException(var2_2);
            }
            if (n <= 0) {
                this.eof = true;
                n = 0;
            }
        }
        ++this.index;
        if (this.previous == '\r') {
            ++this.line;
            this.character = n == 10 ? 0 : 1;
        } else if (n == 10) {
            ++this.line;
            this.character = 0;
        } else {
            ++this.character;
        }
        this.previous = (char)n;
        return this.previous;
    }

    public char next(char c) {
        char c2 = this.next();
        if (c2 != c) {
            throw this.syntaxError("Expected '" + c + "' and instead saw '" + c2 + "'");
        }
        return c2;
    }

    public String next(int n) {
        if (n == 0) {
            return "";
        }
        char[] arrc = new char[n];
        for (int i = 0; i < n; ++i) {
            arrc[i] = this.next();
            if (!this.end()) continue;
            throw this.syntaxError("Substring bounds error");
        }
        return new String(arrc);
    }

    public char nextClean() {
        char c;
        while ((c = this.next()) != '\u0000' && c <= ' ') {
        }
        return c;
    }

    public String nextString(char c) {
        StringBuffer stringBuffer = new StringBuffer();
        block13 : do {
            char c2 = this.next();
            switch (c2) {
                case '\u0000': 
                case '\n': 
                case '\r': {
                    throw this.syntaxError("Unterminated string");
                }
                case '\\': {
                    c2 = this.next();
                    switch (c2) {
                        case 'b': {
                            stringBuffer.append('\b');
                            continue block13;
                        }
                        case 't': {
                            stringBuffer.append('\t');
                            continue block13;
                        }
                        case 'n': {
                            stringBuffer.append('\n');
                            continue block13;
                        }
                        case 'f': {
                            stringBuffer.append('\f');
                            continue block13;
                        }
                        case 'r': {
                            stringBuffer.append('\r');
                            continue block13;
                        }
                        case 'u': {
                            stringBuffer.append((char)Integer.parseInt(this.next(4), 16));
                            continue block13;
                        }
                        case '\"': 
                        case '\'': 
                        case '/': 
                        case '\\': {
                            stringBuffer.append(c2);
                            continue block13;
                        }
                    }
                    throw this.syntaxError("Illegal escape.");
                }
            }
            if (c2 == c) {
                return stringBuffer.toString();
            }
            stringBuffer.append(c2);
        } while (true);
    }

    public String nextTo(char c) {
        StringBuffer stringBuffer = new StringBuffer();
        do {
            char c2;
            if ((c2 = this.next()) == c || c2 == '\u0000' || c2 == '\n' || c2 == '\r') {
                if (c2 != '\u0000') {
                    this.back();
                }
                return stringBuffer.toString().trim();
            }
            stringBuffer.append(c2);
        } while (true);
    }

    public String nextTo(String string) {
        StringBuffer stringBuffer = new StringBuffer();
        do {
            char c;
            if (string.indexOf(c = this.next()) >= 0 || c == '\u0000' || c == '\n' || c == '\r') {
                if (c != '\u0000') {
                    this.back();
                }
                return stringBuffer.toString().trim();
            }
            stringBuffer.append(c);
        } while (true);
    }

    public Object nextValue() {
        char c = this.nextClean();
        switch (c) {
            case '\"': 
            case '\'': {
                return this.nextString(c);
            }
            case '{': {
                this.back();
                return new JSONObject(this);
            }
            case '[': {
                this.back();
                return new JSONArray(this);
            }
        }
        StringBuffer stringBuffer = new StringBuffer();
        while (c >= ' ' && ",:]}/\\\"[{;=#".indexOf(c) < 0) {
            stringBuffer.append(c);
            c = this.next();
        }
        this.back();
        String string = stringBuffer.toString().trim();
        if ("".equals(string)) {
            throw this.syntaxError("Missing value");
        }
        return JSONObject.stringToValue(string);
    }

    public char skipTo(char c) {
        char c2;
        try {
            long l = this.index;
            long l2 = this.character;
            long l3 = this.line;
            this.reader.mark(1000000);
            do {
                if ((c2 = this.next()) != '\u0000') continue;
                this.reader.reset();
                this.index = l;
                this.character = l2;
                this.line = l3;
                return c2;
            } while (c2 != c);
        }
        catch (IOException var3_3) {
            throw new JSONException(var3_3);
        }
        this.back();
        return c2;
    }

    public JSONException syntaxError(String string) {
        return new JSONException(string + this.toString());
    }

    public String toString() {
        return " at " + this.index + " [character " + this.character + " line " + this.line + "]";
    }
}

