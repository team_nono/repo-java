/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.lib.util.json;

import com.ehaqui.ehlogin.lib.util.json.JSONException;
import com.ehaqui.ehlogin.lib.util.json.JSONObject;
import java.io.IOException;
import java.io.Writer;

public class JSONWriter {
    private static final int maxdepth = 200;
    private boolean comma = false;
    protected char mode = 105;
    private final JSONObject[] stack = new JSONObject[200];
    private int top = 0;
    protected Writer writer;

    public JSONWriter(Writer writer) {
        this.writer = writer;
    }

    private JSONWriter append(String string) {
        if (string == null) {
            throw new JSONException("Null pointer");
        }
        if (this.mode == 'o' || this.mode == 'a') {
            try {
                if (this.comma && this.mode == 'a') {
                    this.writer.write(44);
                }
                this.writer.write(string);
            }
            catch (IOException var2_2) {
                throw new JSONException(var2_2);
            }
            if (this.mode == 'o') {
                this.mode = 107;
            }
            this.comma = true;
            return this;
        }
        throw new JSONException("Value out of sequence.");
    }

    public JSONWriter array() {
        if (this.mode == 'i' || this.mode == 'o' || this.mode == 'a') {
            this.push(null);
            this.append("[");
            this.comma = false;
            return this;
        }
        throw new JSONException("Misplaced array.");
    }

    private JSONWriter end(char c, char c2) {
        if (this.mode != c) {
            throw new JSONException(c == 'a' ? "Misplaced endArray." : "Misplaced endObject.");
        }
        this.pop(c);
        try {
            this.writer.write(c2);
        }
        catch (IOException var3_3) {
            throw new JSONException(var3_3);
        }
        this.comma = true;
        return this;
    }

    public JSONWriter endArray() {
        return this.end('a', ']');
    }

    public JSONWriter endObject() {
        return this.end('k', '}');
    }

    public JSONWriter key(String string) {
        if (string == null) {
            throw new JSONException("Null key.");
        }
        if (this.mode == 'k') {
            try {
                this.stack[this.top - 1].putOnce(string, Boolean.TRUE);
                if (this.comma) {
                    this.writer.write(44);
                }
                this.writer.write(JSONObject.quote(string));
                this.writer.write(58);
                this.comma = false;
                this.mode = 111;
                return this;
            }
            catch (IOException var2_2) {
                throw new JSONException(var2_2);
            }
        }
        throw new JSONException("Misplaced key.");
    }

    public JSONWriter object() {
        if (this.mode == 'i') {
            this.mode = 111;
        }
        if (this.mode == 'o' || this.mode == 'a') {
            this.append("{");
            this.push(new JSONObject());
            this.comma = false;
            return this;
        }
        throw new JSONException("Misplaced object.");
    }

    private void pop(char c) {
        char c2;
        if (this.top <= 0) {
            throw new JSONException("Nesting error.");
        }
        char c3 = c2 = this.stack[this.top - 1] == null ? 'a' : 'k';
        if (c2 != c) {
            throw new JSONException("Nesting error.");
        }
        --this.top;
        this.mode = this.top == 0 ? 100 : (this.stack[this.top - 1] == null ? 97 : 107);
    }

    private void push(JSONObject jSONObject) {
        if (this.top >= 200) {
            throw new JSONException("Nesting too deep.");
        }
        this.stack[this.top] = jSONObject;
        this.mode = jSONObject == null ? 97 : 107;
        ++this.top;
    }

    public JSONWriter value(boolean bl) {
        return this.append(bl ? "true" : "false");
    }

    public JSONWriter value(double d) {
        return this.value(new Double(d));
    }

    public JSONWriter value(long l) {
        return this.append(Long.toString(l));
    }

    public JSONWriter value(Object object) {
        return this.append(JSONObject.valueToString(object));
    }
}

