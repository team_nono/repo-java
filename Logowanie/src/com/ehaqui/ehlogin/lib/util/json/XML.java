/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.lib.util.json;

import com.ehaqui.ehlogin.lib.util.json.JSONArray;
import com.ehaqui.ehlogin.lib.util.json.JSONException;
import com.ehaqui.ehlogin.lib.util.json.JSONObject;
import com.ehaqui.ehlogin.lib.util.json.XMLTokener;
import java.util.Iterator;

public class XML {
    public static final Character AMP = new Character('&');
    public static final Character APOS = new Character('\'');
    public static final Character BANG = new Character('!');
    public static final Character EQ = new Character('=');
    public static final Character GT = new Character('>');
    public static final Character LT = new Character('<');
    public static final Character QUEST = new Character('?');
    public static final Character QUOT = new Character('\"');
    public static final Character SLASH = new Character('/');

    public static String escape(String string) {
        StringBuffer stringBuffer = new StringBuffer();
        int n = string.length();
        block7 : for (int i = 0; i < n; ++i) {
            char c = string.charAt(i);
            switch (c) {
                case '&': {
                    stringBuffer.append("&amp;");
                    continue block7;
                }
                case '<': {
                    stringBuffer.append("&lt;");
                    continue block7;
                }
                case '>': {
                    stringBuffer.append("&gt;");
                    continue block7;
                }
                case '\"': {
                    stringBuffer.append("&quot;");
                    continue block7;
                }
                case '\'': {
                    stringBuffer.append("&apos;");
                    continue block7;
                }
                default: {
                    stringBuffer.append(c);
                }
            }
        }
        return stringBuffer.toString();
    }

    public static void noSpace(String string) {
        int n = string.length();
        if (n == 0) {
            throw new JSONException("Empty string.");
        }
        for (int i = 0; i < n; ++i) {
            if (!Character.isWhitespace(string.charAt(i))) continue;
            throw new JSONException("'" + string + "' contains a space character.");
        }
    }

    private static boolean parse(XMLTokener xMLTokener, JSONObject jSONObject, String string) {
        String string2;
        JSONObject jSONObject2 = null;
        Object object = xMLTokener.nextToken();
        if (object == BANG) {
            char c = xMLTokener.next();
            if (c == '-') {
                if (xMLTokener.next() == '-') {
                    xMLTokener.skipPast("-->");
                    return false;
                }
                xMLTokener.back();
            } else if (c == '[') {
                object = xMLTokener.nextToken();
                if ("CDATA".equals(object) && xMLTokener.next() == '[') {
                    String string3 = xMLTokener.nextCDATA();
                    if (string3.length() > 0) {
                        jSONObject.accumulate("content", string3);
                    }
                    return false;
                }
                throw xMLTokener.syntaxError("Expected 'CDATA['");
            }
            int n = 1;
            do {
                if ((object = xMLTokener.nextMeta()) == null) {
                    throw xMLTokener.syntaxError("Missing '>' after '<!'.");
                }
                if (object == LT) {
                    ++n;
                    continue;
                }
                if (object != GT) continue;
                --n;
            } while (n > 0);
            return false;
        }
        if (object == QUEST) {
            xMLTokener.skipPast("?>");
            return false;
        }
        if (object == SLASH) {
            object = xMLTokener.nextToken();
            if (string == null) {
                throw xMLTokener.syntaxError("Mismatched close tag " + object);
            }
            if (!object.equals(string)) {
                throw xMLTokener.syntaxError("Mismatched " + string + " and " + object);
            }
            if (xMLTokener.nextToken() != GT) {
                throw xMLTokener.syntaxError("Misshaped close tag");
            }
            return true;
        }
        if (object instanceof Character) {
            throw xMLTokener.syntaxError("Misshaped tag");
        }
        String string4 = (String)object;
        object = null;
        jSONObject2 = new JSONObject();
        do {
            if (object == null) {
                object = xMLTokener.nextToken();
            }
            if (!(object instanceof String)) break;
            string2 = (String)object;
            object = xMLTokener.nextToken();
            if (object == EQ) {
                object = xMLTokener.nextToken();
                if (!(object instanceof String)) {
                    throw xMLTokener.syntaxError("Missing value");
                }
                jSONObject2.accumulate(string2, XML.stringToValue((String)object));
                object = null;
                continue;
            }
            jSONObject2.accumulate(string2, "");
        } while (true);
        if (object == SLASH) {
            if (xMLTokener.nextToken() != GT) {
                throw xMLTokener.syntaxError("Misshaped tag");
            }
            if (jSONObject2.length() > 0) {
                jSONObject.accumulate(string4, jSONObject2);
            } else {
                jSONObject.accumulate(string4, "");
            }
            return false;
        }
        if (object == GT) {
            do {
                if ((object = xMLTokener.nextContent()) == null) {
                    if (string4 != null) {
                        throw xMLTokener.syntaxError("Unclosed tag " + string4);
                    }
                    return false;
                }
                if (object instanceof String) {
                    string2 = (String)object;
                    if (string2.length() <= 0) continue;
                    jSONObject2.accumulate("content", XML.stringToValue(string2));
                    continue;
                }
                if (object == LT && XML.parse(xMLTokener, jSONObject2, string4)) break;
            } while (true);
            if (jSONObject2.length() == 0) {
                jSONObject.accumulate(string4, "");
            } else if (jSONObject2.length() == 1 && jSONObject2.opt("content") != null) {
                jSONObject.accumulate(string4, jSONObject2.opt("content"));
            } else {
                jSONObject.accumulate(string4, jSONObject2);
            }
            return false;
        }
        throw xMLTokener.syntaxError("Misshaped tag");
    }

    public static Object stringToValue(String string) {
        if ("".equals(string)) {
            return string;
        }
        if ("true".equalsIgnoreCase(string)) {
            return Boolean.TRUE;
        }
        if ("false".equalsIgnoreCase(string)) {
            return Boolean.FALSE;
        }
        if ("null".equalsIgnoreCase(string)) {
            return JSONObject.NULL;
        }
        if ("0".equals(string)) {
            return new Integer(0);
        }
        try {
            char c = string.charAt(0);
            boolean bl = false;
            if (c == '-') {
                c = string.charAt(1);
                bl = true;
            }
            if (c == '0' && string.charAt(bl ? 2 : 1) == '0') {
                return string;
            }
            if (c >= '0' && c <= '9') {
                if (string.indexOf(46) >= 0) {
                    return Double.valueOf(string);
                }
                if (string.indexOf(101) < 0 && string.indexOf(69) < 0) {
                    Long l = new Long(string);
                    if (l == (long)l.intValue()) {
                        return new Integer(l.intValue());
                    }
                    return l;
                }
            }
        }
        catch (Exception var1_2) {
            // empty catch block
        }
        return string;
    }

    public static JSONObject toJSONObject(String string) {
        JSONObject jSONObject = new JSONObject();
        XMLTokener xMLTokener = new XMLTokener(string);
        while (xMLTokener.more() && xMLTokener.skipPast("<")) {
            XML.parse(xMLTokener, jSONObject, null);
        }
        return jSONObject;
    }

    public static String toString(Object object) {
        return XML.toString(object, null);
    }

    public static String toString(Object object, String string) {
        String string2;
        StringBuffer stringBuffer = new StringBuffer();
        if (object instanceof JSONObject) {
            if (string != null) {
                stringBuffer.append('<');
                stringBuffer.append(string);
                stringBuffer.append('>');
            }
            JSONObject jSONObject = (JSONObject)object;
            Iterator iterator = jSONObject.keys();
            while (iterator.hasNext()) {
                JSONArray jSONArray;
                int n;
                int n2;
                String string3 = iterator.next().toString();
                Object object2 = jSONObject.opt(string3);
                if (object2 == null) {
                    object2 = "";
                }
                String string4 = object2 instanceof String ? (String)object2 : null;
                if ("content".equals(string3)) {
                    if (object2 instanceof JSONArray) {
                        jSONArray = (JSONArray)object2;
                        n = jSONArray.length();
                        for (n2 = 0; n2 < n; ++n2) {
                            if (n2 > 0) {
                                stringBuffer.append('\n');
                            }
                            stringBuffer.append(XML.escape(jSONArray.get(n2).toString()));
                        }
                        continue;
                    }
                    stringBuffer.append(XML.escape(object2.toString()));
                    continue;
                }
                if (object2 instanceof JSONArray) {
                    jSONArray = (JSONArray)object2;
                    n = jSONArray.length();
                    for (n2 = 0; n2 < n; ++n2) {
                        object2 = jSONArray.get(n2);
                        if (object2 instanceof JSONArray) {
                            stringBuffer.append('<');
                            stringBuffer.append(string3);
                            stringBuffer.append('>');
                            stringBuffer.append(XML.toString(object2));
                            stringBuffer.append("</");
                            stringBuffer.append(string3);
                            stringBuffer.append('>');
                            continue;
                        }
                        stringBuffer.append(XML.toString(object2, string3));
                    }
                    continue;
                }
                if ("".equals(object2)) {
                    stringBuffer.append('<');
                    stringBuffer.append(string3);
                    stringBuffer.append("/>");
                    continue;
                }
                stringBuffer.append(XML.toString(object2, string3));
            }
            if (string != null) {
                stringBuffer.append("</");
                stringBuffer.append(string);
                stringBuffer.append('>');
            }
            return stringBuffer.toString();
        }
        if (object.getClass().isArray()) {
            object = new JSONArray(object);
        }
        if (object instanceof JSONArray) {
            JSONArray jSONArray = (JSONArray)object;
            int n = jSONArray.length();
            for (int i = 0; i < n; ++i) {
                stringBuffer.append(XML.toString(jSONArray.opt(i), string == null ? "array" : string));
            }
            return stringBuffer.toString();
        }
        String string5 = string2 = object == null ? "null" : XML.escape(object.toString());
        return string == null ? "\"" + string2 + "\"" : (string2.length() == 0 ? "<" + string + "/>" : "<" + string + ">" + string2 + "</" + string + ">");
    }
}

