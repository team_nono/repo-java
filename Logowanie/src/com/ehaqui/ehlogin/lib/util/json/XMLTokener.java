/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.lib.util.json;

import com.ehaqui.ehlogin.lib.util.json.JSONException;
import com.ehaqui.ehlogin.lib.util.json.JSONTokener;
import com.ehaqui.ehlogin.lib.util.json.XML;
import java.util.HashMap;

public class XMLTokener
extends JSONTokener {
    public static final HashMap entity = new HashMap(8);

    public XMLTokener(String string) {
        super(string);
    }

    public String nextCDATA() {
        int n;
        StringBuffer stringBuffer = new StringBuffer();
        do {
            char c = this.next();
            if (this.end()) {
                throw this.syntaxError("Unclosed CDATA");
            }
            stringBuffer.append(c);
        } while ((n = stringBuffer.length() - 3) < 0 || stringBuffer.charAt(n) != ']' || stringBuffer.charAt(n + 1) != ']' || stringBuffer.charAt(n + 2) != '>');
        stringBuffer.setLength(n);
        return stringBuffer.toString();
    }

    public Object nextContent() {
        char c;
        while (Character.isWhitespace(c = this.next())) {
        }
        if (c == '\u0000') {
            return null;
        }
        if (c == '<') {
            return XML.LT;
        }
        StringBuffer stringBuffer = new StringBuffer();
        do {
            if (c == '<' || c == '\u0000') {
                this.back();
                return stringBuffer.toString().trim();
            }
            if (c == '&') {
                stringBuffer.append(this.nextEntity(c));
            } else {
                stringBuffer.append(c);
            }
            c = this.next();
        } while (true);
    }

    public Object nextEntity(char c) {
        char c2;
        StringBuffer stringBuffer = new StringBuffer();
        while (Character.isLetterOrDigit(c2 = this.next()) || c2 == '#') {
            stringBuffer.append(Character.toLowerCase(c2));
        }
        if (c2 != ';') {
            throw this.syntaxError("Missing ';' in XML entity: &" + stringBuffer);
        }
        String string = stringBuffer.toString();
        Object v = entity.get(string);
        return v != null ? v : "" + c + string + ";";
    }

    public Object nextMeta() {
        char c;
        while (Character.isWhitespace(c = this.next())) {
        }
        switch (c) {
            case '\u0000': {
                throw this.syntaxError("Misshaped meta tag");
            }
            case '<': {
                return XML.LT;
            }
            case '>': {
                return XML.GT;
            }
            case '/': {
                return XML.SLASH;
            }
            case '=': {
                return XML.EQ;
            }
            case '!': {
                return XML.BANG;
            }
            case '?': {
                return XML.QUEST;
            }
            case '\"': 
            case '\'': {
                char c2 = c;
                do {
                    if ((c = this.next()) != '\u0000') continue;
                    throw this.syntaxError("Unterminated string");
                } while (c != c2);
                return Boolean.TRUE;
            }
        }
        while (!Character.isWhitespace(c = this.next())) {
            switch (c) {
                case '\u0000': 
                case '!': 
                case '\"': 
                case '\'': 
                case '/': 
                case '<': 
                case '=': 
                case '>': 
                case '?': {
                    this.back();
                    return Boolean.TRUE;
                }
            }
        }
        return Boolean.TRUE;
    }

    public Object nextToken() {
        char c;
        while (Character.isWhitespace(c = this.next())) {
        }
        switch (c) {
            case '\u0000': {
                throw this.syntaxError("Misshaped element");
            }
            case '<': {
                throw this.syntaxError("Misplaced '<'");
            }
            case '>': {
                return XML.GT;
            }
            case '/': {
                return XML.SLASH;
            }
            case '=': {
                return XML.EQ;
            }
            case '!': {
                return XML.BANG;
            }
            case '?': {
                return XML.QUEST;
            }
            case '\"': 
            case '\'': {
                char c2 = c;
                StringBuffer stringBuffer = new StringBuffer();
                do {
                    if ((c = this.next()) == '\u0000') {
                        throw this.syntaxError("Unterminated string");
                    }
                    if (c == c2) {
                        return stringBuffer.toString();
                    }
                    if (c == '&') {
                        stringBuffer.append(this.nextEntity(c));
                        continue;
                    }
                    stringBuffer.append(c);
                } while (true);
            }
        }
        StringBuffer stringBuffer = new StringBuffer();
        do {
            stringBuffer.append(c);
            c = this.next();
            if (Character.isWhitespace(c)) {
                return stringBuffer.toString();
            }
            switch (c) {
                case '\u0000': {
                    return stringBuffer.toString();
                }
                case '!': 
                case '/': 
                case '=': 
                case '>': 
                case '?': 
                case '[': 
                case ']': {
                    this.back();
                    return stringBuffer.toString();
                }
                case '\"': 
                case '\'': 
                case '<': {
                    throw this.syntaxError("Bad character in a name");
                }
            }
        } while (true);
    }

    public boolean skipPast(String string) {
        char c;
        int n;
        int n2 = 0;
        int n3 = string.length();
        char[] arrc = new char[n3];
        for (n = 0; n < n3; ++n) {
            c = this.next();
            if (c == '\u0000') {
                return false;
            }
            arrc[n] = c;
        }
        do {
            int n4 = n2;
            boolean bl = true;
            for (n = 0; n < n3; ++n) {
                if (arrc[n4] != string.charAt(n)) {
                    bl = false;
                    break;
                }
                if (++n4 < n3) continue;
                n4 -= n3;
            }
            if (bl) {
                return true;
            }
            c = this.next();
            if (c == '\u0000') {
                return false;
            }
            arrc[n2] = c;
            if (++n2 < n3) continue;
            n2 -= n3;
        } while (true);
    }

    static {
        entity.put("amp", XML.AMP);
        entity.put("apos", XML.APOS);
        entity.put("gt", XML.GT);
        entity.put("lt", XML.LT);
        entity.put("quot", XML.QUOT);
    }
}

