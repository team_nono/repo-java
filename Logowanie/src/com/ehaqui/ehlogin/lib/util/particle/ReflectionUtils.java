/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 */
package com.ehaqui.ehlogin.lib.util.particle;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.Bukkit;

public final class ReflectionUtils {
    private ReflectionUtils() {
    }

    public static /* varargs */ Constructor<?> getConstructor(Class<?> class_, Class<?> ... arrclass) {
        Class<?>[] arrclass2 = DataType.getPrimitive(arrclass);
        for (Constructor constructor : class_.getConstructors()) {
            if (!DataType.compare(DataType.getPrimitive(constructor.getParameterTypes()), arrclass2)) continue;
            return constructor;
        }
        throw new NoSuchMethodException("There is no such constructor in this class with the specified parameter types");
    }

    public static /* varargs */ Constructor<?> getConstructor(String string, PackageType packageType, Class<?> ... arrclass) {
        return ReflectionUtils.getConstructor(packageType.getClass(string), arrclass);
    }

    public static /* varargs */ Object instantiateObject(Class<?> class_, Object ... arrobject) {
        return ReflectionUtils.getConstructor(class_, DataType.getPrimitive(arrobject)).newInstance(arrobject);
    }

    public static /* varargs */ Object instantiateObject(String string, PackageType packageType, Object ... arrobject) {
        return ReflectionUtils.instantiateObject(packageType.getClass(string), arrobject);
    }

    public static /* varargs */ Method getMethod(Class<?> class_, String string, Class<?> ... arrclass) {
        Class<?>[] arrclass2 = DataType.getPrimitive(arrclass);
        for (Method method : class_.getMethods()) {
            if (!method.getName().equals(string) || !DataType.compare(DataType.getPrimitive(method.getParameterTypes()), arrclass2)) continue;
            return method;
        }
        throw new NoSuchMethodException("There is no such method in this class with the specified name and parameter types");
    }

    public static /* varargs */ Method getMethod(String string, PackageType packageType, String string2, Class<?> ... arrclass) {
        return ReflectionUtils.getMethod(packageType.getClass(string), string2, arrclass);
    }

    public static /* varargs */ Object invokeMethod(Object object, String string, Object ... arrobject) {
        return ReflectionUtils.getMethod(object.getClass(), string, DataType.getPrimitive(arrobject)).invoke(object, arrobject);
    }

    public static /* varargs */ Object invokeMethod(Object object, Class<?> class_, String string, Object ... arrobject) {
        return ReflectionUtils.getMethod(class_, string, DataType.getPrimitive(arrobject)).invoke(object, arrobject);
    }

    public static /* varargs */ Object invokeMethod(Object object, String string, PackageType packageType, String string2, Object ... arrobject) {
        return ReflectionUtils.invokeMethod(object, packageType.getClass(string), string2, arrobject);
    }

    public static Field getField(Class<?> class_, boolean bl, String string) {
        Field field = bl ? class_.getDeclaredField(string) : class_.getField(string);
        field.setAccessible(true);
        return field;
    }

    public static Field getField(String string, PackageType packageType, boolean bl, String string2) {
        return ReflectionUtils.getField(packageType.getClass(string), bl, string2);
    }

    public static Object getValue(Object object, Class<?> class_, boolean bl, String string) {
        return ReflectionUtils.getField(class_, bl, string).get(object);
    }

    public static Object getValue(Object object, String string, PackageType packageType, boolean bl, String string2) {
        return ReflectionUtils.getValue(object, packageType.getClass(string), bl, string2);
    }

    public static Object getValue(Object object, boolean bl, String string) {
        return ReflectionUtils.getValue(object, object.getClass(), bl, string);
    }

    public static void setValue(Object object, Class<?> class_, boolean bl, String string, Object object2) {
        ReflectionUtils.getField(class_, bl, string).set(object, object2);
    }

    public static void setValue(Object object, String string, PackageType packageType, boolean bl, String string2, Object object2) {
        ReflectionUtils.setValue(object, packageType.getClass(string), bl, string2, object2);
    }

    public static void setValue(Object object, boolean bl, String string, Object object2) {
        ReflectionUtils.setValue(object, object.getClass(), bl, string, object2);
    }

    public static enum DataType {
        BYTE(Byte.TYPE, Byte.class),
        SHORT(Short.TYPE, Short.class),
        INTEGER(Integer.TYPE, Integer.class),
        LONG(Long.TYPE, Long.class),
        CHARACTER(Character.TYPE, Character.class),
        FLOAT(Float.TYPE, Float.class),
        DOUBLE(Double.TYPE, Double.class),
        BOOLEAN(Boolean.TYPE, Boolean.class);
        
        private static final Map<Class<?>, DataType> CLASS_MAP;
        private final Class<?> primitive;
        private final Class<?> reference;

        private DataType(Class<?> class_, Class<?> class_2) {
            this.primitive = class_;
            this.reference = class_2;
        }

        public Class<?> getPrimitive() {
            return this.primitive;
        }

        public Class<?> getReference() {
            return this.reference;
        }

        public static DataType fromClass(Class<?> class_) {
            return CLASS_MAP.get(class_);
        }

        public static Class<?> getPrimitive(Class<?> class_) {
            DataType dataType = DataType.fromClass(class_);
            return dataType == null ? class_ : dataType.getPrimitive();
        }

        public static Class<?> getReference(Class<?> class_) {
            DataType dataType = DataType.fromClass(class_);
            return dataType == null ? class_ : dataType.getReference();
        }

        public static Class<?>[] getPrimitive(Class<?>[] arrclass) {
            int n = arrclass == null ? 0 : arrclass.length;
            Class[] arrclass2 = new Class[n];
            for (int i = 0; i < n; ++i) {
                arrclass2[i] = DataType.getPrimitive(arrclass[i]);
            }
            return arrclass2;
        }

        public static Class<?>[] getReference(Class<?>[] arrclass) {
            int n = arrclass == null ? 0 : arrclass.length;
            Class[] arrclass2 = new Class[n];
            for (int i = 0; i < n; ++i) {
                arrclass2[i] = DataType.getReference(arrclass[i]);
            }
            return arrclass2;
        }

        public static Class<?>[] getPrimitive(Object[] arrobject) {
            int n = arrobject == null ? 0 : arrobject.length;
            Class[] arrclass = new Class[n];
            for (int i = 0; i < n; ++i) {
                arrclass[i] = DataType.getPrimitive(arrobject[i].getClass());
            }
            return arrclass;
        }

        public static Class<?>[] getReference(Object[] arrobject) {
            int n = arrobject == null ? 0 : arrobject.length;
            Class[] arrclass = new Class[n];
            for (int i = 0; i < n; ++i) {
                arrclass[i] = DataType.getReference(arrobject[i].getClass());
            }
            return arrclass;
        }

        public static boolean compare(Class<?>[] arrclass, Class<?>[] arrclass2) {
            if (arrclass == null || arrclass2 == null || arrclass.length != arrclass2.length) {
                return false;
            }
            for (int i = 0; i < arrclass.length; ++i) {
                Class class_ = arrclass[i];
                Class class_2 = arrclass2[i];
                if (class_.equals(class_2) || class_.isAssignableFrom(class_2)) continue;
                return false;
            }
            return true;
        }

        static {
            CLASS_MAP = new HashMap();
            for (DataType dataType : DataType.values()) {
                CLASS_MAP.put(dataType.primitive, dataType);
                CLASS_MAP.put(dataType.reference, dataType);
            }
        }
    }

    public static enum PackageType {
        MINECRAFT_SERVER("net.minecraft.server." + PackageType.getServerVersion()),
        CRAFTBUKKIT("org.bukkit.craftbukkit." + PackageType.getServerVersion()),
        CRAFTBUKKIT_BLOCK(CRAFTBUKKIT, "block"),
        CRAFTBUKKIT_CHUNKIO(CRAFTBUKKIT, "chunkio"),
        CRAFTBUKKIT_COMMAND(CRAFTBUKKIT, "command"),
        CRAFTBUKKIT_CONVERSATIONS(CRAFTBUKKIT, "conversations"),
        CRAFTBUKKIT_ENCHANTMENS(CRAFTBUKKIT, "enchantments"),
        CRAFTBUKKIT_ENTITY(CRAFTBUKKIT, "entity"),
        CRAFTBUKKIT_EVENT(CRAFTBUKKIT, "event"),
        CRAFTBUKKIT_GENERATOR(CRAFTBUKKIT, "generator"),
        CRAFTBUKKIT_HELP(CRAFTBUKKIT, "help"),
        CRAFTBUKKIT_INVENTORY(CRAFTBUKKIT, "inventory"),
        CRAFTBUKKIT_MAP(CRAFTBUKKIT, "map"),
        CRAFTBUKKIT_METADATA(CRAFTBUKKIT, "metadata"),
        CRAFTBUKKIT_POTION(CRAFTBUKKIT, "potion"),
        CRAFTBUKKIT_PROJECTILES(CRAFTBUKKIT, "projectiles"),
        CRAFTBUKKIT_SCHEDULER(CRAFTBUKKIT, "scheduler"),
        CRAFTBUKKIT_SCOREBOARD(CRAFTBUKKIT, "scoreboard"),
        CRAFTBUKKIT_UPDATER(CRAFTBUKKIT, "updater"),
        CRAFTBUKKIT_UTIL(CRAFTBUKKIT, "util");
        
        private final String path;

        private PackageType(String string2) {
            this.path = string2;
        }

        private PackageType(PackageType packageType, String string2) {
            this((Object)((Object)packageType) + "." + string2);
        }

        public String getPath() {
            return this.path;
        }

        public Class<?> getClass(String string) {
            return Class.forName((Object)((Object)this) + "." + string);
        }

        public String toString() {
            return this.path;
        }

        public static String getServerVersion() {
            return Bukkit.getServer().getClass().getPackage().getName().substring(23);
        }
    }

}

