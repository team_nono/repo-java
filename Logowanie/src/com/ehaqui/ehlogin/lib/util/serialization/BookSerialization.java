/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.Material
 *  org.bukkit.enchantments.Enchantment
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.BookMeta
 *  org.bukkit.inventory.meta.EnchantmentStorageMeta
 *  org.bukkit.inventory.meta.ItemMeta
 */
package com.ehaqui.ehlogin.lib.util.serialization;

import com.ehaqui.ehlogin.lib.util.json.JSONArray;
import com.ehaqui.ehlogin.lib.util.json.JSONException;
import com.ehaqui.ehlogin.lib.util.json.JSONObject;
import com.ehaqui.ehlogin.lib.util.serialization.EnchantmentSerialization;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;

public class BookSerialization {
    protected BookSerialization() {
    }

    public static BookMeta getBookMeta(String string) {
        try {
            return BookSerialization.getBookMeta(new JSONObject(string));
        }
        catch (JSONException var1_1) {
            var1_1.printStackTrace();
            return null;
        }
    }

    public static BookMeta getBookMeta(JSONObject jSONObject) {
        try {
            ItemStack itemStack = new ItemStack(Material.WRITTEN_BOOK, 1);
            BookMeta bookMeta = (BookMeta)itemStack.getItemMeta();
            String string = null;
            String string2 = null;
            JSONArray jSONArray = null;
            if (jSONObject.has("title")) {
                string = jSONObject.getString("title");
            }
            if (jSONObject.has("author")) {
                string2 = jSONObject.getString("author");
            }
            if (jSONObject.has("pages")) {
                jSONArray = jSONObject.getJSONArray("pages");
            }
            if (string != null) {
                bookMeta.setTitle(string);
            }
            if (string2 != null) {
                bookMeta.setAuthor(string2);
            }
            if (jSONArray != null) {
                String[] arrstring = new String[jSONArray.length()];
                for (int i = 0; i < jSONArray.length(); ++i) {
                    String string3 = jSONArray.getString(i);
                    if (string3.isEmpty() || string3 == null) {
                        string3 = "";
                    }
                    arrstring[i] = string3;
                }
                bookMeta.setPages(arrstring);
            }
            return bookMeta;
        }
        catch (JSONException var1_2) {
            var1_2.printStackTrace();
            return null;
        }
    }

    public static JSONObject serializeBookMeta(BookMeta bookMeta) {
        try {
            JSONObject jSONObject = new JSONObject();
            if (bookMeta.hasTitle()) {
                jSONObject.put("title", bookMeta.getTitle());
            }
            if (bookMeta.hasAuthor()) {
                jSONObject.put("author", bookMeta.getAuthor());
            }
            if (bookMeta.hasPages()) {
                String[] arrstring = bookMeta.getPages().toArray(new String[0]);
                jSONObject.put("pages", arrstring);
            }
            return jSONObject;
        }
        catch (JSONException var1_2) {
            var1_2.printStackTrace();
            return null;
        }
    }

    public static String serializeBookMetaAsString(BookMeta bookMeta) {
        return BookSerialization.serializeBookMetaAsString(bookMeta, false);
    }

    public static String serializeBookMetaAsString(BookMeta bookMeta, boolean bl) {
        return BookSerialization.serializeBookMetaAsString(bookMeta, bl, 5);
    }

    public static String serializeBookMetaAsString(BookMeta bookMeta, boolean bl, int n) {
        try {
            if (bl) {
                return BookSerialization.serializeBookMeta(bookMeta).toString(n);
            }
            return BookSerialization.serializeBookMeta(bookMeta).toString();
        }
        catch (JSONException var3_3) {
            var3_3.printStackTrace();
            return null;
        }
    }

    public static EnchantmentStorageMeta getEnchantedBookMeta(String string) {
        try {
            return BookSerialization.getEnchantedBookMeta(new JSONObject(string));
        }
        catch (JSONException var1_1) {
            var1_1.printStackTrace();
            return null;
        }
    }

    public static EnchantmentStorageMeta getEnchantedBookMeta(JSONObject jSONObject) {
        try {
            ItemStack itemStack = new ItemStack(Material.ENCHANTED_BOOK, 1);
            EnchantmentStorageMeta enchantmentStorageMeta = (EnchantmentStorageMeta)itemStack.getItemMeta();
            if (jSONObject.has("enchantments")) {
                Map<Enchantment, Integer> map = EnchantmentSerialization.getEnchantments(jSONObject.getString("enchantments"));
                for (Enchantment enchantment : map.keySet()) {
                    enchantmentStorageMeta.addStoredEnchant(enchantment, map.get((Object)enchantment).intValue(), true);
                }
            }
            return enchantmentStorageMeta;
        }
        catch (JSONException var1_2) {
            var1_2.printStackTrace();
            return null;
        }
    }

    public static JSONObject serializeEnchantedBookMeta(EnchantmentStorageMeta enchantmentStorageMeta) {
        try {
            JSONObject jSONObject = new JSONObject();
            String string = EnchantmentSerialization.serializeEnchantments(enchantmentStorageMeta.getStoredEnchants());
            jSONObject.put("enchantments", string);
            return jSONObject;
        }
        catch (JSONException var1_2) {
            var1_2.printStackTrace();
            return null;
        }
    }

    public static String serializeEnchantedBookMetaAsString(EnchantmentStorageMeta enchantmentStorageMeta) {
        return BookSerialization.serializeEnchantedBookMetaAsString(enchantmentStorageMeta, false);
    }

    public static String serializeEnchantedBookMetaAsString(EnchantmentStorageMeta enchantmentStorageMeta, boolean bl) {
        return BookSerialization.serializeEnchantedBookMetaAsString(enchantmentStorageMeta, bl, 5);
    }

    public static String serializeEnchantedBookMetaAsString(EnchantmentStorageMeta enchantmentStorageMeta, boolean bl, int n) {
        try {
            if (bl) {
                return BookSerialization.serializeEnchantedBookMeta(enchantmentStorageMeta).toString(n);
            }
            return BookSerialization.serializeEnchantedBookMeta(enchantmentStorageMeta).toString();
        }
        catch (JSONException var3_3) {
            var3_3.printStackTrace();
            return null;
        }
    }
}

