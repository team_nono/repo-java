/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.Color
 */
package com.ehaqui.ehlogin.lib.util.serialization;

import com.ehaqui.ehlogin.lib.util.json.JSONException;
import com.ehaqui.ehlogin.lib.util.json.JSONObject;
import org.bukkit.Color;

public class ColorSerialization {
    protected ColorSerialization() {
    }

    public static JSONObject serializeColor(Color color) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("red", color.getRed());
            jSONObject.put("green", color.getGreen());
            jSONObject.put("blue", color.getBlue());
            return jSONObject;
        }
        catch (JSONException var1_2) {
            var1_2.printStackTrace();
            return null;
        }
    }

    public static Color getColor(String string) {
        try {
            return ColorSerialization.getColor(new JSONObject(string));
        }
        catch (JSONException var1_1) {
            var1_1.printStackTrace();
            return null;
        }
    }

    public static Color getColor(JSONObject jSONObject) {
        try {
            int n = 0;
            int n2 = 0;
            int n3 = 0;
            if (jSONObject.has("red")) {
                n = jSONObject.getInt("red");
            }
            if (jSONObject.has("green")) {
                n2 = jSONObject.getInt("green");
            }
            if (jSONObject.has("blue")) {
                n3 = jSONObject.getInt("blue");
            }
            return Color.fromRGB((int)n, (int)n2, (int)n3);
        }
        catch (JSONException var1_2) {
            var1_2.printStackTrace();
            return null;
        }
    }

    public static String serializeColorAsString(Color color) {
        return ColorSerialization.serializeColorAsString(color, false);
    }

    public static String serializeColorAsString(Color color, boolean bl) {
        return ColorSerialization.serializeColorAsString(color, bl, 5);
    }

    public static String serializeColorAsString(Color color, boolean bl, int n) {
        try {
            if (bl) {
                return ColorSerialization.serializeColor(color).toString(n);
            }
            return ColorSerialization.serializeColor(color).toString();
        }
        catch (JSONException var3_3) {
            var3_3.printStackTrace();
            return null;
        }
    }
}

