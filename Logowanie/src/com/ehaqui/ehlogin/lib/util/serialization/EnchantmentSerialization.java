/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.enchantments.Enchantment
 *  org.bukkit.inventory.ItemStack
 */
package com.ehaqui.ehlogin.lib.util.serialization;

import com.ehaqui.ehlogin.lib.util.serialization.Util;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

public class EnchantmentSerialization {
    protected EnchantmentSerialization() {
    }

    public static String serializeEnchantments(Map<Enchantment, Integer> map) {
        String string = "";
        for (Enchantment enchantment : map.keySet()) {
            string = string + enchantment.getId() + ":" + map.get((Object)enchantment) + ";";
        }
        return string;
    }

    public static Map<Enchantment, Integer> getEnchantments(String string) {
        HashMap<Enchantment, Integer> hashMap = new HashMap<Enchantment, Integer>();
        if (string.isEmpty()) {
            return hashMap;
        }
        String[] arrstring = string.split(";");
        for (int i = 0; i < arrstring.length; ++i) {
            String[] arrstring2 = arrstring[i].split(":");
            if (arrstring2.length < 2) {
                throw new IllegalArgumentException(string + " - Enchantment " + i + " (" + arrstring[i] + "): split must at least have a length of 2");
            }
            if (!Util.isNum(arrstring2[0])) {
                throw new IllegalArgumentException(string + " - Enchantment " + i + " (" + arrstring[i] + "): id is not an integer");
            }
            if (!Util.isNum(arrstring2[1])) {
                throw new IllegalArgumentException(string + " - Enchantment " + i + " (" + arrstring[i] + "): level is not an integer");
            }
            int n = Integer.parseInt(arrstring2[0]);
            int n2 = Integer.parseInt(arrstring2[1]);
            Enchantment enchantment = Enchantment.getById((int)n);
            if (enchantment == null) {
                throw new IllegalArgumentException(string + " - Enchantment " + i + " (" + arrstring[i] + "): no Enchantment with id of " + n);
            }
            hashMap.put(enchantment, n2);
        }
        return hashMap;
    }

    public static Map<Enchantment, Integer> getEnchantsFromOldFormat(String string) {
        HashMap<Enchantment, Integer> hashMap = new HashMap<Enchantment, Integer>();
        if (string.length() == 0) {
            return hashMap;
        }
        String string2 = "" + Long.parseLong(string, 32) + "";
        System.out.println(string2);
        for (int i = 0; i < string2.length(); i += 3) {
            int n = Integer.parseInt(string2.substring(i, i + 2));
            int n2 = Integer.parseInt("" + string2.charAt(i + 2) + "");
            Enchantment enchantment = Enchantment.getById((int)n);
            hashMap.put(enchantment, n2);
        }
        return hashMap;
    }

    public static String convert(String string) {
        Map<Enchantment, Integer> map = EnchantmentSerialization.getEnchantsFromOldFormat(string);
        return EnchantmentSerialization.serializeEnchantments(map);
    }

    public static Map<Enchantment, Integer> convertAndGetEnchantments(String string) {
        String string2 = EnchantmentSerialization.convert(string);
        return EnchantmentSerialization.getEnchantments(string2);
    }

    public static void addEnchantments(String string, ItemStack itemStack) {
        itemStack.addUnsafeEnchantments(EnchantmentSerialization.getEnchantments(string));
    }
}

