/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.Color
 *  org.bukkit.FireworkEffect
 *  org.bukkit.FireworkEffect$Builder
 *  org.bukkit.FireworkEffect$Type
 */
package com.ehaqui.ehlogin.lib.util.serialization;

import com.ehaqui.ehlogin.lib.util.json.JSONArray;
import com.ehaqui.ehlogin.lib.util.json.JSONException;
import com.ehaqui.ehlogin.lib.util.json.JSONObject;
import com.ehaqui.ehlogin.lib.util.serialization.ColorSerialization;
import com.ehaqui.ehlogin.lib.util.serialization.Serializer;
import java.util.List;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;

public class FireworkEffectSerialization {
    protected FireworkEffectSerialization() {
    }

    public static FireworkEffect getFireworkEffect(String string) {
        return FireworkEffectSerialization.getFireworkEffect(string);
    }

    public static FireworkEffect getFireworkEffect(JSONObject jSONObject) {
        try {
            FireworkEffect.Builder builder = FireworkEffect.builder();
            JSONArray jSONArray = jSONObject.getJSONArray("colors");
            for (int i = 0; i < jSONArray.length(); ++i) {
                builder.withColor(ColorSerialization.getColor(jSONArray.getJSONObject(i)));
            }
            JSONArray jSONArray2 = jSONObject.getJSONArray("fade-colors");
            for (int j = 0; j < jSONArray2.length(); ++j) {
                builder.withFade(ColorSerialization.getColor(jSONArray.getJSONObject(j)));
            }
            if (jSONObject.getBoolean("flicker")) {
                builder.withFlicker();
            }
            if (jSONObject.getBoolean("trail")) {
                builder.withTrail();
            }
            builder.with(FireworkEffect.Type.valueOf((String)jSONObject.getString("type")));
            return builder.build();
        }
        catch (IllegalArgumentException var1_2) {
            var1_2.printStackTrace();
        }
        catch (JSONException var1_3) {
            var1_3.printStackTrace();
        }
        return null;
    }

    public static JSONObject serializeFireworkEffect(FireworkEffect fireworkEffect) {
        try {
            JSONObject jSONObject = new JSONObject();
            JSONArray jSONArray = new JSONArray();
            for (Object object : fireworkEffect.getColors()) {
                jSONArray.put(ColorSerialization.serializeColor((Color)object));
            }
            jSONObject.put("colors", jSONArray);
            JSONArray jSONArray2 = new JSONArray();
            for (Color color : fireworkEffect.getFadeColors()) {
                jSONArray2.put(ColorSerialization.serializeColor(color));
            }
            jSONObject.put("fade-colors", jSONArray2);
            jSONObject.put("flicker", fireworkEffect.hasFlicker());
            jSONObject.put("trail", fireworkEffect.hasTrail());
            jSONObject.put("type", fireworkEffect.getType().name());
            return jSONObject;
        }
        catch (JSONException var1_2) {
            var1_2.printStackTrace();
            return null;
        }
    }

    public static String serializeFireworkEffectAsString(FireworkEffect fireworkEffect) {
        return FireworkEffectSerialization.serializeFireworkEffectAsString(fireworkEffect, false);
    }

    public static String serializeFireworkEffectAsString(FireworkEffect fireworkEffect, boolean bl) {
        return FireworkEffectSerialization.serializeFireworkEffectAsString(fireworkEffect, false, 5);
    }

    public static String serializeFireworkEffectAsString(FireworkEffect fireworkEffect, boolean bl, int n) {
        return Serializer.toString(FireworkEffectSerialization.serializeFireworkEffect(fireworkEffect), bl, n);
    }
}

