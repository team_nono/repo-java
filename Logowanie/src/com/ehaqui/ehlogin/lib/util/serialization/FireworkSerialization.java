/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.FireworkEffect
 *  org.bukkit.Material
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.FireworkMeta
 *  org.bukkit.inventory.meta.ItemMeta
 */
package com.ehaqui.ehlogin.lib.util.serialization;

import com.ehaqui.ehlogin.lib.util.json.JSONArray;
import com.ehaqui.ehlogin.lib.util.json.JSONException;
import com.ehaqui.ehlogin.lib.util.json.JSONObject;
import com.ehaqui.ehlogin.lib.util.serialization.FireworkEffectSerialization;
import com.ehaqui.ehlogin.lib.util.serialization.Serializer;
import java.util.List;
import org.bukkit.FireworkEffect;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;

public class FireworkSerialization {
    protected FireworkSerialization() {
    }

    public static FireworkMeta getFireworkMeta(String string) {
        return FireworkSerialization.getFireworkMeta(string);
    }

    public static FireworkMeta getFireworkMeta(JSONObject jSONObject) {
        try {
            FireworkMeta fireworkMeta = (FireworkMeta)new ItemStack(Material.FIREWORK).getItemMeta();
            fireworkMeta.setPower(jSONObject.optInt("power", 1));
            JSONArray jSONArray = jSONObject.getJSONArray("effects");
            for (int i = 0; i < jSONArray.length(); ++i) {
                JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                FireworkEffect fireworkEffect = FireworkEffectSerialization.getFireworkEffect(jSONObject2);
                if (fireworkEffect == null) continue;
                fireworkMeta.addEffect(fireworkEffect);
            }
            return fireworkMeta;
        }
        catch (JSONException var1_2) {
            var1_2.printStackTrace();
            return null;
        }
    }

    public static JSONObject serializeFireworkMeta(FireworkMeta fireworkMeta) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("power", fireworkMeta.getPower());
            JSONArray jSONArray = new JSONArray();
            for (FireworkEffect fireworkEffect : fireworkMeta.getEffects()) {
                jSONArray.put(FireworkEffectSerialization.serializeFireworkEffect(fireworkEffect));
            }
            jSONObject.put("effects", jSONArray);
            return jSONObject;
        }
        catch (JSONException var1_2) {
            var1_2.printStackTrace();
            return null;
        }
    }

    public static String serializeFireworkMetaAsString(FireworkMeta fireworkMeta) {
        return FireworkSerialization.serializeFireworkMetaAsString(fireworkMeta, false);
    }

    public static String serializeFireworkMetaAsString(FireworkMeta fireworkMeta, boolean bl) {
        return FireworkSerialization.serializeFireworkMetaAsString(fireworkMeta, false, 5);
    }

    public static String serializeFireworkMetaAsString(FireworkMeta fireworkMeta, boolean bl, int n) {
        return Serializer.toString(FireworkSerialization.serializeFireworkMeta(fireworkMeta), bl, n);
    }
}

