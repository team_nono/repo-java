/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.Location
 *  org.bukkit.entity.Horse
 *  org.bukkit.entity.Horse$Color
 *  org.bukkit.entity.Horse$Style
 *  org.bukkit.entity.Horse$Variant
 *  org.bukkit.entity.LivingEntity
 *  org.bukkit.inventory.HorseInventory
 *  org.bukkit.inventory.Inventory
 */
package com.ehaqui.ehlogin.lib.util.serialization;

import com.ehaqui.ehlogin.lib.util.json.JSONException;
import com.ehaqui.ehlogin.lib.util.json.JSONObject;
import com.ehaqui.ehlogin.lib.util.serialization.InventorySerialization;
import com.ehaqui.ehlogin.lib.util.serialization.LivingEntitySerialization;
import com.ehaqui.ehlogin.lib.util.serialization.PotionEffectSerialization;
import org.bukkit.Location;
import org.bukkit.entity.Horse;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.HorseInventory;
import org.bukkit.inventory.Inventory;

public class HorseSerialization {
    protected HorseSerialization() {
    }

    public static JSONObject serializeHorse(Horse horse) {
        try {
            JSONObject jSONObject = LivingEntitySerialization.serializeEntity((LivingEntity)horse);
            jSONObject.put("color", horse.getColor().name());
            jSONObject.put("inventory", InventorySerialization.serializeInventory((Inventory)horse.getInventory()));
            jSONObject.put("jump-strength", horse.getJumpStrength());
            jSONObject.put("style", (Object)horse.getStyle());
            jSONObject.put("variant", (Object)horse.getVariant());
            return jSONObject;
        }
        catch (JSONException var1_2) {
            var1_2.printStackTrace();
            return null;
        }
    }

    public static String serializeHorseAsString(Horse horse) {
        return HorseSerialization.serializeHorseAsString(horse, false);
    }

    public static String serializeHorseAsString(Horse horse, boolean bl) {
        return HorseSerialization.serializeHorseAsString(horse, bl, 5);
    }

    public static String serializeHorseAsString(Horse horse, boolean bl, int n) {
        try {
            if (bl) {
                return HorseSerialization.serializeHorse(horse).toString(n);
            }
            return HorseSerialization.serializeHorse(horse).toString();
        }
        catch (JSONException var3_3) {
            var3_3.printStackTrace();
            return null;
        }
    }

    public static Horse spawnHorse(Location location, String string) {
        try {
            return HorseSerialization.spawnHorse(location, new JSONObject(string));
        }
        catch (JSONException var2_2) {
            var2_2.printStackTrace();
            return null;
        }
    }

    public static Horse spawnHorse(Location location, JSONObject jSONObject) {
        try {
            Horse horse = (Horse)LivingEntitySerialization.spawnEntity(location, jSONObject);
            if (jSONObject.has("color")) {
                horse.setColor(Horse.Color.valueOf((String)jSONObject.getString("color")));
            }
            if (jSONObject.has("jump-strength")) {
                horse.setCustomName(jSONObject.getString("name"));
            }
            if (jSONObject.has("style")) {
                horse.setStyle(Horse.Style.valueOf((String)jSONObject.getString("style")));
            }
            if (jSONObject.has("inventory")) {
                PotionEffectSerialization.addPotionEffects(jSONObject.getString("potion-effects"), (LivingEntity)horse);
            }
            if (jSONObject.has("variant")) {
                horse.setVariant(Horse.Variant.valueOf((String)jSONObject.getString("variant")));
            }
            return horse;
        }
        catch (JSONException var2_3) {
            var2_3.printStackTrace();
            return null;
        }
    }
}

