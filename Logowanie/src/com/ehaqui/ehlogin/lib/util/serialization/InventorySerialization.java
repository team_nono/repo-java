/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.entity.Player
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.InventoryHolder
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.PlayerInventory
 */
package com.ehaqui.ehlogin.lib.util.serialization;

import com.ehaqui.ehlogin.lib.util.json.JSONArray;
import com.ehaqui.ehlogin.lib.util.json.JSONException;
import com.ehaqui.ehlogin.lib.util.json.JSONObject;
import com.ehaqui.ehlogin.lib.util.serialization.SingleItemSerialization;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class InventorySerialization {
    protected InventorySerialization() {
    }

    public static JSONArray serializeInventory(Inventory inventory) {
        JSONArray jSONArray = new JSONArray();
        for (int i = 0; i < inventory.getSize(); ++i) {
            JSONObject jSONObject = SingleItemSerialization.serializeItemInInventory(inventory.getItem(i), i);
            if (jSONObject == null) continue;
            jSONArray.put(jSONObject);
        }
        return jSONArray;
    }

    public static JSONObject serializePlayerInventory(PlayerInventory playerInventory) {
        try {
            JSONObject jSONObject = new JSONObject();
            JSONArray jSONArray = InventorySerialization.serializeInventory((Inventory)playerInventory);
            JSONArray jSONArray2 = InventorySerialization.serializeInventory(playerInventory.getArmorContents());
            jSONObject.put("inventory", jSONArray);
            jSONObject.put("armor", jSONArray2);
            return jSONObject;
        }
        catch (JSONException var1_2) {
            var1_2.printStackTrace();
            return null;
        }
    }

    public static String serializePlayerInventoryAsString(PlayerInventory playerInventory) {
        return InventorySerialization.serializePlayerInventoryAsString(playerInventory, false);
    }

    public static String serializePlayerInventoryAsString(PlayerInventory playerInventory, boolean bl) {
        return InventorySerialization.serializePlayerInventoryAsString(playerInventory, bl, 5);
    }

    public static String serializePlayerInventoryAsString(PlayerInventory playerInventory, boolean bl, int n) {
        try {
            if (bl) {
                return InventorySerialization.serializePlayerInventory(playerInventory).toString(n);
            }
            return InventorySerialization.serializePlayerInventory(playerInventory).toString();
        }
        catch (JSONException var3_3) {
            var3_3.printStackTrace();
            return null;
        }
    }

    public static String serializeInventoryAsString(Inventory inventory) {
        return InventorySerialization.serializeInventoryAsString(inventory, false);
    }

    public static String serializeInventoryAsString(Inventory inventory, boolean bl) {
        return InventorySerialization.serializeInventoryAsString(inventory, bl, 5);
    }

    public static String serializeInventoryAsString(Inventory inventory, boolean bl, int n) {
        try {
            if (bl) {
                return InventorySerialization.serializeInventory(inventory).toString(n);
            }
            return InventorySerialization.serializeInventory(inventory).toString();
        }
        catch (JSONException var3_3) {
            var3_3.printStackTrace();
            return null;
        }
    }

    public static String serializeInventoryAsString(ItemStack[] arritemStack) {
        return InventorySerialization.serializeInventoryAsString(arritemStack, false);
    }

    public static String serializeInventoryAsString(ItemStack[] arritemStack, boolean bl) {
        return InventorySerialization.serializeInventoryAsString(arritemStack, bl, 5);
    }

    public static String serializeInventoryAsString(ItemStack[] arritemStack, boolean bl, int n) {
        try {
            if (bl) {
                return InventorySerialization.serializeInventory(arritemStack).toString(n);
            }
            return InventorySerialization.serializeInventory(arritemStack).toString();
        }
        catch (JSONException var3_3) {
            var3_3.printStackTrace();
            return null;
        }
    }

    public static JSONArray serializeInventory(ItemStack[] arritemStack) {
        JSONArray jSONArray = new JSONArray();
        for (int i = 0; i < arritemStack.length; ++i) {
            JSONObject jSONObject = SingleItemSerialization.serializeItemInInventory(arritemStack[i], i);
            if (jSONObject == null) continue;
            jSONArray.put(jSONObject);
        }
        return jSONArray;
    }

    public static Inventory getInventory(String string) {
        Inventory inventory = Bukkit.createInventory((InventoryHolder)null, (int)54);
        inventory.setContents(InventorySerialization.getInventory(string, 54));
        return inventory;
    }

    public static ItemStack[] getInventory(String string, int n) {
        try {
            return InventorySerialization.getInventory(new JSONArray(string), n);
        }
        catch (JSONException var2_2) {
            var2_2.printStackTrace();
            return null;
        }
    }

    public static ItemStack[] getInventory(JSONArray jSONArray, int n) {
        try {
            ItemStack[] arritemStack = new ItemStack[n];
            for (int i = 0; i < jSONArray.length(); ++i) {
                ItemStack itemStack;
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                int n2 = jSONObject.getInt("index");
                if (n2 > n) {
                    throw new IllegalArgumentException("index found is greator than expected size (" + n2 + ">" + n + ")");
                }
                if (n2 > arritemStack.length || n2 < 0) {
                    throw new IllegalArgumentException("Item " + i + " - Slot " + n2 + " does not exist in this inventory");
                }
                arritemStack[n2] = itemStack = SingleItemSerialization.getItem(jSONObject);
            }
            return arritemStack;
        }
        catch (JSONException var2_3) {
            var2_3.printStackTrace();
            return null;
        }
    }

    public static ItemStack[] getInventory(File file, int n) {
        String string = "";
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                string = string + scanner.nextLine() + "\n";
            }
            scanner.close();
            return InventorySerialization.getInventory(string, n);
        }
        catch (FileNotFoundException var3_4) {
            var3_4.printStackTrace();
            return null;
        }
    }

    public static void setInventory(InventoryHolder inventoryHolder, String string) {
        InventorySerialization.setInventory(inventoryHolder.getInventory(), string);
    }

    public static void setInventory(InventoryHolder inventoryHolder, JSONArray jSONArray) {
        InventorySerialization.setInventory(inventoryHolder.getInventory(), jSONArray);
    }

    public static void setInventory(Inventory inventory, String string) {
        try {
            InventorySerialization.setInventory(inventory, new JSONArray(string));
        }
        catch (JSONException var2_2) {
            var2_2.printStackTrace();
        }
    }

    public static void setInventory(Inventory inventory, JSONArray jSONArray) {
        ItemStack[] arritemStack = InventorySerialization.getInventory(jSONArray, inventory.getSize());
        inventory.clear();
        for (int i = 0; i < arritemStack.length; ++i) {
            ItemStack itemStack = arritemStack[i];
            if (itemStack == null) continue;
            inventory.setItem(i, itemStack);
        }
    }

    public static void setPlayerInventory(Player player, String string) {
        try {
            InventorySerialization.setPlayerInventory(player, new JSONObject(string));
        }
        catch (JSONException var2_2) {
            var2_2.printStackTrace();
        }
    }

    public static void setPlayerInventory(Player player, JSONObject jSONObject) {
        try {
            PlayerInventory playerInventory = player.getInventory();
            ItemStack[] arritemStack = InventorySerialization.getInventory(jSONObject.getJSONArray("armor"), 4);
            playerInventory.clear();
            playerInventory.setArmorContents(arritemStack);
            InventorySerialization.setInventory((InventoryHolder)player, jSONObject.getJSONArray("inventory"));
        }
        catch (JSONException var2_3) {
            var2_3.printStackTrace();
        }
    }
}

