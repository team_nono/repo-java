/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.Color
 *  org.bukkit.Material
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 *  org.bukkit.inventory.meta.LeatherArmorMeta
 */
package com.ehaqui.ehlogin.lib.util.serialization;

import com.ehaqui.ehlogin.lib.util.json.JSONException;
import com.ehaqui.ehlogin.lib.util.json.JSONObject;
import com.ehaqui.ehlogin.lib.util.serialization.ColorSerialization;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

public class LeatherArmorSerialization {
    protected LeatherArmorSerialization() {
    }

    public static JSONObject serializeArmor(LeatherArmorMeta leatherArmorMeta) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("color", ColorSerialization.serializeColor(leatherArmorMeta.getColor()));
            return jSONObject;
        }
        catch (JSONException var1_2) {
            var1_2.printStackTrace();
            return null;
        }
    }

    public static String serializeArmorAsString(LeatherArmorMeta leatherArmorMeta) {
        return LeatherArmorSerialization.serializeArmorAsString(leatherArmorMeta, false);
    }

    public static String serializeArmorAsString(LeatherArmorMeta leatherArmorMeta, boolean bl) {
        return LeatherArmorSerialization.serializeArmorAsString(leatherArmorMeta, bl, 5);
    }

    public static String serializeArmorAsString(LeatherArmorMeta leatherArmorMeta, boolean bl, int n) {
        try {
            if (bl) {
                return LeatherArmorSerialization.serializeArmor(leatherArmorMeta).toString(n);
            }
            return LeatherArmorSerialization.serializeArmor(leatherArmorMeta).toString();
        }
        catch (JSONException var3_3) {
            var3_3.printStackTrace();
            return null;
        }
    }

    public static LeatherArmorMeta getLeatherArmorMeta(String string) {
        try {
            return LeatherArmorSerialization.getLeatherArmorMeta(new JSONObject(string));
        }
        catch (JSONException var1_1) {
            var1_1.printStackTrace();
            return null;
        }
    }

    public static LeatherArmorMeta getLeatherArmorMeta(JSONObject jSONObject) {
        try {
            ItemStack itemStack = new ItemStack(Material.LEATHER_HELMET, 1);
            LeatherArmorMeta leatherArmorMeta = (LeatherArmorMeta)itemStack.getItemMeta();
            if (jSONObject.has("color")) {
                leatherArmorMeta.setColor(ColorSerialization.getColor(jSONObject.getJSONObject("color")));
            }
            return leatherArmorMeta;
        }
        catch (JSONException var1_2) {
            var1_2.printStackTrace();
            return null;
        }
    }
}

