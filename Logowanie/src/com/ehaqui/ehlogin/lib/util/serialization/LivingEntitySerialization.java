/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.Location
 *  org.bukkit.World
 *  org.bukkit.entity.Ageable
 *  org.bukkit.entity.Entity
 *  org.bukkit.entity.EntityType
 *  org.bukkit.entity.LivingEntity
 */
package com.ehaqui.ehlogin.lib.util.serialization;

import com.ehaqui.ehlogin.lib.util.json.JSONException;
import com.ehaqui.ehlogin.lib.util.json.JSONObject;
import com.ehaqui.ehlogin.lib.util.serialization.PotionEffectSerialization;
import java.util.Collection;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Ageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;

public class LivingEntitySerialization {
    protected LivingEntitySerialization() {
    }

    public static JSONObject serializeEntity(LivingEntity livingEntity) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("name", livingEntity.getCustomName());
            jSONObject.put("potion-effects", PotionEffectSerialization.serializeEffects(livingEntity.getActivePotionEffects()));
            jSONObject.put("type", livingEntity.getType().getTypeId());
            return jSONObject;
        }
        catch (JSONException var1_2) {
            var1_2.printStackTrace();
            return null;
        }
    }

    public static String serializeEntityAsString(LivingEntity livingEntity) {
        return LivingEntitySerialization.serializeEntityAsString(livingEntity, false);
    }

    public static String serializeEntityAsString(LivingEntity livingEntity, boolean bl) {
        return LivingEntitySerialization.serializeEntityAsString(livingEntity, bl, 5);
    }

    public static String serializeEntityAsString(LivingEntity livingEntity, boolean bl, int n) {
        try {
            if (bl) {
                return LivingEntitySerialization.serializeEntity(livingEntity).toString(n);
            }
            return LivingEntitySerialization.serializeEntity(livingEntity).toString();
        }
        catch (JSONException var3_3) {
            var3_3.printStackTrace();
            return null;
        }
    }

    public static LivingEntity spawnEntity(Location location, String string) {
        try {
            return LivingEntitySerialization.spawnEntity(location, new JSONObject(string));
        }
        catch (JSONException var2_2) {
            var2_2.printStackTrace();
            return null;
        }
    }

    public static LivingEntity spawnEntity(Location location, JSONObject jSONObject) {
        try {
            if (!jSONObject.has("type")) {
                throw new IllegalArgumentException("The type of the entity cannot be determined");
            }
            LivingEntity livingEntity = (LivingEntity)location.getWorld().spawnEntity(location, EntityType.fromId((int)jSONObject.getInt("type")));
            if (jSONObject.has("age") && livingEntity instanceof Ageable) {
                ((Ageable)livingEntity).setAge(jSONObject.getInt("age"));
            }
            if (jSONObject.has("health")) {
                livingEntity.setHealth(jSONObject.getDouble("health"));
            }
            if (jSONObject.has("name")) {
                livingEntity.setCustomName(jSONObject.getString("name"));
            }
            if (jSONObject.has("potion-effects")) {
                // empty if block
            }
            PotionEffectSerialization.addPotionEffects(jSONObject.getString("potion-effects"), livingEntity);
            return livingEntity;
        }
        catch (JSONException var2_3) {
            var2_3.printStackTrace();
            return null;
        }
    }
}

