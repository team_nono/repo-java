/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.Location
 *  org.bukkit.World
 *  org.bukkit.WorldCreator
 */
package com.ehaqui.ehlogin.lib.util.serialization;

import com.ehaqui.ehlogin.lib.util.json.JSONException;
import com.ehaqui.ehlogin.lib.util.json.JSONObject;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;

public class LocationSerialization {
    protected LocationSerialization() {
    }

    public static JSONObject serializeLocation(Location location) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("x", location.getX());
            jSONObject.put("y", location.getY());
            jSONObject.put("z", location.getZ());
            jSONObject.put("pitch", location.getPitch());
            jSONObject.put("yaw", location.getYaw());
            jSONObject.put("world", location.getWorld().getName());
            return jSONObject;
        }
        catch (JSONException var1_2) {
            var1_2.printStackTrace();
            return null;
        }
    }

    public static String serializeLocationAsString(Location location) {
        return LocationSerialization.serializeLocationAsString(location, false);
    }

    public static String serializeLocationAsString(Location location, boolean bl) {
        return LocationSerialization.serializeLocationAsString(location, bl, 5);
    }

    public static String serializeLocationAsString(Location location, boolean bl, int n) {
        try {
            if (bl) {
                return LocationSerialization.serializeLocation(location).toString(n);
            }
            return LocationSerialization.serializeLocation(location).toString();
        }
        catch (JSONException var3_3) {
            var3_3.printStackTrace();
            return null;
        }
    }

    public static Location getLocationMeta(String string) {
        try {
            return LocationSerialization.getLocationMeta(new JSONObject(string));
        }
        catch (JSONException var1_1) {
            var1_1.printStackTrace();
            return null;
        }
    }

    public static Location getLocationMeta(JSONObject jSONObject) {
        try {
            World world = Bukkit.createWorld((WorldCreator)new WorldCreator(jSONObject.getString("world")));
            return new Location(world, jSONObject.getDouble("x"), jSONObject.getDouble("y"), jSONObject.getDouble("z"), Float.parseFloat("" + jSONObject.getDouble("yaw") + ""), Float.parseFloat("" + jSONObject.getDouble("pitch") + ""));
        }
        catch (JSONException var1_2) {
            var1_2.printStackTrace();
            return null;
        }
    }
}

