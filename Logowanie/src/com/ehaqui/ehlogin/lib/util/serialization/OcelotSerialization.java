/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.Location
 *  org.bukkit.entity.LivingEntity
 *  org.bukkit.entity.Ocelot
 *  org.bukkit.entity.Ocelot$Type
 */
package com.ehaqui.ehlogin.lib.util.serialization;

import com.ehaqui.ehlogin.lib.util.json.JSONException;
import com.ehaqui.ehlogin.lib.util.json.JSONObject;
import com.ehaqui.ehlogin.lib.util.serialization.LivingEntitySerialization;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Ocelot;

public class OcelotSerialization {
    protected OcelotSerialization() {
    }

    public static JSONObject serializeOcelot(Ocelot ocelot) {
        try {
            JSONObject jSONObject = LivingEntitySerialization.serializeEntity((LivingEntity)ocelot);
            jSONObject.put("type", ocelot.getCatType().name());
            return jSONObject;
        }
        catch (JSONException var1_2) {
            var1_2.printStackTrace();
            return null;
        }
    }

    public static String serializeOcelotAsString(Ocelot ocelot) {
        return OcelotSerialization.serializeOcelotAsString(ocelot, false);
    }

    public static String serializeOcelotAsString(Ocelot ocelot, boolean bl) {
        return OcelotSerialization.serializeOcelotAsString(ocelot, bl, 5);
    }

    public static String serializeOcelotAsString(Ocelot ocelot, boolean bl, int n) {
        try {
            if (bl) {
                return OcelotSerialization.serializeOcelot(ocelot).toString(n);
            }
            return OcelotSerialization.serializeOcelot(ocelot).toString();
        }
        catch (JSONException var3_3) {
            var3_3.printStackTrace();
            return null;
        }
    }

    public static Ocelot spawnOcelot(Location location, String string) {
        try {
            return OcelotSerialization.spawnOcelot(location, new JSONObject(string));
        }
        catch (JSONException var2_2) {
            var2_2.printStackTrace();
            return null;
        }
    }

    public static Ocelot spawnOcelot(Location location, JSONObject jSONObject) {
        try {
            Ocelot ocelot = (Ocelot)LivingEntitySerialization.spawnEntity(location, jSONObject);
            if (jSONObject.has("type")) {
                ocelot.setCatType(Ocelot.Type.valueOf((String)jSONObject.getString("type")));
            }
            return ocelot;
        }
        catch (JSONException var2_3) {
            var2_3.printStackTrace();
            return null;
        }
    }
}

