/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.entity.LivingEntity
 *  org.bukkit.entity.Player
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.PlayerInventory
 */
package com.ehaqui.ehlogin.lib.util.serialization;

import com.ehaqui.ehlogin.lib.util.json.JSONArray;
import com.ehaqui.ehlogin.lib.util.json.JSONException;
import com.ehaqui.ehlogin.lib.util.json.JSONObject;
import com.ehaqui.ehlogin.lib.util.serialization.InventorySerialization;
import com.ehaqui.ehlogin.lib.util.serialization.LivingEntitySerialization;
import com.ehaqui.ehlogin.lib.util.serialization.PlayerStatsSerialization;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.PlayerInventory;

public class PlayerSerialization {
    protected PlayerSerialization() {
    }

    public static JSONObject serializePlayer(Player player) {
        try {
            JSONObject jSONObject = LivingEntitySerialization.serializeEntity((LivingEntity)player);
            jSONObject.put("ender-chest", InventorySerialization.serializeInventory(player.getEnderChest()));
            jSONObject.put("inventory", InventorySerialization.serializePlayerInventory(player.getInventory()));
            jSONObject.put("stats", PlayerStatsSerialization.serializePlayerStats(player));
            return jSONObject;
        }
        catch (JSONException var1_2) {
            var1_2.printStackTrace();
            return null;
        }
    }

    public static String serializePlayerAsString(Player player) {
        return PlayerSerialization.serializePlayerAsString(player, false);
    }

    public static String serializePlayerAsString(Player player, boolean bl) {
        return PlayerSerialization.serializePlayerAsString(player, bl, 5);
    }

    public static String serializePlayerAsString(Player player, boolean bl, int n) {
        try {
            if (bl) {
                return PlayerSerialization.serializePlayer(player).toString(n);
            }
            return PlayerSerialization.serializePlayer(player).toString();
        }
        catch (JSONException var3_3) {
            var3_3.printStackTrace();
            return null;
        }
    }

    public static void setPlayer(String string, Player player) {
        try {
            PlayerSerialization.setPlayer(new JSONObject(string), player);
        }
        catch (JSONException var2_2) {
            var2_2.printStackTrace();
        }
    }

    public static void setPlayer(JSONObject jSONObject, Player player) {
        try {
            if (jSONObject.has("ender-chest")) {
                InventorySerialization.setInventory(player.getEnderChest(), jSONObject.getJSONArray("ender-chest"));
            }
            if (jSONObject.has("inventory")) {
                InventorySerialization.setPlayerInventory(player, jSONObject.getJSONObject("inventory"));
            }
            if (jSONObject.has("stats")) {
                PlayerStatsSerialization.applyPlayerStats(player, jSONObject.getJSONObject("stats"));
            }
        }
        catch (JSONException var2_2) {
            var2_2.printStackTrace();
        }
    }
}

