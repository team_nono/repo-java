/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.GameMode
 *  org.bukkit.entity.LivingEntity
 *  org.bukkit.entity.Player
 */
package com.ehaqui.ehlogin.lib.util.serialization;

import com.ehaqui.ehlogin.lib.util.json.JSONException;
import com.ehaqui.ehlogin.lib.util.json.JSONObject;
import com.ehaqui.ehlogin.lib.util.serialization.PotionEffectSerialization;
import java.util.Collection;
import org.bukkit.GameMode;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class PlayerStatsSerialization {
    protected PlayerStatsSerialization() {
    }

    public static JSONObject serializePlayerStats(Player player) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("can-fly", player.getAllowFlight());
            jSONObject.put("display-name", player.getDisplayName());
            jSONObject.put("exhaustion", player.getExhaustion());
            jSONObject.put("exp", player.getExp());
            jSONObject.put("flying", player.isFlying());
            jSONObject.put("food", player.getFoodLevel());
            jSONObject.put("gamemode", player.getGameMode().name());
            jSONObject.put("health", player.getHealthScale());
            jSONObject.put("level", player.getLevel());
            jSONObject.put("potion-effects", PotionEffectSerialization.serializeEffects(player.getActivePotionEffects()));
            jSONObject.put("saturation", player.getSaturation());
            return jSONObject;
        }
        catch (JSONException var1_2) {
            var1_2.printStackTrace();
            return null;
        }
    }

    public static String serializePlayerStatsAsString(Player player) {
        return PlayerStatsSerialization.serializePlayerStatsAsString(player, false);
    }

    public static String serializePlayerStatsAsString(Player player, boolean bl) {
        return PlayerStatsSerialization.serializePlayerStatsAsString(player, bl, 5);
    }

    public static String serializePlayerStatsAsString(Player player, boolean bl, int n) {
        try {
            if (bl) {
                return PlayerStatsSerialization.serializePlayerStats(player).toString(n);
            }
            return PlayerStatsSerialization.serializePlayerStats(player).toString();
        }
        catch (JSONException var3_3) {
            var3_3.printStackTrace();
            return null;
        }
    }

    public static void applyPlayerStats(Player player, String string) {
        try {
            PlayerStatsSerialization.applyPlayerStats(player, new JSONObject(string));
        }
        catch (JSONException var2_2) {
            var2_2.printStackTrace();
        }
    }

    public static void applyPlayerStats(Player player, JSONObject jSONObject) {
        try {
            if (jSONObject.has("can-fly")) {
                player.setAllowFlight(jSONObject.getBoolean("can-fly"));
            }
            if (jSONObject.has("display-name")) {
                player.setDisplayName(jSONObject.getString("display-name"));
            }
            if (jSONObject.has("exhaustion")) {
                player.setExhaustion((float)jSONObject.getDouble("exhaustion"));
            }
            if (jSONObject.has("exp")) {
                player.setExp((float)jSONObject.getDouble("exp"));
            }
            if (jSONObject.has("flying")) {
                player.setFlying(jSONObject.getBoolean("flying"));
            }
            if (jSONObject.has("food")) {
                player.setFoodLevel(jSONObject.getInt("food"));
            }
            if (jSONObject.has("health")) {
                player.setHealth(jSONObject.getDouble("health"));
            }
            if (jSONObject.has("gamemode")) {
                player.setGameMode(GameMode.valueOf((String)jSONObject.getString("gamemode")));
            }
            if (jSONObject.has("level")) {
                player.setLevel(jSONObject.getInt("level"));
            }
            if (jSONObject.has("potion-effects")) {
                PotionEffectSerialization.setPotionEffects(jSONObject.getString("potion-effects"), (LivingEntity)player);
            }
            if (jSONObject.has("saturation")) {
                player.setSaturation((float)jSONObject.getDouble("saturation"));
            }
        }
        catch (JSONException var2_2) {
            var2_2.printStackTrace();
        }
    }
}

