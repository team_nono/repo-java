/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.entity.LivingEntity
 *  org.bukkit.potion.PotionEffect
 *  org.bukkit.potion.PotionEffectType
 */
package com.ehaqui.ehlogin.lib.util.serialization;

import com.ehaqui.ehlogin.lib.util.serialization.Util;
import java.util.ArrayList;
import java.util.Collection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class PotionEffectSerialization {
    protected PotionEffectSerialization() {
    }

    public static String serializeEffects(Collection<PotionEffect> collection) {
        String string = "";
        for (PotionEffect potionEffect : collection) {
            string = string + potionEffect.getType().getId() + ":" + potionEffect.getDuration() + ":" + potionEffect.getAmplifier() + ";";
        }
        return string;
    }

    public static Collection<PotionEffect> getPotionEffects(String string) {
        ArrayList<PotionEffect> arrayList = new ArrayList<PotionEffect>();
        if (string.isEmpty()) {
            return arrayList;
        }
        String[] arrstring = string.split(";");
        for (int i = 0; i < arrstring.length; ++i) {
            String[] arrstring2 = arrstring[i].split(":");
            if (arrstring2.length < 3) {
                throw new IllegalArgumentException(string + " - PotionEffect " + i + " (" + arrstring[i] + "): split must at least have a length of 3");
            }
            if (!Util.isNum(arrstring2[0])) {
                throw new IllegalArgumentException(string + " - PotionEffect " + i + " (" + arrstring[i] + "): id is not an integer");
            }
            if (!Util.isNum(arrstring2[1])) {
                throw new IllegalArgumentException(string + " - PotionEffect " + i + " (" + arrstring[i] + "): duration is not an integer");
            }
            if (!Util.isNum(arrstring2[2])) {
                throw new IllegalArgumentException(string + " - PotionEffect " + i + " (" + arrstring[i] + "): amplifier is not an integer");
            }
            int n = Integer.parseInt(arrstring2[0]);
            int n2 = Integer.parseInt(arrstring2[1]);
            int n3 = Integer.parseInt(arrstring2[2]);
            PotionEffectType potionEffectType = PotionEffectType.getById((int)n);
            if (potionEffectType == null) {
                throw new IllegalArgumentException(string + " - PotionEffect " + i + " (" + arrstring[i] + "): no PotionEffectType with id of " + n);
            }
            PotionEffect potionEffect = new PotionEffect(potionEffectType, n2, n3);
            arrayList.add(potionEffect);
        }
        return arrayList;
    }

    public static void addPotionEffects(String string, LivingEntity livingEntity) {
        livingEntity.addPotionEffects(PotionEffectSerialization.getPotionEffects(string));
    }

    public static void setPotionEffects(String string, LivingEntity livingEntity) {
        for (PotionEffect potionEffect : livingEntity.getActivePotionEffects()) {
            livingEntity.removePotionEffect(potionEffect.getType());
        }
        PotionEffectSerialization.addPotionEffects(string, livingEntity);
    }
}

