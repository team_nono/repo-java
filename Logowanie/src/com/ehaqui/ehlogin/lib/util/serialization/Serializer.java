/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.lib.util.serialization;

import com.ehaqui.ehlogin.lib.util.json.JSONException;
import com.ehaqui.ehlogin.lib.util.json.JSONObject;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Scanner;

public class Serializer {
    public static String toString(JSONObject jSONObject) {
        return Serializer.toString(jSONObject, true);
    }

    public static String toString(JSONObject jSONObject, boolean bl) {
        return Serializer.toString(jSONObject, bl, 5);
    }

    public static String toString(JSONObject jSONObject, boolean bl, int n) {
        try {
            if (bl) {
                return jSONObject.toString(n);
            }
            return jSONObject.toString();
        }
        catch (JSONException var3_3) {
            var3_3.printStackTrace();
            return null;
        }
    }

    public static JSONObject getObjectFromFile(File file) {
        return Serializer.getObjectFromStream(new FileInputStream(file));
    }

    public static JSONObject getObjectFromStream(InputStream inputStream) {
        return new JSONObject(Serializer.getStringFromStream(inputStream));
    }

    public static String getStringFromFile(File file) {
        return Serializer.getStringFromStream(new FileInputStream(file));
    }

    public static String getStringFromStream(InputStream inputStream) {
        Scanner scanner = new Scanner(inputStream);
        String string = "";
        while (scanner.hasNextLine()) {
            string = string + scanner.nextLine() + "\n";
        }
        scanner.close();
        return string.trim();
    }
}

