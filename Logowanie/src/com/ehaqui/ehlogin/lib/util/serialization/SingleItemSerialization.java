/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.Material
 *  org.bukkit.enchantments.Enchantment
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.BookMeta
 *  org.bukkit.inventory.meta.EnchantmentStorageMeta
 *  org.bukkit.inventory.meta.FireworkMeta
 *  org.bukkit.inventory.meta.ItemMeta
 *  org.bukkit.inventory.meta.LeatherArmorMeta
 *  org.bukkit.inventory.meta.SkullMeta
 */
package com.ehaqui.ehlogin.lib.util.serialization;

import com.ehaqui.ehlogin.lib.util.json.JSONArray;
import com.ehaqui.ehlogin.lib.util.json.JSONException;
import com.ehaqui.ehlogin.lib.util.json.JSONObject;
import com.ehaqui.ehlogin.lib.util.serialization.BookSerialization;
import com.ehaqui.ehlogin.lib.util.serialization.EnchantmentSerialization;
import com.ehaqui.ehlogin.lib.util.serialization.FireworkSerialization;
import com.ehaqui.ehlogin.lib.util.serialization.LeatherArmorSerialization;
import com.ehaqui.ehlogin.lib.util.serialization.SkullSerialization;
import com.ehaqui.ehlogin.lib.util.serialization.Util;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

public class SingleItemSerialization {
    protected SingleItemSerialization() {
    }

    public static JSONObject serializeItemInInventory(ItemStack itemStack, int n) {
        return SingleItemSerialization.serializeItems(itemStack, true, n);
    }

    public static JSONObject serializeItem(ItemStack itemStack) {
        return SingleItemSerialization.serializeItems(itemStack, false, 0);
    }

    private static JSONObject serializeItems(ItemStack itemStack, boolean bl, int n) {
        try {
            JSONObject jSONObject = new JSONObject();
            if (itemStack == null) {
                return null;
            }
            int n2 = itemStack.getTypeId();
            int n3 = itemStack.getAmount();
            short s = itemStack.getDurability();
            boolean bl2 = itemStack.hasItemMeta();
            String string = null;
            String string2 = null;
            String[] arrstring = null;
            Material material = itemStack.getType();
            JSONObject jSONObject2 = null;
            JSONObject jSONObject3 = null;
            JSONObject jSONObject4 = null;
            JSONObject jSONObject5 = null;
            if (material == Material.BOOK_AND_QUILL || material == Material.WRITTEN_BOOK) {
                jSONObject2 = BookSerialization.serializeBookMeta((BookMeta)itemStack.getItemMeta());
            } else if (material == Material.ENCHANTED_BOOK) {
                jSONObject2 = BookSerialization.serializeEnchantedBookMeta((EnchantmentStorageMeta)itemStack.getItemMeta());
            } else if (Util.isLeatherArmor(material)) {
                jSONObject3 = LeatherArmorSerialization.serializeArmor((LeatherArmorMeta)itemStack.getItemMeta());
            } else if (material == Material.SKULL_ITEM) {
                jSONObject4 = SkullSerialization.serializeSkull((SkullMeta)itemStack.getItemMeta());
            } else if (material == Material.FIREWORK) {
                jSONObject5 = FireworkSerialization.serializeFireworkMeta((FireworkMeta)itemStack.getItemMeta());
            }
            if (bl2) {
                ItemMeta itemMeta = itemStack.getItemMeta();
                if (itemMeta.hasDisplayName()) {
                    string = itemMeta.getDisplayName();
                }
                if (itemMeta.hasLore()) {
                    arrstring = itemMeta.getLore().toArray(new String[0]);
                }
                if (itemMeta.hasEnchants()) {
                    string2 = EnchantmentSerialization.serializeEnchantments(itemMeta.getEnchants());
                }
            }
            jSONObject.put("id", n2);
            jSONObject.put("amount", n3);
            jSONObject.put("data", s);
            if (bl) {
                jSONObject.put("index", n);
            }
            if (string != null) {
                jSONObject.put("name", string);
            }
            if (string2 != null) {
                jSONObject.put("enchantments", string2);
            }
            if (arrstring != null) {
                jSONObject.put("lore", arrstring);
            }
            if (jSONObject2 != null && jSONObject2.length() > 0) {
                jSONObject.put("book-meta", jSONObject2);
            }
            if (jSONObject3 != null && jSONObject3.length() > 0) {
                jSONObject.put("armor-meta", jSONObject3);
            }
            if (jSONObject4 != null && jSONObject4.length() > 0) {
                jSONObject.put("skull-meta", jSONObject4);
            }
            if (jSONObject5 != null && jSONObject5.length() > 0) {
                jSONObject.put("firework-meta", jSONObject5);
            }
            return jSONObject;
        }
        catch (JSONException var3_4) {
            var3_4.printStackTrace();
            return null;
        }
    }

    public static ItemStack getItem(String string) {
        return SingleItemSerialization.getItem(string, 0);
    }

    public static ItemStack getItem(String string, int n) {
        try {
            return SingleItemSerialization.getItem(new JSONObject(string), n);
        }
        catch (JSONException var2_2) {
            var2_2.printStackTrace();
            return null;
        }
    }

    public static ItemStack getItem(JSONObject jSONObject) {
        return SingleItemSerialization.getItem(jSONObject, 0);
    }

    public static ItemStack getItem(JSONObject jSONObject, int n) {
        try {
            BookMeta bookMeta;
            JSONArray jSONArray;
            int n2 = jSONObject.getInt("id");
            int n3 = jSONObject.optInt("amount", 1);
            int n4 = jSONObject.optInt("data", 0);
            String string = null;
            Map<Enchantment, Integer> map = null;
            ArrayList<String> arrayList = null;
            if (jSONObject.has("name")) {
                string = jSONObject.getString("name");
            }
            if (jSONObject.has("enchantments")) {
                map = EnchantmentSerialization.getEnchantments(jSONObject.getString("enchantments"));
            }
            if (jSONObject.has("lore")) {
                jSONArray = jSONObject.getJSONArray("lore");
                arrayList = new ArrayList<String>();
                for (int i = 0; i < jSONArray.length(); ++i) {
                    arrayList.add(jSONArray.getString(i));
                }
            }
            if (Material.getMaterial((int)n2) == null) {
                throw new IllegalArgumentException("Item " + n + " - No Material found with id of " + n2);
            }
            jSONArray = Material.getMaterial((int)n2);
            ItemStack itemStack = new ItemStack((Material)jSONArray, n3, (short)n4);
            if ((jSONArray == Material.BOOK_AND_QUILL || jSONArray == Material.WRITTEN_BOOK) && jSONObject.has("book-meta")) {
                bookMeta = BookSerialization.getBookMeta(jSONObject.getJSONObject("book-meta"));
                itemStack.setItemMeta((ItemMeta)bookMeta);
            } else if (jSONArray == Material.ENCHANTED_BOOK && jSONObject.has("book-meta")) {
                bookMeta = BookSerialization.getEnchantedBookMeta(jSONObject.getJSONObject("book-meta"));
                itemStack.setItemMeta((ItemMeta)bookMeta);
            } else if (Util.isLeatherArmor((Material)jSONArray) && jSONObject.has("armor-meta")) {
                bookMeta = LeatherArmorSerialization.getLeatherArmorMeta(jSONObject.getJSONObject("armor-meta"));
                itemStack.setItemMeta((ItemMeta)bookMeta);
            } else if (jSONArray == Material.SKULL_ITEM && jSONObject.has("skull-meta")) {
                bookMeta = SkullSerialization.getSkullMeta(jSONObject.getJSONObject("skull-meta"));
                itemStack.setItemMeta((ItemMeta)bookMeta);
            } else if (jSONArray == Material.FIREWORK && jSONObject.has("firework-meta")) {
                bookMeta = FireworkSerialization.getFireworkMeta(jSONObject.getJSONObject("firework-meta"));
                itemStack.setItemMeta((ItemMeta)bookMeta);
            }
            bookMeta = itemStack.getItemMeta();
            if (string != null) {
                bookMeta.setDisplayName(string);
            }
            if (arrayList != null) {
                bookMeta.setLore(arrayList);
            }
            itemStack.setItemMeta((ItemMeta)bookMeta);
            if (map != null) {
                itemStack.addUnsafeEnchantments(map);
            }
            return itemStack;
        }
        catch (JSONException var2_3) {
            var2_3.printStackTrace();
            return null;
        }
    }

    public static String serializeItemInInventoryAsString(ItemStack itemStack, int n) {
        return SingleItemSerialization.serializeItemInInventoryAsString(itemStack, n, false);
    }

    public static String serializeItemInInventoryAsString(ItemStack itemStack, int n, boolean bl) {
        return SingleItemSerialization.serializeItemInInventoryAsString(itemStack, n, bl, 5);
    }

    public static String serializeItemInInventoryAsString(ItemStack itemStack, int n, boolean bl, int n2) {
        try {
            if (bl) {
                return SingleItemSerialization.serializeItemInInventory(itemStack, n).toString(n2);
            }
            return SingleItemSerialization.serializeItemInInventory(itemStack, n).toString();
        }
        catch (JSONException var4_4) {
            var4_4.printStackTrace();
            return null;
        }
    }

    public static String serializeItemAsString(ItemStack itemStack) {
        return SingleItemSerialization.serializeItemAsString(itemStack, false);
    }

    public static String serializeItemAsString(ItemStack itemStack, boolean bl) {
        return SingleItemSerialization.serializeItemAsString(itemStack, bl, 5);
    }

    public static String serializeItemAsString(ItemStack itemStack, boolean bl, int n) {
        try {
            if (bl) {
                return SingleItemSerialization.serializeItem(itemStack).toString(n);
            }
            return SingleItemSerialization.serializeItem(itemStack).toString();
        }
        catch (JSONException var3_3) {
            var3_3.printStackTrace();
            return null;
        }
    }
}

