/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.Material
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 *  org.bukkit.inventory.meta.SkullMeta
 */
package com.ehaqui.ehlogin.lib.util.serialization;

import com.ehaqui.ehlogin.lib.util.json.JSONException;
import com.ehaqui.ehlogin.lib.util.json.JSONObject;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

public class SkullSerialization {
    protected SkullSerialization() {
    }

    public static JSONObject serializeSkull(SkullMeta skullMeta) {
        try {
            JSONObject jSONObject = new JSONObject();
            if (skullMeta.hasOwner()) {
                jSONObject.put("owner", skullMeta.getOwner());
            }
            return jSONObject;
        }
        catch (JSONException var1_2) {
            var1_2.printStackTrace();
            return null;
        }
    }

    public static String serializeSkullAsString(SkullMeta skullMeta) {
        return SkullSerialization.serializeSkullAsString(skullMeta, false);
    }

    public static String serializeSkullAsString(SkullMeta skullMeta, boolean bl) {
        return SkullSerialization.serializeSkullAsString(skullMeta, bl, 5);
    }

    public static String serializeSkullAsString(SkullMeta skullMeta, boolean bl, int n) {
        try {
            if (bl) {
                return SkullSerialization.serializeSkull(skullMeta).toString(n);
            }
            return SkullSerialization.serializeSkull(skullMeta).toString();
        }
        catch (JSONException var3_3) {
            var3_3.printStackTrace();
            return null;
        }
    }

    public static SkullMeta getSkullMeta(String string) {
        try {
            return SkullSerialization.getSkullMeta(new JSONObject(string));
        }
        catch (JSONException var1_1) {
            var1_1.printStackTrace();
            return null;
        }
    }

    public static SkullMeta getSkullMeta(JSONObject jSONObject) {
        try {
            ItemStack itemStack = new ItemStack(Material.SKULL_ITEM);
            SkullMeta skullMeta = (SkullMeta)itemStack.getItemMeta();
            if (jSONObject.has("owner")) {
                skullMeta.setOwner(jSONObject.getString("owner"));
            }
            return skullMeta;
        }
        catch (JSONException var1_2) {
            var1_2.printStackTrace();
            return null;
        }
    }
}

