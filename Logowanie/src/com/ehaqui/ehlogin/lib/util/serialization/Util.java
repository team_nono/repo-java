/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.Material
 */
package com.ehaqui.ehlogin.lib.util.serialization;

import org.bukkit.Material;

public class Util {
    protected Util() {
    }

    public static boolean isNum(String string) {
        try {
            Integer.parseInt(string);
            return true;
        }
        catch (NumberFormatException var1_1) {
            return false;
        }
    }

    public static boolean isLeatherArmor(Material material) {
        return material == Material.LEATHER_HELMET || material == Material.LEATHER_CHESTPLATE || material == Material.LEATHER_LEGGINGS || material == Material.LEATHER_BOOTS;
    }

    public static boolean keyFound(String[] arrstring, String string) {
        for (String string2 : arrstring) {
            if (!string2.equalsIgnoreCase(string)) continue;
        }
        return false;
    }
}

