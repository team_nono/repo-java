/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.Color
 *  org.bukkit.DyeColor
 *  org.bukkit.Location
 *  org.bukkit.entity.LivingEntity
 *  org.bukkit.entity.Wolf
 */
package com.ehaqui.ehlogin.lib.util.serialization;

import com.ehaqui.ehlogin.lib.util.json.JSONException;
import com.ehaqui.ehlogin.lib.util.json.JSONObject;
import com.ehaqui.ehlogin.lib.util.serialization.ColorSerialization;
import com.ehaqui.ehlogin.lib.util.serialization.LivingEntitySerialization;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Wolf;

public class WolfSerialization {
    protected WolfSerialization() {
    }

    public static JSONObject serializeWolf(Wolf wolf) {
        try {
            JSONObject jSONObject = LivingEntitySerialization.serializeEntity((LivingEntity)wolf);
            jSONObject.put("collar-color", ColorSerialization.serializeColor(wolf.getCollarColor().getColor()));
            return jSONObject;
        }
        catch (JSONException var1_2) {
            var1_2.printStackTrace();
            return null;
        }
    }

    public static String serializeWolfAsString(Wolf wolf) {
        return WolfSerialization.serializeWolfAsString(wolf, false);
    }

    public static String serializeWolfAsString(Wolf wolf, boolean bl) {
        return WolfSerialization.serializeWolfAsString(wolf, bl, 5);
    }

    public static String serializeWolfAsString(Wolf wolf, boolean bl, int n) {
        try {
            if (bl) {
                return WolfSerialization.serializeWolf(wolf).toString(n);
            }
            return WolfSerialization.serializeWolf(wolf).toString();
        }
        catch (JSONException var3_3) {
            var3_3.printStackTrace();
            return null;
        }
    }

    public static Wolf spawnWolf(Location location, String string) {
        try {
            return WolfSerialization.spawnWolf(location, new JSONObject(string));
        }
        catch (JSONException var2_2) {
            var2_2.printStackTrace();
            return null;
        }
    }

    public static Wolf spawnWolf(Location location, JSONObject jSONObject) {
        try {
            Wolf wolf = (Wolf)LivingEntitySerialization.spawnEntity(location, jSONObject);
            wolf.setCollarColor(DyeColor.getByColor((Color)ColorSerialization.getColor(jSONObject.getString("collar-color"))));
            return wolf;
        }
        catch (JSONException var2_3) {
            var2_3.printStackTrace();
            return null;
        }
    }
}

