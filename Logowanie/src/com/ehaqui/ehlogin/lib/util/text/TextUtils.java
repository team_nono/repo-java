/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.lib.util.text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class TextUtils {
    public static String join(CharSequence charSequence, Object[] arrobject) {
        StringBuilder stringBuilder = new StringBuilder();
        boolean bl = true;
        for (Object object : arrobject) {
            if (bl) {
                bl = false;
            } else {
                stringBuilder.append(charSequence);
            }
            stringBuilder.append(object);
        }
        return stringBuilder.toString();
    }

    public static String join(CharSequence charSequence, Iterable<?> iterable) {
        StringBuilder stringBuilder = new StringBuilder();
        boolean bl = true;
        for (Object obj : iterable) {
            if (bl) {
                bl = false;
            } else {
                stringBuilder.append(charSequence);
            }
            stringBuilder.append(obj);
        }
        return stringBuilder.toString();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static String getStringFromInputStream(InputStream inputStream) {
        StringBuilder stringBuilder;
        BufferedReader bufferedReader = null;
        stringBuilder = new StringBuilder();
        try {
            String string;
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            while ((string = bufferedReader.readLine()) != null) {
                stringBuilder.append(string);
            }
        }
        catch (IOException var4_5) {
            var4_5.printStackTrace();
        }
        finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                }
                catch (IOException var4_6) {
                    var4_6.printStackTrace();
                }
            }
        }
        return stringBuilder.toString();
    }
}

