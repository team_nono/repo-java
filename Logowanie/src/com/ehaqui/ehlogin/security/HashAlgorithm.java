/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.security;

import com.ehaqui.ehlogin.security.crypts.description.BCRYPT2Y;
import com.ehaqui.ehlogin.security.crypts.description.CRAZYCRYPT1;
import com.ehaqui.ehlogin.security.crypts.description.DOUBLEMD5;
import com.ehaqui.ehlogin.security.crypts.description.EncryptionMethod;
import com.ehaqui.ehlogin.security.crypts.description.IPB3;
import com.ehaqui.ehlogin.security.crypts.description.IPB4;
import com.ehaqui.ehlogin.security.crypts.description.JOOMLA;
import com.ehaqui.ehlogin.security.crypts.description.MD5;
import com.ehaqui.ehlogin.security.crypts.description.MD5VB;
import com.ehaqui.ehlogin.security.crypts.description.MYBB;
import com.ehaqui.ehlogin.security.crypts.description.PHPBB;
import com.ehaqui.ehlogin.security.crypts.description.PHPFUSION;
import com.ehaqui.ehlogin.security.crypts.description.PLAINTEXT;
import com.ehaqui.ehlogin.security.crypts.description.ROYALAUTH;
import com.ehaqui.ehlogin.security.crypts.description.SALTEDSHA512;
import com.ehaqui.ehlogin.security.crypts.description.SHA1;
import com.ehaqui.ehlogin.security.crypts.description.SHA256;
import com.ehaqui.ehlogin.security.crypts.description.SHA512;
import com.ehaqui.ehlogin.security.crypts.description.SMF;
import com.ehaqui.ehlogin.security.crypts.description.WBB3;
import com.ehaqui.ehlogin.security.crypts.description.WBB4;
import com.ehaqui.ehlogin.security.crypts.description.WHIRLPOOL;
import com.ehaqui.ehlogin.security.crypts.description.WORDPRESS;
import com.ehaqui.ehlogin.security.crypts.description.XAUTH;
import com.ehaqui.ehlogin.security.crypts.description.XFBCRYPT;
import java.util.HashMap;

public enum HashAlgorithm {
    BCRYPT2Y(BCRYPT2Y.class),
    CRAZYCRYPT1(CRAZYCRYPT1.class),
    DOUBLEMD5(DOUBLEMD5.class),
    IPB3(IPB3.class),
    IPB4(IPB4.class),
    JOOMLA(JOOMLA.class),
    MD5(MD5.class),
    MD5VB(MD5VB.class),
    MYBB(MYBB.class),
    PHPBB(PHPBB.class),
    PHPFUSION(PHPFUSION.class),
    PLAINTEXT(PLAINTEXT.class),
    ROYALAUTH(ROYALAUTH.class),
    SALTEDSHA512(SALTEDSHA512.class),
    SHA1(SHA1.class),
    SHA256(SHA256.class),
    SHA512(SHA512.class),
    SMF(SMF.class),
    WBB3(WBB3.class),
    WBB4(WBB4.class),
    WHIRLPOOL(WHIRLPOOL.class),
    WORDPRESS(WORDPRESS.class),
    XAUTH(XAUTH.class),
    XFBCRYPT(XFBCRYPT.class),
    CUSTOM(null);
    
    private final Class<? extends EncryptionMethod> clazz;
    private static final HashMap<String, HashAlgorithm> _BYNAME;

    private HashAlgorithm(Class<? extends EncryptionMethod> class_) {
        this.clazz = class_;
    }

    public Class<? extends EncryptionMethod> getClazz() {
        return this.clazz;
    }

    public static HashAlgorithm getByName(String string) {
        HashAlgorithm hashAlgorithm = _BYNAME.get(string.toLowerCase());
        if (hashAlgorithm == null) {
            return CUSTOM;
        }
        return hashAlgorithm;
    }

    static {
        _BYNAME = new HashMap();
        for (HashAlgorithm hashAlgorithm : HashAlgorithm.values()) {
            _BYNAME.put(hashAlgorithm.name().toLowerCase(), hashAlgorithm);
        }
    }
}

