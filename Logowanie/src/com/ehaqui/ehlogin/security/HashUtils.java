/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.security;

import com.ehaqui.ehlogin.security.MessageDigestAlgorithm;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class HashUtils {
    private HashUtils() {
    }

    public static String sha1(String string) {
        return HashUtils.hash(string, MessageDigestAlgorithm.SHA1);
    }

    public static String sha256(String string) {
        return HashUtils.hash(string, MessageDigestAlgorithm.SHA256);
    }

    public static String sha512(String string) {
        return HashUtils.hash(string, MessageDigestAlgorithm.SHA512);
    }

    public static String md5(String string) {
        return HashUtils.hash(string, MessageDigestAlgorithm.MD5);
    }

    public static MessageDigest getDigest(MessageDigestAlgorithm messageDigestAlgorithm) {
        try {
            return MessageDigest.getInstance(messageDigestAlgorithm.getKey());
        }
        catch (NoSuchAlgorithmException var1_1) {
            throw new UnsupportedOperationException("Your system seems not to support the hash algorithm '" + messageDigestAlgorithm.getKey() + "'");
        }
    }

    private static String hash(String string, MessageDigestAlgorithm messageDigestAlgorithm) {
        MessageDigest messageDigest = HashUtils.getDigest(messageDigestAlgorithm);
        messageDigest.reset();
        messageDigest.update(string.getBytes());
        byte[] arrby = messageDigest.digest();
        return String.format("%0" + (arrby.length << 1) + "x", new BigInteger(1, arrby));
    }
}

