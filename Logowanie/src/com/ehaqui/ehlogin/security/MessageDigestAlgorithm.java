/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.security;

public enum MessageDigestAlgorithm {
    MD5("MD5"),
    SHA1("SHA-1"),
    SHA256("SHA-256"),
    SHA512("SHA-512");
    
    private final String key;

    private MessageDigestAlgorithm(String string2) {
        this.key = string2;
    }

    public String getKey() {
        return this.key;
    }
}

