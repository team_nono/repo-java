/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.security;

import com.ehaqui.ehlogin.User;
import com.ehaqui.ehlogin.security.HashAlgorithm;
import com.ehaqui.ehlogin.security.crypts.description.EncryptionMethod;
import com.ehaqui.ehlogin.security.crypts.description.HashedPassword;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class PasswordSecurity {
    private HashAlgorithm algorithm;

    public PasswordSecurity(String string) {
        this.algorithm = HashAlgorithm.getByName(string);
    }

    public HashedPassword computeHash(String string, String string2) {
        String string3 = string2.toLowerCase();
        EncryptionMethod encryptionMethod = PasswordSecurity.initializeEncryptionMethod(this.algorithm);
        return encryptionMethod.computeHash(string, string3);
    }

    public boolean comparePassword(String string, User user) {
        HashedPassword hashedPassword = user.getPassword();
        return hashedPassword != null && this.comparePassword(string, hashedPassword, user);
    }

    public boolean comparePassword(String string, HashedPassword hashedPassword, User user) {
        String string2;
        EncryptionMethod encryptionMethod = PasswordSecurity.initializeEncryptionMethod(this.algorithm);
        return PasswordSecurity.methodMatches(encryptionMethod, string, hashedPassword, string2 = user.getName().toLowerCase()) || this.compareWithAllEncryptionMethods(string, hashedPassword, string2);
    }

    public void reload(String string) {
        this.algorithm = HashAlgorithm.getByName(string);
    }

    private boolean compareWithAllEncryptionMethods(String string, HashedPassword hashedPassword, String string2) {
        for (HashAlgorithm hashAlgorithm : HashAlgorithm.values()) {
            EncryptionMethod encryptionMethod;
            if (HashAlgorithm.CUSTOM.equals((Object)hashAlgorithm) || !PasswordSecurity.methodMatches(encryptionMethod = PasswordSecurity.initializeEncryptionMethod(hashAlgorithm), string, hashedPassword, string2)) continue;
            return true;
        }
        return false;
    }

    private static boolean methodMatches(EncryptionMethod encryptionMethod, String string, HashedPassword hashedPassword, String string2) {
        return encryptionMethod != null && (!encryptionMethod.hasSeparateSalt() || hashedPassword.getSalt() != null) && encryptionMethod.comparePassword(string, hashedPassword, string2);
    }

    public static EncryptionMethod initializeEncryptionMethod(HashAlgorithm hashAlgorithm) {
        try {
            if (HashAlgorithm.CUSTOM.equals((Object)hashAlgorithm) || HashAlgorithm.PLAINTEXT.equals((Object)hashAlgorithm)) {
                return null;
            }
            Constructor constructor = hashAlgorithm.getClazz().getConstructors()[0];
            Class<?>[] arrclass = constructor.getParameterTypes();
            if (arrclass.length == 0) {
                return (EncryptionMethod)constructor.newInstance(new Object[0]);
            }
            throw new UnsupportedOperationException("Did not find default constructor or constructor with settings parameter in class " + hashAlgorithm.getClazz().getSimpleName());
        }
        catch (IllegalAccessException | InstantiationException | InvocationTargetException var1_2) {
            throw new UnsupportedOperationException("Constructor for '" + hashAlgorithm.getClazz().getSimpleName() + "' could not be invoked. (Is there no default constructor?)", var1_2);
        }
    }
}

