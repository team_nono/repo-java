/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.security;

import java.security.SecureRandom;
import java.util.Random;

public final class RandomString {
    private static final String CHARS = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final Random RANDOM = new SecureRandom();
    private static final int HEX_MAX_INDEX = 16;
    private static final int LOWER_ALPHANUMERIC_INDEX = 36;

    private RandomString() {
    }

    public static String generate(int n) {
        return RandomString.generate(n, 36);
    }

    public static String generateHex(int n) {
        return RandomString.generate(n, 16);
    }

    public static String generateLowerUpper(int n) {
        return RandomString.generate(n, "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".length());
    }

    private static String generate(int n, int n2) {
        if (n < 0) {
            throw new IllegalArgumentException("Length must be positive but was " + n);
        }
        StringBuilder stringBuilder = new StringBuilder(n);
        for (int i = 0; i < n; ++i) {
            stringBuilder.append("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".charAt(RANDOM.nextInt(n2)));
        }
        return stringBuilder.toString();
    }
}

