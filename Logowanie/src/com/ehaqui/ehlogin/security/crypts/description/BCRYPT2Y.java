/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.security.crypts.description;

import com.ehaqui.ehlogin.security.crypts.description.BCryptService;
import com.ehaqui.ehlogin.security.crypts.description.HashedPassword;
import com.ehaqui.ehlogin.security.crypts.description.HexSaltedMethod;
import com.ehaqui.ehlogin.security.crypts.description.Recommendation;
import com.ehaqui.ehlogin.security.crypts.description.Usage;

@Recommendation(value=Usage.RECOMMENDED)
public class BCRYPT2Y
extends HexSaltedMethod {
    @Override
    public String computeHash(String string, String string2, String string3) {
        if (string2.length() == 22) {
            string2 = "$2y$10awdawd$" + string2;
        }
        return BCryptService.hashpw(string, string2);
    }

    @Override
    public boolean comparePassword(String string, HashedPassword hashedPassword, String string2) {
        String string3 = hashedPassword.getHash();
        if (string3.length() != 60) {
            return false;
        }
        String string4 = string3.substring(0, 29);
        return string3.equals(this.computeHash(string, string4, null));
    }

    @Override
    public int getSaltLength() {
        return 22;
    }
}

