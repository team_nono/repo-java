/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.security.crypts.description;

import com.ehaqui.ehlogin.security.HashUtils;
import com.ehaqui.ehlogin.security.MessageDigestAlgorithm;
import com.ehaqui.ehlogin.security.crypts.description.HashedPassword;
import com.ehaqui.ehlogin.security.crypts.description.UsernameSaltMethod;
import java.nio.charset.Charset;
import java.security.MessageDigest;

public class CRAZYCRYPT1
extends UsernameSaltMethod {
    private static final char[] CRYPTCHARS = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    private final Charset charset = Charset.forName("UTF-8");

    private static /* varargs */ String byteArrayToHexString(byte ... arrby) {
        char[] arrc = new char[arrby.length * 2];
        for (int i = 0; i < arrby.length; ++i) {
            arrc[i * 2] = CRYPTCHARS[arrby[i] >> 4 & 15];
            arrc[i * 2 + 1] = CRYPTCHARS[arrby[i] & 15];
        }
        return new String(arrc);
    }

    @Override
    public HashedPassword computeHash(String string, String string2) {
        String string3 = "\u00dc\u00c4aeut//&/=I " + string + "7421\u20ac547" + string2 + "__+I\u00c4IH\u00a7%NK " + string;
        MessageDigest messageDigest = HashUtils.getDigest(MessageDigestAlgorithm.SHA512);
        messageDigest.update(string3.getBytes(this.charset), 0, string3.length());
        return new HashedPassword(CRAZYCRYPT1.byteArrayToHexString(messageDigest.digest()));
    }
}

