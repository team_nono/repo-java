/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.security.crypts.description;

import com.ehaqui.ehlogin.security.crypts.description.HashedPassword;

public interface EncryptionMethod {
    public HashedPassword computeHash(String var1, String var2);

    public String computeHash(String var1, String var2, String var3);

    public boolean comparePassword(String var1, HashedPassword var2, String var3);

    public String generateSalt();

    public boolean hasSeparateSalt();
}

