/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.security.crypts.description;

public class HashedPassword {
    private final String hash;
    private final String salt;

    public HashedPassword(String string, String string2) {
        this.hash = string;
        this.salt = string2;
    }

    public HashedPassword(String string) {
        this(string, null);
    }

    public String getHash() {
        return this.hash;
    }

    public String getSalt() {
        return this.salt;
    }
}

