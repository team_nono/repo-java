/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.security.crypts.description;

import com.ehaqui.ehlogin.security.RandomString;
import com.ehaqui.ehlogin.security.crypts.description.EncryptionMethod;
import com.ehaqui.ehlogin.security.crypts.description.HasSalt;
import com.ehaqui.ehlogin.security.crypts.description.HashedPassword;
import com.ehaqui.ehlogin.security.crypts.description.Recommendation;
import com.ehaqui.ehlogin.security.crypts.description.SaltType;
import com.ehaqui.ehlogin.security.crypts.description.Usage;

@Recommendation(value=Usage.ACCEPTABLE)
@HasSalt(value=SaltType.TEXT)
public abstract class HexSaltedMethod
implements EncryptionMethod {
    public abstract int getSaltLength();

    @Override
    public abstract String computeHash(String var1, String var2, String var3);

    @Override
    public HashedPassword computeHash(String string, String string2) {
        String string3 = this.generateSalt();
        return new HashedPassword(this.computeHash(string, string3, null));
    }

    @Override
    public abstract boolean comparePassword(String var1, HashedPassword var2, String var3);

    @Override
    public String generateSalt() {
        return RandomString.generateHex(this.getSaltLength());
    }

    @Override
    public boolean hasSeparateSalt() {
        return false;
    }
}

