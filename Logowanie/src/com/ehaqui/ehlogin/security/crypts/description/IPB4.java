/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  net.md_5.bungee.BungeeCord
 */
package com.ehaqui.ehlogin.security.crypts.description;

import com.ehaqui.ehlogin.security.RandomString;
import com.ehaqui.ehlogin.security.crypts.description.BCryptService;
import com.ehaqui.ehlogin.security.crypts.description.EncryptionMethod;
import com.ehaqui.ehlogin.security.crypts.description.HasSalt;
import com.ehaqui.ehlogin.security.crypts.description.HashedPassword;
import com.ehaqui.ehlogin.security.crypts.description.Recommendation;
import com.ehaqui.ehlogin.security.crypts.description.SaltType;
import com.ehaqui.ehlogin.security.crypts.description.Usage;
import java.util.logging.Logger;
import net.md_5.bungee.BungeeCord;

@Recommendation(value=Usage.DOES_NOT_WORK)
@HasSalt(value=SaltType.TEXT, length=22)
public class IPB4
implements EncryptionMethod {
    @Override
    public String computeHash(String string, String string2, String string3) {
        return BCryptService.hashpw(string, "$2a$13$" + string2);
    }

    @Override
    public HashedPassword computeHash(String string, String string2) {
        String string3 = this.generateSalt();
        return new HashedPassword(this.computeHash(string, string3, string2), string3);
    }

    @Override
    public boolean comparePassword(String string, HashedPassword hashedPassword, String string2) {
        try {
            return hashedPassword.getHash().length() > 3 && BCryptService.checkpw(string, hashedPassword.getHash());
        }
        catch (IllegalArgumentException var4_4) {
            BungeeCord.getInstance().getLogger().severe("Bcrypt checkpw() returned " + var4_4);
            return false;
        }
    }

    @Override
    public String generateSalt() {
        return RandomString.generateLowerUpper(22);
    }

    @Override
    public boolean hasSeparateSalt() {
        return true;
    }
}

