/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.security.crypts.description;

import com.ehaqui.ehlogin.security.HashUtils;
import com.ehaqui.ehlogin.security.crypts.description.HashedPassword;
import com.ehaqui.ehlogin.security.crypts.description.HexSaltedMethod;
import com.ehaqui.ehlogin.security.crypts.description.Recommendation;
import com.ehaqui.ehlogin.security.crypts.description.Usage;

@Recommendation(value=Usage.RECOMMENDED)
public class JOOMLA
extends HexSaltedMethod {
    @Override
    public String computeHash(String string, String string2, String string3) {
        return HashUtils.md5(new StringBuilder().append(string).append(string2).toString()) + ":" + string2;
    }

    @Override
    public boolean comparePassword(String string, HashedPassword hashedPassword, String string2) {
        String string3 = hashedPassword.getHash();
        String[] arrstring = string3.split(":");
        return arrstring.length == 2 && string3.equals(this.computeHash(string, arrstring[1], null));
    }

    @Override
    public int getSaltLength() {
        return 32;
    }
}

