/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.security.crypts.description;

import com.ehaqui.ehlogin.security.HashUtils;
import com.ehaqui.ehlogin.security.crypts.description.HashedPassword;
import com.ehaqui.ehlogin.security.crypts.description.HexSaltedMethod;

public class MD5VB
extends HexSaltedMethod {
    @Override
    public String computeHash(String string, String string2, String string3) {
        return "$MD5vb$" + string2 + "$" + HashUtils.md5(new StringBuilder().append(HashUtils.md5(string)).append(string2).toString());
    }

    @Override
    public boolean comparePassword(String string, HashedPassword hashedPassword, String string2) {
        String string3 = hashedPassword.getHash();
        String[] arrstring = string3.split("\\$");
        return arrstring.length == 4 && string3.equals(this.computeHash(string, arrstring[2], string2));
    }

    @Override
    public int getSaltLength() {
        return 16;
    }
}

