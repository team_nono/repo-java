/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.security.crypts.description;

import com.ehaqui.ehlogin.security.HashUtils;
import com.ehaqui.ehlogin.security.RandomString;
import com.ehaqui.ehlogin.security.crypts.description.HasSalt;
import com.ehaqui.ehlogin.security.crypts.description.Recommendation;
import com.ehaqui.ehlogin.security.crypts.description.SaltType;
import com.ehaqui.ehlogin.security.crypts.description.SeparateSaltMethod;
import com.ehaqui.ehlogin.security.crypts.description.Usage;

@Recommendation(value=Usage.ACCEPTABLE)
@HasSalt(value=SaltType.TEXT, length=8)
public class MYBB
extends SeparateSaltMethod {
    @Override
    public String computeHash(String string, String string2, String string3) {
        return HashUtils.md5(HashUtils.md5(string2) + HashUtils.md5(string));
    }

    @Override
    public String generateSalt() {
        return RandomString.generateLowerUpper(8);
    }
}

