/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.security.crypts.description;

import com.ehaqui.ehlogin.security.HashUtils;
import com.ehaqui.ehlogin.security.MessageDigestAlgorithm;
import com.ehaqui.ehlogin.security.crypts.description.HashedPassword;
import com.ehaqui.ehlogin.security.crypts.description.HexSaltedMethod;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;

public class PHPBB
extends HexSaltedMethod {
    private static final String itoa64 = "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    private static String md5(String string) {
        try {
            byte[] arrby = string.getBytes("ISO-8859-1");
            MessageDigest messageDigest = HashUtils.getDigest(MessageDigestAlgorithm.MD5);
            byte[] arrby2 = messageDigest.digest(arrby);
            return PHPBB.bytes2hex(arrby2);
        }
        catch (UnsupportedEncodingException var1_2) {
            throw new RuntimeException(var1_2);
        }
    }

    private static int hexToInt(char c) {
        if (c >= '0' && c <= '9') {
            return c - 48;
        }
        if ((c = Character.toUpperCase(c)) >= 'A' && c <= 'F') {
            return c - 65 + 10;
        }
        throw new IllegalArgumentException("Not a hex character: " + c);
    }

    private static String bytes2hex(byte[] arrby) {
        StringBuilder stringBuilder = new StringBuilder(32);
        for (byte by : arrby) {
            String string = Integer.toHexString(by & 255);
            if (string.length() < 2) {
                stringBuilder.append('0');
            }
            stringBuilder.append(string);
        }
        return stringBuilder.toString();
    }

    private static String pack(String string) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < string.length(); i += 2) {
            char c = string.charAt(i);
            char c2 = string.charAt(i + 1);
            char c3 = (char)(PHPBB.hexToInt(c) * 16 + PHPBB.hexToInt(c2));
            stringBuilder.append(c3);
        }
        return stringBuilder.toString();
    }

    private String phpbb_hash(String string, String string2) {
        String string3 = string2;
        StringBuilder stringBuilder = new StringBuilder();
        int n = 6;
        for (int i = 0; i < n; i += 16) {
            string3 = PHPBB.md5(string2 + string3);
            stringBuilder.append(PHPBB.pack(PHPBB.md5(string3)));
        }
        String string4 = this._hash_crypt_private(string, this._hash_gensalt_private(stringBuilder.substring(0, n), "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"));
        if (string4.length() == 34) {
            return string4;
        }
        return PHPBB.md5(string);
    }

    private String _hash_gensalt_private(String string, String string2) {
        return this._hash_gensalt_private(string, string2, 6);
    }

    private String _hash_gensalt_private(String string, String string2, int n) {
        if (n < 4 || n > 31) {
            n = 8;
        }
        String string3 = "$H$";
        string3 = string3 + string2.charAt(Math.min(n + 3, 30));
        string3 = string3 + this._hash_encode64(string, 6);
        return string3;
    }

    private String _hash_encode64(String string, int n) {
        StringBuilder stringBuilder = new StringBuilder();
        int n2 = 0;
        do {
            int n3 = string.charAt(n2++);
            stringBuilder.append("./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".charAt(n3 & 63));
            if (n2 < n) {
                n3 |= string.charAt(n2) << 8;
            }
            stringBuilder.append("./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".charAt(n3 >> 6 & 63));
            if (n2++ >= n) break;
            if (n2 < n) {
                n3 |= string.charAt(n2) << 16;
            }
            stringBuilder.append("./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".charAt(n3 >> 12 & 63));
            if (n2++ >= n) break;
            stringBuilder.append("./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".charAt(n3 >> 18 & 63));
        } while (n2 < n);
        return stringBuilder.toString();
    }

    private String _hash_crypt_private(String string, String string2) {
        String string3 = "*";
        if (!string2.substring(0, 3).equals("$H$")) {
            return string3;
        }
        int n = "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".indexOf(string2.charAt(3));
        if (n < 7 || n > 30) {
            return string3;
        }
        int n2 = 1 << n;
        String string4 = string2.substring(4, 12);
        if (string4.length() != 8) {
            return string3;
        }
        String string5 = PHPBB.md5(string4 + string);
        String string6 = PHPBB.pack(string5);
        do {
            string6 = PHPBB.pack(PHPBB.md5(string6 + string));
        } while (--n2 > 0);
        string3 = string2.substring(0, 12);
        string3 = string3 + this._hash_encode64(string6, 16);
        return string3;
    }

    private boolean phpbb_check_hash(String string, String string2) {
        if (string2.length() == 34) {
            return this._hash_crypt_private(string, string2).equals(string2);
        }
        return PHPBB.md5(string).equals(string2);
    }

    @Override
    public String computeHash(String string, String string2, String string3) {
        return this.phpbb_hash(string, string2);
    }

    @Override
    public boolean comparePassword(String string, HashedPassword hashedPassword, String string2) {
        return this.phpbb_check_hash(string, hashedPassword.getHash());
    }

    @Override
    public int getSaltLength() {
        return 16;
    }
}

