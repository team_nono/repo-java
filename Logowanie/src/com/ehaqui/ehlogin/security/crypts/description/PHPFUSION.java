/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.security.crypts.description;

import com.ehaqui.ehlogin.security.HashUtils;
import com.ehaqui.ehlogin.security.RandomString;
import com.ehaqui.ehlogin.security.crypts.description.AsciiRestricted;
import com.ehaqui.ehlogin.security.crypts.description.Recommendation;
import com.ehaqui.ehlogin.security.crypts.description.SeparateSaltMethod;
import com.ehaqui.ehlogin.security.crypts.description.Usage;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

@Recommendation(value=Usage.DO_NOT_USE)
@AsciiRestricted
public class PHPFUSION
extends SeparateSaltMethod {
    @Override
    public String computeHash(String string, String string2, String string3) {
        String string4 = "HmacSHA256";
        String string5 = HashUtils.sha1(string2);
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec(string5.getBytes("UTF-8"), string4);
            Mac mac = Mac.getInstance(string4);
            mac.init(secretKeySpec);
            byte[] arrby = mac.doFinal(string.getBytes("ASCII"));
            StringBuilder stringBuilder = new StringBuilder();
            for (byte by : arrby) {
                String string6 = Integer.toHexString(255 & by);
                if (string6.length() == 1) {
                    stringBuilder.append('0');
                }
                stringBuilder.append(string6);
            }
            return stringBuilder.toString();
        }
        catch (UnsupportedEncodingException | InvalidKeyException | NoSuchAlgorithmException var6_7) {
            throw new UnsupportedOperationException("Cannot create PHPFUSION hash for " + string3, var6_7);
        }
    }

    @Override
    public String generateSalt() {
        return RandomString.generateHex(12);
    }
}

