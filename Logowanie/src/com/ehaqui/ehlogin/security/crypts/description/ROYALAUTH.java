/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.security.crypts.description;

import com.ehaqui.ehlogin.security.HashUtils;
import com.ehaqui.ehlogin.security.crypts.description.UnsaltedMethod;

public class ROYALAUTH
extends UnsaltedMethod {
    @Override
    public String computeHash(String string) {
        for (int i = 0; i < 25; ++i) {
            string = HashUtils.sha512(string);
        }
        return string;
    }
}

