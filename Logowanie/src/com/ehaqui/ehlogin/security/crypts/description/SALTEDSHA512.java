/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.security.crypts.description;

import com.ehaqui.ehlogin.security.HashUtils;
import com.ehaqui.ehlogin.security.RandomString;
import com.ehaqui.ehlogin.security.crypts.description.Recommendation;
import com.ehaqui.ehlogin.security.crypts.description.SeparateSaltMethod;
import com.ehaqui.ehlogin.security.crypts.description.Usage;

@Recommendation(value=Usage.RECOMMENDED)
public class SALTEDSHA512
extends SeparateSaltMethod {
    @Override
    public String computeHash(String string, String string2, String string3) {
        return HashUtils.sha512(string + string2);
    }

    @Override
    public String generateSalt() {
        return RandomString.generateHex(32);
    }
}

