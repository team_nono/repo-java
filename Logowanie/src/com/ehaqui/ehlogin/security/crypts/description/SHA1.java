/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.security.crypts.description;

import com.ehaqui.ehlogin.security.HashUtils;
import com.ehaqui.ehlogin.security.crypts.description.UnsaltedMethod;

public class SHA1
extends UnsaltedMethod {
    @Override
    public String computeHash(String string) {
        return HashUtils.sha1(string);
    }
}

