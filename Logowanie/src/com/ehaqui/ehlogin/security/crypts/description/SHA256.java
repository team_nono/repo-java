/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.security.crypts.description;

import com.ehaqui.ehlogin.security.HashUtils;
import com.ehaqui.ehlogin.security.crypts.description.HashedPassword;
import com.ehaqui.ehlogin.security.crypts.description.HexSaltedMethod;
import com.ehaqui.ehlogin.security.crypts.description.Recommendation;
import com.ehaqui.ehlogin.security.crypts.description.Usage;

@Recommendation(value=Usage.RECOMMENDED)
public class SHA256
extends HexSaltedMethod {
    @Override
    public String computeHash(String string, String string2, String string3) {
        return "$SHA$" + string2 + "$" + HashUtils.sha256(new StringBuilder().append(HashUtils.sha256(string)).append(string2).toString());
    }

    @Override
    public boolean comparePassword(String string, HashedPassword hashedPassword, String string2) {
        String string3 = hashedPassword.getHash();
        String[] arrstring = string3.split("\\$");
        return arrstring.length == 4 && string3.equals(this.computeHash(string, arrstring[2], ""));
    }

    @Override
    public int getSaltLength() {
        return 16;
    }
}

