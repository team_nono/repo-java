/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.security.crypts.description;

import com.ehaqui.ehlogin.security.HashUtils;
import com.ehaqui.ehlogin.security.crypts.description.HashedPassword;
import com.ehaqui.ehlogin.security.crypts.description.UsernameSaltMethod;

public class SMF
extends UsernameSaltMethod {
    @Override
    public HashedPassword computeHash(String string, String string2) {
        return new HashedPassword(HashUtils.sha1(string2.toLowerCase() + string));
    }
}

