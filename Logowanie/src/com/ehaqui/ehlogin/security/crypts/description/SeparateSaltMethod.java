/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.security.crypts.description;

import com.ehaqui.ehlogin.security.crypts.description.EncryptionMethod;
import com.ehaqui.ehlogin.security.crypts.description.HashedPassword;

public abstract class SeparateSaltMethod
implements EncryptionMethod {
    @Override
    public abstract String computeHash(String var1, String var2, String var3);

    @Override
    public abstract String generateSalt();

    @Override
    public HashedPassword computeHash(String string, String string2) {
        String string3 = this.generateSalt();
        return new HashedPassword(this.computeHash(string, string3, string2), string3);
    }

    @Override
    public boolean comparePassword(String string, HashedPassword hashedPassword, String string2) {
        return hashedPassword.getHash().equals(this.computeHash(string, hashedPassword.getSalt(), null));
    }

    @Override
    public boolean hasSeparateSalt() {
        return true;
    }
}

