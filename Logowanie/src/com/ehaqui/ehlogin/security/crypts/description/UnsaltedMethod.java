/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.security.crypts.description;

import com.ehaqui.ehlogin.security.crypts.description.EncryptionMethod;
import com.ehaqui.ehlogin.security.crypts.description.HasSalt;
import com.ehaqui.ehlogin.security.crypts.description.HashedPassword;
import com.ehaqui.ehlogin.security.crypts.description.Recommendation;
import com.ehaqui.ehlogin.security.crypts.description.SaltType;
import com.ehaqui.ehlogin.security.crypts.description.Usage;

@Recommendation(value=Usage.DO_NOT_USE)
@HasSalt(value=SaltType.NONE)
public abstract class UnsaltedMethod
implements EncryptionMethod {
    public abstract String computeHash(String var1);

    @Override
    public HashedPassword computeHash(String string, String string2) {
        return new HashedPassword(this.computeHash(string));
    }

    @Override
    public String computeHash(String string, String string2, String string3) {
        return this.computeHash(string);
    }

    @Override
    public boolean comparePassword(String string, HashedPassword hashedPassword, String string2) {
        return hashedPassword.getHash().equals(this.computeHash(string));
    }

    @Override
    public String generateSalt() {
        return null;
    }

    @Override
    public boolean hasSeparateSalt() {
        return false;
    }
}

