/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.security.crypts.description;

public enum Usage {
    RECOMMENDED,
    ACCEPTABLE,
    DO_NOT_USE,
    DOES_NOT_WORK;
    

    private Usage() {
    }
}

