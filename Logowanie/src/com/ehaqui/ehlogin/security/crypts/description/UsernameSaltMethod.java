/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.security.crypts.description;

import com.ehaqui.ehlogin.security.crypts.description.EncryptionMethod;
import com.ehaqui.ehlogin.security.crypts.description.HasSalt;
import com.ehaqui.ehlogin.security.crypts.description.HashedPassword;
import com.ehaqui.ehlogin.security.crypts.description.Recommendation;
import com.ehaqui.ehlogin.security.crypts.description.SaltType;
import com.ehaqui.ehlogin.security.crypts.description.Usage;

@Recommendation(value=Usage.DO_NOT_USE)
@HasSalt(value=SaltType.USERNAME)
public abstract class UsernameSaltMethod
implements EncryptionMethod {
    @Override
    public abstract HashedPassword computeHash(String var1, String var2);

    @Override
    public boolean comparePassword(String string, HashedPassword hashedPassword, String string2) {
        return hashedPassword.getHash().equals(this.computeHash(string, string2).getHash());
    }

    @Override
    public String computeHash(String string, String string2, String string3) {
        return this.computeHash(string, string3).getHash();
    }

    @Override
    public String generateSalt() {
        return null;
    }

    @Override
    public boolean hasSeparateSalt() {
        return false;
    }
}

