/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.security.crypts.description;

import com.ehaqui.ehlogin.security.HashUtils;
import com.ehaqui.ehlogin.security.RandomString;
import com.ehaqui.ehlogin.security.crypts.description.HasSalt;
import com.ehaqui.ehlogin.security.crypts.description.Recommendation;
import com.ehaqui.ehlogin.security.crypts.description.SaltType;
import com.ehaqui.ehlogin.security.crypts.description.SeparateSaltMethod;
import com.ehaqui.ehlogin.security.crypts.description.Usage;

@Recommendation(value=Usage.ACCEPTABLE)
@HasSalt(value=SaltType.TEXT, length=40)
public class WBB3
extends SeparateSaltMethod {
    @Override
    public String computeHash(String string, String string2, String string3) {
        return HashUtils.sha1(string2.concat(HashUtils.sha1(string2.concat(HashUtils.sha1(string)))));
    }

    @Override
    public String generateSalt() {
        return RandomString.generateHex(40);
    }
}

