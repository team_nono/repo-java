/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.security.crypts.description;

import com.ehaqui.ehlogin.security.crypts.description.BCryptService;
import com.ehaqui.ehlogin.security.crypts.description.HashedPassword;
import com.ehaqui.ehlogin.security.crypts.description.HexSaltedMethod;
import com.ehaqui.ehlogin.security.crypts.description.Recommendation;
import com.ehaqui.ehlogin.security.crypts.description.Usage;

@Recommendation(value=Usage.RECOMMENDED)
public class WBB4
extends HexSaltedMethod {
    @Override
    public String computeHash(String string, String string2, String string3) {
        return BCryptService.hashpw(BCryptService.hashpw(string, string2), string2);
    }

    @Override
    public boolean comparePassword(String string, HashedPassword hashedPassword, String string2) {
        if (hashedPassword.getHash().length() != 60) {
            return false;
        }
        String string3 = hashedPassword.getHash().substring(0, 29);
        return this.computeHash(string, string3, null).equals(hashedPassword.getHash());
    }

    @Override
    public String generateSalt() {
        return BCryptService.gensalt(8);
    }

    @Override
    public int getSaltLength() {
        return 8;
    }
}

