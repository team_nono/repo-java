/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.security.crypts.description;

import com.ehaqui.ehlogin.security.crypts.description.UnsaltedMethod;
import java.util.Arrays;

public class WHIRLPOOL
extends UnsaltedMethod {
    public static final int DIGESTBITS = 512;
    public static final int DIGESTBYTES = 64;
    protected static final int R = 10;
    private static final String sbox = "\u1823\uc6e8\u87b8\u014f\u36a6\ud2f5\u796f\u9152\u60bc\u9b8e\ua30c\u7b35\u1de0\ud7c2\u2e4b\ufe57\u1577\u37e5\u9ff0\u4ada\u58c9\u290a\ub1a0\u6b85\ubd5d\u10f4\ucb3e\u0567\ue427\u418b\ua77d\u95d8\ufbee\u7c66\udd17\u479e\uca2d\ubf07\uad5a\u8333\u6302\uaa71\uc819\u49d9\uf2e3\u5b88\u9a26\u32b0\ue90f\ud580\ubecd\u3448\uff7a\u905f\u2068\u1aae\ub454\u9322\u64f1\u7312\u4008\uc3ec\udba1\u8d3d\u9700\ucf2b\u7682\ud61b\ub5af\u6a50\u45f3\u30ef\u3f55\ua2ea\u65ba\u2fc0\ude1c\ufd4d\u9275\u068a\ub2e6\u0e1f\u62d4\ua896\uf9c5\u2559\u8472\u394c\u5e78\u388c\ud1a5\ue261\ub321\u9c1e\u43c7\ufc04\u5199\u6d0d\ufadf\u7e24\u3bab\uce11\u8f4e\ub7eb\u3c81\u94f7\ub913\u2cd3\ue76e\uc403\u5644\u7fa9\u2abb\uc153\udc0b\u9d6c\u3174\uf646\uac89\u14e1\u163a\u6909\u70b6\ud0ed\ucc42\u98a4\u285c\uf886";
    private static final long[][] C;
    private static final long[] rc;
    protected final byte[] bitLength = new byte[32];
    protected final byte[] buffer = new byte[64];
    protected int bufferBits = 0;
    protected int bufferPos = 0;
    protected final long[] hash = new long[8];
    protected final long[] K = new long[8];
    protected final long[] L = new long[8];
    protected final long[] block = new long[8];
    protected final long[] state = new long[8];

    protected static String display(byte[] arrby) {
        char[] arrc = new char[2 * arrby.length];
        String string = "0123456789ABCDEF";
        for (int i = 0; i < arrby.length; ++i) {
            int n = arrby[i] & 255;
            arrc[2 * i] = string.charAt(n >>> 4);
            arrc[2 * i + 1] = string.charAt(n & 15);
        }
        return String.valueOf(arrc);
    }

    protected void processBuffer() {
        int n = 0;
        int n2 = 0;
        while (n < 8) {
            this.block[n] = (long)this.buffer[n2] << 56 ^ ((long)this.buffer[n2 + 1] & 255) << 48 ^ ((long)this.buffer[n2 + 2] & 255) << 40 ^ ((long)this.buffer[n2 + 3] & 255) << 32 ^ ((long)this.buffer[n2 + 4] & 255) << 24 ^ ((long)this.buffer[n2 + 5] & 255) << 16 ^ ((long)this.buffer[n2 + 6] & 255) << 8 ^ (long)this.buffer[n2 + 7] & 255;
            ++n;
            n2 += 8;
        }
        for (n = 0; n < 8; ++n) {
            this.K[n] = this.hash[n];
            this.state[n] = this.block[n] ^ this.K[n];
        }
        for (n = 1; n <= 10; ++n) {
            int n3;
            int n4;
            for (n2 = 0; n2 < 8; ++n2) {
                this.L[n2] = 0;
                n3 = 0;
                n4 = 56;
                while (n3 < 8) {
                    long[] arrl = this.L;
                    int n5 = n2;
                    arrl[n5] = arrl[n5] ^ C[n3][(int)(this.K[n2 - n3 & 7] >>> n4) & 255];
                    ++n3;
                    n4 -= 8;
                }
            }
            for (n2 = 0; n2 < 8; ++n2) {
                this.K[n2] = this.L[n2];
            }
            long[] arrl = this.K;
            arrl[0] = arrl[0] ^ rc[n];
            for (n2 = 0; n2 < 8; ++n2) {
                this.L[n2] = this.K[n2];
                n3 = 0;
                n4 = 56;
                while (n3 < 8) {
                    long[] arrl2 = this.L;
                    int n6 = n2;
                    arrl2[n6] = arrl2[n6] ^ C[n3][(int)(this.state[n2 - n3 & 7] >>> n4) & 255];
                    ++n3;
                    n4 -= 8;
                }
            }
            for (n2 = 0; n2 < 8; ++n2) {
                this.state[n2] = this.L[n2];
            }
        }
        for (n = 0; n < 8; ++n) {
            long[] arrl = this.hash;
            int n7 = n;
            arrl[n7] = arrl[n7] ^ (this.state[n] ^ this.block[n]);
        }
    }

    public void NESSIEinit() {
        Arrays.fill(this.bitLength, 0);
        this.bufferPos = 0;
        this.bufferBits = 0;
        this.buffer[0] = 0;
        Arrays.fill(this.hash, 0);
    }

    public void NESSIEadd(byte[] arrby, long l) {
        int n;
        int n2 = 0;
        int n3 = 8 - ((int)l & 7) & 7;
        int n4 = this.bufferBits & 7;
        long l2 = l;
        int n5 = 0;
        for (int i = 31; i >= 0; --i) {
            this.bitLength[i] = (byte)(n5 += (this.bitLength[i] & 255) + ((int)l2 & 255));
            n5 >>>= 8;
            l2 >>>= 8;
        }
        while (l > 8) {
            n = arrby[n2] << n3 & 255 | (arrby[n2 + 1] & 255) >>> 8 - n3;
            if (n < 0 || n >= 256) {
                throw new RuntimeException("LOGIC ERROR");
            }
            byte[] arrby2 = this.buffer;
            int n6 = this.bufferPos++;
            arrby2[n6] = (byte)(arrby2[n6] | n >>> n4);
            this.bufferBits += 8 - n4;
            if (this.bufferBits == 512) {
                this.processBuffer();
                this.bufferPos = 0;
                this.bufferBits = 0;
            }
            this.buffer[this.bufferPos] = (byte)(n << 8 - n4 & 255);
            this.bufferBits += n4;
            l -= 8;
            ++n2;
        }
        if (l > 0) {
            n = arrby[n2] << n3 & 255;
            byte[] arrby3 = this.buffer;
            int n7 = this.bufferPos;
            arrby3[n7] = (byte)(arrby3[n7] | n >>> n4);
        } else {
            n = 0;
        }
        if ((long)n4 + l < 8) {
            this.bufferBits = (int)((long)this.bufferBits + l);
        } else {
            ++this.bufferPos;
            this.bufferBits += 8 - n4;
            l -= (long)(8 - n4);
            if (this.bufferBits == 512) {
                this.processBuffer();
                this.bufferPos = 0;
                this.bufferBits = 0;
            }
            this.buffer[this.bufferPos] = (byte)(n << 8 - n4 & 255);
            this.bufferBits += (int)l;
        }
    }

    public void NESSIEfinalize(byte[] arrby) {
        byte[] arrby2 = this.buffer;
        int n = this.bufferPos++;
        arrby2[n] = (byte)(arrby2[n] | 128 >>> (this.bufferBits & 7));
        if (this.bufferPos > 32) {
            while (this.bufferPos < 64) {
                this.buffer[this.bufferPos++] = 0;
            }
            this.processBuffer();
            this.bufferPos = 0;
        }
        while (this.bufferPos < 32) {
            this.buffer[this.bufferPos++] = 0;
        }
        System.arraycopy(this.bitLength, 0, this.buffer, 32, 32);
        this.processBuffer();
        int n2 = 0;
        int n3 = 0;
        while (n2 < 8) {
            long l = this.hash[n2];
            arrby[n3] = (byte)(l >>> 56);
            arrby[n3 + 1] = (byte)(l >>> 48);
            arrby[n3 + 2] = (byte)(l >>> 40);
            arrby[n3 + 3] = (byte)(l >>> 32);
            arrby[n3 + 4] = (byte)(l >>> 24);
            arrby[n3 + 5] = (byte)(l >>> 16);
            arrby[n3 + 6] = (byte)(l >>> 8);
            arrby[n3 + 7] = (byte)l;
            ++n2;
            n3 += 8;
        }
    }

    public void NESSIEadd(String string) {
        if (string.length() > 0) {
            byte[] arrby = new byte[string.length()];
            for (int i = 0; i < string.length(); ++i) {
                arrby[i] = (byte)string.charAt(i);
            }
            this.NESSIEadd(arrby, 8 * arrby.length);
        }
    }

    @Override
    public String computeHash(String string) {
        byte[] arrby = new byte[64];
        this.NESSIEinit();
        this.NESSIEadd(string);
        this.NESSIEfinalize(arrby);
        return WHIRLPOOL.display(arrby);
    }

    static {
        int n;
        int n2;
        C = new long[8][256];
        rc = new long[11];
        for (n = 0; n < 256; ++n) {
            long l;
            n2 = "\u1823\uc6e8\u87b8\u014f\u36a6\ud2f5\u796f\u9152\u60bc\u9b8e\ua30c\u7b35\u1de0\ud7c2\u2e4b\ufe57\u1577\u37e5\u9ff0\u4ada\u58c9\u290a\ub1a0\u6b85\ubd5d\u10f4\ucb3e\u0567\ue427\u418b\ua77d\u95d8\ufbee\u7c66\udd17\u479e\uca2d\ubf07\uad5a\u8333\u6302\uaa71\uc819\u49d9\uf2e3\u5b88\u9a26\u32b0\ue90f\ud580\ubecd\u3448\uff7a\u905f\u2068\u1aae\ub454\u9322\u64f1\u7312\u4008\uc3ec\udba1\u8d3d\u9700\ucf2b\u7682\ud61b\ub5af\u6a50\u45f3\u30ef\u3f55\ua2ea\u65ba\u2fc0\ude1c\ufd4d\u9275\u068a\ub2e6\u0e1f\u62d4\ua896\uf9c5\u2559\u8472\u394c\u5e78\u388c\ud1a5\ue261\ub321\u9c1e\u43c7\ufc04\u5199\u6d0d\ufadf\u7e24\u3bab\uce11\u8f4e\ub7eb\u3c81\u94f7\ub913\u2cd3\ue76e\uc403\u5644\u7fa9\u2abb\uc153\udc0b\u9d6c\u3174\uf646\uac89\u14e1\u163a\u6909\u70b6\ud0ed\ucc42\u98a4\u285c\uf886".charAt(n / 2);
            long l2 = (n & 1) == 0 ? (long)(n2 >>> 8) : (long)(n2 & 255);
            long l3 = l2 << 1;
            if (l3 >= 256) {
                l3 ^= 285;
            }
            if ((l = l3 << 1) >= 256) {
                l ^= 285;
            }
            long l4 = l ^ l2;
            long l5 = l << 1;
            if (l5 >= 256) {
                l5 ^= 285;
            }
            long l6 = l5 ^ l2;
            WHIRLPOOL.C[0][n] = l2 << 56 | l2 << 48 | l << 40 | l2 << 32 | l5 << 24 | l4 << 16 | l3 << 8 | l6;
            for (int i = 1; i < 8; ++i) {
                WHIRLPOOL.C[i][n] = C[i - 1][n] >>> 8 | C[i - 1][n] << 56;
            }
        }
        WHIRLPOOL.rc[0] = 0;
        for (n = 1; n <= 10; ++n) {
            n2 = 8 * (n - 1);
            WHIRLPOOL.rc[n] = C[0][n2] & -72057594037927936L ^ C[1][n2 + 1] & 0xFF000000000000L ^ C[2][n2 + 2] & 0xFF0000000000L ^ C[3][n2 + 3] & 0xFF00000000L ^ C[4][n2 + 4] & 0xFF000000L ^ C[5][n2 + 5] & 0xFF0000 ^ C[6][n2 + 6] & 65280 ^ C[7][n2 + 7] & 255;
        }
    }
}

