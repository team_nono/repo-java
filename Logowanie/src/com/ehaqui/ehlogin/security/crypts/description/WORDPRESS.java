/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.security.crypts.description;

import com.ehaqui.ehlogin.security.HashUtils;
import com.ehaqui.ehlogin.security.MessageDigestAlgorithm;
import com.ehaqui.ehlogin.security.crypts.description.HasSalt;
import com.ehaqui.ehlogin.security.crypts.description.HashedPassword;
import com.ehaqui.ehlogin.security.crypts.description.Recommendation;
import com.ehaqui.ehlogin.security.crypts.description.SaltType;
import com.ehaqui.ehlogin.security.crypts.description.UnsaltedMethod;
import com.ehaqui.ehlogin.security.crypts.description.Usage;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Arrays;

@Recommendation(value=Usage.ACCEPTABLE)
@HasSalt(value=SaltType.TEXT, length=9)
public class WORDPRESS
extends UnsaltedMethod {
    private static final String itoa64 = "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private final SecureRandom randomGen = new SecureRandom();

    private String encode64(byte[] arrby, int n) {
        StringBuilder stringBuilder = new StringBuilder();
        int n2 = 0;
        if (arrby.length < n) {
            byte[] arrby2 = new byte[n];
            System.arraycopy(arrby, 0, arrby2, 0, arrby.length);
            Arrays.fill(arrby2, arrby.length, n - 1, 0);
            arrby = arrby2;
        }
        do {
            int n3 = arrby[n2] + (arrby[n2] < 0 ? 256 : 0);
            stringBuilder.append("./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".charAt(n3 & 63));
            if (++n2 < n) {
                n3 |= arrby[n2] + (arrby[n2] < 0 ? 256 : 0) << 8;
            }
            stringBuilder.append("./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".charAt(n3 >> 6 & 63));
            if (n2++ >= n) break;
            if (n2 < n) {
                n3 |= arrby[n2] + (arrby[n2] < 0 ? 256 : 0) << 16;
            }
            stringBuilder.append("./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".charAt(n3 >> 12 & 63));
            if (n2++ >= n) break;
            stringBuilder.append("./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".charAt(n3 >> 18 & 63));
        } while (n2 < n);
        return stringBuilder.toString();
    }

    private String crypt(String string, String string2) {
        String string3;
        String string4 = "*0";
        if ((string2.length() < 2 ? string2 : string2.substring(0, 2)).equalsIgnoreCase(string4)) {
            string4 = "*1";
        }
        String string5 = string3 = string2.length() < 3 ? string2 : string2.substring(0, 3);
        if (!string3.equals("$P$") && !string3.equals("$H$")) {
            return string4;
        }
        int n = "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".indexOf(string2.charAt(3));
        if (n < 7 || n > 30) {
            return string4;
        }
        int n2 = 1 << n;
        String string6 = string2.substring(4, 12);
        if (string6.length() != 8) {
            return string4;
        }
        MessageDigest messageDigest = HashUtils.getDigest(MessageDigestAlgorithm.MD5);
        byte[] arrby = this.stringToUtf8(string);
        byte[] arrby2 = messageDigest.digest(this.stringToUtf8(string6 + string));
        do {
            byte[] arrby3 = new byte[arrby2.length + arrby.length];
            System.arraycopy(arrby2, 0, arrby3, 0, arrby2.length);
            System.arraycopy(arrby, 0, arrby3, arrby2.length, arrby.length);
            arrby2 = messageDigest.digest(arrby3);
        } while (--n2 > 0);
        string4 = string2.substring(0, 12);
        string4 = string4 + this.encode64(arrby2, 16);
        return string4;
    }

    private String gensaltPrivate(byte[] arrby) {
        String string = "$P$";
        int n = 8;
        string = string + "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".charAt(Math.min(n + 5, 30));
        string = string + this.encode64(arrby, 6);
        return string;
    }

    private byte[] stringToUtf8(String string) {
        try {
            return string.getBytes("UTF-8");
        }
        catch (UnsupportedEncodingException var2_2) {
            throw new UnsupportedOperationException("This system doesn't support UTF-8!", var2_2);
        }
    }

    @Override
    public String computeHash(String string) {
        byte[] arrby = new byte[6];
        this.randomGen.nextBytes(arrby);
        return this.crypt(string, this.gensaltPrivate(this.stringToUtf8(new String(arrby))));
    }

    @Override
    public boolean comparePassword(String string, HashedPassword hashedPassword, String string2) {
        String string3 = hashedPassword.getHash();
        String string4 = this.crypt(string, string3);
        return string4.equals(string3);
    }
}

