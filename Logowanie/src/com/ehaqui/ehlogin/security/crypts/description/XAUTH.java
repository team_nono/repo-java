/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.security.crypts.description;

import com.ehaqui.ehlogin.security.crypts.description.HashedPassword;
import com.ehaqui.ehlogin.security.crypts.description.HexSaltedMethod;
import com.ehaqui.ehlogin.security.crypts.description.Recommendation;
import com.ehaqui.ehlogin.security.crypts.description.Usage;
import com.ehaqui.ehlogin.security.crypts.description.WHIRLPOOL;

@Recommendation(value=Usage.RECOMMENDED)
public class XAUTH
extends HexSaltedMethod {
    private static String getWhirlpool(String string) {
        WHIRLPOOL wHIRLPOOL = new WHIRLPOOL();
        byte[] arrby = new byte[64];
        wHIRLPOOL.NESSIEinit();
        wHIRLPOOL.NESSIEadd(string);
        wHIRLPOOL.NESSIEfinalize(arrby);
        return WHIRLPOOL.display(arrby);
    }

    @Override
    public String computeHash(String string, String string2, String string3) {
        String string4 = XAUTH.getWhirlpool(string2 + string).toLowerCase();
        int n = string.length() >= string4.length() ? string4.length() - 1 : string.length();
        return string4.substring(0, n) + string2 + string4.substring(n);
    }

    @Override
    public boolean comparePassword(String string, HashedPassword hashedPassword, String string2) {
        int n;
        String string3 = hashedPassword.getHash();
        int n2 = n = string.length() >= string3.length() ? string3.length() - 1 : string.length();
        if (n + 12 > string3.length()) {
            return false;
        }
        String string4 = string3.substring(n, n + 12);
        return string3.equals(this.computeHash(string, string4, null));
    }

    @Override
    public int getSaltLength() {
        return 12;
    }
}

