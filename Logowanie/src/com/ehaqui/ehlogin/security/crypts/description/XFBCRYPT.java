/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  net.md_5.bungee.BungeeCord
 */
package com.ehaqui.ehlogin.security.crypts.description;

import com.ehaqui.ehlogin.security.crypts.description.BCryptService;
import com.ehaqui.ehlogin.security.crypts.description.EncryptionMethod;
import com.ehaqui.ehlogin.security.crypts.description.HashedPassword;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.md_5.bungee.BungeeCord;

public class XFBCRYPT
implements EncryptionMethod {
    public static final String SCHEME_CLASS = "XenForo_Authentication_Core12";
    private static final Pattern HASH_PATTERN = Pattern.compile("\"hash\";s.*\"(.*)?\"");

    @Override
    public String generateSalt() {
        return BCryptService.gensalt();
    }

    @Override
    public String computeHash(String string, String string2, String string3) {
        return BCryptService.hashpw(string, string2);
    }

    @Override
    public HashedPassword computeHash(String string, String string2) {
        String string3 = this.generateSalt();
        return new HashedPassword(BCryptService.hashpw(string, string3), null);
    }

    @Override
    public boolean comparePassword(String string, HashedPassword hashedPassword, String string2) {
        try {
            return hashedPassword.getHash().length() > 3 && BCryptService.checkpw(string, hashedPassword.getHash());
        }
        catch (IllegalArgumentException var4_4) {
            BungeeCord.getInstance().getLogger().severe("XfBCrypt checkpw() returned " + var4_4);
            return false;
        }
    }

    @Override
    public boolean hasSeparateSalt() {
        return false;
    }

    public static String getHashFromBlob(byte[] arrby) {
        String string = new String(arrby);
        Matcher matcher = HASH_PATTERN.matcher(string);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return "*";
    }

    public static String serializeHash(String string) {
        return "a:1:{s:4:\"hash\";s:" + string.length() + ":\"" + string + "\";}";
    }
}

