/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.spigot;

import com.ehaqui.ehlogin.lib.config.Configuration;
import com.ehaqui.ehlogin.lib.database.Database;
import com.ehaqui.ehlogin.spigot.EhLoginSpigot;
import com.ehaqui.ehlogin.spigot.Setting;
import com.ehaqui.ehlogin.spigot.util.Logger;
import java.sql.Connection;

public class Connector {
    private static Database database;

    public static boolean setup() {
        Logger.info("Loading configuration", new Object[0]);
        Configuration.loadLocalConfig(EhLoginSpigot.getInstance(), Setting.class);
        if (!Connector.setupDatabase()) {
            return false;
        }
        return true;
    }

    private static boolean setupDatabase() {
        Logger.info("Connecting to Database", new Object[0]);
        database = new Database(EhLoginSpigot.getInstance().getLogger(), Setting.DATABASE_IP, null, Setting.DATABASE_NAME, Setting.DATABASE_USERNAME, Setting.DATABASE_PASSWORD);
        try {
            database.setup();
        }
        catch (Exception var0) {
            return false;
        }
        if (database.getConnection() != null) {
            return true;
        }
        return false;
    }

    public static Database getDatabase() {
        return database;
    }
}

