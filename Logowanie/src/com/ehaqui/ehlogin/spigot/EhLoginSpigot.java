/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.Server
 *  org.bukkit.event.Listener
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.plugin.java.JavaPlugin
 */
package com.ehaqui.ehlogin.spigot;

import com.ehaqui.ehlogin.spigot.Connector;
import com.ehaqui.ehlogin.spigot.listeners.BlockListener;
import com.ehaqui.ehlogin.spigot.listeners.EntityListener;
import com.ehaqui.ehlogin.spigot.listeners.PlayerListener;
import com.ehaqui.ehlogin.spigot.util.Logger;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public final class EhLoginSpigot
extends JavaPlugin {
    private static String USER = "250245";
    private static Plugin instance;

    public void onEnable() {
        EhLoginSpigot.loadConfig0();
        instance = this;
        if (!Connector.setup()) {
            this.getLogger().severe("Can't connect to Database. Stoping the server!");
            this.getServer().shutdown();
        }
        Bukkit.getPluginManager().registerEvents((Listener)new BlockListener(), (Plugin)this);
        Bukkit.getPluginManager().registerEvents((Listener)new EntityListener(), (Plugin)this);
        Bukkit.getPluginManager().registerEvents((Listener)new PlayerListener(), (Plugin)this);
        Logger.info("\tPurchase by https://www.spigotmc.org/members/." + USER + "/", new Object[0]);
        Logger.info("\tThank you for buying!", new Object[0]);
    }

    public static Plugin getInstance() {
        return instance;
    }
}

