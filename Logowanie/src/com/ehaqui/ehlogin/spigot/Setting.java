/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.spigot;

import com.ehaqui.ehlogin.lib.config.ConfigValue;
import java.util.Arrays;
import java.util.List;

public class Setting {
    @ConfigValue(value="version")
    public static int VERSION = 1;
    @ConfigValue(value="debug")
    public static boolean DEBUG = false;
    @ConfigValue(value="database.ip")
    public static String DATABASE_IP = "localhost";
    @ConfigValue(value="database.password")
    public static String DATABASE_PASSWORD = "passworld";
    @ConfigValue(value="database.username")
    public static String DATABASE_USERNAME = "username";
    @ConfigValue(value="database.name")
    public static String DATABASE_NAME = "minecraft";
    @ConfigValue(value="config.kick-not-autenticated")
    public static boolean KICK_NOT_AUTENTICATED = false;
    @ConfigValue(value="config.not-autenticated-message")
    public static String NOT_AUTENTICATED_MESSAGE = "&cYou are not logged. Type /login!";
    @ConfigValue(value="config.join.kick-message")
    public static String JOIN_KICK_MESSAGE = "You don't have permission to join!";
    @ConfigValue(value="config.join.only-premium")
    public static boolean JOIN_ONLY_PREMIUM = false;
    @ConfigValue(value="config.join.permission.enabled")
    public static boolean JOIN_WITH_PERMISSION = false;
    @ConfigValue(value="config.join.permission.permissions")
    public static List<String> JOIN_PERMISSIONS = Arrays.asList("permission.vip");

    public String toString() {
        return "Setting()";
    }
}

