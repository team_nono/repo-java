/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.OfflinePlayer
 *  org.bukkit.entity.Entity
 *  org.bukkit.entity.Player
 *  org.bukkit.potion.PotionEffectType
 */
package com.ehaqui.ehlogin.spigot;

import com.ehaqui.ehlogin.spigot.Connector;
import com.ehaqui.ehlogin.spigot.Setting;
import com.ehaqui.ehlogin.spigot.util.Logger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;

public class UserManager {
    private static List<UUID> autenticated = new ArrayList<UUID>();
    private static Map<Player, Long> lastSend = new HashMap<Player, Long>();

    public static boolean isLogged(Entity entity) {
        if (entity == null || !(entity instanceof Player)) {
            return true;
        }
        Player player = (Player)entity;
        return UserManager.isLogged(player);
    }

    public static boolean isLogged(Player player) {
        if (autenticated.contains(player.getUniqueId())) {
            return true;
        }
        try {
            PreparedStatement preparedStatement = Connector.getDatabase().getConnection().prepareStatement("SELECT name FROM `mc_users_sessions` WHERE name = ? AND status = 'LOGGED'");
            preparedStatement.setString(1, player.getName());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                autenticated.add(player.getUniqueId());
                player.removePotionEffect(PotionEffectType.BLINDNESS);
                Logger.info(player.getName() + " it's now autenticated!", new Object[0]);
                return true;
            }
        }
        catch (SQLException var1_2) {
            var1_2.printStackTrace();
        }
        return false;
    }

    public static void sendNotLogged(Player player) {
        long l = System.currentTimeMillis();
        if (!lastSend.containsKey((Object)player) || l - lastSend.get((Object)player) > 3000) {
            player.sendMessage(Setting.NOT_AUTENTICATED_MESSAGE);
            lastSend.put(player, l);
        }
    }

    public static boolean isPremium(OfflinePlayer offlinePlayer) {
        try {
            PreparedStatement preparedStatement = Connector.getDatabase().getConnection().prepareStatement("SELECT `premium` FROM `mc_user_infos` WHERE (`puuid` = ? OR `ouuid` = ?) AND premium = 'true' ");
            preparedStatement.setString(1, offlinePlayer.getUniqueId().toString());
            preparedStatement.setString(2, offlinePlayer.getUniqueId().toString());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return true;
            }
        }
        catch (Exception var1_2) {
            var1_2.printStackTrace();
        }
        return false;
    }

    public static List<UUID> getAutenticated() {
        return autenticated;
    }
}

