/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.entity.ArmorStand
 *  org.bukkit.entity.Entity
 *  org.bukkit.entity.HumanEntity
 *  org.bukkit.entity.ItemFrame
 *  org.bukkit.entity.Player
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.EventPriority
 *  org.bukkit.event.Listener
 *  org.bukkit.event.entity.EntityDamageByEntityEvent
 *  org.bukkit.event.entity.EntityDamageEvent
 *  org.bukkit.event.entity.EntityInteractEvent
 *  org.bukkit.event.entity.EntityTargetEvent
 *  org.bukkit.event.entity.FoodLevelChangeEvent
 *  org.bukkit.event.hanging.HangingBreakByEntityEvent
 *  org.bukkit.event.hanging.HangingBreakEvent
 */
package com.ehaqui.ehlogin.spigot.listeners;

import com.ehaqui.ehlogin.spigot.UserManager;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingBreakEvent;

public class EntityListener
implements Listener {
    @EventHandler(priority=EventPriority.LOWEST, ignoreCancelled=1)
    public void onHangingBreak(HangingBreakEvent hangingBreakEvent) {
        if (hangingBreakEvent.isCancelled()) {
            return;
        }
        if (hangingBreakEvent instanceof HangingBreakByEntityEvent) {
            this.onHangingBreakByEntity((HangingBreakByEntityEvent)hangingBreakEvent);
            return;
        }
    }

    private void onHangingBreakByEntity(HangingBreakByEntityEvent hangingBreakByEntityEvent) {
        boolean bl = false;
        if (hangingBreakByEntityEvent.getRemover() instanceof Player) {
            bl = UserManager.isLogged((Player)hangingBreakByEntityEvent.getRemover());
        }
        if (!bl) {
            hangingBreakByEntityEvent.setCancelled(true);
        }
    }

    @EventHandler(priority=EventPriority.LOWEST, ignoreCancelled=1)
    public void onEntityDamage(EntityDamageEvent entityDamageEvent) {
        if (entityDamageEvent.isCancelled()) {
            return;
        }
        if (entityDamageEvent instanceof EntityDamageByEntityEvent) {
            this.onEntityDamageByEntity((EntityDamageByEntityEvent)entityDamageEvent);
        }
    }

    private void onEntityDamageByEntity(EntityDamageByEntityEvent entityDamageByEntityEvent) {
        Entity entity = entityDamageByEntityEvent.getDamager();
        Entity entity2 = entityDamageByEntityEvent.getEntity();
        if (entity2 instanceof ItemFrame || entity2 instanceof ArmorStand) {
            boolean bl = false;
            if (entity instanceof Player) {
                bl = UserManager.isLogged((Player)entity);
            }
            if (!bl) {
                entityDamageByEntityEvent.setCancelled(true);
                return;
            }
        }
    }

    @EventHandler(ignoreCancelled=1, priority=EventPriority.LOWEST)
    public void onDamage(EntityDamageEvent entityDamageEvent) {
        if (!UserManager.isLogged(entityDamageEvent.getEntity())) {
            entityDamageEvent.getEntity().setFireTicks(0);
            entityDamageEvent.setDamage(0.0);
            entityDamageEvent.setCancelled(true);
        }
    }

    @EventHandler(ignoreCancelled=1, priority=EventPriority.LOWEST)
    public void onAttack(EntityDamageByEntityEvent entityDamageByEntityEvent) {
        if (!UserManager.isLogged(entityDamageByEntityEvent.getDamager())) {
            entityDamageByEntityEvent.setCancelled(true);
        }
    }

    @EventHandler(ignoreCancelled=1, priority=EventPriority.LOWEST)
    public void onEntityTarget(EntityTargetEvent entityTargetEvent) {
        if (!UserManager.isLogged(entityTargetEvent.getEntity())) {
            entityTargetEvent.setTarget(null);
            entityTargetEvent.setCancelled(true);
        }
    }

    @EventHandler(ignoreCancelled=1, priority=EventPriority.LOWEST)
    public void onFoodLevelChange(FoodLevelChangeEvent foodLevelChangeEvent) {
        if (!UserManager.isLogged((Entity)foodLevelChangeEvent.getEntity())) {
            foodLevelChangeEvent.setCancelled(true);
        }
    }

    @EventHandler(ignoreCancelled=1, priority=EventPriority.HIGHEST)
    public void onEntityInteract(EntityInteractEvent entityInteractEvent) {
        if (!UserManager.isLogged(entityInteractEvent.getEntity())) {
            entityInteractEvent.setCancelled(true);
        }
    }

    @EventHandler(ignoreCancelled=1, priority=EventPriority.LOWEST)
    public void onLowestEntityInteract(EntityInteractEvent entityInteractEvent) {
        if (!UserManager.isLogged(entityInteractEvent.getEntity())) {
            entityInteractEvent.setCancelled(true);
        }
    }
}

