/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.Location
 *  org.bukkit.Material
 *  org.bukkit.OfflinePlayer
 *  org.bukkit.block.Block
 *  org.bukkit.entity.EntityType
 *  org.bukkit.entity.HumanEntity
 *  org.bukkit.entity.Player
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.EventPriority
 *  org.bukkit.event.Listener
 *  org.bukkit.event.block.Action
 *  org.bukkit.event.entity.FoodLevelChangeEvent
 *  org.bukkit.event.inventory.InventoryClickEvent
 *  org.bukkit.event.inventory.InventoryInteractEvent
 *  org.bukkit.event.inventory.InventoryOpenEvent
 *  org.bukkit.event.player.AsyncPlayerChatEvent
 *  org.bukkit.event.player.PlayerArmorStandManipulateEvent
 *  org.bukkit.event.player.PlayerCommandPreprocessEvent
 *  org.bukkit.event.player.PlayerDropItemEvent
 *  org.bukkit.event.player.PlayerInteractEntityEvent
 *  org.bukkit.event.player.PlayerInteractEvent
 *  org.bukkit.event.player.PlayerJoinEvent
 *  org.bukkit.event.player.PlayerLoginEvent
 *  org.bukkit.event.player.PlayerLoginEvent$Result
 *  org.bukkit.event.player.PlayerMoveEvent
 *  org.bukkit.event.player.PlayerQuitEvent
 *  org.bukkit.potion.PotionEffect
 *  org.bukkit.potion.PotionEffectType
 */
package com.ehaqui.ehlogin.spigot.listeners;

import com.ehaqui.ehlogin.spigot.Setting;
import com.ehaqui.ehlogin.spigot.UserManager;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class PlayerListener
implements Listener {
    @EventHandler(priority=EventPriority.LOWEST, ignoreCancelled=1)
    public void onPlayerLogin(PlayerLoginEvent playerLoginEvent) {
        Player player = playerLoginEvent.getPlayer();
        UserManager.isLogged(player);
        if (!UserManager.isLogged(player) && Setting.KICK_NOT_AUTENTICATED) {
            playerLoginEvent.setResult(PlayerLoginEvent.Result.KICK_OTHER);
            playerLoginEvent.setKickMessage(Setting.NOT_AUTENTICATED_MESSAGE);
            return;
        }
        boolean bl = false;
        if (Setting.JOIN_WITH_PERMISSION) {
            for (String string : Setting.JOIN_PERMISSIONS) {
                if (!player.hasPermission(string)) continue;
                bl = true;
                break;
            }
        }
        if (Setting.JOIN_ONLY_PREMIUM && !bl && UserManager.isPremium((OfflinePlayer)player)) {
            bl = true;
        }
        if (!bl && (Setting.JOIN_WITH_PERMISSION || Setting.JOIN_ONLY_PREMIUM)) {
            playerLoginEvent.setResult(PlayerLoginEvent.Result.KICK_OTHER);
            playerLoginEvent.setKickMessage(Setting.JOIN_KICK_MESSAGE);
            return;
        }
    }

    @EventHandler(priority=EventPriority.LOWEST, ignoreCancelled=1)
    public void onPlayerQuit(PlayerQuitEvent playerQuitEvent) {
        Player player = playerQuitEvent.getPlayer();
        UserManager.getAutenticated().remove(player.getUniqueId());
        for (UUID uUID : UserManager.getAutenticated()) {
            if (Bukkit.getPlayer((UUID)uUID) != null && Bukkit.getPlayer((UUID)uUID).isOnline()) continue;
            UserManager.getAutenticated().remove(uUID);
        }
    }

    @EventHandler(priority=EventPriority.LOWEST, ignoreCancelled=1)
    public void onPlayerJoin(PlayerJoinEvent playerJoinEvent) {
        Player player = playerJoinEvent.getPlayer();
        if (!UserManager.isLogged(player)) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, Integer.MAX_VALUE, 1));
        }
    }

    @EventHandler(priority=EventPriority.LOWEST, ignoreCancelled=1)
    public void onPlayerCommand(PlayerCommandPreprocessEvent playerCommandPreprocessEvent) {
        Player player = playerCommandPreprocessEvent.getPlayer();
        if (!UserManager.isLogged(player)) {
            playerCommandPreprocessEvent.setCancelled(true);
            UserManager.sendNotLogged(player);
        }
    }

    @EventHandler(priority=EventPriority.LOWEST, ignoreCancelled=1)
    public void onPlayerChat(AsyncPlayerChatEvent asyncPlayerChatEvent) {
        Player player = asyncPlayerChatEvent.getPlayer();
        if (!UserManager.isLogged(player)) {
            asyncPlayerChatEvent.setCancelled(true);
            UserManager.sendNotLogged(player);
        }
    }

    @EventHandler(priority=EventPriority.LOWEST, ignoreCancelled=1)
    public void onPlayerDropItem(PlayerDropItemEvent playerDropItemEvent) {
        Player player = playerDropItemEvent.getPlayer();
        if (!UserManager.isLogged(player)) {
            playerDropItemEvent.setCancelled(true);
        }
    }

    @EventHandler(priority=EventPriority.LOWEST, ignoreCancelled=1)
    public void onPlayerInteract(PlayerInteractEvent playerInteractEvent) {
        Player player = playerInteractEvent.getPlayer();
        if (!UserManager.isLogged(player)) {
            playerInteractEvent.setCancelled(true);
        }
    }

    @EventHandler(priority=EventPriority.LOWEST, ignoreCancelled=1)
    public void onInventoryClickEvent(InventoryClickEvent inventoryClickEvent) {
        Player player;
        if (inventoryClickEvent.getWhoClicked() instanceof Player && !UserManager.isLogged(player = (Player)inventoryClickEvent.getWhoClicked())) {
            inventoryClickEvent.setCancelled(true);
        }
    }

    @EventHandler(priority=EventPriority.LOWEST, ignoreCancelled=1)
    public void onInventorOpenEvent(InventoryOpenEvent inventoryOpenEvent) {
        Player player;
        if (inventoryOpenEvent.getPlayer() instanceof Player && !UserManager.isLogged(player = (Player)inventoryOpenEvent.getPlayer())) {
            inventoryOpenEvent.setCancelled(true);
        }
    }

    @EventHandler(priority=EventPriority.LOWEST, ignoreCancelled=1)
    public void onInventoryInteractEvent(InventoryInteractEvent inventoryInteractEvent) {
        Player player;
        if (inventoryInteractEvent.getWhoClicked() instanceof Player && !UserManager.isLogged(player = (Player)inventoryInteractEvent.getWhoClicked())) {
            inventoryInteractEvent.setCancelled(true);
        }
    }

    @EventHandler(priority=EventPriority.LOWEST, ignoreCancelled=1)
    public void onHunger(FoodLevelChangeEvent foodLevelChangeEvent) {
        Player player;
        if (foodLevelChangeEvent.getEntityType() == EntityType.PLAYER && !UserManager.isLogged(player = (Player)foodLevelChangeEvent.getEntity())) {
            foodLevelChangeEvent.setCancelled(true);
        }
    }

    @EventHandler(priority=EventPriority.LOWEST, ignoreCancelled=1)
    public void onPlayerExtinguishFire(PlayerInteractEvent playerInteractEvent) {
        Player player = playerInteractEvent.getPlayer();
        if (playerInteractEvent.getAction() == Action.LEFT_CLICK_BLOCK) {
            Block block = player.getTargetBlock((Set)null, 5);
            if (block == null || block.getType() != Material.FIRE) {
                return;
            }
            if (!UserManager.isLogged(player)) {
                playerInteractEvent.setCancelled(true);
            }
        }
    }

    @EventHandler(priority=EventPriority.LOWEST, ignoreCancelled=1)
    public void onPlayerArmorStandManipulateEvent(PlayerArmorStandManipulateEvent playerArmorStandManipulateEvent) {
        Player player = playerArmorStandManipulateEvent.getPlayer();
        if (!UserManager.isLogged(player)) {
            playerArmorStandManipulateEvent.setCancelled(true);
        }
    }

    @EventHandler(priority=EventPriority.LOWEST, ignoreCancelled=1)
    public void onPlayerInteractEntity(PlayerInteractEntityEvent playerInteractEntityEvent) {
        Player player = playerInteractEntityEvent.getPlayer();
        if (!UserManager.isLogged(player)) {
            playerInteractEntityEvent.setCancelled(true);
        }
    }

    @EventHandler(priority=EventPriority.LOWEST, ignoreCancelled=1)
    public void onPlayerMove(PlayerMoveEvent playerMoveEvent) {
        Player player = playerMoveEvent.getPlayer();
        Location location = playerMoveEvent.getFrom();
        Location location2 = playerMoveEvent.getTo();
        if (location.getBlockX() == location2.getBlockX() && location.getBlockZ() == location2.getBlockZ() && location.getY() - location2.getY() >= 0.0) {
            return;
        }
        if (!UserManager.isLogged(player)) {
            playerMoveEvent.setTo(playerMoveEvent.getFrom());
            UserManager.sendNotLogged(player);
        }
    }
}

