/*
 * Decompiled with CFR 0_118.
 */
package com.ehaqui.ehlogin.spigot.util;

import com.ehaqui.ehlogin.spigot.EhLoginSpigot;
import com.ehaqui.ehlogin.spigot.Setting;

public class Logger {
    public static java.util.logging.Logger logger = EhLoginSpigot.getInstance().getLogger();

    public static /* varargs */ void info(String string, Object ... arrobject) {
        for (String string2 : String.format(string, arrobject).split("\n")) {
            logger.info(Logger.colorize("" + string2));
        }
    }

    public static /* varargs */ void warning(String string, Object ... arrobject) {
        for (String string2 : String.format(string, arrobject).split("\n")) {
            logger.warning(Logger.colorize("" + string2));
        }
    }

    public static /* varargs */ void severe(String string, Object ... arrobject) {
        for (String string2 : String.format(string, arrobject).split("\n")) {
            logger.severe(Logger.colorize("" + string2));
        }
    }

    public static /* varargs */ void error(String string, Object ... arrobject) {
        for (String string2 : String.format(string, arrobject).split("\n")) {
            Logger.info("&c[ERROR] &r%s", string2);
        }
    }

    public static /* varargs */ void debug(String string, Object ... arrobject) {
        if (Setting.DEBUG) {
            Logger.info(String.format(string, arrobject), new Object[0]);
        }
    }

    public static /* varargs */ void debugError(String string, Object ... arrobject) {
        if (Setting.DEBUG) {
            Logger.error(String.format(string, arrobject), new Object[0]);
        }
    }

    public static String colorize(String string) {
        if (string == null) {
            return null;
        }
        return string.replace("&", "\u00a7");
    }
}

