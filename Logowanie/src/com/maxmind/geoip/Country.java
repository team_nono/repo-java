/*
 * Decompiled with CFR 0_118.
 */
package com.maxmind.geoip;

public class Country {
    private String code;
    private String name;

    public Country(String string, String string2) {
        this.code = string;
        this.name = string2;
    }

    public String getCode() {
        return this.code;
    }

    public String getName() {
        return this.name;
    }
}

