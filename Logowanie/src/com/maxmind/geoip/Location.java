/*
 * Decompiled with CFR 0_118.
 */
package com.maxmind.geoip;

public class Location {
    public String countryCode;
    public String countryName;
    public String region;
    public String city;
    public String postalCode;
    public float latitude;
    public float longitude;
    public int dma_code;
    public int area_code;
    public int metro_code;
    private static final double EARTH_DIAMETER = 12756.4;
    private static final double PI = 3.14159265;
    private static final double RAD_CONVERT = 0.017453292500000002;

    public double distance(Location location) {
        float f = this.latitude;
        float f2 = this.longitude;
        float f3 = location.latitude;
        float f4 = location.longitude;
        f = (float)((double)f * 0.017453292500000002);
        f3 = (float)((double)f3 * 0.017453292500000002);
        double d = f3 - f;
        double d2 = (double)(f4 - f2) * 0.017453292500000002;
        double d3 = Math.pow(Math.sin(d / 2.0), 2.0) + Math.cos(f) * Math.cos(f3) * Math.pow(Math.sin(d2 / 2.0), 2.0);
        return 12756.4 * Math.atan2(Math.sqrt(d3), Math.sqrt(1.0 - d3));
    }
}

