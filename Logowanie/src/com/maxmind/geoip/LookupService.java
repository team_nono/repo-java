/*
 * Decompiled with CFR 0_118.
 */
package com.maxmind.geoip;

import com.maxmind.geoip.Country;
import com.maxmind.geoip.DatabaseInfo;
import com.maxmind.geoip.Location;
import com.maxmind.geoip.Region;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.io.RandomAccessFile;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class LookupService {
	private RandomAccessFile file = null;
	private File databaseFile = null;
	private DatabaseInfo databaseInfo = null;
	byte databaseType = 1;
	int[] databaseSegments;
	int recordLength;
	String licenseKey;
	int dboptions;
	byte[] dbbuffer;
	byte[] index_cache;
	long mtime;
	int last_netmask;
	private static final int US_OFFSET = 1;
	private static final int CANADA_OFFSET = 677;
	private static final int WORLD_OFFSET = 1353;
	private static final int FIPS_RANGE = 360;
	private static final int COUNTRY_BEGIN = 16776960;
	private static final int STATE_BEGIN_REV0 = 16700000;
	private static final int STATE_BEGIN_REV1 = 16000000;
	private static final int STRUCTURE_INFO_MAX_SIZE = 20;
	private static final int DATABASE_INFO_MAX_SIZE = 100;
	public static final int GEOIP_STANDARD = 0;
	public static final int GEOIP_MEMORY_CACHE = 1;
	public static final int GEOIP_CHECK_CACHE = 2;
	public static final int GEOIP_INDEX_CACHE = 4;
	public static final int GEOIP_UNKNOWN_SPEED = 0;
	public static final int GEOIP_DIALUP_SPEED = 1;
	public static final int GEOIP_CABLEDSL_SPEED = 2;
	public static final int GEOIP_CORPORATE_SPEED = 3;
	private static final int SEGMENT_RECORD_LENGTH = 3;
	private static final int STANDARD_RECORD_LENGTH = 3;
	private static final int ORG_RECORD_LENGTH = 4;
	private static final int MAX_RECORD_LENGTH = 4;
	private static final int MAX_ORG_RECORD_LENGTH = 300;
	private static final int FULL_RECORD_LENGTH = 60;
	private final Country UNKNOWN_COUNTRY = new Country("--", "N/A");
	private static final String[] countryCode = new String[] { "--", "AP", "EU", "AD", "AE", "AF", "AG", "AI", "AL",
			"AM", "CW", "AO", "AQ", "AR", "AS", "AT", "AU", "AW", "AZ", "BA", "BB", "BD", "BE", "BF", "BG", "BH", "BI",
			"BJ", "BM", "BN", "BO", "BR", "BS", "BT", "BV", "BW", "BY", "BZ", "CA", "CC", "CD", "CF", "CG", "CH", "CI",
			"CK", "CL", "CM", "CN", "CO", "CR", "CU", "CV", "CX", "CY", "CZ", "DE", "DJ", "DK", "DM", "DO", "DZ", "EC",
			"EE", "EG", "EH", "ER", "ES", "ET", "FI", "FJ", "FK", "FM", "FO", "FR", "SX", "GA", "GB", "GD", "GE", "GF",
			"GH", "GI", "GL", "GM", "GN", "GP", "GQ", "GR", "GS", "GT", "GU", "GW", "GY", "HK", "HM", "HN", "HR", "HT",
			"HU", "ID", "IE", "IL", "IN", "IO", "IQ", "IR", "IS", "IT", "JM", "JO", "JP", "KE", "KG", "KH", "KI", "KM",
			"KN", "KP", "KR", "KW", "KY", "KZ", "LA", "LB", "LC", "LI", "LK", "LR", "LS", "LT", "LU", "LV", "LY", "MA",
			"MC", "MD", "MG", "MH", "MK", "ML", "MM", "MN", "MO", "MP", "MQ", "MR", "MS", "MT", "MU", "MV", "MW", "MX",
			"MY", "MZ", "NA", "NC", "NE", "NF", "NG", "NI", "NL", "NO", "NP", "NR", "NU", "NZ", "OM", "PA", "PE", "PF",
			"PG", "PH", "PK", "PL", "PM", "PN", "PR", "PS", "PT", "PW", "PY", "QA", "RE", "RO", "RU", "RW", "SA", "SB",
			"SC", "SD", "SE", "SG", "SH", "SI", "SJ", "SK", "SL", "SM", "SN", "SO", "SR", "ST", "SV", "SY", "SZ", "TC",
			"TD", "TF", "TG", "TH", "TJ", "TK", "TM", "TN", "TO", "TL", "TR", "TT", "TV", "TW", "TZ", "UA", "UG", "UM",
			"US", "UY", "UZ", "VA", "VC", "VE", "VG", "VI", "VN", "VU", "WF", "WS", "YE", "YT", "RS", "ZA", "ZM", "ME",
			"ZW", "A1", "A2", "O1", "AX", "GG", "IM", "JE", "BL", "MF", "BQ", "SS", "O1" };
	private static final String[] countryName = new String[] { "N/A", "Asia/Pacific Region", "Europe", "Andorra",
			"United Arab Emirates", "Afghanistan", "Antigua and Barbuda", "Anguilla", "Albania", "Armenia", "Curacao",
			"Angola", "Antarctica", "Argentina", "American Samoa", "Austria", "Australia", "Aruba", "Azerbaijan",
			"Bosnia and Herzegovina", "Barbados", "Bangladesh", "Belgium", "Burkina Faso", "Bulgaria", "Bahrain",
			"Burundi", "Benin", "Bermuda", "Brunei Darussalam", "Bolivia", "Brazil", "Bahamas", "Bhutan",
			"Bouvet Island", "Botswana", "Belarus", "Belize", "Canada", "Cocos (Keeling) Islands",
			"Congo, The Democratic Republic of the", "Central African Republic", "Congo", "Switzerland",
			"Cote D'Ivoire", "Cook Islands", "Chile", "Cameroon", "China", "Colombia", "Costa Rica", "Cuba",
			"Cape Verde", "Christmas Island", "Cyprus", "Czech Republic", "Germany", "Djibouti", "Denmark", "Dominica",
			"Dominican Republic", "Algeria", "Ecuador", "Estonia", "Egypt", "Western Sahara", "Eritrea", "Spain",
			"Ethiopia", "Finland", "Fiji", "Falkland Islands (Malvinas)", "Micronesia, Federated States of",
			"Faroe Islands", "France", "Sint Maarten (Dutch part)", "Gabon", "United Kingdom", "Grenada", "Georgia",
			"French Guiana", "Ghana", "Gibraltar", "Greenland", "Gambia", "Guinea", "Guadeloupe", "Equatorial Guinea",
			"Greece", "South Georgia and the South Sandwich Islands", "Guatemala", "Guam", "Guinea-Bissau", "Guyana",
			"Hong Kong", "Heard Island and McDonald Islands", "Honduras", "Croatia", "Haiti", "Hungary", "Indonesia",
			"Ireland", "Israel", "India", "British Indian Ocean Territory", "Iraq", "Iran, Islamic Republic of",
			"Iceland", "Italy", "Jamaica", "Jordan", "Japan", "Kenya", "Kyrgyzstan", "Cambodia", "Kiribati", "Comoros",
			"Saint Kitts and Nevis", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait",
			"Cayman Islands", "Kazakhstan", "Lao People's Democratic Republic", "Lebanon", "Saint Lucia",
			"Liechtenstein", "Sri Lanka", "Liberia", "Lesotho", "Lithuania", "Luxembourg", "Latvia", "Libya", "Morocco",
			"Monaco", "Moldova, Republic of", "Madagascar", "Marshall Islands", "Macedonia", "Mali", "Myanmar",
			"Mongolia", "Macau", "Northern Mariana Islands", "Martinique", "Mauritania", "Montserrat", "Malta",
			"Mauritius", "Maldives", "Malawi", "Mexico", "Malaysia", "Mozambique", "Namibia", "New Caledonia", "Niger",
			"Norfolk Island", "Nigeria", "Nicaragua", "Netherlands", "Norway", "Nepal", "Nauru", "Niue", "New Zealand",
			"Oman", "Panama", "Peru", "French Polynesia", "Papua New Guinea", "Philippines", "Pakistan", "Poland",
			"Saint Pierre and Miquelon", "Pitcairn Islands", "Puerto Rico", "Palestinian Territory", "Portugal",
			"Palau", "Paraguay", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saudi Arabia",
			"Solomon Islands", "Seychelles", "Sudan", "Sweden", "Singapore", "Saint Helena", "Slovenia",
			"Svalbard and Jan Mayen", "Slovakia", "Sierra Leone", "San Marino", "Senegal", "Somalia", "Suriname",
			"Sao Tome and Principe", "El Salvador", "Syrian Arab Republic", "Swaziland", "Turks and Caicos Islands",
			"Chad", "French Southern Territories", "Togo", "Thailand", "Tajikistan", "Tokelau", "Turkmenistan",
			"Tunisia", "Tonga", "Timor-Leste", "Turkey", "Trinidad and Tobago", "Tuvalu", "Taiwan",
			"Tanzania, United Republic of", "Ukraine", "Uganda", "United States Minor Outlying Islands",
			"United States", "Uruguay", "Uzbekistan", "Holy See (Vatican City State)",
			"Saint Vincent and the Grenadines", "Venezuela", "Virgin Islands, British", "Virgin Islands, U.S.",
			"Vietnam", "Vanuatu", "Wallis and Futuna", "Samoa", "Yemen", "Mayotte", "Serbia", "South Africa", "Zambia",
			"Montenegro", "Zimbabwe", "Anonymous Proxy", "Satellite Provider", "Other", "Aland Islands", "Guernsey",
			"Isle of Man", "Jersey", "Saint Barthelemy", "Saint Martin", "Bonaire, Saint Eustatius and Saba",
			"South Sudan", "Other" };

	public LookupService(String string) {
		this(new File(string));
	}

	public LookupService(File file) {
		this.databaseFile = file;
		try {
			this.file = new RandomAccessFile(file, "r");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.init();
	}

	public LookupService(String string, int n) {
		this(new File(string), n);
	}

	public LookupService(File file, int n) {
		this.databaseFile = file;
		try {
			this.file = new RandomAccessFile(file, "r");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.dboptions = n;
		this.init();
	}

	private synchronized void init() {
		int n;
		byte[] arrby = new byte[3];
		byte[] arrby2 = new byte[3];
		if (this.file == null) {
			return;
		}
		if ((this.dboptions & 2) != 0) {
			this.mtime = this.databaseFile.lastModified();
		}
		try {
			this.file.seek(this.file.length() - 3);

			for (int i = 0; i < 20; ++i) {
				this.file.readFully(arrby);
				if (arrby[0] == -1 && arrby[1] == -1 && arrby[2] == -1) {
					this.databaseType = this.file.readByte();
					if (this.databaseType >= 106) {
						this.databaseType = (byte) (this.databaseType - 105);
					}
					if (this.databaseType == 7) {
						this.databaseSegments = new int[1];
						this.databaseSegments[0] = 16700000;
						this.recordLength = 3;
						break;
					}
					if (this.databaseType == 3) {
						this.databaseSegments = new int[1];
						this.databaseSegments[0] = 16000000;
						this.recordLength = 3;
						break;
					}
					if (this.databaseType != 6 && this.databaseType != 2 && this.databaseType != 5
							&& this.databaseType != 23 && this.databaseType != 4 && this.databaseType != 22
							&& this.databaseType != 11 && this.databaseType != 24 && this.databaseType != 9
							&& this.databaseType != 21 && this.databaseType != 32 && this.databaseType != 33
							&& this.databaseType != 31 && this.databaseType != 30)
						break;
					this.databaseSegments = new int[1];
					this.databaseSegments[0] = 0;
					this.recordLength = this.databaseType == 6 || this.databaseType == 2 || this.databaseType == 21
							|| this.databaseType == 32 || this.databaseType == 33 || this.databaseType == 31
							|| this.databaseType == 30 || this.databaseType == 9 ? 3 : 4;
					this.file.readFully(arrby2);
					for (int j = 0; j < 3; ++j) {
						int[] arrn = this.databaseSegments;
						arrn[0] = arrn[0] + (LookupService.unsignedByteToInt(arrby2[j]) << j * 8);
					}
					break;
				}
				this.file.seek(this.file.getFilePointer() - 4);
			}

			if (this.databaseType == 1 || this.databaseType == 12 || this.databaseType == 8
					|| this.databaseType == 10) {
				this.databaseSegments = new int[1];
				this.databaseSegments[0] = 16776960;
				this.recordLength = 3;
			}
			if ((this.dboptions & 1) == 1) {
				n = (int) this.file.length();
				this.dbbuffer = new byte[n];
				this.file.seek(0);
				this.file.readFully(this.dbbuffer, 0, n);
				this.databaseInfo = this.getDatabaseInfo();
				this.file.close();
			}
			if ((this.dboptions & 4) != 0) {
				n = this.databaseSegments[0] * this.recordLength * 2;
				this.index_cache = new byte[n];
				if (this.index_cache != null) {
					this.file.seek(0);
					this.file.readFully(this.index_cache, 0, n);
				}
			} else {
				this.index_cache = null;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public synchronized void close() {
		try {
			if (this.file != null) {
				this.file.close();
			}
			this.file = null;
		} catch (IOException var1_1) {
			// empty catch block
		}
	}

	public Country getCountryV6(String string) {
		InetAddress inetAddress;
		try {
			inetAddress = InetAddress.getByName(string);
		} catch (UnknownHostException var3_3) {
			return this.UNKNOWN_COUNTRY;
		}
		return this.getCountryV6(inetAddress);
	}

	public Country getCountry(String string) {
		InetAddress inetAddress;
		try {
			inetAddress = InetAddress.getByName(string);
		} catch (UnknownHostException var3_3) {
			return this.UNKNOWN_COUNTRY;
		}
		return this.getCountry(LookupService.bytesToLong(inetAddress.getAddress()));
	}

	public synchronized Country getCountry(InetAddress inetAddress) {
		return this.getCountry(LookupService.bytesToLong(inetAddress.getAddress()));
	}

	public synchronized Country getCountryV6(InetAddress inetAddress) {
		if (this.file == null && (this.dboptions & 1) == 0) {
			throw new IllegalStateException("Database has been closed.");
		}
		int n = this.seekCountryV6(inetAddress) - 16776960;
		if (n == 0) {
			return this.UNKNOWN_COUNTRY;
		}
		return new Country(countryCode[n], countryName[n]);
	}

	public synchronized Country getCountry(long l) {
		if (this.file == null && (this.dboptions & 1) == 0) {
			throw new IllegalStateException("Database has been closed.");
		}
		int n = this.seekCountry(l) - 16776960;
		if (n == 0) {
			return this.UNKNOWN_COUNTRY;
		}
		return new Country(countryCode[n], countryName[n]);
	}

	public int getID(String string) {
		InetAddress inetAddress;
		try {
			inetAddress = InetAddress.getByName(string);
		} catch (UnknownHostException var3_3) {
			return 0;
		}
		return this.getID(LookupService.bytesToLong(inetAddress.getAddress()));
	}

	public int getID(InetAddress inetAddress) {
		return this.getID(LookupService.bytesToLong(inetAddress.getAddress()));
	}

	public synchronized int getID(long l) {
		if (this.file == null && (this.dboptions & 1) == 0) {
			throw new IllegalStateException("Database has been closed.");
		}
		int n = this.seekCountry(l) - this.databaseSegments[0];
		return n;
	}

	public int last_netmask() {
		return this.last_netmask;
	}

	public void netmask(int n) {
		this.last_netmask = n;
	}

	public synchronized DatabaseInfo getDatabaseInfo() {
		if (this.databaseInfo != null) {
			return this.databaseInfo;
		}
		try {
			int n;
			this._check_mtime();
			boolean bl = false;
			byte[] arrby = new byte[3];
			this.file.seek(this.file.length() - 3);
			for (n = 0; n < 20; ++n) {
				int n2 = this.file.read(arrby);
				if (n2 == 3 && (arrby[0] & 255) == 255 && (arrby[1] & 255) == 255 && (arrby[2] & 255) == 255) {
					bl = true;
					break;
				}
				this.file.seek(this.file.getFilePointer() - 4);
			}
			if (bl) {
				this.file.seek(this.file.getFilePointer() - 6);
			} else {
				this.file.seek(this.file.length() - 3);
			}
			for (n = 0; n < 100; ++n) {
				this.file.readFully(arrby);
				if (arrby[0] == 0 && arrby[1] == 0 && arrby[2] == 0) {
					byte[] arrby2 = new byte[n];
					this.file.readFully(arrby2);
					this.databaseInfo = new DatabaseInfo(new String(arrby2));
					return this.databaseInfo;
				}
				this.file.seek(this.file.getFilePointer() - 4);
			}
		} catch (Exception var1_2) {
			var1_2.printStackTrace();
		}
		return new DatabaseInfo("");
	}

	synchronized void _check_mtime() {
		try {
			long l;
			if ((this.dboptions & 2) != 0 && (l = this.databaseFile.lastModified()) != this.mtime) {
				this.close();
				this.file = new RandomAccessFile(this.databaseFile, "r");
				this.databaseInfo = null;
				this.init();
			}
		} catch (IOException var1_2) {
			System.out.println("file not found");
		}
	}

	public Location getLocationV6(String string) {
		InetAddress inetAddress;
		try {
			inetAddress = InetAddress.getByName(string);
		} catch (UnknownHostException var3_3) {
			return null;
		}
		return this.getLocationV6(inetAddress);
	}

	public Location getLocation(InetAddress inetAddress) {
		return this.getLocation(LookupService.bytesToLong(inetAddress.getAddress()));
	}

	public Location getLocation(String string) {
		InetAddress inetAddress;
		try {
			inetAddress = InetAddress.getByName(string);
		} catch (UnknownHostException var3_3) {
			return null;
		}
		return this.getLocation(inetAddress);
	}

	public synchronized Region getRegion(String string) {
		InetAddress inetAddress;
		try {
			inetAddress = InetAddress.getByName(string);
		} catch (UnknownHostException var3_3) {
			return null;
		}
		return this.getRegion(LookupService.bytesToLong(inetAddress.getAddress()));
	}

	public synchronized Region getRegion(long l) {
		Region region = new Region();
		int n = 0;
		if (this.databaseType == 7) {
			n = this.seekCountry(l) - 16700000;
			char[] arrc = new char[2];
			if (n >= 1000) {
				region.countryCode = "US";
				region.countryName = "United States";
				arrc[0] = (char) ((n - 1000) / 26 + 65);
				arrc[1] = (char) ((n - 1000) % 26 + 65);
				region.region = new String(arrc);
			} else {
				region.countryCode = countryCode[n];
				region.countryName = countryName[n];
				region.region = "";
			}
		} else if (this.databaseType == 3) {
			n = this.seekCountry(l) - 16000000;
			char[] arrc = new char[2];
			if (n < 1) {
				region.countryCode = "";
				region.countryName = "";
				region.region = "";
			} else if (n < 677) {
				region.countryCode = "US";
				region.countryName = "United States";
				arrc[0] = (char) ((n - 1) / 26 + 65);
				arrc[1] = (char) ((n - 1) % 26 + 65);
				region.region = new String(arrc);
			} else if (n < 1353) {
				region.countryCode = "CA";
				region.countryName = "Canada";
				arrc[0] = (char) ((n - 677) / 26 + 65);
				arrc[1] = (char) ((n - 677) % 26 + 65);
				region.region = new String(arrc);
			} else {
				region.countryCode = countryCode[(n - 1353) / 360];
				region.countryName = countryName[(n - 1353) / 360];
				region.region = "";
			}
		}
		return region;
	}

	public synchronized Location getLocationV6(InetAddress inetAddress) {
		byte[] arrby = new byte[60];
		int n = 0;
		Location location = new Location();
		int n2 = 0;
		double d = 0.0;
		double d2 = 0.0;
		try {
			int n3;
			int n4 = this.seekCountryV6(inetAddress);
			if (n4 == this.databaseSegments[0]) {
				return null;
			}
			int n5 = n4 + (2 * this.recordLength - 1) * this.databaseSegments[0];
			if ((this.dboptions & 1) == 1) {
				System.arraycopy(this.dbbuffer, n5, arrby, 0, Math.min(this.dbbuffer.length - n5, 60));
			} else {
				this.file.seek(n5);
				this.file.readFully(arrby);
			}
			location.countryCode = countryCode[LookupService.unsignedByteToInt(arrby[0])];
			location.countryName = countryName[LookupService.unsignedByteToInt(arrby[0])];
			while (arrby[++n + n2] != 0) {
				++n2;
			}
			if (n2 > 0) {
				location.region = new String(arrby, n, n2);
			}
			n2 = 0;
			while (arrby[(n += n2 + 1) + n2] != 0) {
				++n2;
			}
			if (n2 > 0) {
				location.city = new String(arrby, n, n2, "ISO-8859-1");
			}
			n2 = 0;
			while (arrby[(n += n2 + 1) + n2] != 0) {
				++n2;
			}
			if (n2 > 0) {
				location.postalCode = new String(arrby, n, n2);
			}
			n += n2 + 1;
			for (n3 = 0; n3 < 3; ++n3) {
				d += (double) (LookupService.unsignedByteToInt(arrby[n + n3]) << n3 * 8);
			}
			location.latitude = (float) d / 10000.0f - 180.0f;
			n += 3;
			for (n3 = 0; n3 < 3; ++n3) {
				d2 += (double) (LookupService.unsignedByteToInt(arrby[n + n3]) << n3 * 8);
			}
			location.longitude = (float) d2 / 10000.0f - 180.0f;
			location.metro_code = 0;
			location.dma_code = 0;
			location.area_code = 0;
			if (this.databaseType == 2) {
				int n6 = 0;
				if ("US".equals(location.countryCode)) {
					n += 3;
					for (n3 = 0; n3 < 3; ++n3) {
						n6 += LookupService.unsignedByteToInt(arrby[n + n3]) << n3 * 8;
					}
					location.metro_code = location.dma_code = n6 / 1000;
					location.area_code = n6 % 1000;
				}
			}
		} catch (IOException var13_12) {
			System.err.println("IO Exception while seting up segments");
		}
		return location;
	}

	public synchronized Location getLocation(long l) {
		byte[] arrby = new byte[60];
		int n = 0;
		Location location = new Location();
		int n2 = 0;
		double d = 0.0;
		double d2 = 0.0;
		try {
			int n3;
			int n4 = this.seekCountry(l);
			if (n4 == this.databaseSegments[0]) {
				return null;
			}
			int n5 = n4 + (2 * this.recordLength - 1) * this.databaseSegments[0];
			if ((this.dboptions & 1) == 1) {
				System.arraycopy(this.dbbuffer, n5, arrby, 0, Math.min(this.dbbuffer.length - n5, 60));
			} else {
				this.file.seek(n5);
				try {
					this.file.readFully(arrby);
				} catch (IOException var14_10) {
					// empty catch block
				}
			}
			location.countryCode = countryCode[LookupService.unsignedByteToInt(arrby[0])];
			location.countryName = countryName[LookupService.unsignedByteToInt(arrby[0])];
			while (arrby[++n + n2] != 0) {
				++n2;
			}
			if (n2 > 0) {
				location.region = new String(arrby, n, n2);
			}
			n2 = 0;
			while (arrby[(n += n2 + 1) + n2] != 0) {
				++n2;
			}
			if (n2 > 0) {
				location.city = new String(arrby, n, n2, "ISO-8859-1");
			}
			n2 = 0;
			while (arrby[(n += n2 + 1) + n2] != 0) {
				++n2;
			}
			if (n2 > 0) {
				location.postalCode = new String(arrby, n, n2);
			}
			n += n2 + 1;
			for (n3 = 0; n3 < 3; ++n3) {
				d += (double) (LookupService.unsignedByteToInt(arrby[n + n3]) << n3 * 8);
			}
			location.latitude = (float) d / 10000.0f - 180.0f;
			n += 3;
			for (n3 = 0; n3 < 3; ++n3) {
				d2 += (double) (LookupService.unsignedByteToInt(arrby[n + n3]) << n3 * 8);
			}
			location.longitude = (float) d2 / 10000.0f - 180.0f;
			location.metro_code = 0;
			location.dma_code = 0;
			location.area_code = 0;
			if (this.databaseType == 2) {
				int n6 = 0;
				if ("US".equals(location.countryCode)) {
					n += 3;
					for (n3 = 0; n3 < 3; ++n3) {
						n6 += LookupService.unsignedByteToInt(arrby[n + n3]) << n3 * 8;
					}
					location.metro_code = location.dma_code = n6 / 1000;
					location.area_code = n6 % 1000;
				}
			}
		} catch (IOException var14_12) {
			System.err.println("IO Exception while seting up segments");
		}
		return location;
	}

	public String getOrg(InetAddress inetAddress) {
		return this.getOrg(LookupService.bytesToLong(inetAddress.getAddress()));
	}

	public String getOrg(String string) {
		InetAddress inetAddress;
		try {
			inetAddress = InetAddress.getByName(string);
		} catch (UnknownHostException var3_3) {
			return null;
		}
		return this.getOrg(inetAddress);
	}

	public synchronized String getOrg(long l) {
		byte[] arrby = new byte[300];
		try {
			int n;
			int n2 = this.seekCountry(l);
			if (n2 == this.databaseSegments[0]) {
				return null;
			}
			int n3 = n2 + (2 * this.recordLength - 1) * this.databaseSegments[0];
			if ((this.dboptions & 1) == 1) {
				System.arraycopy(this.dbbuffer, n3, arrby, 0, Math.min(this.dbbuffer.length - n3, 300));
			} else {
				this.file.seek(n3);
				try {
					this.file.readFully(arrby);
				} catch (IOException var7_5) {
					// empty catch block
				}
			}
			for (n = 0; n < arrby.length && arrby[n] != 0; ++n) {
			}
			String string = new String(arrby, 0, n, "ISO-8859-1");
			return string;
		} catch (IOException var7_7) {
			System.out.println("IO Exception");
			return null;
		}
	}

	public String getOrgV6(String string) {
		InetAddress inetAddress;
		try {
			inetAddress = InetAddress.getByName(string);
		} catch (UnknownHostException var3_3) {
			return null;
		}
		return this.getOrgV6(inetAddress);
	}

	public synchronized String getOrgV6(InetAddress inetAddress) {
		byte[] arrby = new byte[300];
		try {
			int n;
			int n2 = this.seekCountryV6(inetAddress);
			if (n2 == this.databaseSegments[0]) {
				return null;
			}
			int n3 = n2 + (2 * this.recordLength - 1) * this.databaseSegments[0];
			if ((this.dboptions & 1) == 1) {
				System.arraycopy(this.dbbuffer, n3, arrby, 0, Math.min(this.dbbuffer.length - n3, 300));
			} else {
				this.file.seek(n3);
				this.file.readFully(arrby);
			}
			for (n = 0; n < arrby.length && arrby[n] != 0; ++n) {
			}
			String string = new String(arrby, 0, n, "ISO-8859-1");
			return string;
		} catch (IOException var6_6) {
			System.out.println("IO Exception");
			return null;
		}
	}

	private synchronized int seekCountryV6(InetAddress inetAddress) {
		byte[] arrby;
		byte[] arrby2 = inetAddress.getAddress();
		if (arrby2.length == 4) {
			arrby = new byte[16];
			System.arraycopy(arrby2, 0, arrby, 12, 4);
			arrby2 = arrby;
		}
		arrby = new byte[8];
		int[] arrn = new int[2];
		int n = 0;
		this._check_mtime();
		for (int i = 127; i >= 0; --i) {
			int n2;
			int n3;
			int n4;
			if ((this.dboptions & 1) == 1) {
				for (n4 = 0; n4 < 8; ++n4) {
					arrby[n4] = this.dbbuffer[2 * this.recordLength * n + n4];
				}
			} else if ((this.dboptions & 4) != 0) {
				for (n4 = 0; n4 < 8; ++n4) {
					arrby[n4] = this.index_cache[2 * this.recordLength * n + n4];
				}
			} else {
				try {
					this.file.seek(2 * this.recordLength * n);
					this.file.readFully(arrby);
				} catch (IOException var7_8) {
					System.out.println("IO Exception");
				}
			}
			for (n4 = 0; n4 < 2; ++n4) {
				arrn[n4] = 0;
				for (n3 = 0; n3 < this.recordLength; ++n3) {
					n2 = arrby[n4 * this.recordLength + n3];
					if (n2 < 0) {
						n2 += 256;
					}
					int[] arrn2 = arrn;
					int n5 = n4;
					arrn2[n5] = arrn2[n5] + (n2 << n3 * 8);
				}
			}
			n4 = 127 - i;
			n3 = n4 >> 3;
			n2 = 1 << (n4 & 7 ^ 7);
			if ((arrby2[n3] & n2) > 0) {
				if (arrn[1] >= this.databaseSegments[0]) {
					this.last_netmask = 128 - i;
					return arrn[1];
				}
				n = arrn[1];
				continue;
			}
			if (arrn[0] >= this.databaseSegments[0]) {
				this.last_netmask = 128 - i;
				return arrn[0];
			}
			n = arrn[0];
		}
		System.err.println("Error seeking country while seeking " + inetAddress.getHostAddress());
		return 0;
	}

	private synchronized int seekCountry(long l) {
		byte[] arrby = new byte[8];
		int[] arrn = new int[2];
		int n = 0;
		this._check_mtime();
		for (int i = 31; i >= 0; --i) {
			int n2;
			if ((this.dboptions & 1) == 1) {
				for (n2 = 0; n2 < 2 * this.recordLength; ++n2) {
					arrby[n2] = this.dbbuffer[2 * this.recordLength * n + n2];
				}
			} else if ((this.dboptions & 4) != 0) {
				for (n2 = 0; n2 < 2 * this.recordLength; ++n2) {
					arrby[n2] = this.index_cache[2 * this.recordLength * n + n2];
				}
			} else {
				try {
					this.file.seek(2 * this.recordLength * n);
					this.file.readFully(arrby);
				} catch (IOException var7_7) {
					System.out.println("IO Exception");
				}
			}
			for (n2 = 0; n2 < 2; ++n2) {
				arrn[n2] = 0;
				for (int j = 0; j < this.recordLength; ++j) {
					int n3 = arrby[n2 * this.recordLength + j];
					if (n3 < 0) {
						n3 += 256;
					}
					int[] arrn2 = arrn;
					int n4 = n2;
					arrn2[n4] = arrn2[n4] + (n3 << j * 8);
				}
			}
			if ((l & (long) (1 << i)) > 0) {
				if (arrn[1] >= this.databaseSegments[0]) {
					this.last_netmask = 32 - i;
					return arrn[1];
				}
				n = arrn[1];
				continue;
			}
			if (arrn[0] >= this.databaseSegments[0]) {
				this.last_netmask = 32 - i;
				return arrn[0];
			}
			n = arrn[0];
		}
		System.err.println("Error seeking country while seeking " + l);
		return 0;
	}

	private static long bytesToLong(byte[] arrby) {
		long l = 0;
		for (int i = 0; i < 4; ++i) {
			long l2 = arrby[i];
			if (l2 < 0) {
				l2 += 256;
			}
			l += l2 << (3 - i) * 8;
		}
		return l;
	}

	private static int unsignedByteToInt(byte by) {
		return by & 255;
	}

	static {
		if (countryCode.length != countryName.length) {
			throw new AssertionError((Object) "countryCode.length!=countryName.length");
		}
	}
}
