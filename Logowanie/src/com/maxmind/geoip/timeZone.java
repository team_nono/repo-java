/*
 * Decompiled with CFR 0_118.
 */
package com.maxmind.geoip;

public class timeZone {
    public static String timeZoneByCountryAndRegion(String string, String string2) {
        String string3 = null;
        if (string == null) {
            return null;
        }
        if (string2 == null) {
            string2 = "";
        }
        if ("AD".equals(string)) {
            string3 = "Europe/Andorra";
        } else if ("AE".equals(string)) {
            string3 = "Asia/Dubai";
        } else if ("AF".equals(string)) {
            string3 = "Asia/Kabul";
        } else if ("AG".equals(string)) {
            string3 = "America/Antigua";
        } else if ("AI".equals(string)) {
            string3 = "America/Anguilla";
        } else if ("AL".equals(string)) {
            string3 = "Europe/Tirane";
        } else if ("AM".equals(string)) {
            string3 = "Asia/Yerevan";
        } else if ("AN".equals(string)) {
            string3 = "America/Curacao";
        } else if ("AO".equals(string)) {
            string3 = "Africa/Luanda";
        } else if ("AQ".equals(string)) {
            string3 = "Antarctica/South_Pole";
        } else if ("AR".equals(string)) {
            if ("01".equals(string2)) {
                string3 = "America/Argentina/Buenos_Aires";
            } else if ("02".equals(string2)) {
                string3 = "America/Argentina/Catamarca";
            } else if ("03".equals(string2)) {
                string3 = "America/Argentina/Tucuman";
            } else if ("04".equals(string2)) {
                string3 = "America/Argentina/Rio_Gallegos";
            } else if ("05".equals(string2)) {
                string3 = "America/Argentina/Cordoba";
            } else if ("06".equals(string2)) {
                string3 = "America/Argentina/Tucuman";
            } else if ("07".equals(string2)) {
                string3 = "America/Argentina/Buenos_Aires";
            } else if ("08".equals(string2)) {
                string3 = "America/Argentina/Buenos_Aires";
            } else if ("09".equals(string2)) {
                string3 = "America/Argentina/Tucuman";
            } else if ("10".equals(string2)) {
                string3 = "America/Argentina/Jujuy";
            } else if ("11".equals(string2)) {
                string3 = "America/Argentina/San_Luis";
            } else if ("12".equals(string2)) {
                string3 = "America/Argentina/La_Rioja";
            } else if ("13".equals(string2)) {
                string3 = "America/Argentina/Mendoza";
            } else if ("14".equals(string2)) {
                string3 = "America/Argentina/Buenos_Aires";
            } else if ("15".equals(string2)) {
                string3 = "America/Argentina/San_Luis";
            } else if ("16".equals(string2)) {
                string3 = "America/Argentina/Buenos_Aires";
            } else if ("17".equals(string2)) {
                string3 = "America/Argentina/Salta";
            } else if ("18".equals(string2)) {
                string3 = "America/Argentina/San_Juan";
            } else if ("19".equals(string2)) {
                string3 = "America/Argentina/San_Luis";
            } else if ("20".equals(string2)) {
                string3 = "America/Argentina/Rio_Gallegos";
            } else if ("21".equals(string2)) {
                string3 = "America/Argentina/Buenos_Aires";
            } else if ("22".equals(string2)) {
                string3 = "America/Argentina/Catamarca";
            } else if ("23".equals(string2)) {
                string3 = "America/Argentina/Ushuaia";
            } else if ("24".equals(string2)) {
                string3 = "America/Argentina/Tucuman";
            }
        } else if ("AS".equals(string)) {
            string3 = "Pacific/Pago_Pago";
        } else if ("AT".equals(string)) {
            string3 = "Europe/Vienna";
        } else if ("AU".equals(string)) {
            if ("01".equals(string2)) {
                string3 = "Australia/Sydney";
            } else if ("02".equals(string2)) {
                string3 = "Australia/Sydney";
            } else if ("03".equals(string2)) {
                string3 = "Australia/Darwin";
            } else if ("04".equals(string2)) {
                string3 = "Australia/Brisbane";
            } else if ("05".equals(string2)) {
                string3 = "Australia/Adelaide";
            } else if ("06".equals(string2)) {
                string3 = "Australia/Hobart";
            } else if ("07".equals(string2)) {
                string3 = "Australia/Melbourne";
            } else if ("08".equals(string2)) {
                string3 = "Australia/Perth";
            }
        } else if ("AW".equals(string)) {
            string3 = "America/Aruba";
        } else if ("AX".equals(string)) {
            string3 = "Europe/Mariehamn";
        } else if ("AZ".equals(string)) {
            string3 = "Asia/Baku";
        } else if ("BA".equals(string)) {
            string3 = "Europe/Sarajevo";
        } else if ("BB".equals(string)) {
            string3 = "America/Barbados";
        } else if ("BD".equals(string)) {
            string3 = "Asia/Dhaka";
        } else if ("BE".equals(string)) {
            string3 = "Europe/Brussels";
        } else if ("BF".equals(string)) {
            string3 = "Africa/Ouagadougou";
        } else if ("BG".equals(string)) {
            string3 = "Europe/Sofia";
        } else if ("BH".equals(string)) {
            string3 = "Asia/Bahrain";
        } else if ("BI".equals(string)) {
            string3 = "Africa/Bujumbura";
        } else if ("BJ".equals(string)) {
            string3 = "Africa/Porto-Novo";
        } else if ("BL".equals(string)) {
            string3 = "America/St_Barthelemy";
        } else if ("BM".equals(string)) {
            string3 = "Atlantic/Bermuda";
        } else if ("BN".equals(string)) {
            string3 = "Asia/Brunei";
        } else if ("BO".equals(string)) {
            string3 = "America/La_Paz";
        } else if ("BQ".equals(string)) {
            string3 = "America/Curacao";
        } else if ("BR".equals(string)) {
            if ("01".equals(string2)) {
                string3 = "America/Rio_Branco";
            } else if ("02".equals(string2)) {
                string3 = "America/Maceio";
            } else if ("03".equals(string2)) {
                string3 = "America/Sao_Paulo";
            } else if ("04".equals(string2)) {
                string3 = "America/Manaus";
            } else if ("05".equals(string2)) {
                string3 = "America/Bahia";
            } else if ("06".equals(string2)) {
                string3 = "America/Fortaleza";
            } else if ("07".equals(string2)) {
                string3 = "America/Sao_Paulo";
            } else if ("08".equals(string2)) {
                string3 = "America/Sao_Paulo";
            } else if ("11".equals(string2)) {
                string3 = "America/Campo_Grande";
            } else if ("13".equals(string2)) {
                string3 = "America/Belem";
            } else if ("14".equals(string2)) {
                string3 = "America/Cuiaba";
            } else if ("15".equals(string2)) {
                string3 = "America/Sao_Paulo";
            } else if ("16".equals(string2)) {
                string3 = "America/Belem";
            } else if ("17".equals(string2)) {
                string3 = "America/Recife";
            } else if ("18".equals(string2)) {
                string3 = "America/Sao_Paulo";
            } else if ("20".equals(string2)) {
                string3 = "America/Fortaleza";
            } else if ("21".equals(string2)) {
                string3 = "America/Sao_Paulo";
            } else if ("22".equals(string2)) {
                string3 = "America/Recife";
            } else if ("23".equals(string2)) {
                string3 = "America/Sao_Paulo";
            } else if ("24".equals(string2)) {
                string3 = "America/Porto_Velho";
            } else if ("25".equals(string2)) {
                string3 = "America/Boa_Vista";
            } else if ("26".equals(string2)) {
                string3 = "America/Sao_Paulo";
            } else if ("27".equals(string2)) {
                string3 = "America/Sao_Paulo";
            } else if ("28".equals(string2)) {
                string3 = "America/Maceio";
            } else if ("29".equals(string2)) {
                string3 = "America/Sao_Paulo";
            } else if ("30".equals(string2)) {
                string3 = "America/Recife";
            } else if ("31".equals(string2)) {
                string3 = "America/Araguaina";
            }
        } else if ("BS".equals(string)) {
            string3 = "America/Nassau";
        } else if ("BT".equals(string)) {
            string3 = "Asia/Thimphu";
        } else if ("BV".equals(string)) {
            string3 = "Antarctica/Syowa";
        } else if ("BW".equals(string)) {
            string3 = "Africa/Gaborone";
        } else if ("BY".equals(string)) {
            string3 = "Europe/Minsk";
        } else if ("BZ".equals(string)) {
            string3 = "America/Belize";
        } else if ("CA".equals(string)) {
            if ("AB".equals(string2)) {
                string3 = "America/Edmonton";
            } else if ("BC".equals(string2)) {
                string3 = "America/Vancouver";
            } else if ("MB".equals(string2)) {
                string3 = "America/Winnipeg";
            } else if ("NB".equals(string2)) {
                string3 = "America/Halifax";
            } else if ("NL".equals(string2)) {
                string3 = "America/St_Johns";
            } else if ("NS".equals(string2)) {
                string3 = "America/Halifax";
            } else if ("NT".equals(string2)) {
                string3 = "America/Yellowknife";
            } else if ("NU".equals(string2)) {
                string3 = "America/Rankin_Inlet";
            } else if ("ON".equals(string2)) {
                string3 = "America/Toronto";
            } else if ("PE".equals(string2)) {
                string3 = "America/Halifax";
            } else if ("QC".equals(string2)) {
                string3 = "America/Montreal";
            } else if ("SK".equals(string2)) {
                string3 = "America/Regina";
            } else if ("YT".equals(string2)) {
                string3 = "America/Whitehorse";
            }
        } else if ("CC".equals(string)) {
            string3 = "Indian/Cocos";
        } else if ("CD".equals(string)) {
            if ("01".equals(string2)) {
                string3 = "Africa/Kinshasa";
            } else if ("02".equals(string2)) {
                string3 = "Africa/Kinshasa";
            } else if ("03".equals(string2)) {
                string3 = "Africa/Kinshasa";
            } else if ("04".equals(string2)) {
                string3 = "Africa/Lubumbashi";
            } else if ("05".equals(string2)) {
                string3 = "Africa/Lubumbashi";
            } else if ("06".equals(string2)) {
                string3 = "Africa/Kinshasa";
            } else if ("07".equals(string2)) {
                string3 = "Africa/Lubumbashi";
            } else if ("08".equals(string2)) {
                string3 = "Africa/Kinshasa";
            } else if ("09".equals(string2)) {
                string3 = "Africa/Lubumbashi";
            } else if ("10".equals(string2)) {
                string3 = "Africa/Lubumbashi";
            } else if ("11".equals(string2)) {
                string3 = "Africa/Lubumbashi";
            } else if ("12".equals(string2)) {
                string3 = "Africa/Lubumbashi";
            }
        } else if ("CF".equals(string)) {
            string3 = "Africa/Bangui";
        } else if ("CG".equals(string)) {
            string3 = "Africa/Brazzaville";
        } else if ("CH".equals(string)) {
            string3 = "Europe/Zurich";
        } else if ("CI".equals(string)) {
            string3 = "Africa/Abidjan";
        } else if ("CK".equals(string)) {
            string3 = "Pacific/Rarotonga";
        } else if ("CL".equals(string)) {
            string3 = "America/Santiago";
        } else if ("CM".equals(string)) {
            string3 = "Africa/Lagos";
        } else if ("CN".equals(string)) {
            if ("01".equals(string2)) {
                string3 = "Asia/Shanghai";
            } else if ("02".equals(string2)) {
                string3 = "Asia/Shanghai";
            } else if ("03".equals(string2)) {
                string3 = "Asia/Shanghai";
            } else if ("04".equals(string2)) {
                string3 = "Asia/Shanghai";
            } else if ("05".equals(string2)) {
                string3 = "Asia/Harbin";
            } else if ("06".equals(string2)) {
                string3 = "Asia/Chongqing";
            } else if ("07".equals(string2)) {
                string3 = "Asia/Shanghai";
            } else if ("08".equals(string2)) {
                string3 = "Asia/Harbin";
            } else if ("09".equals(string2)) {
                string3 = "Asia/Shanghai";
            } else if ("10".equals(string2)) {
                string3 = "Asia/Shanghai";
            } else if ("11".equals(string2)) {
                string3 = "Asia/Chongqing";
            } else if ("12".equals(string2)) {
                string3 = "Asia/Shanghai";
            } else if ("13".equals(string2)) {
                string3 = "Asia/Urumqi";
            } else if ("14".equals(string2)) {
                string3 = "Asia/Chongqing";
            } else if ("15".equals(string2)) {
                string3 = "Asia/Chongqing";
            } else if ("16".equals(string2)) {
                string3 = "Asia/Chongqing";
            } else if ("18".equals(string2)) {
                string3 = "Asia/Chongqing";
            } else if ("19".equals(string2)) {
                string3 = "Asia/Harbin";
            } else if ("20".equals(string2)) {
                string3 = "Asia/Harbin";
            } else if ("21".equals(string2)) {
                string3 = "Asia/Chongqing";
            } else if ("22".equals(string2)) {
                string3 = "Asia/Harbin";
            } else if ("23".equals(string2)) {
                string3 = "Asia/Shanghai";
            } else if ("24".equals(string2)) {
                string3 = "Asia/Chongqing";
            } else if ("25".equals(string2)) {
                string3 = "Asia/Shanghai";
            } else if ("26".equals(string2)) {
                string3 = "Asia/Chongqing";
            } else if ("28".equals(string2)) {
                string3 = "Asia/Shanghai";
            } else if ("29".equals(string2)) {
                string3 = "Asia/Chongqing";
            } else if ("30".equals(string2)) {
                string3 = "Asia/Chongqing";
            } else if ("31".equals(string2)) {
                string3 = "Asia/Chongqing";
            } else if ("32".equals(string2)) {
                string3 = "Asia/Chongqing";
            } else if ("33".equals(string2)) {
                string3 = "Asia/Chongqing";
            }
        } else if ("CO".equals(string)) {
            string3 = "America/Bogota";
        } else if ("CR".equals(string)) {
            string3 = "America/Costa_Rica";
        } else if ("CU".equals(string)) {
            string3 = "America/Havana";
        } else if ("CV".equals(string)) {
            string3 = "Atlantic/Cape_Verde";
        } else if ("CW".equals(string)) {
            string3 = "America/Curacao";
        } else if ("CX".equals(string)) {
            string3 = "Indian/Christmas";
        } else if ("CY".equals(string)) {
            string3 = "Asia/Nicosia";
        } else if ("CZ".equals(string)) {
            string3 = "Europe/Prague";
        } else if ("DE".equals(string)) {
            string3 = "Europe/Berlin";
        } else if ("DJ".equals(string)) {
            string3 = "Africa/Djibouti";
        } else if ("DK".equals(string)) {
            string3 = "Europe/Copenhagen";
        } else if ("DM".equals(string)) {
            string3 = "America/Dominica";
        } else if ("DO".equals(string)) {
            string3 = "America/Santo_Domingo";
        } else if ("DZ".equals(string)) {
            string3 = "Africa/Algiers";
        } else if ("EC".equals(string)) {
            if ("01".equals(string2)) {
                string3 = "Pacific/Galapagos";
            } else if ("02".equals(string2)) {
                string3 = "America/Guayaquil";
            } else if ("03".equals(string2)) {
                string3 = "America/Guayaquil";
            } else if ("04".equals(string2)) {
                string3 = "America/Guayaquil";
            } else if ("05".equals(string2)) {
                string3 = "America/Guayaquil";
            } else if ("06".equals(string2)) {
                string3 = "America/Guayaquil";
            } else if ("07".equals(string2)) {
                string3 = "America/Guayaquil";
            } else if ("08".equals(string2)) {
                string3 = "America/Guayaquil";
            } else if ("09".equals(string2)) {
                string3 = "America/Guayaquil";
            } else if ("10".equals(string2)) {
                string3 = "America/Guayaquil";
            } else if ("11".equals(string2)) {
                string3 = "America/Guayaquil";
            } else if ("12".equals(string2)) {
                string3 = "America/Guayaquil";
            } else if ("13".equals(string2)) {
                string3 = "America/Guayaquil";
            } else if ("14".equals(string2)) {
                string3 = "America/Guayaquil";
            } else if ("15".equals(string2)) {
                string3 = "America/Guayaquil";
            } else if ("17".equals(string2)) {
                string3 = "America/Guayaquil";
            } else if ("18".equals(string2)) {
                string3 = "America/Guayaquil";
            } else if ("19".equals(string2)) {
                string3 = "America/Guayaquil";
            } else if ("20".equals(string2)) {
                string3 = "America/Guayaquil";
            } else if ("22".equals(string2)) {
                string3 = "America/Guayaquil";
            } else if ("24".equals(string2)) {
                string3 = "America/Guayaquil";
            }
        } else if ("EE".equals(string)) {
            string3 = "Europe/Tallinn";
        } else if ("EG".equals(string)) {
            string3 = "Africa/Cairo";
        } else if ("EH".equals(string)) {
            string3 = "Africa/El_Aaiun";
        } else if ("ER".equals(string)) {
            string3 = "Africa/Asmara";
        } else if ("ES".equals(string)) {
            if ("07".equals(string2)) {
                string3 = "Europe/Madrid";
            } else if ("27".equals(string2)) {
                string3 = "Europe/Madrid";
            } else if ("29".equals(string2)) {
                string3 = "Europe/Madrid";
            } else if ("31".equals(string2)) {
                string3 = "Europe/Madrid";
            } else if ("32".equals(string2)) {
                string3 = "Europe/Madrid";
            } else if ("34".equals(string2)) {
                string3 = "Europe/Madrid";
            } else if ("39".equals(string2)) {
                string3 = "Europe/Madrid";
            } else if ("51".equals(string2)) {
                string3 = "Africa/Ceuta";
            } else if ("52".equals(string2)) {
                string3 = "Europe/Madrid";
            } else if ("53".equals(string2)) {
                string3 = "Atlantic/Canary";
            } else if ("54".equals(string2)) {
                string3 = "Europe/Madrid";
            } else if ("55".equals(string2)) {
                string3 = "Europe/Madrid";
            } else if ("56".equals(string2)) {
                string3 = "Europe/Madrid";
            } else if ("57".equals(string2)) {
                string3 = "Europe/Madrid";
            } else if ("58".equals(string2)) {
                string3 = "Europe/Madrid";
            } else if ("59".equals(string2)) {
                string3 = "Europe/Madrid";
            } else if ("60".equals(string2)) {
                string3 = "Europe/Madrid";
            }
        } else if ("ET".equals(string)) {
            string3 = "Africa/Addis_Ababa";
        } else if ("FI".equals(string)) {
            string3 = "Europe/Helsinki";
        } else if ("FJ".equals(string)) {
            string3 = "Pacific/Fiji";
        } else if ("FK".equals(string)) {
            string3 = "Atlantic/Stanley";
        } else if ("FM".equals(string)) {
            string3 = "Pacific/Pohnpei";
        } else if ("FO".equals(string)) {
            string3 = "Atlantic/Faroe";
        } else if ("FR".equals(string)) {
            string3 = "Europe/Paris";
        } else if ("FX".equals(string)) {
            string3 = "Europe/Paris";
        } else if ("GA".equals(string)) {
            string3 = "Africa/Libreville";
        } else if ("GB".equals(string)) {
            string3 = "Europe/London";
        } else if ("GD".equals(string)) {
            string3 = "America/Grenada";
        } else if ("GE".equals(string)) {
            string3 = "Asia/Tbilisi";
        } else if ("GF".equals(string)) {
            string3 = "America/Cayenne";
        } else if ("GG".equals(string)) {
            string3 = "Europe/Guernsey";
        } else if ("GH".equals(string)) {
            string3 = "Africa/Accra";
        } else if ("GI".equals(string)) {
            string3 = "Europe/Gibraltar";
        } else if ("GL".equals(string)) {
            if ("01".equals(string2)) {
                string3 = "America/Thule";
            } else if ("02".equals(string2)) {
                string3 = "America/Godthab";
            } else if ("03".equals(string2)) {
                string3 = "America/Godthab";
            }
        } else if ("GM".equals(string)) {
            string3 = "Africa/Banjul";
        } else if ("GN".equals(string)) {
            string3 = "Africa/Conakry";
        } else if ("GP".equals(string)) {
            string3 = "America/Guadeloupe";
        } else if ("GQ".equals(string)) {
            string3 = "Africa/Malabo";
        } else if ("GR".equals(string)) {
            string3 = "Europe/Athens";
        } else if ("GS".equals(string)) {
            string3 = "Atlantic/South_Georgia";
        } else if ("GT".equals(string)) {
            string3 = "America/Guatemala";
        } else if ("GU".equals(string)) {
            string3 = "Pacific/Guam";
        } else if ("GW".equals(string)) {
            string3 = "Africa/Bissau";
        } else if ("GY".equals(string)) {
            string3 = "America/Guyana";
        } else if ("HK".equals(string)) {
            string3 = "Asia/Hong_Kong";
        } else if ("HN".equals(string)) {
            string3 = "America/Tegucigalpa";
        } else if ("HR".equals(string)) {
            string3 = "Europe/Zagreb";
        } else if ("HT".equals(string)) {
            string3 = "America/Port-au-Prince";
        } else if ("HU".equals(string)) {
            string3 = "Europe/Budapest";
        } else if ("ID".equals(string)) {
            if ("01".equals(string2)) {
                string3 = "Asia/Pontianak";
            } else if ("02".equals(string2)) {
                string3 = "Asia/Makassar";
            } else if ("03".equals(string2)) {
                string3 = "Asia/Jakarta";
            } else if ("04".equals(string2)) {
                string3 = "Asia/Jakarta";
            } else if ("05".equals(string2)) {
                string3 = "Asia/Jakarta";
            } else if ("06".equals(string2)) {
                string3 = "Asia/Jakarta";
            } else if ("07".equals(string2)) {
                string3 = "Asia/Jakarta";
            } else if ("08".equals(string2)) {
                string3 = "Asia/Jakarta";
            } else if ("09".equals(string2)) {
                string3 = "Asia/Jayapura";
            } else if ("10".equals(string2)) {
                string3 = "Asia/Jakarta";
            } else if ("11".equals(string2)) {
                string3 = "Asia/Pontianak";
            } else if ("12".equals(string2)) {
                string3 = "Asia/Makassar";
            } else if ("13".equals(string2)) {
                string3 = "Asia/Makassar";
            } else if ("14".equals(string2)) {
                string3 = "Asia/Makassar";
            } else if ("15".equals(string2)) {
                string3 = "Asia/Jakarta";
            } else if ("16".equals(string2)) {
                string3 = "Asia/Makassar";
            } else if ("17".equals(string2)) {
                string3 = "Asia/Makassar";
            } else if ("18".equals(string2)) {
                string3 = "Asia/Makassar";
            } else if ("19".equals(string2)) {
                string3 = "Asia/Pontianak";
            } else if ("20".equals(string2)) {
                string3 = "Asia/Makassar";
            } else if ("21".equals(string2)) {
                string3 = "Asia/Makassar";
            } else if ("22".equals(string2)) {
                string3 = "Asia/Makassar";
            } else if ("23".equals(string2)) {
                string3 = "Asia/Makassar";
            } else if ("24".equals(string2)) {
                string3 = "Asia/Jakarta";
            } else if ("25".equals(string2)) {
                string3 = "Asia/Pontianak";
            } else if ("26".equals(string2)) {
                string3 = "Asia/Pontianak";
            } else if ("28".equals(string2)) {
                string3 = "Asia/Jayapura";
            } else if ("29".equals(string2)) {
                string3 = "Asia/Makassar";
            } else if ("30".equals(string2)) {
                string3 = "Asia/Jakarta";
            } else if ("31".equals(string2)) {
                string3 = "Asia/Makassar";
            } else if ("32".equals(string2)) {
                string3 = "Asia/Jakarta";
            } else if ("33".equals(string2)) {
                string3 = "Asia/Jakarta";
            } else if ("34".equals(string2)) {
                string3 = "Asia/Makassar";
            } else if ("35".equals(string2)) {
                string3 = "Asia/Pontianak";
            } else if ("36".equals(string2)) {
                string3 = "Asia/Jayapura";
            } else if ("37".equals(string2)) {
                string3 = "Asia/Pontianak";
            } else if ("38".equals(string2)) {
                string3 = "Asia/Makassar";
            } else if ("39".equals(string2)) {
                string3 = "Asia/Jayapura";
            } else if ("40".equals(string2)) {
                string3 = "Asia/Pontianak";
            } else if ("41".equals(string2)) {
                string3 = "Asia/Makassar";
            }
        } else if ("IE".equals(string)) {
            string3 = "Europe/Dublin";
        } else if ("IL".equals(string)) {
            string3 = "Asia/Jerusalem";
        } else if ("IM".equals(string)) {
            string3 = "Europe/Isle_of_Man";
        } else if ("IN".equals(string)) {
            string3 = "Asia/Kolkata";
        } else if ("IO".equals(string)) {
            string3 = "Indian/Chagos";
        } else if ("IQ".equals(string)) {
            string3 = "Asia/Baghdad";
        } else if ("IR".equals(string)) {
            string3 = "Asia/Tehran";
        } else if ("IS".equals(string)) {
            string3 = "Atlantic/Reykjavik";
        } else if ("IT".equals(string)) {
            string3 = "Europe/Rome";
        } else if ("JE".equals(string)) {
            string3 = "Europe/Jersey";
        } else if ("JM".equals(string)) {
            string3 = "America/Jamaica";
        } else if ("JO".equals(string)) {
            string3 = "Asia/Amman";
        } else if ("JP".equals(string)) {
            string3 = "Asia/Tokyo";
        } else if ("KE".equals(string)) {
            string3 = "Africa/Nairobi";
        } else if ("KG".equals(string)) {
            string3 = "Asia/Bishkek";
        } else if ("KH".equals(string)) {
            string3 = "Asia/Phnom_Penh";
        } else if ("KI".equals(string)) {
            string3 = "Pacific/Tarawa";
        } else if ("KM".equals(string)) {
            string3 = "Indian/Comoro";
        } else if ("KN".equals(string)) {
            string3 = "America/St_Kitts";
        } else if ("KP".equals(string)) {
            string3 = "Asia/Pyongyang";
        } else if ("KR".equals(string)) {
            string3 = "Asia/Seoul";
        } else if ("KW".equals(string)) {
            string3 = "Asia/Kuwait";
        } else if ("KY".equals(string)) {
            string3 = "America/Cayman";
        } else if ("KZ".equals(string)) {
            if ("01".equals(string2)) {
                string3 = "Asia/Almaty";
            } else if ("02".equals(string2)) {
                string3 = "Asia/Almaty";
            } else if ("03".equals(string2)) {
                string3 = "Asia/Qyzylorda";
            } else if ("04".equals(string2)) {
                string3 = "Asia/Aqtobe";
            } else if ("05".equals(string2)) {
                string3 = "Asia/Qyzylorda";
            } else if ("06".equals(string2)) {
                string3 = "Asia/Aqtau";
            } else if ("07".equals(string2)) {
                string3 = "Asia/Oral";
            } else if ("08".equals(string2)) {
                string3 = "Asia/Qyzylorda";
            } else if ("09".equals(string2)) {
                string3 = "Asia/Aqtau";
            } else if ("10".equals(string2)) {
                string3 = "Asia/Qyzylorda";
            } else if ("11".equals(string2)) {
                string3 = "Asia/Almaty";
            } else if ("12".equals(string2)) {
                string3 = "Asia/Qyzylorda";
            } else if ("13".equals(string2)) {
                string3 = "Asia/Aqtobe";
            } else if ("14".equals(string2)) {
                string3 = "Asia/Qyzylorda";
            } else if ("15".equals(string2)) {
                string3 = "Asia/Almaty";
            } else if ("16".equals(string2)) {
                string3 = "Asia/Aqtobe";
            } else if ("17".equals(string2)) {
                string3 = "Asia/Almaty";
            }
        } else if ("LA".equals(string)) {
            string3 = "Asia/Vientiane";
        } else if ("LB".equals(string)) {
            string3 = "Asia/Beirut";
        } else if ("LC".equals(string)) {
            string3 = "America/St_Lucia";
        } else if ("LI".equals(string)) {
            string3 = "Europe/Vaduz";
        } else if ("LK".equals(string)) {
            string3 = "Asia/Colombo";
        } else if ("LR".equals(string)) {
            string3 = "Africa/Monrovia";
        } else if ("LS".equals(string)) {
            string3 = "Africa/Maseru";
        } else if ("LT".equals(string)) {
            string3 = "Europe/Vilnius";
        } else if ("LU".equals(string)) {
            string3 = "Europe/Luxembourg";
        } else if ("LV".equals(string)) {
            string3 = "Europe/Riga";
        } else if ("LY".equals(string)) {
            string3 = "Africa/Tripoli";
        } else if ("MA".equals(string)) {
            string3 = "Africa/Casablanca";
        } else if ("MC".equals(string)) {
            string3 = "Europe/Monaco";
        } else if ("MD".equals(string)) {
            string3 = "Europe/Chisinau";
        } else if ("ME".equals(string)) {
            string3 = "Europe/Podgorica";
        } else if ("MF".equals(string)) {
            string3 = "America/Marigot";
        } else if ("MG".equals(string)) {
            string3 = "Indian/Antananarivo";
        } else if ("MH".equals(string)) {
            string3 = "Pacific/Kwajalein";
        } else if ("MK".equals(string)) {
            string3 = "Europe/Skopje";
        } else if ("ML".equals(string)) {
            string3 = "Africa/Bamako";
        } else if ("MM".equals(string)) {
            string3 = "Asia/Rangoon";
        } else if ("MN".equals(string)) {
            if ("06".equals(string2)) {
                string3 = "Asia/Choibalsan";
            } else if ("11".equals(string2)) {
                string3 = "Asia/Ulaanbaatar";
            } else if ("17".equals(string2)) {
                string3 = "Asia/Choibalsan";
            } else if ("19".equals(string2)) {
                string3 = "Asia/Hovd";
            } else if ("20".equals(string2)) {
                string3 = "Asia/Ulaanbaatar";
            } else if ("21".equals(string2)) {
                string3 = "Asia/Ulaanbaatar";
            } else if ("25".equals(string2)) {
                string3 = "Asia/Ulaanbaatar";
            }
        } else if ("MO".equals(string)) {
            string3 = "Asia/Macau";
        } else if ("MP".equals(string)) {
            string3 = "Pacific/Saipan";
        } else if ("MQ".equals(string)) {
            string3 = "America/Martinique";
        } else if ("MR".equals(string)) {
            string3 = "Africa/Nouakchott";
        } else if ("MS".equals(string)) {
            string3 = "America/Montserrat";
        } else if ("MT".equals(string)) {
            string3 = "Europe/Malta";
        } else if ("MU".equals(string)) {
            string3 = "Indian/Mauritius";
        } else if ("MV".equals(string)) {
            string3 = "Indian/Maldives";
        } else if ("MW".equals(string)) {
            string3 = "Africa/Blantyre";
        } else if ("MX".equals(string)) {
            if ("01".equals(string2)) {
                string3 = "America/Mexico_City";
            } else if ("02".equals(string2)) {
                string3 = "America/Tijuana";
            } else if ("03".equals(string2)) {
                string3 = "America/Hermosillo";
            } else if ("04".equals(string2)) {
                string3 = "America/Merida";
            } else if ("05".equals(string2)) {
                string3 = "America/Mexico_City";
            } else if ("06".equals(string2)) {
                string3 = "America/Chihuahua";
            } else if ("07".equals(string2)) {
                string3 = "America/Monterrey";
            } else if ("08".equals(string2)) {
                string3 = "America/Mexico_City";
            } else if ("09".equals(string2)) {
                string3 = "America/Mexico_City";
            } else if ("10".equals(string2)) {
                string3 = "America/Mazatlan";
            } else if ("11".equals(string2)) {
                string3 = "America/Mexico_City";
            } else if ("12".equals(string2)) {
                string3 = "America/Mexico_City";
            } else if ("13".equals(string2)) {
                string3 = "America/Mexico_City";
            } else if ("14".equals(string2)) {
                string3 = "America/Mazatlan";
            } else if ("15".equals(string2)) {
                string3 = "America/Chihuahua";
            } else if ("16".equals(string2)) {
                string3 = "America/Mexico_City";
            } else if ("17".equals(string2)) {
                string3 = "America/Mexico_City";
            } else if ("18".equals(string2)) {
                string3 = "America/Mazatlan";
            } else if ("19".equals(string2)) {
                string3 = "America/Monterrey";
            } else if ("20".equals(string2)) {
                string3 = "America/Mexico_City";
            } else if ("21".equals(string2)) {
                string3 = "America/Mexico_City";
            } else if ("22".equals(string2)) {
                string3 = "America/Mexico_City";
            } else if ("23".equals(string2)) {
                string3 = "America/Cancun";
            } else if ("24".equals(string2)) {
                string3 = "America/Mexico_City";
            } else if ("25".equals(string2)) {
                string3 = "America/Mazatlan";
            } else if ("26".equals(string2)) {
                string3 = "America/Hermosillo";
            } else if ("27".equals(string2)) {
                string3 = "America/Merida";
            } else if ("28".equals(string2)) {
                string3 = "America/Monterrey";
            } else if ("29".equals(string2)) {
                string3 = "America/Mexico_City";
            } else if ("30".equals(string2)) {
                string3 = "America/Mexico_City";
            } else if ("31".equals(string2)) {
                string3 = "America/Merida";
            } else if ("32".equals(string2)) {
                string3 = "America/Monterrey";
            }
        } else if ("MY".equals(string)) {
            if ("01".equals(string2)) {
                string3 = "Asia/Kuala_Lumpur";
            } else if ("02".equals(string2)) {
                string3 = "Asia/Kuala_Lumpur";
            } else if ("03".equals(string2)) {
                string3 = "Asia/Kuala_Lumpur";
            } else if ("04".equals(string2)) {
                string3 = "Asia/Kuala_Lumpur";
            } else if ("05".equals(string2)) {
                string3 = "Asia/Kuala_Lumpur";
            } else if ("06".equals(string2)) {
                string3 = "Asia/Kuala_Lumpur";
            } else if ("07".equals(string2)) {
                string3 = "Asia/Kuala_Lumpur";
            } else if ("08".equals(string2)) {
                string3 = "Asia/Kuala_Lumpur";
            } else if ("09".equals(string2)) {
                string3 = "Asia/Kuala_Lumpur";
            } else if ("11".equals(string2)) {
                string3 = "Asia/Kuching";
            } else if ("12".equals(string2)) {
                string3 = "Asia/Kuala_Lumpur";
            } else if ("13".equals(string2)) {
                string3 = "Asia/Kuala_Lumpur";
            } else if ("14".equals(string2)) {
                string3 = "Asia/Kuala_Lumpur";
            } else if ("15".equals(string2)) {
                string3 = "Asia/Kuching";
            } else if ("16".equals(string2)) {
                string3 = "Asia/Kuching";
            }
        } else if ("MZ".equals(string)) {
            string3 = "Africa/Maputo";
        } else if ("NA".equals(string)) {
            string3 = "Africa/Windhoek";
        } else if ("NC".equals(string)) {
            string3 = "Pacific/Noumea";
        } else if ("NE".equals(string)) {
            string3 = "Africa/Niamey";
        } else if ("NF".equals(string)) {
            string3 = "Pacific/Norfolk";
        } else if ("NG".equals(string)) {
            string3 = "Africa/Lagos";
        } else if ("NI".equals(string)) {
            string3 = "America/Managua";
        } else if ("NL".equals(string)) {
            string3 = "Europe/Amsterdam";
        } else if ("NO".equals(string)) {
            string3 = "Europe/Oslo";
        } else if ("NP".equals(string)) {
            string3 = "Asia/Kathmandu";
        } else if ("NR".equals(string)) {
            string3 = "Pacific/Nauru";
        } else if ("NU".equals(string)) {
            string3 = "Pacific/Niue";
        } else if ("NZ".equals(string)) {
            if ("85".equals(string2)) {
                string3 = "Pacific/Auckland";
            } else if ("E7".equals(string2)) {
                string3 = "Pacific/Auckland";
            } else if ("E8".equals(string2)) {
                string3 = "Pacific/Auckland";
            } else if ("E9".equals(string2)) {
                string3 = "Pacific/Auckland";
            } else if ("F1".equals(string2)) {
                string3 = "Pacific/Auckland";
            } else if ("F2".equals(string2)) {
                string3 = "Pacific/Auckland";
            } else if ("F3".equals(string2)) {
                string3 = "Pacific/Auckland";
            } else if ("F4".equals(string2)) {
                string3 = "Pacific/Auckland";
            } else if ("F5".equals(string2)) {
                string3 = "Pacific/Auckland";
            } else if ("F6".equals(string2)) {
                string3 = "Pacific/Auckland";
            } else if ("F7".equals(string2)) {
                string3 = "Pacific/Chatham";
            } else if ("F8".equals(string2)) {
                string3 = "Pacific/Auckland";
            } else if ("F9".equals(string2)) {
                string3 = "Pacific/Auckland";
            } else if ("G1".equals(string2)) {
                string3 = "Pacific/Auckland";
            } else if ("G2".equals(string2)) {
                string3 = "Pacific/Auckland";
            } else if ("G3".equals(string2)) {
                string3 = "Pacific/Auckland";
            }
        } else if ("OM".equals(string)) {
            string3 = "Asia/Muscat";
        } else if ("PA".equals(string)) {
            string3 = "America/Panama";
        } else if ("PE".equals(string)) {
            string3 = "America/Lima";
        } else if ("PF".equals(string)) {
            string3 = "Pacific/Marquesas";
        } else if ("PG".equals(string)) {
            string3 = "Pacific/Port_Moresby";
        } else if ("PH".equals(string)) {
            string3 = "Asia/Manila";
        } else if ("PK".equals(string)) {
            string3 = "Asia/Karachi";
        } else if ("PL".equals(string)) {
            string3 = "Europe/Warsaw";
        } else if ("PM".equals(string)) {
            string3 = "America/Miquelon";
        } else if ("PN".equals(string)) {
            string3 = "Pacific/Pitcairn";
        } else if ("PR".equals(string)) {
            string3 = "America/Puerto_Rico";
        } else if ("PS".equals(string)) {
            string3 = "Asia/Gaza";
        } else if ("PT".equals(string)) {
            if ("02".equals(string2)) {
                string3 = "Europe/Lisbon";
            } else if ("03".equals(string2)) {
                string3 = "Europe/Lisbon";
            } else if ("04".equals(string2)) {
                string3 = "Europe/Lisbon";
            } else if ("05".equals(string2)) {
                string3 = "Europe/Lisbon";
            } else if ("06".equals(string2)) {
                string3 = "Europe/Lisbon";
            } else if ("07".equals(string2)) {
                string3 = "Europe/Lisbon";
            } else if ("08".equals(string2)) {
                string3 = "Europe/Lisbon";
            } else if ("09".equals(string2)) {
                string3 = "Europe/Lisbon";
            } else if ("10".equals(string2)) {
                string3 = "Atlantic/Madeira";
            } else if ("11".equals(string2)) {
                string3 = "Europe/Lisbon";
            } else if ("13".equals(string2)) {
                string3 = "Europe/Lisbon";
            } else if ("14".equals(string2)) {
                string3 = "Europe/Lisbon";
            } else if ("16".equals(string2)) {
                string3 = "Europe/Lisbon";
            } else if ("17".equals(string2)) {
                string3 = "Europe/Lisbon";
            } else if ("18".equals(string2)) {
                string3 = "Europe/Lisbon";
            } else if ("19".equals(string2)) {
                string3 = "Europe/Lisbon";
            } else if ("20".equals(string2)) {
                string3 = "Europe/Lisbon";
            } else if ("21".equals(string2)) {
                string3 = "Europe/Lisbon";
            } else if ("22".equals(string2)) {
                string3 = "Europe/Lisbon";
            } else if ("23".equals(string2)) {
                string3 = "Atlantic/Azores";
            }
        } else if ("PW".equals(string)) {
            string3 = "Pacific/Palau";
        } else if ("PY".equals(string)) {
            string3 = "America/Asuncion";
        } else if ("QA".equals(string)) {
            string3 = "Asia/Qatar";
        } else if ("RE".equals(string)) {
            string3 = "Indian/Reunion";
        } else if ("RO".equals(string)) {
            string3 = "Europe/Bucharest";
        } else if ("RS".equals(string)) {
            string3 = "Europe/Belgrade";
        } else if ("RU".equals(string)) {
            if ("01".equals(string2)) {
                string3 = "Europe/Volgograd";
            } else if ("02".equals(string2)) {
                string3 = "Asia/Irkutsk";
            } else if ("03".equals(string2)) {
                string3 = "Asia/Novokuznetsk";
            } else if ("04".equals(string2)) {
                string3 = "Asia/Novosibirsk";
            } else if ("05".equals(string2)) {
                string3 = "Asia/Vladivostok";
            } else if ("06".equals(string2)) {
                string3 = "Europe/Moscow";
            } else if ("07".equals(string2)) {
                string3 = "Europe/Volgograd";
            } else if ("08".equals(string2)) {
                string3 = "Europe/Samara";
            } else if ("09".equals(string2)) {
                string3 = "Europe/Moscow";
            } else if ("10".equals(string2)) {
                string3 = "Europe/Moscow";
            } else if ("11".equals(string2)) {
                string3 = "Asia/Irkutsk";
            } else if ("12".equals(string2)) {
                string3 = "Europe/Volgograd";
            } else if ("13".equals(string2)) {
                string3 = "Asia/Yekaterinburg";
            } else if ("14".equals(string2)) {
                string3 = "Asia/Irkutsk";
            } else if ("15".equals(string2)) {
                string3 = "Asia/Anadyr";
            } else if ("16".equals(string2)) {
                string3 = "Europe/Samara";
            } else if ("17".equals(string2)) {
                string3 = "Europe/Volgograd";
            } else if ("18".equals(string2)) {
                string3 = "Asia/Krasnoyarsk";
            } else if ("20".equals(string2)) {
                string3 = "Asia/Irkutsk";
            } else if ("21".equals(string2)) {
                string3 = "Europe/Moscow";
            } else if ("22".equals(string2)) {
                string3 = "Europe/Volgograd";
            } else if ("23".equals(string2)) {
                string3 = "Europe/Kaliningrad";
            } else if ("24".equals(string2)) {
                string3 = "Europe/Volgograd";
            } else if ("25".equals(string2)) {
                string3 = "Europe/Moscow";
            } else if ("26".equals(string2)) {
                string3 = "Asia/Kamchatka";
            } else if ("27".equals(string2)) {
                string3 = "Europe/Volgograd";
            } else if ("28".equals(string2)) {
                string3 = "Europe/Moscow";
            } else if ("29".equals(string2)) {
                string3 = "Asia/Novokuznetsk";
            } else if ("30".equals(string2)) {
                string3 = "Asia/Vladivostok";
            } else if ("31".equals(string2)) {
                string3 = "Asia/Krasnoyarsk";
            } else if ("32".equals(string2)) {
                string3 = "Asia/Omsk";
            } else if ("33".equals(string2)) {
                string3 = "Asia/Yekaterinburg";
            } else if ("34".equals(string2)) {
                string3 = "Asia/Yekaterinburg";
            } else if ("35".equals(string2)) {
                string3 = "Asia/Yekaterinburg";
            } else if ("36".equals(string2)) {
                string3 = "Asia/Anadyr";
            } else if ("37".equals(string2)) {
                string3 = "Europe/Moscow";
            } else if ("38".equals(string2)) {
                string3 = "Europe/Volgograd";
            } else if ("39".equals(string2)) {
                string3 = "Asia/Krasnoyarsk";
            } else if ("40".equals(string2)) {
                string3 = "Asia/Yekaterinburg";
            } else if ("41".equals(string2)) {
                string3 = "Europe/Moscow";
            } else if ("42".equals(string2)) {
                string3 = "Europe/Moscow";
            } else if ("43".equals(string2)) {
                string3 = "Europe/Moscow";
            } else if ("44".equals(string2)) {
                string3 = "Asia/Magadan";
            } else if ("45".equals(string2)) {
                string3 = "Europe/Samara";
            } else if ("46".equals(string2)) {
                string3 = "Europe/Samara";
            } else if ("47".equals(string2)) {
                string3 = "Europe/Moscow";
            } else if ("48".equals(string2)) {
                string3 = "Europe/Moscow";
            } else if ("49".equals(string2)) {
                string3 = "Europe/Moscow";
            } else if ("50".equals(string2)) {
                string3 = "Asia/Yekaterinburg";
            } else if ("51".equals(string2)) {
                string3 = "Europe/Moscow";
            } else if ("52".equals(string2)) {
                string3 = "Europe/Moscow";
            } else if ("53".equals(string2)) {
                string3 = "Asia/Novosibirsk";
            } else if ("54".equals(string2)) {
                string3 = "Asia/Omsk";
            } else if ("55".equals(string2)) {
                string3 = "Europe/Samara";
            } else if ("56".equals(string2)) {
                string3 = "Europe/Moscow";
            } else if ("57".equals(string2)) {
                string3 = "Europe/Samara";
            } else if ("58".equals(string2)) {
                string3 = "Asia/Yekaterinburg";
            } else if ("59".equals(string2)) {
                string3 = "Asia/Vladivostok";
            } else if ("60".equals(string2)) {
                string3 = "Europe/Kaliningrad";
            } else if ("61".equals(string2)) {
                string3 = "Europe/Volgograd";
            } else if ("62".equals(string2)) {
                string3 = "Europe/Moscow";
            } else if ("63".equals(string2)) {
                string3 = "Asia/Yakutsk";
            } else if ("64".equals(string2)) {
                string3 = "Asia/Sakhalin";
            } else if ("65".equals(string2)) {
                string3 = "Europe/Samara";
            } else if ("66".equals(string2)) {
                string3 = "Europe/Moscow";
            } else if ("67".equals(string2)) {
                string3 = "Europe/Samara";
            } else if ("68".equals(string2)) {
                string3 = "Europe/Volgograd";
            } else if ("69".equals(string2)) {
                string3 = "Europe/Moscow";
            } else if ("70".equals(string2)) {
                string3 = "Europe/Volgograd";
            } else if ("71".equals(string2)) {
                string3 = "Asia/Yekaterinburg";
            } else if ("72".equals(string2)) {
                string3 = "Europe/Moscow";
            } else if ("73".equals(string2)) {
                string3 = "Europe/Samara";
            } else if ("74".equals(string2)) {
                string3 = "Asia/Krasnoyarsk";
            } else if ("75".equals(string2)) {
                string3 = "Asia/Novosibirsk";
            } else if ("76".equals(string2)) {
                string3 = "Europe/Moscow";
            } else if ("77".equals(string2)) {
                string3 = "Europe/Moscow";
            } else if ("78".equals(string2)) {
                string3 = "Asia/Yekaterinburg";
            } else if ("79".equals(string2)) {
                string3 = "Asia/Irkutsk";
            } else if ("80".equals(string2)) {
                string3 = "Asia/Yekaterinburg";
            } else if ("81".equals(string2)) {
                string3 = "Europe/Samara";
            } else if ("82".equals(string2)) {
                string3 = "Asia/Irkutsk";
            } else if ("83".equals(string2)) {
                string3 = "Europe/Moscow";
            } else if ("84".equals(string2)) {
                string3 = "Europe/Volgograd";
            } else if ("85".equals(string2)) {
                string3 = "Europe/Moscow";
            } else if ("86".equals(string2)) {
                string3 = "Europe/Moscow";
            } else if ("87".equals(string2)) {
                string3 = "Asia/Novosibirsk";
            } else if ("88".equals(string2)) {
                string3 = "Europe/Moscow";
            } else if ("89".equals(string2)) {
                string3 = "Asia/Vladivostok";
            } else if ("90".equals(string2)) {
                string3 = "Asia/Yekaterinburg";
            } else if ("91".equals(string2)) {
                string3 = "Asia/Krasnoyarsk";
            } else if ("92".equals(string2)) {
                string3 = "Asia/Anadyr";
            } else if ("93".equals(string2)) {
                string3 = "Asia/Irkutsk";
            }
        } else if ("RW".equals(string)) {
            string3 = "Africa/Kigali";
        } else if ("SA".equals(string)) {
            string3 = "Asia/Riyadh";
        } else if ("SB".equals(string)) {
            string3 = "Pacific/Guadalcanal";
        } else if ("SC".equals(string)) {
            string3 = "Indian/Mahe";
        } else if ("SD".equals(string)) {
            string3 = "Africa/Khartoum";
        } else if ("SE".equals(string)) {
            string3 = "Europe/Stockholm";
        } else if ("SG".equals(string)) {
            string3 = "Asia/Singapore";
        } else if ("SH".equals(string)) {
            string3 = "Atlantic/St_Helena";
        } else if ("SI".equals(string)) {
            string3 = "Europe/Ljubljana";
        } else if ("SJ".equals(string)) {
            string3 = "Arctic/Longyearbyen";
        } else if ("SK".equals(string)) {
            string3 = "Europe/Bratislava";
        } else if ("SL".equals(string)) {
            string3 = "Africa/Freetown";
        } else if ("SM".equals(string)) {
            string3 = "Europe/San_Marino";
        } else if ("SN".equals(string)) {
            string3 = "Africa/Dakar";
        } else if ("SO".equals(string)) {
            string3 = "Africa/Mogadishu";
        } else if ("SR".equals(string)) {
            string3 = "America/Paramaribo";
        } else if ("SS".equals(string)) {
            string3 = "Africa/Juba";
        } else if ("ST".equals(string)) {
            string3 = "Africa/Sao_Tome";
        } else if ("SV".equals(string)) {
            string3 = "America/El_Salvador";
        } else if ("SX".equals(string)) {
            string3 = "America/Curacao";
        } else if ("SY".equals(string)) {
            string3 = "Asia/Damascus";
        } else if ("SZ".equals(string)) {
            string3 = "Africa/Mbabane";
        } else if ("TC".equals(string)) {
            string3 = "America/Grand_Turk";
        } else if ("TD".equals(string)) {
            string3 = "Africa/Ndjamena";
        } else if ("TF".equals(string)) {
            string3 = "Indian/Kerguelen";
        } else if ("TG".equals(string)) {
            string3 = "Africa/Lome";
        } else if ("TH".equals(string)) {
            string3 = "Asia/Bangkok";
        } else if ("TJ".equals(string)) {
            string3 = "Asia/Dushanbe";
        } else if ("TK".equals(string)) {
            string3 = "Pacific/Fakaofo";
        } else if ("TL".equals(string)) {
            string3 = "Asia/Dili";
        } else if ("TM".equals(string)) {
            string3 = "Asia/Ashgabat";
        } else if ("TN".equals(string)) {
            string3 = "Africa/Tunis";
        } else if ("TO".equals(string)) {
            string3 = "Pacific/Tongatapu";
        } else if ("TR".equals(string)) {
            string3 = "Asia/Istanbul";
        } else if ("TT".equals(string)) {
            string3 = "America/Port_of_Spain";
        } else if ("TV".equals(string)) {
            string3 = "Pacific/Funafuti";
        } else if ("TW".equals(string)) {
            string3 = "Asia/Taipei";
        } else if ("TZ".equals(string)) {
            string3 = "Africa/Dar_es_Salaam";
        } else if ("UA".equals(string)) {
            if ("01".equals(string2)) {
                string3 = "Europe/Kiev";
            } else if ("02".equals(string2)) {
                string3 = "Europe/Kiev";
            } else if ("03".equals(string2)) {
                string3 = "Europe/Uzhgorod";
            } else if ("04".equals(string2)) {
                string3 = "Europe/Zaporozhye";
            } else if ("05".equals(string2)) {
                string3 = "Europe/Zaporozhye";
            } else if ("06".equals(string2)) {
                string3 = "Europe/Uzhgorod";
            } else if ("07".equals(string2)) {
                string3 = "Europe/Zaporozhye";
            } else if ("08".equals(string2)) {
                string3 = "Europe/Simferopol";
            } else if ("09".equals(string2)) {
                string3 = "Europe/Kiev";
            } else if ("10".equals(string2)) {
                string3 = "Europe/Zaporozhye";
            } else if ("11".equals(string2)) {
                string3 = "Europe/Simferopol";
            } else if ("12".equals(string2)) {
                string3 = "Europe/Kiev";
            } else if ("13".equals(string2)) {
                string3 = "Europe/Kiev";
            } else if ("14".equals(string2)) {
                string3 = "Europe/Zaporozhye";
            } else if ("15".equals(string2)) {
                string3 = "Europe/Uzhgorod";
            } else if ("16".equals(string2)) {
                string3 = "Europe/Zaporozhye";
            } else if ("17".equals(string2)) {
                string3 = "Europe/Simferopol";
            } else if ("18".equals(string2)) {
                string3 = "Europe/Zaporozhye";
            } else if ("19".equals(string2)) {
                string3 = "Europe/Kiev";
            } else if ("20".equals(string2)) {
                string3 = "Europe/Simferopol";
            } else if ("21".equals(string2)) {
                string3 = "Europe/Kiev";
            } else if ("22".equals(string2)) {
                string3 = "Europe/Uzhgorod";
            } else if ("23".equals(string2)) {
                string3 = "Europe/Kiev";
            } else if ("24".equals(string2)) {
                string3 = "Europe/Uzhgorod";
            } else if ("25".equals(string2)) {
                string3 = "Europe/Uzhgorod";
            } else if ("26".equals(string2)) {
                string3 = "Europe/Zaporozhye";
            } else if ("27".equals(string2)) {
                string3 = "Europe/Kiev";
            }
        } else if ("UG".equals(string)) {
            string3 = "Africa/Kampala";
        } else if ("UM".equals(string)) {
            string3 = "Pacific/Wake";
        } else if ("US".equals(string)) {
            if ("AK".equals(string2)) {
                string3 = "America/Anchorage";
            } else if ("AL".equals(string2)) {
                string3 = "America/Chicago";
            } else if ("AR".equals(string2)) {
                string3 = "America/Chicago";
            } else if ("AZ".equals(string2)) {
                string3 = "America/Phoenix";
            } else if ("CA".equals(string2)) {
                string3 = "America/Los_Angeles";
            } else if ("CO".equals(string2)) {
                string3 = "America/Denver";
            } else if ("CT".equals(string2)) {
                string3 = "America/New_York";
            } else if ("DC".equals(string2)) {
                string3 = "America/New_York";
            } else if ("DE".equals(string2)) {
                string3 = "America/New_York";
            } else if ("FL".equals(string2)) {
                string3 = "America/New_York";
            } else if ("GA".equals(string2)) {
                string3 = "America/New_York";
            } else if ("HI".equals(string2)) {
                string3 = "Pacific/Honolulu";
            } else if ("IA".equals(string2)) {
                string3 = "America/Chicago";
            } else if ("ID".equals(string2)) {
                string3 = "America/Denver";
            } else if ("IL".equals(string2)) {
                string3 = "America/Chicago";
            } else if ("IN".equals(string2)) {
                string3 = "America/Indiana/Indianapolis";
            } else if ("KS".equals(string2)) {
                string3 = "America/Chicago";
            } else if ("KY".equals(string2)) {
                string3 = "America/New_York";
            } else if ("LA".equals(string2)) {
                string3 = "America/Chicago";
            } else if ("MA".equals(string2)) {
                string3 = "America/New_York";
            } else if ("MD".equals(string2)) {
                string3 = "America/New_York";
            } else if ("ME".equals(string2)) {
                string3 = "America/New_York";
            } else if ("MI".equals(string2)) {
                string3 = "America/New_York";
            } else if ("MN".equals(string2)) {
                string3 = "America/Chicago";
            } else if ("MO".equals(string2)) {
                string3 = "America/Chicago";
            } else if ("MS".equals(string2)) {
                string3 = "America/Chicago";
            } else if ("MT".equals(string2)) {
                string3 = "America/Denver";
            } else if ("NC".equals(string2)) {
                string3 = "America/New_York";
            } else if ("ND".equals(string2)) {
                string3 = "America/Chicago";
            } else if ("NE".equals(string2)) {
                string3 = "America/Chicago";
            } else if ("NH".equals(string2)) {
                string3 = "America/New_York";
            } else if ("NJ".equals(string2)) {
                string3 = "America/New_York";
            } else if ("NM".equals(string2)) {
                string3 = "America/Denver";
            } else if ("NV".equals(string2)) {
                string3 = "America/Los_Angeles";
            } else if ("NY".equals(string2)) {
                string3 = "America/New_York";
            } else if ("OH".equals(string2)) {
                string3 = "America/New_York";
            } else if ("OK".equals(string2)) {
                string3 = "America/Chicago";
            } else if ("OR".equals(string2)) {
                string3 = "America/Los_Angeles";
            } else if ("PA".equals(string2)) {
                string3 = "America/New_York";
            } else if ("RI".equals(string2)) {
                string3 = "America/New_York";
            } else if ("SC".equals(string2)) {
                string3 = "America/New_York";
            } else if ("SD".equals(string2)) {
                string3 = "America/Chicago";
            } else if ("TN".equals(string2)) {
                string3 = "America/Chicago";
            } else if ("TX".equals(string2)) {
                string3 = "America/Chicago";
            } else if ("UT".equals(string2)) {
                string3 = "America/Denver";
            } else if ("VA".equals(string2)) {
                string3 = "America/New_York";
            } else if ("VT".equals(string2)) {
                string3 = "America/New_York";
            } else if ("WA".equals(string2)) {
                string3 = "America/Los_Angeles";
            } else if ("WI".equals(string2)) {
                string3 = "America/Chicago";
            } else if ("WV".equals(string2)) {
                string3 = "America/New_York";
            } else if ("WY".equals(string2)) {
                string3 = "America/Denver";
            }
        } else if ("UY".equals(string)) {
            string3 = "America/Montevideo";
        } else if ("UZ".equals(string)) {
            if ("01".equals(string2)) {
                string3 = "Asia/Tashkent";
            } else if ("02".equals(string2)) {
                string3 = "Asia/Samarkand";
            } else if ("03".equals(string2)) {
                string3 = "Asia/Tashkent";
            } else if ("05".equals(string2)) {
                string3 = "Asia/Samarkand";
            } else if ("06".equals(string2)) {
                string3 = "Asia/Tashkent";
            } else if ("07".equals(string2)) {
                string3 = "Asia/Samarkand";
            } else if ("08".equals(string2)) {
                string3 = "Asia/Samarkand";
            } else if ("09".equals(string2)) {
                string3 = "Asia/Samarkand";
            } else if ("10".equals(string2)) {
                string3 = "Asia/Samarkand";
            } else if ("12".equals(string2)) {
                string3 = "Asia/Samarkand";
            } else if ("13".equals(string2)) {
                string3 = "Asia/Tashkent";
            } else if ("14".equals(string2)) {
                string3 = "Asia/Tashkent";
            }
        } else if ("VA".equals(string)) {
            string3 = "Europe/Vatican";
        } else if ("VC".equals(string)) {
            string3 = "America/St_Vincent";
        } else if ("VE".equals(string)) {
            string3 = "America/Caracas";
        } else if ("VG".equals(string)) {
            string3 = "America/Tortola";
        } else if ("VI".equals(string)) {
            string3 = "America/St_Thomas";
        } else if ("VN".equals(string)) {
            string3 = "Asia/Phnom_Penh";
        } else if ("VU".equals(string)) {
            string3 = "Pacific/Efate";
        } else if ("WF".equals(string)) {
            string3 = "Pacific/Wallis";
        } else if ("WS".equals(string)) {
            string3 = "Pacific/Pago_Pago";
        } else if ("YE".equals(string)) {
            string3 = "Asia/Aden";
        } else if ("YT".equals(string)) {
            string3 = "Indian/Mayotte";
        } else if ("YU".equals(string)) {
            string3 = "Europe/Belgrade";
        } else if ("ZA".equals(string)) {
            string3 = "Africa/Johannesburg";
        } else if ("ZM".equals(string)) {
            string3 = "Africa/Lusaka";
        } else if ("ZW".equals(string)) {
            string3 = "Africa/Harare";
        }
        return string3;
    }
}

